;{ support_demo.pb
;  Version 0.1 [2014/08/11]
;  Copyright (C) 2014 Ronny Krueger
;
;  This file is part of AWPB-Tools.
;
;  AWPB-Tools is free software: you can redistribute it and/or modify
;  it under the terms of the GNU Lesser General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  AWPB-Tools is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU Lesser General Public License for more details.
;
;  You should have received a copy of the GNU Lesser General Public License
;  along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #Use_AWSupport = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

OpenConsole()

PrintN("AWPB-Tools Support-Demo")
PrintN("")

Define TestString1.s, TestString2.s, TestString3.s, TestString4.s, TestString5.s

TestString1 = "     Teststring #1     "
TestString2 = "     " + Chr(34) + "Teststring #2" + Chr(34) + "     "
TestString3 = "     <Teststring #3>     "
TestString4 = "abcdeabcdeabcde"
TestString5 = "abcdefghijklmnopqrstuvwxyz����"

UseModule AWSupport
  PrintN("Teststring 1: '" + TestString1 + "'")
  PrintN("Teststring 2: '" + TestString2 + "'")
  PrintN("Teststring 3: '" + TestString3 + "'")
  PrintN("TrimWS(TestString 1): '" + TrimWS(TestString1) + "'")
  PrintN("TrimWSLeft(TestString 1): '" + TrimWSLeft(TestString1) + "'")
  PrintN("TrimWSRight(TestString 1): '" + TrimWSRight(TestString1) + "'")
  PrintN("TrimWS(TestString 2): '" + TrimWS(TestString2) + "'")
  PrintN("TrimWSQ(TestString 2): '" + TrimWSQ(TestString2) + "'")
  PrintN("TrimWS(TestString 3): '" + TrimWS(TestString3) + "'")
  PrintN("TrimWSGL(TestString 3): '" + TrimWSGL(TestString3) + "'")
  PrintN("CombinePath(C:\abcd, datei.txt): " + CombinePath("C:\abcd", "datei.txt"))
  PrintN("FindLastChar(TestString4,'c'): " + Str(FindLastChar(TestString4, 'c')))
  PrintN("CalcMD5(TestString5, #PB_Ascii): " + CalcMD5(TestString5, #PB_Ascii))
  PrintN("CalcMD5(TestString5, #PB_UTF8): " + CalcMD5(TestString5, #PB_UTF8))
  PrintN("CalcMD5(TestString5, #PB_UniCode): " + CalcMD5(TestString5, #PB_Unicode))  
UnuseModule AWSupport
  
PrintN("Press return to exit")
Input()

