﻿;{   compile_test.pb
;    Version 0.1 [2014/11/26]
;    Copyright (C) 2014 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #AdditionalLibPath = ""
  #Enable_DynamicSQLite3Lib = 1
  #Enable_ChunkyFileCustomEncryption = 1
  #Enable_ChunkyFileAES = 1
  #Enable_ChunkyFileJSON = 1
  #Enable_ChunkyFileXML = 1
  #Enable_OpenSSL = 1
  #Enable_EnableMessageAES = 1
  
  #Use_AWCharset = 1
  #Use_AWChunkyFile = 1
  #Use_AWCustomCrypt = 1
  #Use_AWDate = 1
  #Use_AWCode = 1
  #Use_AWGnuPG = 1
  #Use_AWLocale = 1
  #Use_AWMessage = 1
  #Use_AWRegExSup = 1
  #Use_AWSQLiteDB = 1
  #Use_AWSqlStatement = 1
  #Use_AWSupport = 1
  #Use_AWZip = 1
  #Use_AWZipArc = 1
  #Use_AWSmtp = 1
  #Use_AWPop3 = 1
  #Use_AWMediaInfo = 1
  #Use_AWImap = 1
  
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"


