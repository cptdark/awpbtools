﻿;{   dcraw_demo1.pb
;    Version 0.1 [2015/11/12]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
	#Use_AWDcRaw = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

Define img1.i, img2.i, dcrawexe.s, file.s, win1.i
Define win1.i, win2.i, scr1.i, scr2.i, event.i

dcrawexe = "C:\Users\ils\Desktop\Dev\empis\Collector\bin\dcraw\dcraw-9.26-ms-64-bit.exe"
file     = "C:\Users\ils\Desktop\Dev\empis\Collector\data\check\P1030227.RW2"

OpenConsole()

PrintN("SWPBTools::AWDcRaw Demo1")

AWDcRaw::SetExe(dcrawexe)

Print("Getting thumbnail image ... ")
img1 = AWDcRaw::GetThumbnail(file)
If IsImage(img1)
	PrintN("success")
	
	PrintN("Depth:  " + Str(ImageDepth(img1)))
	PrintN("Width:  " + Str(ImageWidth(img1)))
	PrintN("height: " + Str(ImageHeight(img1)))
	PrintN("Format: " + Hex(ImageFormat(img1)))
	
	win1 = OpenWindow(#PB_Any, 0, 0, 800, 600, "Embedded Thumbnail")
		scr1 = ScrollAreaGadget(#PB_Any, 0, 0, WindowWidth(win1, #PB_Window_InnerCoordinate), WindowHeight(win1, #PB_Window_InnerCoordinate), ImageWidth(img1), ImageHeight(img1))
			ImageGadget(#PB_Any, 0, 0, ImageWidth(img1), ImageHeight(img1), ImageID(img1))
		CloseGadgetList()
	SmartWindowRefresh(win1, #True)
Else
	PrintN("failure")
EndIf

Print("Getting preview image ... ")
img2 = AWDcRaw::GetPreview(file)
If IsImage(img2)
	PrintN("success")
	
	PrintN("Depth:  " + Str(ImageDepth(img2)))
	PrintN("Width:  " + Str(ImageWidth(img2)))
	PrintN("height: " + Str(ImageHeight(img2)))
	PrintN("Format: " + Hex(ImageFormat(img2)))
	
	win2 = OpenWindow(#PB_Any, 801, 0, 800, 600, "Preview Image")
		scr2 = ScrollAreaGadget(#PB_Any, 0, 0, WindowWidth(win2, #PB_Window_InnerCoordinate), WindowHeight(win2, #PB_Window_InnerCoordinate), ImageWidth(img2), ImageHeight(img2))
			ImageGadget(#PB_Any, 0, 0, ImageWidth(img2), ImageHeight(img2), ImageID(img2))
		CloseGadgetList()
	SmartWindowRefresh(win2, #True)

Else
	PrintN("failure")
EndIf

Repeat
	event = WaitWindowEvent()
Until Event = #PB_Event_CloseWindow

If IsWindow(win1) : CloseWindow(win1) : EndIf
If IsWindow(win2) : CloseWindow(win2) : EndIf
If IsImage(img1) : FreeImage(img1) : EndIf
If IsImage(img2) : FreeImage(img2) : EndIf

CloseConsole()

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 29
; Folding = -
; EnableUnicode
; EnableXP