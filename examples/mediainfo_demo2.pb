;{   mediainfo_demo.pb
;    Version 0.2 [2014/11/25]
;    Copyright (C) 2011-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #Use_AWMediaInfo = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

OpenConsole()

PrintN("AWPB-Tools MediaInfoLib-Demo 2")
PrintN("")

Define *mi.AWMediaInfo::MediaInfo, filename.s, framerate.f, frames.i, *mixml.AWMediaInfoXML::MediaInfoXML
NewList values.s()

Print("Opening libmediainfo.so / mediainfo.dll ... ")
If AWMediaInfo::InitLibrary("C:\Users\ils\Desktop\Dev\empis\Collector\bin\mediainfo\x64")
	PrintN("success")

	Print("Allocating MediaInfo-Object ... ")
	*mi = AWMediaInfo::New()
	If *mi
		PrintN("success")
		PrintN("Dateiname (mit absoluten oder relativen Pfad) eingeben: ")
		filename = OpenFileRequester("MediaInfo select file", "", "All files (*.*)|*.*", 0)
	  Print("Opening " + filename + " ... ")
	  If *mi\Open(filename)
	  	PrintN("success")
	  	PrintN("FileSize: " + Str(*mi\GetFileSize()))	  	  	
	  	
	  	Print("Getting XML Info ... ")
	  	*mixml = *mi\GetMediaInfoXML()
	  	If *mixml
		  	PrintN("success")
		  	
		  	Print("Getting framerate ... ")
		  	If *mixml\GetValueList("video", #pb_any, "frame_rate", values())
		  		PrintN("success")
		  		ForEach values()
		  			PrintN("## " + values())
		  		Next
			  Else
			  	PrintN("failed")
				EndIf
		  	
		  	
		  	*mixml\Destroy()
		  Else
		  	PrintN("failed")
			EndIf
	  Else
	  	PrintN("failed")
		EndIf
	  *mi\Destroy()
	Else
	  PrintN("failed")
	EndIf
Else
	PrintN("failed")
EndIf

PrintN("Press return to exit")
Input()



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 56
; FirstLine = 27
; Folding = -
; EnableUnicode
; EnableXP