;{	 ziparc_demo.pb
;	 Version 0.2 [2014/08/12]
;    Copyright (C) 2011-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #Use_AWZipArc = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

OpenConsole()

PrintN("AWPB-Tools ZIPArchive-Demo")
PrintN("")

Define *zip.AWZipArc::ZIPArchive

Print("Allocating ZIPArchive ... ")
*zip = AWZipArc::New()
If *zip
  PrintN("success")
  PrintN("Creating Structured Demo-Archive")
  Print("Adding files ... ")
  If *zip\AddFile("altmail_demo.pb", "mailenc\altmail_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("attmail_demo.pb", "mailenc\attmail_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("htmlmail_demo.pb", "mailenc\htmlmail_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("plainmail_demo.pb", "mailenc\plainmail_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("maildec_demo.pb", "maildec\maildec_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("pop3_demo.pb", "client\pop3_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("smtp_demo.pb", "client\smtp_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("chunkyfile_demo.pb", "chunkyfile_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("date_demo.pb", "date_demo.pb") : Print("+") : Else : Print("-") : EndIf
  If *zip\AddFile("ziparc_demo.pb", "ziparc_demo.pb") : Print("+") : Else : Print("-") : EndIf
  PrintN(" ... done")
  *zip\SetComment("Der Archiv-Kommentar!")
  Print("Building Archive ... ")
  If *zip\Build()
    PrintN("success")
    PrintN("Archive size: " + Str(*zip\GetZIPSize()))
    Print("Saving to file ... ")
    If *zip\SaveZIP("ziparc.zip")
      PrintN("success")
    Else
      PrintN("failed")
    EndIf
  Else
    PrintN("failed")
  EndIf
  *zip\Destroy()
EndIf

PrintN("Press return to exit")
Input()


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 35
; FirstLine = 21
; Folding = -
; EnableUnicode
; EnableXP