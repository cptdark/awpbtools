;{   sqlitedb_demo2.pb
;    Version 0.2 [2014/08/12]
;    Copyright (C) 2012-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #Use_AWSQLiteDB = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

Define *db.AWSQLiteDB::SQLiteDB, a.i

OpenConsole()
PrintN("AWPB_SQLiteDB Demo 2")
Print("Creating SQLiteDB Object ... ")
*db = AWSQLiteDB::New()
If *db
	PrintN("success")
	Print("Opening database file ... ")
	If *db\OpenDB("test2.db", "", "")
		PrintN("success")
		Print("Testing if table 'tabelle1' exists ... ")
		If Not *db\TableExists("tabelle1")
			PrintN("not found")
			Print("Creating table 'tabelle1' ... ")
			If *db\Update("CREATE TABLE tabelle1 (name TEXT, age INT)")
				PrintN("success")
			Else
				PrintN("failed")
				*db\CloseDB()
				*db\Destroy()
				PrintN("Press Enter to exit")
				Input()
				CloseConsole()
				End
			EndIf			
		Else
			PrintN("found");
		EndIf
		Print("Insert some data into table ... ")
		
		If *db\BeginBatch()
			*db\Batch("INSERT INTO tabelle1 VALUES ('Meier', 28)")
			*db\Batch("INSERT INTO tabelle1 VALUES ('M�ller', 29)")
			*db\Batch("INSERT INTO tabelle1 VALUES ('Schneider', 18)")
			*db\Batch("INSERT INTO tabelle1 VALUES ('Lehmann', 58)")
			If *db\EndBatch(#True)
				PrintN("done")
			Else
				PrintN("failed")
			EndIf
		Else
			PrintN("done")
		EndIf
		Print("Show Table content ... ")
		If *db\Query("SELECT * FROM tabelle1")
			PrintN("ok")
			For a = 1 To *db\Columns()
				PrintN("Column " + Str(a) + " Name: " + *db\ColumnName(a-1))
			Next a
			PrintN("Content:")
			a = 1
			While *db\NextRow()
				PrintN("(" + Str(a) + ") Name: " + *db\GetString(0) + " - Age: " + Str(*db\GetLong(1)))
				a + 1
			Wend
		Else
			PrintN("failed")
		EndIf
		*db\CloseDB()
	Else
		PrintN("Failed")
	EndIf
	*db\Destroy()
Else
	PrintN("Failed")
EndIf

PrintN("Press Enter to exit")
Input()
CloseConsole()
End


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 32
; FirstLine = 29
; Folding = -
; EnableUnicode
; EnableXP