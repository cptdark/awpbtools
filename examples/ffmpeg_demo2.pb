﻿;{   ffmpeg_demo2.pb
;    Version 0.1 [2015/11/19]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
	#Use_AWFFMpeg = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

Define ffprobeexe.s, ffmpegexe.s, file.s, folder.s
Define *fileinfo.AWFFMpeg::FileInfo

ffprobeexe = "C:\Users\ils\Desktop\Dev\empis\Collector\bin\ffmpeg\x64\ffprobe.exe"
ffmpegexe  = "C:\Users\ils\Desktop\Dev\empis\Collector\bin\ffmpeg\x64\ffmpeg.exe"
file       = "C:\Users\ils\Desktop\Dev\empis\Collector\data\check\tutorial-videofile-36752.mp4"
folder     = "C:\Users\ils\Desktop\Dev\empis\Collector\data\check"

OpenConsole()

PrintN("AWPBTools::FFMpeg Demo 2")

AWFFMpeg::SetExe(ffmpegexe, ffprobeexe)

Print("Analyzing file ... ")
*fileinfo = AWFFMpeg::AnalyzeVideoFile(file)
If *fileinfo
	PrintN("success")
	PrintN("Framerate:   " + StrD(*fileinfo\FrameRate))
	PrintN("Framecount:  " + Str(*fileinfo\FrameCount))
	PrintN("Width:       " + Str(*fileinfo\Width))
	PrintN("Height:      " + Str(*fileinfo\Height))
	PrintN("Aspectratio: " + StrD(*fileinfo\AspectRatio))
	
	AWFFMpeg::FreeFileInfo(*fileinfo)
Else
	PrintN("failure")
EndIf

PrintN("Press return to exit")
Input()

CloseConsole()

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 45
; FirstLine = 7
; Folding = -
; EnableUnicode
; EnableXP
; EnablePurifier