﻿;{	 settings_demo.pb
;	   Version 0.1 [2015/11/04]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

EnableExplicit

DeclareModule AWPBTools_Settings
  #Use_AWSettings = 1
EndDeclareModule
XIncludeFile "../include/awpbtools/awpbtools.pbi"

OpenConsole()

PrintN("AWPB-Tools Settings-Demo")
PrintN("")

Define *sf.AWSettings::Settings, binstr1.s, binstr2.s
NewList sections.s()
NewList keys.s()

binstr1 = "Ein langer Teststring, der als Binärdaten gespeichert werden soll."
binstr2 = "test"

Print("Allocating Settings ... ")
*sf = AWSettings::New()
If *sf
  PrintN("success")
  Print("Opening file ... ")
  If *sf\Open("settingsdata.ini")
    PrintN("success")
	  Print("Adding some data ... ")

	  *sf\SetSection("some Integers")
	  *sf\SetLong("ein long", 85265473)
	  *sf\SetWord("ein word", 15695)
	  *sf\SetByte("ein byte", 130)
	  *sf\SetQuad("ein quad", 9223372036854775)
	  
 	  *sf\SetSection("some floats")
 	  *sf\SetFloat("ein float", 123.456)
 	  *sf\SetDouble("ein double", 123456789.321654)
 	  
 	  *sf\SetDefaultSection()
 	  *sf\SetString("ein String", "der hier \ mit trenner \ sollte in der default sektion auftauchen")
 	  
 	  *sf\SetSection("binary data")
 	  *sf\SetBinary("binstr1", @binstr1, StringByteLength(binstr1))
	  *sf\SetBinary("binstr2", @binstr2, StringByteLength(binstr2))
    
	  PrintN("success")
	  
    PrintN("Sections / Keys:")
    *sf\GetSections(sections())
    
    ForEach sections()
    	*sf\SetSection(sections())
    	*sf\GetKeys(keys())
    	PrintN("[" + sections() + "]")
    	ForEach keys()
	    	PrintN(keys() + " = " + *sf\GetString(keys(), "NOT SET"))
	    Next
    Next
	  
	  Print("Writing data to file ... ")
	  
	  If *sf\Write()
	  	PrintN("success")
	  Else
	  	PrintN("failed")
	  EndIf
  Else
    PrintN("failed")
  EndIf
  *sf\Destroy()
Else
  PrintN("failed")
EndIf

PrintN("Press return to exit")
Input()

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 44
; Folding = -
; EnableUnicode
; EnableXP
; CPU = 1
; EnablePurifier = 1,1,1,1