;{   awgnupg.pbi
;    Version 0.4 [2014/11/20]
;    Copyright (C) 2011-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

; notice: Key may be the HEX-Representation or an eMail-Address

XIncludeFile "awsupport.pbi"
XIncludeFile "awregexsup.pbi"

DeclareModule AWGnuPG
  Declare.s GetAvailableMailAdresses(GnuPGCmd.s)
  Declare.s GetAvailableSecretMailAdresses(GnuPGCmd.s)
  Declare.s GetAllAvailableMailAdresses(GnuPGCmd.s)
  Declare.s GetAllAvailableSecretMailAdresses(GnuPGCmd.s)
  Declare.s Encrypt(GnuPGCmd.s, GnuPGIn.s, keyIDs.s)
  Declare.s Decrypt(GnuPGCmd.s, GnuPGIn.s, pass.s)
  Declare.s SignText(GnuPGCmd.s, GnuPGIn.s, keyID.s, pass.s)
  Declare.i VerifySignature(GnuPGCmd.s, GnuPGIn.s)
  Declare.s SignAndEncrypt(GnuPGCmd.s, GnuPGIn.s, userKeyID.s, keyIDs.s, pass.s)
  Declare.i KeyExists(GnuPGCmd.s, keyID.s)
  Declare.s SelectGPGRequester()
  Declare.s GetVersion(GnuPGCmd.s)
EndDeclareModule

Module AWGnuPG
  EnableExplicit

  Procedure.s ExecuteGnuPGStr(GnuPGCmd.s, GnuPGArgs.s, GnuPGInput.s, *ExitCode = #Null)
    Protected GnuPGProc, GnuPGOut.s, ec.i
  
    ec        = -1
    GnuPGOut  = ""
    If GnuPGCmd <> ""
      GnuPGProc = RunProgram(GnuPGCmd, GnuPGArgs, "", #PB_Program_Open | #PB_Program_Read | #PB_Program_Write)
      If GnuPGProc
        If GnuPGInput <> ""
          WriteProgramString(GnuPGProc, GnuPGInput, #PB_Ascii)
        EndIf
        WriteProgramData(GnuPGProc, #PB_Program_Eof, 0)
        While ProgramRunning(GnuPGProc)       
          GnuPGOut + ReadProgramString(GnuPGProc, #PB_Ascii) + Chr(13) + Chr(10)
        Wend
        ec = ProgramExitCode(GnuPGProc)
        KillProgram(GnuPGProc)
        CloseProgram(GnuPGProc)
        GnuPGOut = AWSupport::TrimWSRight(GnuPGOut)
      EndIf
    EndIf
    
    If *ExitCode : PokeI(*ExitCode, ec) : EndIf
    
    ProcedureReturn GnuPGOut
  EndProcedure
  Procedure.i ExecuteGnuPGCode(GnuPGCmd.s, GnuPGArgs.s, GnuPGInput.s)
    Protected ec.i, res.s
    
    ec  = -1
    res = ExecuteGnuPGStr(GnuPGCmd, GnuPGArgs, GnuPGInput, @ec)
    If ec = 0
      ProcedureReturn #True
    EndIf
    
    ProcedureReturn #False
  EndProcedure
  Procedure.s GetAllAvailableMailAdresses(GnuPGCmd.s)
    Protected keys.s
    
    keys = ExecuteGnuPGStr(GnuPGCmd, "--list-keys", "")
    If Len(keys)
      keys = AWRegExSup::CreateMailAdrList(keys)
    EndIf
    
    ProcedureReturn keys        
  EndProcedure
  Procedure.s GetAllAvailableSecretMailAdresses(GnuPGCmd.s)
    Protected keys.s
    
    keys = ExecuteGnuPGStr(GnuPGCmd, "--list-secret-keys", "")
    If Len(keys)
      keys = AWRegExSup::CreateMailAdrList(keys)
    EndIf
    
    ProcedureReturn keys        
  EndProcedure
  Procedure.s GetAvailableMailAdresses(GnuPGCmd.s)
    Protected args.s, keywin, result.b, done.b, keys.s
    Protected keylist, infotext, btnaccept, btncancel, Event, WindowID, GadgetID, EventType
    Protected a, k.s, name.s, email.s
    
    keys = ExecuteGnuPGStr(GnuPGCmd, "--list-keys", "")
    If Len(keys)
      keys = ReplaceString(keys, Chr(13) + Chr(10), Chr(13))
      
      keywin = OpenWindow(#PB_Any, 0, 0, 450, 310, "..:: GnuPG keys ::..",  #PB_Window_TitleBar | #PB_Window_ScreenCentered )
      If IsWindow(keywin)
        keylist   = ListIconGadget(#PB_Any, 10, 30, 430, 230, "Name", 200, #PB_ListIcon_CheckBoxes | #PB_ListIcon_FullRowSelect | #PB_ListIcon_AlwaysShowSelection)
                    AddGadgetColumn(keylist, 1, "eMail", 200)
        infotext  = TextGadget(#PB_Any, 10, 10, 90, 20, "select receipients")
        btnaccept = ButtonGadget(#PB_Any, 340, 270, 100, 30, "Accept")
        btncancel = ButtonGadget(#PB_Any, 230, 270, 100, 30, "Cancel")
        
        done      = #False
        result    = #False
            
        For a = 1 To CountString(keys, Chr(13))
          k = Trim(StringField(keys, a, Chr(13)))
        
          If Left(k, 3) = "uid"
            name  = Trim(Mid(k, 4, FindString(k, "<", 4) - 4))
            email = Trim(AWRegExSup::CreateMailAdrList(k))
            AddGadgetItem(keylist, -1, name + Chr(10) + email)
          EndIf
        Next a
              
        Repeat
          Event     = WaitWindowEvent()     
          WindowID  = EventWindow()     
          GadgetID  = EventGadget()     
          EventType = EventType()
          
          If Event = #PB_Event_Gadget
            If GadgetID = keylist
              
            ElseIf GadgetID = btnaccept
              result = #True
              done   = #True
            ElseIf GadgetID = btncancel
              result = #False
              done   = #True
            EndIf
          EndIf
        Until done
  
        If result
          k = ""
          For a = 0 To CountGadgetItems(keylist) - 1
            If (GetGadgetItemState(keylist, a) & #PB_ListIcon_Checked) = #PB_ListIcon_Checked
              k + GetGadgetItemText(keylist, a, 1) + " "
            EndIf
          Next a
        EndIf
  
        CloseWindow(keywin)       
                
        If result
          keys = AWRegExSup::CreateMailAdrList(k)         
        Else
          keys = ""
        EndIf
  
        ProcedureReturn keys        
      EndIf     
    EndIf
     
    ProcedureReturn ""
  EndProcedure
  Procedure.s GetAvailableSecretMailAdresses(GnuPGCmd.s)
    Protected args.s, keywin, result.b, done.b, keys.s
    Protected keylist, infotext, btnaccept, btncancel, Event, WindowID, GadgetID, EventType
    Protected a, k.s, name.s, email.s
    
    keys = ExecuteGnuPGStr(GnuPGCmd, "--list-secret-keys", "")
    If Len(keys)
      keys = ReplaceString(keys, Chr(13) + Chr(10), Chr(13))
      
      keywin = OpenWindow(#PB_Any, 0, 0, 450, 310, "..:: GnuPG secret keys ::..",  #PB_Window_TitleBar | #PB_Window_ScreenCentered )
      If IsWindow(keywin)
        keylist   = ListIconGadget(#PB_Any, 10, 30, 430, 230, "Name", 200, #PB_ListIcon_CheckBoxes | #PB_ListIcon_FullRowSelect | #PB_ListIcon_AlwaysShowSelection)
                    AddGadgetColumn(keylist, 1, "eMail", 200)
        infotext  = TextGadget(#PB_Any, 10, 10, 90, 20, "select receipients")
        btnaccept = ButtonGadget(#PB_Any, 340, 270, 100, 30, "Accept")
        btncancel = ButtonGadget(#PB_Any, 230, 270, 100, 30, "Cancel")
        
        done      = #False
        result    = #False
  
        For a = 1 To CountString(keys, Chr(13))
          k = Trim(StringField(keys, a, Chr(13)))
        
          If Left(k, 3) = "uid"
            name  = Trim(Mid(k, 4, FindString(k, "<", 4) - 4))
            email = Trim(AWRegExSup::CreateMailAdrList(k))
            AddGadgetItem(keylist, -1, name + Chr(10) + email)
          EndIf
        Next a
        
        Repeat
          Event     = WaitWindowEvent()     
          WindowID  = EventWindow()     
          GadgetID  = EventGadget()     
          EventType = EventType()
          
          If Event = #PB_Event_Gadget
            If GadgetID = keylist
              
            ElseIf GadgetID = btnaccept
              result = #True
              done   = #True
            ElseIf GadgetID = btncancel
              result = #False
              done   = #True
            EndIf
          EndIf
        Until done
  
        If result
          k = ""
          For a = 0 To CountGadgetItems(keylist) - 1
            If (GetGadgetItemState(keylist, a) & #PB_ListIcon_Checked) = #PB_ListIcon_Checked
              k + GetGadgetItemText(keylist, a, 1) + " "
            EndIf
          Next a
        EndIf
  
        CloseWindow(keywin)       
                
        If result
          keys = AWRegExSup::CreateMailAdrList(k)         
        Else
          keys = ""
        EndIf
  
        ProcedureReturn keys        
      EndIf     
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.s Encrypt(GnuPGCmd.s, GnuPGIn.s, keyIDs.s)
    Protected args.s, c.i, a.i, RecOk.b, res.s
    
    res   = ""
    c     = CountString(keyIDs, ":")
    args  = ""
    RecOk = #False
  
    For a = 1 To c
      args + " -r " + StringField(keyIDs, a, ":")
    Next a
    If (c = 0) And (Len(keyIDs) > 0)
      args = " -r " + keyIDs
      RecOk = #True
    ElseIf c <> 0
      RecOk = #True
    EndIf
    args = Trim(args) + " --encrypt --armor --yes --no-tty"
  
    If RecOk
      res = ExecuteGnuPGStr(GnuPGCmd, args, GnuPGIn)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s Decrypt(GnuPGCmd.s, GnuPGIn.s, pass.s)
    Protected res.s, args.s
  
    args = "--decrypt --armor --no-tty --yes --passphrase " + pass
    res  = ExecuteGnuPGStr(GnuPGCmd, args, GnuPGIn)
  
    ProcedureReturn res
  EndProcedure
  Procedure.s SignText(GnuPGCmd.s, GnuPGIn.s, keyID.s, pass.s)
    Protected res.s, args.s
  
    args = "--clearsign --no-tty --yes --local-user " + keyID + " --passphrase " + pass
    res  = ExecuteGnuPGStr(GnuPGCmd, args, GnuPGIn)
  
    ProcedureReturn res
  EndProcedure
  Procedure.i VerifySignature(GnuPGCmd.s, GnuPGIn.s)
    Protected res.i, args.s
  
    args = "--verify --no-tty"
    res  = ExecuteGnuPGCode(GnuPGCmd, args, GnuPGIn)
  
    ProcedureReturn res
  EndProcedure
  Procedure.s SignAndEncrypt(GnuPGCmd.s, GnuPGIn.s, userKeyID.s, keyIDs.s, pass.s)
    Protected res.s, args.s, c.i, a.i, RecOk.b
  
    res   = ""
    c     = CountString(keyIDs, ":")
    args  = ""
    RecOk = #False
  
    For a = 1 To c
      args + " -r " + StringField(keyIDs, a, ":")
    Next a
    If (c = 0) And (Len(keyIDs) > 0)
      args = " -r " + keyIDs
      RecOk = #True
    ElseIf c <> 0
      RecOk = #True
    EndIf
    args = Trim(args) + " -se --armor --no-tty --yes --local-user " + userKeyID + " --passphrase " + pass
  
    If RecOk
      res = ExecuteGnuPGStr(GnuPGCmd, args, GnuPGIn)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i KeyExists(GnuPGCmd.s, keyID.s)
    Protected res.i, args.s
  
    args = "--list-keys " + keyID
    res  = ExecuteGnuPGCode(GnuPGCmd, args, "")
  
    ProcedureReturn res
  EndProcedure
  Procedure.s SelectGPGRequester()
    Protected GnuPGCmd.s, Pattern.s
    
    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      Pattern = "gpg.exe|gpg.exe|gnupg.exe|gnupg.exe|All executables|*.exe"
    CompilerElse
      Pattern = "gpg|gpg|gnupg|gnupg|All files|*"
    CompilerEndIf 
    GnuPGCmd.s = OpenFileRequester("Select GPG executable", "", Pattern, 0)
    
    ProcedureReturn GnuPGCmd
  EndProcedure
  Procedure.s GetVersion(GnuPGCmd.s)
    Protected ver.s, spos.i
    
    ver  = ExecuteGnuPGStr(GnuPGCmd, "--version", "")
    ver  = StringField(ver, 1, Chr(13))
    spos = AWSupport::FindLastChar(StringField(ver, 1, Chr(13)), ' ')
    If spos 
      ver = Mid(ver, spos + 1)
    EndIf
    ver = AWSupport::TrimWS(ver)
    
    ProcedureReturn ver 
  EndProcedure
EndModule


