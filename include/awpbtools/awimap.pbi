;{   awimap.pbi
;    Version 0.2 [2015/10/26]
;    Copyright (C) 2014-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awnetwork.pbi"
XIncludeFile "awsupport.pbi"

DeclareModule AWImap
  #SSL_NONE     = $00000000
  #SSL_TRY      = $00000001
  #SSL_FORCE    = $00000002
  #SSL_FULL     = $00000004
  #CONN_AUTO    = $00000000
  #CONN_DEFAULT = #SSL_TRY | #CONN_AUTO
  
  #ERR_NONE     =  0
  #ERR_FAIL     = -1
  #ERR_LOGIN    = -2
  #ERR_USER     = -3
  #ERR_PASS     = -4
  #ERR_TIMEOUT  = AWNetwork::#ERR_TIMEOUT
  
  #FLAG_NONE        = $00000000
  #FLAG_SEEN        = $00000001
  #FLAG_ANSWERED    = $00000002
  #FLAG_FLAGGED     = $00000004
  #FLAG_DELETED     = $00000008
  #FLAG_DRAFT       = $00000010
  #FLAG_RECENT      = $00000020
  
  #REQ_MESSAGES     = $00000001
  #REQ_RECENT       = $00000002
  #REQ_UIDNEXT      = $00000004
  #REQ_UIDVALIDITY  = $00000008
  #REQ_UNSEEN       = $00000010
  
  Structure sResSELECT
    exists.l
    recent.l
    permanentflags.l
    flags.l
    uidvalidity.q
    uidnext.q
    unseen.q
    writable.b
  EndStructure

  Interface IMAPClient
    Destroy               ()
    SetTimeout            (timeout.i)
    SetHost               (host.s, port.i)
    SetLogin              (user.s, pass.s)
    SetProxyHost          (host.s, port.i)
    SetProxyLogin         (user.s, pass.s)    
    Connect.i             (flags.i = #CONN_DEFAULT)
    DisConnect            ()
    GetLog.s							()
    ClearLog							()
    EnableLog							(enabled.i)
    GetLastError.s        ()
    ClearLastError        ()
    NOOP.i                ()
    List.i                (reference.s, mailbox.s = "*")
    LSUB.i                (reference.s, mailbox.s = "*")
    CREATE.i              (mailbox.s)
    DELETE.i              (mailbox.s)
    RENAME.i              (mailbox_old.s, mailbox_new.s)
    SUBSCRIBE.i           (mailbox.s)
    UNSUBSCRIBE.i         (mailbox.s)
    Select.i              (mailbox.s)
    EXAMINE.i             (mailbox.s)
    CLOSE.i               ()
    EXPUNGE.i             ()
    CHECK.i               ()
    STATUS.i              (mailbox.s, req.i)
    APPEND.i              (mailbox.s, message.s)
    STORE.i               (sequence.s, operation.s, values.s)
    FETCH.i               (index.i)
    FETCH_Range.i         (index_from.i, index_to.i)
    FETCH_Header.i        (index.i)
    FETCH_Header_Range.i  (index_from.i, index_to.i)
    FETCH_Flags.i         (index.i)
    FETCH_Flags_Range.i   (index_from.i, index_to.i)
    FETCH_Size.i          (index.i)
    FETCH_Size_Range.i    (index_from.i, index_to.i)
    FETCH_UID.i           (index.i)
    FETCH_UID_Range.i     (index_from.i, index_to.i)
    COPY.i                (index.i, mailbox.s)
    COPY_Range.i          (index_from.i, index_to.i, mailbox.s)
    SEARCH.i              (criteria.s)
    SetFlags.i            (index.i, flags.i)
    SetFlags_Range.i      (index_from.i, index_to.i, flags.i)
    AddFlags.i            (index.i, flags.i)
    AddFlags_Range.i      (index_from.i, index_to.i, flags.i)
    DelFlags.i            (index.i, flags.i)
    DelFlags_Range.i      (index_from.i, index_to.i, flags.i)
    DeleteMessage.i       (index.i)
    DeleteMessage_Range.i (index_from.i, index_to.i)
    GetReceivedString.s   ()
    GetSELECTResult.i     ()
    GetEXAMINEResult.i    ()
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      SetSSLMethod        (sslmethod.i)
    CompilerEndIf
    
  EndInterface
  
  Declare.i New()
  Declare.i OpenSSLAvailable()
EndDeclareModule

Module AWImap
  EnableExplicit
  
  #ERR_OK   = AWNetwork::#ERR_OK
  #ERR_ERR  = AWNetwork::#ERR_ERR
  
  Macro IsSet(X, Y)
    (((X) & (Y)) = (Y))
  EndMacro
  
  Structure sIMAPClient
    *vt.IMAPClient
    *Conn.AWNetwork::Connection
    Caps.s
    resSELECT.sResSELECT
  EndStructure
  
  Procedure.i New()
    Protected *this.sIMAPClient, *ci.AWNetwork::ConnInfo
  
    *this = AllocateStructure(sIMAPClient)
    If *this
      *this\vt      = ?IMAPClient_VT
      *this\Caps    = ""
      *this\Conn    = AWNetwork::New()
      If *this\Conn
        *this\Conn\ClearLog()
        *ci          = *this\Conn\GetConnInfo()
        *ci\IMAP_Seq = 0
        *ci\Timeout  = 15000
        *ci\Protocol = AWNetwork::#PR_SMTP
        CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
          *ci\SSLMethod = AWNetwork::#SSL_METHOD_V23
        CompilerEndIf
        
        ProcedureReturn *this
      EndIf
      FreeStructure(*this)
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i OpenSSLAvailable()
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      ProcedureReturn AWPBT_EnableOpenSSL::initialized
    CompilerElse
      ProcedureReturn #False
    CompilerEndIf
  EndProcedure

  Procedure   SetTimeout(*this.sIMAPClient, timeout.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci         = *this\Conn\GetConnInfo()
    *ci\Timeout = timeout
  EndProcedure
  Procedure   SetHost(*this.sIMAPClient, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\Host = host
    *ci\Port = port
  EndProcedure
  Procedure   SetLogin(*this.sIMAPClient, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\User = user
    *ci\Pass = pass
  EndProcedure
  Procedure   SetProxyHost(*this.sIMAPClient, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyHost = host
    *ci\ProxyPort = port
  EndProcedure
  Procedure   SetProxyLogin(*this.sIMAPClient, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyUser = user
    *ci\ProxyPass = pass
  EndProcedure
  CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
    Procedure   SetSSLMethod(*this.sIMAPClient, sslmethod.i)
      Protected *ci.AWNetwork::ConnInfo
    
      *ci           = *this\Conn\GetConnInfo()
      *ci\SSLMethod = sslmethod
    EndProcedure
  CompilerEndIf
  
  Procedure.i ParseFlagsString(*this.sIMAPClient, flags.s)
    Protected flg.i
    
    flg = #FLAG_NONE
    If FindString(flags, "\seen", 1, #PB_String_NoCase)
      flg | #FLAG_SEEN
    ElseIf FindString(flags, "\answered", 1, #PB_String_NoCase)
      flg | #FLAG_ANSWERED
    ElseIf FindString(flags, "\flagged", 1, #PB_String_NoCase)
      flg | #FLAG_FLAGGED
    ElseIf FindString(flags, "\deleted", 1, #PB_String_NoCase)
      flg | #FLAG_DELETED
    ElseIf FindString(flags, "\draft", 1, #PB_String_NoCase)
      flg | #FLAG_DRAFT
    ElseIf FindString(flags, "\recent", 1, #PB_String_NoCase)
      flg | #FLAG_RECENT
    EndIf
    
    ProcedureReturn flg
  EndProcedure
  Procedure   ParseSelect(*this.sIMAPClient)
    Protected l.s, np.i, text.s, p.i, s.s
    
    np   = 1
    text = *this\Conn\GetReceivedString()
    With *this\resSELECT
      \unseen         = -1
      \uidvalidity    = -1
      \uidnext        = -1
      \exists         = -1
      \recent         = -1
      \writable       = #False
      \permanentflags = #FLAG_NONE
    
      While np <> -1
        l = LCase(AWSupport::GetLine(text, np, @np))
        If AWSupport::StartsWith(l, "* OK [UNSEEN ", #True)
          p = FindString(l, "]" ,14)
          If p
            s = Trim(Mid(l, 14, p - 14))
            If s <> ""
              \unseen = Val(s)
            EndIf
          EndIf
        ElseIf AWSupport::StartsWith(l, "* OK [UIDVALIDITY ", #True)
          p = FindString(l, "]" ,19)
          If p
            s = Trim(Mid(l, 19, p - 19))
            If s <> ""
              \uidvalidity = Val(s)
            EndIf
          EndIf
        ElseIf AWSupport::StartsWith(l, "* OK [UIDNEXT ", #True)
          p = FindString(l, "]" ,15)
          If p
            s = Trim(Mid(l, 15, p - 15))
            If s <> ""
              \uidnext = Val(s)
            EndIf
          EndIf
        ElseIf AWSupport::StartsWith(l, "* OK [PERMANENTFLAGS ", #True)
          p = FindString(l, "]" ,22)
          If p
            s = Trim(Mid(l, 22, p - 22))
            If s <> ""
              \permanentflags = ParseFlagsString(*this, s)
            EndIf          
          EndIf
        ElseIf AWSupport::StartsWith(l, "* FLAGS ", #True)
          s = Trim(Mid(l, 22, p - 22))
          If s <> ""
            \flags = ParseFlagsString(*this, s)
          EndIf
        ElseIf AWSupport::StartsWith(l, "* ")
          If FindString(l, " EXISTS", 3, #PB_String_NoCase)
            \exists = Val(StringField(l, 2, " "))
          ElseIf FindString(l, " RECENT", 3, #PB_String_NoCase)
            \recent = Val(StringField(l, 2, " "))
          EndIf
        Else
          If FindString(l, " [READ-WRITE] ", 1, #PB_String_NoCase)
            \writable = #True
          ElseIf FindString(l, " [READ-ONLY] ", 1, #PB_String_NoCase)
            \writable = #False
          EndIf
        EndIf
      Wend
    EndWith
  EndProcedure
  
  Procedure.i IMAP_Execute(*this.sIMAPClient, cmd.s, lineend.i, retries.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci          = *this\Conn\GetConnInfo()
    *ci\IMAP_Seq + 1
    
    ProcedureReturn *this\Conn\Execute("a" + Str(*ci\IMAP_Seq) + " " + cmd, lineend, retries)
  EndProcedure
  Procedure.i IMAP_Upload(*this.sIMAPClient, cmd.s, value.s, retries.i)
    Protected *ci.AWNetwork::ConnInfo, res.i, l.i
    
    *ci          = *this\Conn\GetConnInfo()
    *ci\IMAP_Seq + 1
    l            = Len(value)
    res = *this\Conn\Execute("a" + Str(*ci\IMAP_Seq) + " " + cmd + " {" + Str(l) + "}", AWNetwork::#LE_IMAP_DT, retries)
    If res = #ERR_NONE
      res = *this\Conn\Execute(value, AWNetwork::#LE_IMAP, retries)
    EndIf
    
    ProcedureReturn res
  EndProcedure

  Procedure.i _LOGIN(*this.sIMAPClient)
    Protected res.i, retries.i, done.i, *ci.AWNetwork::ConnInfo
    
    *ci     = *this\Conn\GetConnInfo()
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "LOGIN " + Chr(34) + *ci\User + Chr(34) + " " + Chr(34) + *ci\Pass + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
      
    ProcedureReturn res
  EndProcedure
  Procedure.i _CAPABILITY(*this.sIMAPClient)
    Protected res.i, retries.i, done.i, *ci.AWNetwork::ConnInfo, l.s, ns.i, text.s
    
    *ci        = *this\Conn\GetConnInfo()
    retries    = 1
    res        = #ERR_FAIL
    done       = #False
    *this\Caps = ""
    Repeat
      If IMAP_Execute(*this, "CAPABILITY", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ns   = 1
      text = *this\Conn\GetReceivedString()
      While ns <> -1
        l = AWSupport::GetLine(text, ns, @ns)
        If AWSupport::StartsWith(l, "* CAPABILITY ")
          *this\Caps = Mid(l, 14) + " "
        EndIf
      Wend
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _STARTTLS(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    res = #ERR_FAIL
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      retries = 1
      done    = #False
      Repeat
        If IMAP_Execute(*this, "STARTTLS", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
            Default:                  res = #ERR_FAIL : retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)

      If res = #ERR_NONE
      	If *this\Conn\UpgradeSSL() <> AWNetwork::#SSL_ERR_NONE
      		res = #ERR_FAIL
      	EndIf
      EndIf
    CompilerEndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i _LOGOUT(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "LOGOUT", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _NOOP(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "NOOP", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _LIST(*this.sIMAPClient, reference.s, mailbox.s = "*")
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "LIST " + Chr(34) + reference + Chr(34) + " " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _LSUB(*this.sIMAPClient, reference.s, mailbox.s = "*")
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "LSUB " + Chr(34) + reference + Chr(34) + " " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _CREATE(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "CREATE " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _DELETE(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "DELETE " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _RENAME(*this.sIMAPClient, mailbox_old.s, mailbox_new.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "RENAME " + Chr(34) + mailbox_old + Chr(34) + " " + Chr(34) + mailbox_new + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _SUBSCRIBE(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "SUBSCRIBE " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _UNSUBSCRIBE(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "UNSUBSCRIBE " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _SELECT(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "SELECT " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ParseSelect(*this)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _EXAMINE(*this.sIMAPClient, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "EXAMINE " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ParseSelect(*this)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _CLOSE(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "CLOSE", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
        
    ProcedureReturn res
  EndProcedure
  Procedure.i _EXPUNGE(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "EXPUNGE", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _CHECK(*this.sIMAPClient)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "CHECK", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _STATUS(*this.sIMAPClient, mailbox.s, req.i)
    Protected res.i, retries.i, done.i, cmd.s, flg.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      cmd = "STATUS " + Chr(34) + mailbox + Chr(34) + " ("
      flg = #False
      If IsSet(req, #REQ_MESSAGES)    :                              cmd + "MESSAGES"    : flg = #True : EndIf
      If IsSet(req, #REQ_RECENT)      : If flg : cmd + " " : EndIf : cmd + "RECENT"      : flg = #True : EndIf
      If IsSet(req, #REQ_UIDNEXT)     : If flg : cmd + " " : EndIf : cmd + "UIDNEXT"     : flg = #True : EndIf
      If IsSet(req, #REQ_UIDVALIDITY) : If flg : cmd + " " : EndIf : cmd + "UIDVALIDITY" : flg = #True : EndIf
      If IsSet(req, #REQ_UNSEEN)      : If flg : cmd + " " : EndIf : cmd + "UNSEEN"      : flg = #True : EndIf
      cmd + ")"
      If IMAP_Execute(*this, cmd, AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _APPEND(*this.sIMAPClient, mailbox.s, message.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Upload(*this, "APPEND " + Chr(34) + mailbox + Chr(34), message, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _STORE(*this.sIMAPClient, sequence.s, operation.s, values.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "STORE " + sequence + " " + operation + " (" + values + ")", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH(*this.sIMAPClient, index.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index) + " (RFC822)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Range(*this.sIMAPClient, index_from.i, index_to.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index_from) + ":" + Str(index_to) + " (RFC822)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Header(*this.sIMAPClient, index.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index) + " (RFC822.HEADER)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Header_Range(*this.sIMAPClient, index_from.i, index_to.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index_from) + ":" + Str(index_to) + " (RFC822.HEADER)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Flags(*this.sIMAPClient, index.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index) + " (FLAGS)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Flags_Range(*this.sIMAPClient, index_from.i, index_to.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index_from) + ":" + Str(index_to) + " (FLAGS)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Size(*this.sIMAPClient, index.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index) + " (RFC822.SIZE)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_Size_Range(*this.sIMAPClient, index_from.i, index_to.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index_from) + ":" + Str(index_to) + " (RFC822.SIZE)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_UID(*this.sIMAPClient, index.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index) + " (UID)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _FETCH_UID_Range(*this.sIMAPClient, index_from.i, index_to.i)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "FETCH " + Str(index_from) + ":" + Str(index_to) + " (UID)", AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _COPY(*this.sIMAPClient, index.i, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "COPY " + Str(index) + " " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ParseSelect(*this)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _COPY_Range(*this.sIMAPClient, index_from.i, index_to.i, mailbox.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "COPY " + Str(index_from) + ":" + Str(index_to) + " " + Chr(34) + mailbox + Chr(34), AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ParseSelect(*this)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _SEARCH(*this.sIMAPClient, criteria.s)
    Protected res.i, retries.i, done.i
    
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If IMAP_Execute(*this, "SEARCH " + criteria, AWNetwork::#LE_IMAP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case AWNetwork::#ERR_OK:  res = #ERR_NONE : done = #True
          Default:                  res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ParseSelect(*this)
    
    ProcedureReturn res
  EndProcedure
  
  Procedure.s _MakeFlagsString(flags.i)
    Protected flg.s, f.i
    
    flg = ""
    f   = #False
    If IsSet(flags, #FLAG_SEEN)     :                            flg + "\Seen"     : f = #True : EndIf
    If IsSet(flags, #FLAG_ANSWERED) : If f : flg + " " : EndIf : flg + "\Answered" : f = #True : EndIf
    If IsSet(flags, #FLAG_FLAGGED)  : If f : flg + " " : EndIf : flg + "\Flagged"  : f = #True : EndIf
    If IsSet(flags, #FLAG_DRAFT)    : If f : flg + " " : EndIf : flg + "\Draft"    : f = #True : EndIf
    If IsSet(flags, #FLAG_DELETED)  : If f : flg + " " : EndIf : flg + "\Deleted"  : f = #True : EndIf
    
    ProcedureReturn flg
  EndProcedure
  Procedure.i _SetFlags(*this.sIMAPClient, index.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index), "FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _SetFlags_Range(*this.sIMAPClient, index_from.i, index_to.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index_from) + ":" + Str(index_to), "FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _AddFlags(*this.sIMAPClient, index.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index), "+FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _AddFlags_Range(*this.sIMAPClient, index_from.i, index_to.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index_from) + ":" + Str(index_to), "+FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _DelFlags(*this.sIMAPClient, index.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index), "-FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _DelFlags_Range(*this.sIMAPClient, index_from.i, index_to.i, flags.i)
    ProcedureReturn _STORE(*this, Str(index_from) + ":" + Str(index_to), "-FLAGS.SILENT", _MakeFlagsString(flags))
  EndProcedure
  Procedure.i _DeleteMessage(*this.sIMAPClient, index.i)
    ProcedureReturn _AddFlags(*this, index, #FLAG_DELETED)
  EndProcedure
  Procedure.i _DeleteMessage_Range(*this.sIMAPClient, index_from.i, index_to.i)
    ProcedureReturn _AddFlags_Range(*this, index_from, index_to, #FLAG_DELETED)
  EndProcedure
  
  Procedure.i GetSELECTResult(*this.sIMAPClient)
    ProcedureReturn @*this\resSELECT
  EndProcedure
  Procedure.i GetEXAMINEResult(*this.sIMAPClient)
    ProcedureReturn @*this\resSELECT
  EndProcedure
  
  Procedure.i DoLogin(*this.sIMAPClient, flags.i)
    ProcedureReturn _LOGIN(*this)
  EndProcedure
  Procedure.i DoSecure(*this.sIMAPClient, flags.i)
    Protected res.i
    
    res = #ERR_NONE
    If IsSet(flags, #SSL_FORCE) Or IsSet(flags, #SSL_TRY)
      res = #ERR_FAIL
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
  		  If OpenSSLAvailable()
  		    If FindString(*this\Caps, "STARTTLS", 1, #PB_String_NoCase) > 0
  					; TLS starten; OpenSSL erforderlich 
            res = _STARTTLS(*this)
            If res = #ERR_NONE
          	  res = _CAPABILITY(*this)
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If (res <> #ERR_NONE) And IsSet(flags, #SSL_TRY)
        res = #ERR_NONE
      EndIf
    EndIf

    ProcedureReturn res
  EndProcedure
  Procedure.i Connect(*this.sIMAPClient, flags.i = #CONN_DEFAULT)
    Protected res.i, rc.i, *ci.AWNetwork::ConnInfo, loggedin.i, secured.i, tsb.i, tse.i, ts.s
    
    *ci                       = *this\Conn\GetConnInfo()
    res                       = #ERR_FAIL
    *this\Caps                = ""
    loggedin                  = #False
    secured                   = #False
    With *this\resSELECT
      \flags           = #FLAG_NONE
      \permanentflags  = #FLAG_NONE
      \recent          = -1
      \uidvalidity     = -1
      \unseen          = -1
      \exists          = -1
    EndWith
    
    With *this\Conn
      res = \Connect() 
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        If res = AWNetwork::#ERR_NONE
          If IsSet(flags, #SSL_FULL)
            res = #ERR_FAIL
            If OpenSSLAvailable()
              res = *this\Conn\UpgradeSSL()
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If res = AWNetwork::#ERR_NONE
        \ReadResult(AWNetwork::#LE_IMAP)
        If AWSupport::StartsWith(*this\Conn\GetReceivedString(), "* PREAUTH", #True)
          loggedin = #True
          res      = #ERR_NONE
        ElseIf *this\Conn\GetResultCode() = AWNetwork::#ERR_OK
          res      = #ERR_NONE
        Else
          res = #ERR_FAIL
        EndIf
      EndIf
      If res = #ERR_NONE
        If _CAPABILITY(*this) = #ERR_NONE
          If FindString(*this\Caps, "IMAP4rev1", 1, #PB_String_NoCase) = 0
            res = #ERR_FAIL
          EndIf
        EndIf
      EndIf
      If res = #ERR_NONE
        secured  = DoSecure(*this, flags)
        loggedin = DoLogin(*this, flags)
        If secured  <> #ERR_NONE : secured  = DoSecure(*this, flags) : EndIf
        If loggedin <> #ERR_NONE : loggedin = DoLogin(*this, flags)  : EndIf
        If (secured <> #ERR_NONE) Or (loggedin <> #ERR_NONE)
          res = #ERR_LOGIN
        EndIf
      EndIf
      If res <> #ERR_NONE                   ; nicht eingeloggt? Verbindung komplett trennen
        \Disconnect()
      EndIf
    EndWith
    
    ProcedureReturn res
  EndProcedure
  Procedure   DisConnect(*this.sIMAPClient)
    Protected res.i
    
    res = _LOGOUT(*this)
    If res = #ERR_NONE
      *this\Conn\DisConnect()
    EndIf
    
    ProcedureReturn res
  EndProcedure
  
  Procedure   Destroy(*this.sIMAPClient)
    DisConnect(*this)
    If *this\Conn : *this\Conn\Destroy() : EndIf
    FreeStructure(*this)
  EndProcedure
  Procedure.s GetLog(*this.sIMAPClient)
		ProcedureReturn *this\Conn\GetLog()
  EndProcedure
  Procedure   ClearLog(*this.sIMAPClient)
  	*this\Conn\ClearLog()
  EndProcedure
  Procedure   EnableLog(*this.sIMAPClient, enabled.i)
  	*this\Conn\EnableLog(enabled)
  EndProcedure
  Procedure.s GetLastError(*this.sIMAPClient)
    ProcedureReturn *this\Conn\GetLastError()
  EndProcedure
  Procedure   ClearLastError(*this.sIMAPClient)
    *this\Conn\ClearLastError()
  EndProcedure
  Procedure.s GetReceivedString(*this.sIMAPClient)
    ProcedureReturn *this\Conn\GetReceivedString()
  EndProcedure
  
  DataSection
    IMAPClient_VT:
      Data.i @Destroy()
      Data.i @SetTimeout()
      Data.i @SetHost()
      Data.i @SetLogin()
      Data.i @SetProxyHost()
      Data.i @SetProxyLogin()
      Data.i @Connect()
      Data.i @DisConnect()
      Data.i @GetLog()
      Data.i @ClearLog()
      Data.i @EnableLog()
      Data.i @GetLastError()
      Data.i @ClearLastError()
      Data.i @_NOOP()
      Data.i @_LIST()
      Data.i @_LSUB()
      Data.i @_CREATE()
      Data.i @_DELETE()
      Data.i @_RENAME()
      Data.i @_SUBSCRIBE()
      Data.i @_UNSUBSCRIBE()
      Data.i @_SELECT()
      Data.i @_EXAMINE()
      Data.i @_CLOSE()
      Data.i @_EXPUNGE()
      Data.i @_CHECK()
      Data.i @_STATUS()
      Data.i @_APPEND()
      Data.i @_STORE()
      Data.i @_FETCH()
      Data.i @_FETCH_Range()
      Data.i @_FETCH_Header()
      Data.i @_FETCH_Header_Range()
      Data.i @_FETCH_Flags()
      Data.i @_FETCH_Flags_Range()
      Data.i @_FETCH_Size()
      Data.i @_FETCH_Size_Range()
      Data.i @_FETCH_UID()
      Data.i @_FETCH_UID_Range()
      Data.i @_COPY()
      Data.i @_COPY_Range()
      Data.i @_SEARCH()
      Data.i @_SetFlags()
      Data.i @_SetFlags_Range()
      Data.i @_AddFlags()
      Data.i @_AddFlags_Range()
      Data.i @_DelFlags()
      Data.i @_DelFlags_Range()
      Data.i @_DeleteMessage()
      Data.i @_DeleteMessage_Range()
      Data.i @GetReceivedString()
      Data.i @GetSELECTResult()
      Data.i @GetEXAMINEResult()
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        Data.i @SetSSLMethod()
      CompilerEndIf
  EndDataSection
EndModule

 
; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 151
; FirstLine = 147
; Folding = --------------
; EnableUnicode
; EnableXP