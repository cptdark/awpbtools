;{   awcharset.pbi
;    Version 0.4 [2014/08/11]
;    Copyright (C) 2014 Ronny Kr�ger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;
; Code based on and inspired by:
;
; {==============================================================================|
; | Project : Ararat Synapse                                       | 005.002.002 |
; |==============================================================================|
; | Content: Charset conversion support                                          |
; |==============================================================================|
; | Copyright (c)1999-2004, Lukas Gebauer                                        |
; | All rights reserved.                                                         |
; |                                                                              |
; | Redistribution And use in source And binary forms, With Or without           |
; | modification, are permitted provided that the following conditions are met:  |
; |                                                                              |
; | Redistributions of source code must retain the above copyright notice, this  |
; | List of conditions And the following disclaimer.                             |
; |                                                                              |
; | Redistributions in binary form must reproduce the above copyright notice,    |
; | this List of conditions And the following disclaimer in the documentation    |
; | And/Or other materials provided With the distribution.                       |
; |                                                                              |
; | Neither the name of Lukas Gebauer nor the names of its contributors may      |
; | be used To endorse Or promote products derived from this software without    |
; | specific prior written permission.                                           |
; |                                                                              |
; | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS And CONTRIBUTORS "AS IS"  |
; | And ANY EXPRESS Or IMPLIED WARRANTIES, INCLUDING, BUT Not LIMITED To, THE    |
; | IMPLIED WARRANTIES OF MERCHANTABILITY And FITNESS For A PARTICULAR PURPOSE   |
; | ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS Or CONTRIBUTORS BE LIABLE For  |
; | ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, Or CONSEQUENTIAL       |
; | DAMAGES (INCLUDING, BUT Not LIMITED To, PROCUREMENT OF SUBSTITUTE GOODS Or   |
; | SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER   |
; | CAUSED And ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT           |
; | LIABILITY, Or TORT (INCLUDING NEGLIGENCE Or OTHERWISE) ARISING IN ANY WAY    |
; | OUT OF THE USE OF THIS SOFTWARE, EVEN If ADVISED OF THE POSSIBILITY OF SUCH  |
; | DAMAGE.                                                                      |
; |==============================================================================|
; | The Initial Developer of the Original Code is Lukas Gebauer (Czech Republic).|
; | Portions created by Lukas Gebauer are Copyright (c)2000-2004.                |
; | All Rights Reserved.                                                         |
; |==============================================================================|
; | Contributor(s):                                                              |
; |==============================================================================|
; | History: see HISTORY.HTM from distribution package                           |
; |          (Found at URL: http://www.ararat.cz/synapse/)                       |
; |==============================================================================}
;}

XIncludeFile "awsupport.pbi"

DeclareModule AWCharset
  Enumeration
  	#CS_UNKNOWN                 ;* unbekanntes Charset, nur zur Ergebnisauswertung
  	#CS_ISO_8859_1
  	#CS_ISO_8859_2
  	#CS_ISO_8859_3
  	#CS_ISO_8859_4
  	#CS_ISO_8859_5
  	#CS_ISO_8859_6
  	#CS_ISO_8859_7
  	#CS_ISO_8859_8
  	#CS_ISO_8859_9
  	#CS_ISO_8859_10
  	#CS_ISO_8859_13
  	#CS_ISO_8859_14 
  	#CS_ISO_8859_15
  	#CS_CP1250
  	#CS_CP1251
  	#CS_CP1252
  	#CS_CP1253
  	#CS_CP1254
  	#CS_CP1255
    #CS_CP1256
  	#CS_CP1257
  	#CS_CP1258
  	#CS_KOI8_R
  	#CS_CP895
  	#CS_CP852
  	#CS_UCS_2
  	#CS_UCS_4
  	#CS_UTF_8
  	#CS_UTF_7
    #CS_UTF_7mod
  	#CS_UCS_2LE
  	#CS_UCS_4LE
    ; next are supported by Iconv only...
    #CS_UTF_16
  	#CS_UTF_16LE
  	#CS_UTF_32
  	#CS_UTF_32LE
  	#CS_C99
  	#CS_JAVA
  	#CS_ISO_8859_16
  	#CS_KOI8_U
  	#CS_KOI8_RU
    #CS_CP862
  	#CS_CP866
  	#CS_MAC
  	#CS_MACCE
  	#CS_MACICE
  	#CS_MACCRO
  	#CS_MACRO
  	#CS_MACCYR
  	#CS_MACUK
  	#CS_MACGR
  	#CS_MACTU
    #CS_MACHEB
  	#CS_MACAR
  	#CS_MACTH
  	#CS_ROMAN8
  	#CS_NEXTSTEP
  	#CS_ARMASCII
  	#CS_GEORGIAN_AC
  	#CS_GEORGIAN_PS
    #CS_KOI8_T
  	#CS_MULELAO
  	#CS_CP1133
  	#CS_TIS620
  	#CS_CP874
  	#CS_VISCII
  	#CS_TCVN
  	#CS_ISO_IR_14
  	#CS_JIS_X0201
    #CS_JIS_X0208
  	#CS_JIS_X0212
  	#CS_GB1988_80
  	#CS_GB2312_80
  	#CS_ISO_IR_165
  	#CS_ISO_IR_149
  	#CS_EUC_JP
    #CS_SHIFT_JIS
  	#CS_CP932
  	#CS_ISO_2022_JP
  	#CS_ISO_2022_JP1
  	#CS_ISO_2022_JP2
  	#CS_GB2312
  	#CS_CP936
    #CS_GB18030
  	#CS_ISO_2022_CN
  	#CS_ISO_2022_CNE
  	#CS_HZ
  	#CS_EUC_TW
  	#CS_BIG5
  	#CS_CP950
  	#CS_BIG5_HKSCS
    #CS_EUC_KR
  	#CS_CP949
  	#CS_CP1361
  	#CS_ISO_2022_KR
  	#CS_CP737
  	#CS_CP775
  	#CS_CP853
  	#CS_CP855
  	#CS_CP857
    #CS_CP858
  	#CS_CP860
  	#CS_CP861
  	#CS_CP863
  	#CS_CP864
  	#CS_CP865
  	#CS_CP869
  	#CS_CP1125
  EndEnumeration
  
  Declare.i GetCharsetCode(charset.s)
  Declare.s GetCharsetName(charset.i)
  Declare.i GetHTMLCharsetCode(html.s)
  Declare.s GetHTMLCharsetName(html.s)
  Declare.i GetSysCharsetCode()
  Declare.s GetSysCharsetName()
EndDeclareModule

Module AWCharset
  EnableExplicit
  
  Procedure.i GetCharsetCode(charset.s)
    Protected c.s, t.i
    
    c = UCase(charset)
  	If c = "ISO-8859-1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CP819"
  		t = #CS_ISO_8859_1
  	ElseIf c = "IBM819"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO-IR-100"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO8859-1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO_8859-1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO_8859-1:1987"
  		t = #CS_ISO_8859_1
  	ElseIf c = "L1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "LATIN1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CSISOLATIN1"
  		t = #CS_ISO_8859_1
  	ElseIf c = "UTF-8"
  		t = #CS_UTF_8
  	ElseIf c = "ISO-10646-UCS-2"
  		t = #CS_UCS_2
  	ElseIf c = "UCS-2"
  		t = #CS_UCS_2
  	ElseIf c = "CSUNICODE"
  		t = #CS_UCS_2
  	ElseIf c = "UCS-2BE"
  		t = #CS_UCS_2
  	ElseIf c = "UNICODE-1-1"
  		t = #CS_UCS_2
  	ElseIf c = "UNICODEBIG"
  		t = #CS_UCS_2
  	ElseIf c = "CSUNICODE11"
  		t = #CS_UCS_2
  	ElseIf c = "UCS-2LE"
  		t = #CS_UCS_2LE
  	ElseIf c = "UNICODELITTLE"
  		t = #CS_UCS_2LE
  	ElseIf c = "ISO-10646-UCS-4"
  		t = #CS_UCS_4
  	ElseIf c = "UCS-4"
  		t = #CS_UCS_4
  	ElseIf c = "CSUCS4"
  		t = #CS_UCS_4
  	ElseIf c = "UCS-4BE"
  		t = #CS_UCS_4
  	ElseIf c = "UCS-4LE"
  		t = #CS_UCS_2LE
  	ElseIf c = "UTF-16"
  		t = #CS_UTF_16
  	ElseIf c = "UTF-16BE"
  		t = #CS_UTF_16
  	ElseIf c = "UTF-16LE"
  		t = #CS_UTF_16LE
  	ElseIf c = "UTF-32"
  		t = #CS_UTF_32
  	ElseIf c = "UTF-32BE"
  		t = #CS_UTF_32
  	ElseIf c = "UTF-32LE"
  		t = #CS_UTF_32
  	ElseIf c = "UNICODE-1-1-UTF-7"
  		t = #CS_UTF_7
  	ElseIf c = "UTF-7"
  		t = #CS_UTF_7
  	ElseIf c = "CSUNICODE11UTF7"
  		t = #CS_UTF_7
  	ElseIf c = "C99"
  		t = #CS_C99
  	ElseIf c = "JAVA"
  		t = #CS_JAVA
  	ElseIf c = "US-ASCII"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ANSI_X3.4-1968"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ANSI_X3.4-1986"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ASCII"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CP367"
  		t = #CS_ISO_8859_1
  	ElseIf c = "IBM367"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO-IR-6"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO646-US"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO_646.IRV:1991"
  		t = #CS_ISO_8859_1
  	ElseIf c = "US"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CSASCII"
  		t = #CS_ISO_8859_1
  	ElseIf c = "ISO-8859-2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "ISO-IR-101"
  		t = #CS_ISO_8859_2
  	ElseIf c = "ISO8859-2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "ISO_8859-2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "ISO_8859-2:1987"
  		t = #CS_ISO_8859_2
  	ElseIf c = "L2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "LATIN2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "CSISOLATIN2"
  		t = #CS_ISO_8859_2
  	ElseIf c = "ISO-8859-3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "ISO-IR-109"
  		t = #CS_ISO_8859_3
  	ElseIf c = "ISO8859-3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "ISO_8859-3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "ISO_8859-3:1988"
  		t = #CS_ISO_8859_3
  	ElseIf c = "L3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "LATIN3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "CSISOLATIN3"
  		t = #CS_ISO_8859_3
  	ElseIf c = "ISO-8859-4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "ISO-IR-110"
  		t = #CS_ISO_8859_4
  	ElseIf c = "ISO8859-4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "ISO_8859-4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "ISO_8859-4:1988"
  		t = #CS_ISO_8859_4
  	ElseIf c = "L4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "LATIN4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "CSISOLATIN4"
  		t = #CS_ISO_8859_4
  	ElseIf c = "ISO-8859-5"
  		t = #CS_ISO_8859_5
  	ElseIf c = "CYRILLIC"
  		t = #CS_ISO_8859_5
  	ElseIf c = "ISO-IR-144"
  		t = #CS_ISO_8859_5
  	ElseIf c = "ISO8859-5"
  		t = #CS_ISO_8859_5
  	ElseIf c = "ISO_8859-5"
  		t = #CS_ISO_8859_5
  	ElseIf c = "ISO_8859-5:1988"
  		t = #CS_ISO_8859_5
  	ElseIf c = "CSISOLATINCYRILLIC"
  		t = #CS_ISO_8859_5
  	ElseIf c = "ISO-8859-6"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ARABIC"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ASMO-708"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ECMA-114"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ISO-IR-127"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ISO8859-6"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ISO_8859-6"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ISO_8859-6:1987"
  		t = #CS_ISO_8859_6
  	ElseIf c = "CSISOLATINARABIC"
  		t = #CS_ISO_8859_6
  	ElseIf c = "ISO-8859-7"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ECMA-118"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ELOT_928"
  		t = #CS_ISO_8859_7
  	ElseIf c = "GREEK"
  		t = #CS_ISO_8859_7
  	ElseIf c = "GREEK8"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ISO-IR-126"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ISO8859-7"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ISO_8859-7"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ISO_8859-7:1987"
  		t = #CS_ISO_8859_7
  	ElseIf c = "CSISOLATINGREEK"
  		t = #CS_ISO_8859_7
  	ElseIf c = "ISO-8859-8"
  		t = #CS_ISO_8859_8
  	ElseIf c = "HEBREW"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO_8859-8"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO-IR-138"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO8859-8"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO_8859-8:1988"
  		t = #CS_ISO_8859_8
  	ElseIf c = "CSISOLATINHEBREW"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO-8859-8-I"
  		t = #CS_ISO_8859_8
  	ElseIf c = "ISO-8859-9"
  		t = #CS_ISO_8859_9
  	ElseIf c = "ISO-IR-148"
  		t = #CS_ISO_8859_9
  	ElseIf c = "ISO8859-9"
  		t = #CS_ISO_8859_9
  	ElseIf c = "ISO_8859-9"
  		t = #CS_ISO_8859_9
  	ElseIf c = "ISO_8859-9:1989"
  		t = #CS_ISO_8859_9
  	ElseIf c = "L5"
  		t = #CS_ISO_8859_9
  	ElseIf c = "LATIN5"
  		t = #CS_ISO_8859_9
  	ElseIf c = "CSISOLATIN5"
  		t = #CS_ISO_8859_9
  	ElseIf c = "ISO-8859-10"
  		t = #CS_ISO_8859_10
  	ElseIf c = "ISO-IR-157"
  		t = #CS_ISO_8859_10
  	ElseIf c = "ISO8859-10"
  		t = #CS_ISO_8859_10
  	ElseIf c = "ISO_8859-10"
  		t = #CS_ISO_8859_10
  	ElseIf c = "ISO_8859-10:1992"
  		t = #CS_ISO_8859_10
  	ElseIf c = "L6"
  		t = #CS_ISO_8859_10
  	ElseIf c = "LATIN6"
  		t = #CS_ISO_8859_10
  	ElseIf c = "CSISOLATIN6"
  		t = #CS_ISO_8859_10
  	ElseIf c = "ISO-8859-13"
  		t = #CS_ISO_8859_13
  	ElseIf c = "ISO-IR-179"
  		t = #CS_ISO_8859_13
  	ElseIf c = "ISO8859-13"
  		t = #CS_ISO_8859_13
  	ElseIf c = "ISO_8859-13"
  		t = #CS_ISO_8859_13
  	ElseIf c = "L7"
  		t = #CS_ISO_8859_13
  	ElseIf c = "LATIN7"
  		t = #CS_ISO_8859_13
  	ElseIf c = "ISO-8859-14"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO-CELTIC"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO-IR-199"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO8859-14"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO_8859-14"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO_8859-14:1998"
  		t = #CS_ISO_8859_14
  	ElseIf c = "L8"
  		t = #CS_ISO_8859_14
  	ElseIf c = "LATIN8"
  		t = #CS_ISO_8859_14
  	ElseIf c = "ISO-8859-15"
  		t = #CS_ISO_8859_15
  	ElseIf c = "ISO-IR-203"
  		t = #CS_ISO_8859_15
  	ElseIf c = "ISO8859-15"
  		t = #CS_ISO_8859_15
  	ElseIf c = "ISO_8859-15"
  		t = #CS_ISO_8859_15
  	ElseIf c = "ISO_8859-15:1998"
  		t = #CS_ISO_8859_15
  	ElseIf c = "ISO-8859-16"
  		t = #CS_ISO_8859_16
  	ElseIf c = "ISO-IR-226"
  		t = #CS_ISO_8859_16
  	ElseIf c = "ISO8859-16"
  		t = #CS_ISO_8859_16
  	ElseIf c = "ISO_8859-16"
  		t = #CS_ISO_8859_16
  	ElseIf c = "ISO_8859-16:2000"
  		t = #CS_ISO_8859_16
  	ElseIf c = "KOI8-R"
  		t = #CS_KOI8_R
  	ElseIf c = "CSKOI8R"
  		t = #CS_KOI8_R
  	ElseIf c = "KOI8-U"
  		t = #CS_KOI8_U
  	ElseIf c = "KOI8-RU"
  		t = #CS_KOI8_RU
  	ElseIf c = "WINDOWS-1250"
  		t = #CS_CP1250
  	ElseIf c = "CP1250"
  		t = #CS_CP1250
  	ElseIf c = "MS-EE"
  		t = #CS_CP1250
  	ElseIf c = "WINDOWS-1251"
  		t = #CS_CP1251
  	ElseIf c = "CP1251"
  		t = #CS_CP1251
  	ElseIf c = "MS-CYRL"
  		t = #CS_CP1251
  	ElseIf c = "WINDOWS-1252"
  		t = #CS_CP1252
  	ElseIf c = "CP1252"
  		t = #CS_CP1252
  	ElseIf c = "MS-ANSI"
  		t = #CS_CP1252
  	ElseIf c = "WINDOWS-1253"
  		t = #CS_CP1253
  	ElseIf c = "CP1253"
  		t = #CS_CP1253
  	ElseIf c = "MS-GREEK"
  		t = #CS_CP1253
  	ElseIf c = "WINDOWS-1254"
  		t = #CS_CP1254
  	ElseIf c = "CP1254"
  		t = #CS_CP1254
  	ElseIf c = "MS-TURK"
  		t = #CS_CP1254
  	ElseIf c = "WINDOWS-1255"
  		t = #CS_CP1255
  	ElseIf c = "CP1255"
  		t = #CS_CP1255
  	ElseIf c = "MS-HEBR"
  		t = #CS_CP1255
  	ElseIf c = "WINDOWS-1256"
  		t = #CS_CP1256
  	ElseIf c = "CP1256"
  		t = #CS_CP1256
  	ElseIf c = "MS-ARAB"
  		t = #CS_CP1256
  	ElseIf c = "WINDOWS-1257"
  		t = #CS_CP1257
  	ElseIf c = "CP1257"
  		t = #CS_CP1257
  	ElseIf c = "WINBALTRIM"
  		t = #CS_CP1257
  	ElseIf c = "WINDOWS-1258"
  		t = #CS_CP1258
  	ElseIf c = "CP1258"
  		t = #CS_CP1258
  	ElseIf c = "850"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CP850"
  		t = #CS_ISO_8859_1
  	ElseIf c = "IBM850"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CSPC850MULTILINGUAL"
  		t = #CS_ISO_8859_1
  	ElseIf c = "862"
  		t = #CS_CP862
  	ElseIf c = "CP862"
  		t = #CS_CP862
  	ElseIf c = "IBM862"
  		t = #CS_CP862
  	ElseIf c = "CSPC862LATINHEBREW"
  		t = #CS_CP862
  	ElseIf c = "866"
  		t = #CS_CP866
  	ElseIf c = "CP866"
  		t = #CS_CP866
  	ElseIf c = "IBM866"
  		t = #CS_CP866
  	ElseIf c = "CSIBM866"
  		t = #CS_CP866
  	ElseIf c = "MAC"
  		t = #CS_MAC
  	ElseIf c = "MACINTOSH"
  		t = #CS_MAC
  	ElseIf c = "MACROMAN"
  		t = #CS_MAC
  	ElseIf c = "CSMACINTOSH"
  		t = #CS_MAC
  	ElseIf c = "MACCENTRALEUROPE"
  		t = #CS_MACCE
  	ElseIf c = "MACICELAND"
  		t = #CS_MACICE
  	ElseIf c = "MACCROATIAN"
  		t = #CS_MACCRO
  	ElseIf c = "MACROMANIA"
  		t = #CS_MACRO
  	ElseIf c = "MACCYRILLIC"
  		t = #CS_MACCYR
  	ElseIf c = "MACUKRAINE"
  		t = #CS_MACUK
  	ElseIf c = "MACGREEK"
  		t = #CS_MACGR
  	ElseIf c = "MACTURKISH"
  		t = #CS_MACTU
  	ElseIf c = "MACHEBREW"
  		t = #CS_MACHEB
  	ElseIf c = "MACARABIC"
  		t = #CS_MACAR
  	ElseIf c = "MACTHAI"
  		t = #CS_MACTH
  	ElseIf c = "HP-ROMAN8"
  		t = #CS_ROMAN8
  	ElseIf c = "R8"
  		t = #CS_ROMAN8
  	ElseIf c = "ROMAN8"
  		t = #CS_ROMAN8
  	ElseIf c = "CSHPROMAN8"
  		t = #CS_ROMAN8
  	ElseIf c = "NEXTSTEP"
  		t = #CS_NEXTSTEP
  	ElseIf c = "ARMSCII-8"
  		t = #CS_ARMASCII
  	ElseIf c = "GEORGIAN-ACADEMY"
  		t = #CS_GEORGIAN_AC
  	ElseIf c = "GEORGIAN-PS"
  		t = #CS_GEORGIAN_PS
  	ElseIf c = "KOI8-T"
  		t = #CS_KOI8_T
  	ElseIf c = "MULELAO-1"
  		t = #CS_MULELAO
  	ElseIf c = "CP1133"
  		t = #CS_CP1133
  	ElseIf c = "IBM-CP1133"
  		t = #CS_CP1133
  	ElseIf c = "TIS-620"
  		t = #CS_TIS620
  	ElseIf c = "ISO-IR-166"
  		t = #CS_TIS620
  	ElseIf c = "TIS620"
  		t = #CS_TIS620
  	ElseIf c = "TIS620-0"
  		t = #CS_TIS620
  	ElseIf c = "TIS620.2529-1"
  		t = #CS_TIS620
  	ElseIf c = "TIS620.2533-0"
  		t = #CS_TIS620
  	ElseIf c = "TIS620.2533-1"
  		t = #CS_TIS620
  	ElseIf c = "CP874"
  		t = #CS_CP874
  	ElseIf c = "WINDOWS-874"
  		t = #CS_CP874
  	ElseIf c = "VISCII"
  		t = #CS_VISCII
  	ElseIf c = "VISCII1.1-1"
  		t = #CS_VISCII
  	ElseIf c = "CSVISCII"
  		t = #CS_VISCII
  	ElseIf c = "TCVN"
  		t = #CS_TCVN
  	ElseIf c = "TCVN-5712"
  		t = #CS_TCVN
  	ElseIf c = "TCVN5712-1"
  		t = #CS_TCVN
  	ElseIf c = "TCVN5712-1:1993"
  		t = #CS_TCVN
  	ElseIf c = "ISO-IR-14"
  		t = #CS_ISO_IR_14
  	ElseIf c = "ISO646-JP"
  		t = #CS_ISO_IR_14
  	ElseIf c = "JIS_C6220-1969-RO"
  		t = #CS_ISO_IR_14
  	ElseIf c = "JP"
  		t = #CS_ISO_IR_14
  	ElseIf c = "CSISO14JISC6220RO"
  		t = #CS_ISO_IR_14
  	ElseIf c = "JISX0201-1976"
  		t = #CS_JIS_X0201
  	ElseIf c = "JIS_X0201"
  		t = #CS_JIS_X0201
  	ElseIf c = "X0201"
  		t = #CS_JIS_X0201
  	ElseIf c = "CSHALFWIDTHKATAKANA"
  		t = #CS_JIS_X0201
  	ElseIf c = "ISO-IR-87"
  		t = #CS_JIS_X0208
  	ElseIf c = "JIS0208"
  		t = #CS_JIS_X0208
  	ElseIf c = "JIS_C6226-1983"
  		t = #CS_JIS_X0208
  	ElseIf c = "JIS_X0208"
  		t = #CS_JIS_X0208
  	ElseIf c = "JIS_X0208-1983"
  		t = #CS_JIS_X0208
  	ElseIf c = "JIS_X0208-1990"
  		t = #CS_JIS_X0208
  	ElseIf c = "X0208"
  		t = #CS_JIS_X0208
  	ElseIf c = "CSISO87JISX0208"
  		t = #CS_JIS_X0208
  	ElseIf c = "ISO-IR-159"
  		t = #CS_JIS_X0212
  	ElseIf c = "JIS_X0212"
  		t = #CS_JIS_X0212
  	ElseIf c = "JIS_X0212-1990"
  		t = #CS_JIS_X0212
  	ElseIf c = "JIS_X0212.1990-0"
  		t = #CS_JIS_X0212
  	ElseIf c = "X0212"
  		t = #CS_JIS_X0212
  	ElseIf c = "CSISO159JISX02121990"
  		t = #CS_JIS_X0212
  	ElseIf c = "CN"
  		t = #CS_GB1988_80
  	ElseIf c = "GB_1988-80"
  		t = #CS_GB1988_80
  	ElseIf c = "ISO-IR-57"
  		t = #CS_GB1988_80
  	ElseIf c = "ISO646-CN"
  		t = #CS_GB1988_80
  	ElseIf c = "CSISO57GB1988"
  		t = #CS_GB1988_80
  	ElseIf c = "CHINESE"
  		t = #CS_GB2312_80
  	ElseIf c = "GB_2312-80"
  		t = #CS_GB2312_80
  	ElseIf c = "ISO-IR-58"
  		t = #CS_GB2312_80
  	ElseIf c = "CSISO58GB231280"
  		t = #CS_GB2312_80
  	ElseIf c = "CN-GB-ISOIR165"
  		t = #CS_ISO_IR_165
  	ElseIf c = "ISO-IR-165"
  		t = #CS_ISO_IR_165
  	ElseIf c = "ISO-IR-149"
  		t = #CS_ISO_IR_149
  	ElseIf c = "KOREAN"
  		t = #CS_ISO_IR_149
  	ElseIf c = "KSC_5601"
  		t = #CS_ISO_IR_149
  	ElseIf c = "KS_C_5601-1987"
  		t = #CS_ISO_IR_149
  	ElseIf c = "KS_C_5601-1989"
  		t = #CS_ISO_IR_149
  	ElseIf c = "CSKSC56011987"
  		t = #CS_ISO_IR_149
  	ElseIf c = "EUC-JP"
  		t = #CS_EUC_JP
  	ElseIf c = "EUCJP"
  		t = #CS_EUC_JP
  	ElseIf c = "EXTENDED_UNIX_CODE_PACKED_FORMAT_FOR_JAPANESE"
  		t = #CS_EUC_JP
  	ElseIf c = "CSEUCPKDFMTJAPANESE"
  		t = #CS_EUC_JP
  	ElseIf c = "SHIFT-JIS"
  		t = #CS_SHIFT_JIS
  	ElseIf c = "MS_KANJI"
  		t = #CS_SHIFT_JIS
  	ElseIf c = "SHIFT_JIS"
  		t = #CS_SHIFT_JIS
  	ElseIf c = "SJIS"
  		t = #CS_SHIFT_JIS
  	ElseIf c = "CSSHIFTJIS"
  		t = #CS_SHIFT_JIS
  	ElseIf c = "CP932"
  		t = #CS_CP932
  	ElseIf c = "ISO-2022-JP"
  		t = #CS_ISO_2022_JP
  	ElseIf c = "CSISO2022JP"
  		t = #CS_ISO_2022_JP
  	ElseIf c = "ISO-2022-JP-1"
  		t = #CS_ISO_2022_JP1
  	ElseIf c = "ISO-2022-JP-2"
  		t = #CS_ISO_2022_JP2
  	ElseIf c = "CSISO2022JP2"
  		t = #CS_ISO_2022_JP2
  	ElseIf c = "CN-GB"
  		t = #CS_GB2312
  	ElseIf c = "EUC-CN"
  		t = #CS_GB2312
  	ElseIf c = "EUCCN"
  		t = #CS_GB2312
  	ElseIf c = "GB2312"
  		t = #CS_GB2312
  	ElseIf c = "CSGB2312"
  		t = #CS_GB2312
  	ElseIf c = "CP936"
  		t = #CS_CP936
  	ElseIf c = "GBK"
  		t = #CS_CP936
  	ElseIf c = "GB18030"
  		t = #CS_GB18030
  	ElseIf c = "ISO-2022-CN"
  		t = #CS_ISO_2022_CN
  	ElseIf c = "CSISO2022CN"
  		t = #CS_ISO_2022_CN
  	ElseIf c = "ISO-2022-CN-EXT"
  		t = #CS_ISO_2022_CNE
  	ElseIf c = "HZ"
  		t = #CS_HZ
  	ElseIf c = "HZ-GB-2312"
  		t = #CS_HZ
  	ElseIf c = "EUC-TW"
  		t = #CS_EUC_TW
  	ElseIf c = "EUCTW"
  		t = #CS_EUC_TW
  	ElseIf c = "CSEUCTW"
  		t = #CS_EUC_TW
  	ElseIf c = "BIG5"
  		t = #CS_BIG5
  	ElseIf c = "BIG-5"
  		t = #CS_BIG5
  	ElseIf c = "BIG-FIVE"
  		t = #CS_BIG5
  	ElseIf c = "BIGFIVE"
  		t = #CS_BIG5
  	ElseIf c = "CN-BIG5"
  		t = #CS_BIG5
  	ElseIf c = "CSBIG5"
  		t = #CS_BIG5
  	ElseIf c = "CP950"
  		t = #CS_CP950
  	ElseIf c = "BIG5-HKSCS"
  		t = #CS_BIG5_HKSCS
  	ElseIf c = "BIG5HKSCS"
  		t = #CS_BIG5_HKSCS
  	ElseIf c = "EUC-KR"
  		t = #CS_EUC_KR
  	ElseIf c = "EUCKR"
  		t = #CS_EUC_KR
  	ElseIf c = "CSEUCKR"
  		t = #CS_EUC_KR
  	ElseIf c = "CP949"
  		t = #CS_CP949
  	ElseIf c = "UHC"
  		t = #CS_CP949
  	ElseIf c = "CP1361"
  		t = #CS_CP1361
  	ElseIf c = "JOHAB"
  		t = #CS_CP1361
  	ElseIf c = "ISO-2022-KR"
  		t = #CS_ISO_2022_KR
  	ElseIf c = "CSISO2022KR"
  		t = #CS_ISO_2022_KR
  	ElseIf c = "437"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CP437"
  		t = #CS_ISO_8859_1
  	ElseIf c = "IBM437"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CSPC8CODEPAGE437"
  		t = #CS_ISO_8859_1
  	ElseIf c = "CP737"
  		t = #CS_CP737
  	ElseIf c = "CP775"
  		t = #CS_CP775
  	ElseIf c = "IBM775"
  		t = #CS_CP775
  	ElseIf c = "CSPC775BALTIC"
  		t = #CS_CP775
  	ElseIf c = "852"
  		t = #CS_CP852
  	ElseIf c = "CP852"
  		t = #CS_CP852
  	ElseIf c = "IBM852"
  		t = #CS_CP852
  	ElseIf c = "CSPCP852"
  		t = #CS_CP852
  	ElseIf c = "CP853"
  		t = #CS_CP853
  	ElseIf c = "855"
  		t = #CS_CP855
  	ElseIf c = "CP855"
  		t = #CS_CP855
  	ElseIf c = "IBM855"
  		t = #CS_CP855
  	ElseIf c = "CSIBM855"
  		t = #CS_CP855
  	ElseIf c = "857"
  		t = #CS_CP857
  	ElseIf c = "CP857"
  		t = #CS_CP857
  	ElseIf c = "IBM857"
  		t = #CS_CP857
  	ElseIf c = "CSIBM857"
  		t = #CS_CP857
  	ElseIf c = "CP858"
  		t = #CS_CP858
  	ElseIf c = "860"
  		t = #CS_CP860
  	ElseIf c = "CP860"
  		t = #CS_CP860
  	ElseIf c = "IBM860"
  		t = #CS_CP860
  	ElseIf c = "CSIBM860"
  		t = #CS_CP860
  	ElseIf c = "861"
  		t = #CS_CP861
  	ElseIf c = "CP-IS"
  		t = #CS_CP861
  	ElseIf c = "CP861"
  		t = #CS_CP861
  	ElseIf c = "IBM861"
  		t = #CS_CP861
  	ElseIf c = "CSIBM861"
  		t = #CS_CP861
  	ElseIf c = "863"
  		t = #CS_CP863
  	ElseIf c = "CP863"
  		t = #CS_CP863
  	ElseIf c = "IBM863"
  		t = #CS_CP863
  	ElseIf c = "CSIBM863"
  		t = #CS_CP863
  	ElseIf c = "CP864"
  		t = #CS_CP864
  	ElseIf c = "IBM864"
  		t = #CS_CP864
  	ElseIf c = "CSIBM864"
  		t = #CS_CP864
  	ElseIf c = "865"
  		t = #CS_CP865
  	ElseIf c = "CP865"
  		t = #CS_CP865
  	ElseIf c = "IBM865"
  		t = #CS_CP865
  	ElseIf c = "CSIBM865"
  		t = #CS_CP865
  	ElseIf c = "869"
  		t = #CS_CP869
  	ElseIf c = "CP-GR"
  		t = #CS_CP869
  	ElseIf c = "CP869"
  		t = #CS_CP869
  	ElseIf c = "IBM869"
  		t = #CS_CP869
  	ElseIf c = "CSIBM869"
  		t = #CS_CP869
  	ElseIf c = "CP1125"
  		t = #CS_CP1125
    Else
      t = #CS_UNKNOWN
    EndIf
    
    ProcedureReturn t
  EndProcedure
  Procedure.s GetCharsetName(charset.i)
    Protected c.s
    
    Select charset
  		Case #CS_UTF_8
  			c = "utf-8"
  		Case #CS_UCS_2
  			c = "ucs-2"
  		Case #CS_UCS_4
  			c = "ucs-4"
  		Case #CS_UTF_16
  			c = "utf-16"
  		Case #CS_UTF_16LE
  			c = "utf-16le"
  		Case #CS_UTF_32
  			c = "utf-32"
  		Case #CS_UTF_7
  			c = "utf-7"
  		Case #CS_C99
  			c = "c99"
  		Case #CS_JAVA
  			c = "java"
  		Case #CS_ISO_8859_1
  			c = "iso-8859-1"
  		Case #CS_ISO_8859_2
  			c = "iso-8859-2"
  		Case #CS_ISO_8859_3
  			c = "iso-8859-3"
  		Case #CS_ISO_8859_4
  			c = "iso-8859-4"
  		Case #CS_ISO_8859_5
  			c = "iso-8859-5"
  		Case #CS_ISO_8859_6
  			c = "iso-8859-6"
  		Case #CS_ISO_8859_7
  			c = "iso-8859-7"
  		Case #CS_ISO_8859_8
  			c = "iso-8859-8"
  		Case #CS_ISO_8859_9
  			c = "iso-8859-9"
  		Case #CS_ISO_8859_10
  			c = "iso-8859-10"
  		Case #CS_ISO_8859_13
  			c = "iso-8859-13"
  		Case #CS_ISO_8859_14
  			c = "iso-8859-14"
  		Case #CS_ISO_8859_15
  			c = "iso-8859-15"
  		Case #CS_ISO_8859_16
  			c = "iso-8859-16"
  		Case #CS_KOI8_R
  			c = "koi8-r"
  		Case #CS_KOI8_U
  			c = "koi8-u"
  		Case #CS_KOI8_RU
  			c = "koi8-ru"
  		Case #CS_CP1250
  			c = "cp1250"
  		Case #CS_CP1251
  			c = "cp1251"
  		Case #CS_CP1252
  			c = "cp1252"
  		Case #CS_CP1253
  			c = "cp1253"
  		Case #CS_CP1254
  			c = "cp1254"
  		Case #CS_CP1255
  			c = "cp1255"
  		Case #CS_CP1256
  			c = "cp1256"
  		Case #CS_CP1257
  			c = "cp1257"
  		Case #CS_CP1258
  			c = "cp1258"
  		Case #CS_CP862
  			c = "cp862"
  		Case #CS_CP866
  			c = "cp866"
  		Case #CS_MAC
  			c = "mac"
  		Case #CS_MACCE
  			c = "maccentraleurope"
  		Case #CS_MACICE
  			c = "maciceland"
  		Case #CS_MACCRO
  			c = "maccroatian"
  		Case #CS_MACRO
  			c = "macromania"
  		Case #CS_MACCYR
  			c = "maccyrillic"
  		Case #CS_MACUK
  			c = "macukraine"
  		Case #CS_MACGR
  			c = "macgreek"
  		Case #CS_MACTU
  			c = "macturkish"
  		Case #CS_MACHEB
  			c = "machebrew"
  		Case #CS_MACAR
  			c = "macarabic"
  		Case #CS_MACTH
  			c = "macthai"
  		Case #CS_ROMAN8
  			c = "roman8"
  		Case #CS_NEXTSTEP
  			c = "nextstep"
  		Case #CS_ARMASCII
  			c = "armscii-8"
  		Case #CS_GEORGIAN_AC
  			c = "georgian-academy"
  		Case #CS_GEORGIAN_PS
  			c = "georgian-ps"
  		Case #CS_KOI8_T
  			c = "koi8-t"
  		Case #CS_MULELAO
  			c = "mulelao-1"
  		Case #CS_CP1133
  			c = "cp1133"
  		Case #CS_TIS620
  			c = "tis-620"
  		Case #CS_CP874
  			c = "cp874"
  		Case #CS_VISCII
  			c = "viscii"
  		Case #CS_TCVN
  			c = "tcvn"
  		Case #CS_ISO_IR_14
  			c = "iso-ir-14"
  		Case #CS_JIS_X0201
  			c = "jis_x0201"
  		Case #CS_JIS_X0208
  			c = "jis_x0208"
  		Case #CS_JIS_X0212
  			c = "jis_x0212"
  		Case #CS_GB1988_80
  			c = "gb_1988-80"
  		Case #CS_GB2312_80
  			c = "gb_2312-80"
  		Case #CS_ISO_IR_165
  			c = "iso-ir-165"
  		Case #CS_ISO_IR_149
  			c = "iso-ir-149"
  		Case #CS_EUC_JP
  			c = "euc-jp"
  		Case #CS_SHIFT_JIS
  			c = "shift-jis"
  		Case #CS_CP932
  			c = "cp932"
  		Case #CS_ISO_2022_JP
  			c = "iso-2022-jp"
  		Case #CS_ISO_2022_JP1
  			c = "iso-2022-jp-1"
  		Case #CS_ISO_2022_JP2
  			c = "iso-2022-jp-2"
  		Case #CS_GB2312
  			c = "gb2312"
  		Case #CS_CP936
  			c = "cp936 gbk"
  		Case #CS_GB18030
  			c = "gb18030"
  		Case #CS_ISO_2022_CN
  			c = "iso-2022-cn"
  		Case #CS_ISO_2022_CNE
  			c = "iso-2022-cn-ext"
  		Case #CS_HZ
  			c = "hz"
  		Case #CS_EUC_TW
  			c = "euc-tw"
  		Case #CS_BIG5
  			c = "big5"
  		Case #CS_CP950
  			c = "cp950"
  		Case #CS_BIG5_HKSCS
  			c = "big5-hkscs"
  		Case #CS_EUC_KR
  			c = "euc-kr"
  		Case #CS_CP949
  			c = "cp949"
  		Case #CS_CP1361
  			c = "cp1361"
  		Case #CS_ISO_2022_KR
  			c = "iso-2022-kr"
  		Case #CS_CP737
  			c = "cp737"
  		Case #CS_CP775
  			c = "cp775"
  		Case #CS_CP852
  			c = "cp852"
  		Case #CS_CP853
  			c = "cp853"
  		Case #CS_CP855
  			c = "cp855"
  		Case #CS_CP857
  			c = "cp857"
  		Case #CS_CP858
  			c = "cp858"
  		Case #CS_CP860
  			c = "cp860"
  		Case #CS_CP861
  			c = "cp861"
  		Case #CS_CP863
  			c = "cp863"
  		Case #CS_CP864
  			c = "cp864"
  		Case #CS_CP865
  			c = "cp865"
  		Case #CS_CP869
  			c = "cp869"
  		Case #CS_CP1125
  			c = "cp1125"
      Default
        c = "unknown"
    EndSelect
    
    ProcedureReturn c
  EndProcedure
  Procedure.s GetHTMLCharsetName(html.s)
    Protected p.i, e.i, res.s
    
    res = ""
    p   = FindString(html, "charset", p)
    If p
      p = FindString(html, "=", p)
      If p
        e = FindString(html, ">", p + 2)
        If e
          res = AWSupport::TrimWSQ(Mid(html, p + 1, e - p - 1))
          e   = FindString(res, Chr(34), 1)
          If e
            res = AWSupport::TrimWSQ(Left(res, e - 1))
          EndIf
        EndIf
      EndIf
    EndIf

    ProcedureReturn res
  EndProcedure  
  Procedure.i GetHTMLCharsetCode(html.s)
    ProcedureReturn GetCharsetCode(GetHTMLCharsetName(html))
  EndProcedure
  Procedure.i GetSysCharsetCode()
  	Protected c.i
  	
  	CompilerSelect #PB_Compiler_OS
  		CompilerCase #PB_OS_Windows
  			Protected acp.i
  			
  			acp = GetACP_()
  			Select acp
  		    Case 437, 850, 20127
  		      c = #CS_ISO_8859_1
  		    Case 737
  		      c = #CS_CP737
  		    Case 775
  		      c = #CS_CP775
  		    Case 852
  		      c = #CS_CP852
  		    Case 855
  		      c = #CS_CP855
  		    Case 857
  		      c = #CS_CP857
  		    Case 858
  		      c = #CS_CP858
  		    Case 860
  		      c = #CS_CP860
  		    Case 861
  		      c = #CS_CP861
  		    Case 862
  		      c = #CS_CP862
  		    Case 863
  		      c = #CS_CP863
  		    Case 864
  		      c = #CS_CP864
  		    Case 865
  		      c = #CS_CP865
  		    Case 866
  		      c = #CS_CP866
  		    Case 869
  		      c = #CS_CP869
  		    Case 874
  		      c = #CS_ISO_8859_15
  		    Case 895
  		      c = #CS_CP895
  		    Case 932
  		      c = #CS_CP932
  		    Case 936
  		      c = #CS_CP936
  		    Case 949
  		      c = #CS_CP949
  		    Case 950
  		      c = #CS_CP950
  		    Case 1200
  		      c = #CS_UCS_2LE
  		    Case 1201
  		      c = #CS_UCS_2
  		    Case 1250
  		      c = #CS_CP1250
  		    Case 1251
  		      c = #CS_CP1251
  		    Case 1252
  		      c = #CS_CP1252
  		    Case 1253
  		      c = #CS_CP1253
  		    Case 1254
  		      c = #CS_CP1254
  		    Case 1255
  		      c = #CS_CP1255
  		    Case 1256
  		      c = #CS_CP1256
  		    Case 1257
  		      c = #CS_CP1257
  		    Case 1258
  		      c = #CS_CP1258
  		    Case 1361
  		      c = #CS_CP1361
  		    Case 10000
  		      c = #CS_MAC
  		    Case 10004
  		      c = #CS_MACAR
  		    Case 10005
  		      c = #CS_MACHEB
  		    Case 10006
  		      c = #CS_MACGR
  		    Case 10007
  		      c = #CS_MACCYR
  		    Case 10010
  		      c = #CS_MACRO
  		    Case 10017
  		      c = #CS_MACUK
  		    Case 10021
  		      c = #CS_MACTH
  		    Case 10029
  		      c = #CS_MACCE
  		    Case 10079
  		      c = #CS_MACICE
  		    Case 10081
  		      c = #CS_MACTU
  		    Case 10082
  		      c = #CS_MACCRO
  		    Case 12000
  		      c = #CS_UCS_4LE
  		    Case 12001
  		      c = #CS_UCS_4
  		    Case 20866
  		      c = #CS_KOI8_R
  		    Case 20932
  		      c = #CS_JIS_X0208
  		    Case 20936
  		      c = #CS_GB2312
  		    Case 21866
  		      c = #CS_KOI8_U
  		    Case 28591
  		      c = #CS_ISO_8859_1
  		    Case 28592
  		      c = #CS_ISO_8859_2
  		    Case 28593
  		      c = #CS_ISO_8859_3
  		    Case 28594
  		      c = #CS_ISO_8859_4
  		    Case 28595
  		      c = #CS_ISO_8859_5
  		    Case 28596, 708
  		      c = #CS_ISO_8859_6
  		    Case 28597
  		      c = #CS_ISO_8859_7
  		    Case 28598, 38598
  		      c = #CS_ISO_8859_8
  		    Case 28599
  		      c = #CS_ISO_8859_9
  		    Case 28605
  		      c = #CS_ISO_8859_15
  		    Case 50220
  		      c = #CS_ISO_2022_JP
  		    Case 50221
  		      c = #CS_ISO_2022_JP1
  		    Case 50222
  		      c = #CS_ISO_2022_JP2
  		    Case 50225
  		      c = #CS_ISO_2022_KR
  		    Case 50227
  		      c = #CS_ISO_2022_CN
  		    Case 50229
  		      c = #CS_ISO_2022_CNE
  		    Case 51932
  		      c = #CS_EUC_JP
  		    Case 51936
  		      c = #CS_GB2312
  		    Case 51949
  		      c = #CS_EUC_KR
  		    Case 52936
  		      c = #CS_HZ
  		    Case 54936
  		      c = #CS_GB18030
  		    Case 65000
  		      c = #CS_UTF_7
  		    Case 65001
  		      c = #CS_UTF_8
  		    Case 0
  		      c = #CS_UCS_2LE
  		  	Default
      			c = #CS_CP1252					
      	EndSelect
      CompilerCase #PB_OS_Linux
      	Protected res.s
      	
      	res = PeekS(nl_langinfo_(14))
      	res = UCase(res)
      	
      	If (FindString(res, "KAMENICKY") > 0) Or (FindString(res, "895") > 0)
      		c = #CS_CP895
      	ElseIf FindString(res, "MUTF-7") > 0
      		c = #CS_UTF_7mod
      	Else
      		c = GetCharsetCode(res)
      		If c = #CS_UNKNOWN
      			c = #CS_ISO_8859_1
      		EndIf
      	EndIf
    CompilerEndSelect
  	
  	ProcedureReturn c
  EndProcedure
  Procedure.s GetSysCharsetName()
  	ProcedureReturn GetCharsetName(GetSysCharsetCode())
  EndProcedure
EndModule

