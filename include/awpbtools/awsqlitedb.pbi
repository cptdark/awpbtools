;{   awsqlitedb.pbi
;    Version 0.6 [2015/10/26]
;    Copyright (C) 2012-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awpbtools.pbi"
XIncludeFile "awsqlstatement.pbi"
CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
	XIncludeFile "awdynsqlite3.pbi"
CompilerEndIf
  
DeclareModule AWSQLiteDB
  Interface SQLiteDB
    Destroy             ()
    Query.i             (sqlcmd.s)
    Update.i            (sqlcmd.s)
    BeginTransaction.i  ()
    EndTransaction.i    ()
    CancelTransaction.i ()
    BeginBatch.i        ()
    Batch.i             (sqlcmd.s)
    EndBatch.i          (save.i)
    TableExists.i       (tablename.s)
  	TriggerExists.i			(triggername.s)
    OpenDB.i            (filename.s, user.s, pass.s)
    OpenMemoryDB.i      ()
    CloseDB             ()
    Handle.i            ()
    GetBLOB.i           (Column.i)
    GetString.s         (Column.i)
    GetQuad.q           (Column.i)
    GetLong.i           (Column.i)
    GetFloat.f          (Column.i)
    GetDouble.d         (Column.i)
    NextRow.b           ()
    ColumnName.s        (Column.i)
    ColumnType.i        (Column.i)
    ColumnSize.i        (Column.i)
    Columns.i           ()
    DatabaseError.s     ()
    FinishQuery         ()
    PrepareStatement.i  (sqlcmd.s)
    IsReady.b           ()
    SaveMemoryDB.i			(filename.s)
    LoadMemoryDB.i			(filename.s)
  EndInterface
  
  Declare.i New()
  Declare.s SQLiteLibVersion()
  Declare.i SQLiteAvailable()
EndDeclareModule

Module AWSqliteDB
  EnableExplicit
  
  CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
  	Macro _OpenDatabase(DataBase, DatabaseName, User, Password, Plugin)
  		AWDynSQLite::DynSQLite_OpenDatabase(DatabaseName, User, Password)
  	EndMacro
  	Macro _CloseDatabase(DataBase)
  		AWDynSQLite::DynSQLite_CloseDatabase(DataBase)
  	EndMacro
  	Macro _IsDatabase(DataBase)
  		AWDynSQLite::DynSQLite_IsDatabase(DataBase)
  	EndMacro
  	Macro _DatabaseQuery(DataBase, Request)
  		AWDynSQLite::DynSQLite_DatabaseQuery(DataBase, Request)
  	EndMacro
  	Macro _FinishDatabaseQuery(DataBase)
  		AWDynSQLite::DynSQLite_FinishDatabaseQuery(DataBase)
  	EndMacro
  	Macro _DatabaseUpdate(DataBase, Request)
  		AWDynSQLite::DynSQLite_DatabaseUpdate(DataBase, Request)
  	EndMacro
  	Macro _DatabaseError(Database)
  		AWDynSQLite::DynSQLite_DatabaseError(Database)
  	EndMacro
  	Macro _DatabaseColumns(Database)
  		AWDynSQLite::DynSQLite_DatabaseColumns(Database)
  	EndMacro
  	Macro _DatabaseColumnName(Database, Column)
  		AWDynSQLite::DynSQLite_DatabaseColumnName(Database, Column)
  	EndMacro
  	Macro _DatabaseColumnType(Database, Column)
  		AWDynSQLite::DynSQLite_DatabaseColumnType(Database, Column)
  	EndMacro
  	Macro _DatabaseColumnSize(Database, Column)
  		AWDynSQLite::DynSQLite_DatabaseColumnSize(Database, Column)
  	EndMacro
  	Macro _NextDatabaseRow(Database)
  		AWDynSQLite::DynSQLite_NextDatabaseRow(Database)
  	EndMacro
  	Macro _GetDatabaseDouble(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseDouble(Database, Column)
  	EndMacro
  	Macro _GetDatabaseFloat(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseFloat(Database, Column)
  	EndMacro
  	Macro _GetDatabaseLong(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseLong(Database, Column)
  	EndMacro
  	Macro _GetDatabaseQuad(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseQuad(Database, Column)
  	EndMacro
  	Macro _GetDatabaseString(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseString(Database, Column)
  	EndMacro
  	Macro _GetDatabaseBLOB(Database, Column)
  		AWDynSQLite::DynSQLite_GetDatabaseBLOB(Database, Column)
  	EndMacro
  CompilerElse
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Linux
        ImportC #PB_Compiler_Home + "purelibraries/linux/libraries/libpbsqlite3.a"
      CompilerCase #PB_OS_Windows
        ImportC "sqlite3.lib"
      CompilerEndSelect
      sqlite3_libversion()
      sqlite3_backup_init(*pDest, zDestName.p-ascii, *pSource, zSourceName.p-ascii)
      sqlite3_backup_step(*sqlite3_backup, nPage)
      sqlite3_backup_finish(*sqlite3_backup)
      sqlite3_errcode(*db)
    EndImport
    
    Macro _OpenDatabase(DataBase, DatabaseName, User, Password, Plugin)
  		OpenDatabase(DataBase, DatabaseName, User, Password, Plugin)
  	EndMacro
  	Macro _CloseDatabase(DataBase)
  		CloseDatabase(DataBase)
  	EndMacro
  	Macro _IsDatabase(DataBase)
  		IsDatabase(DataBase)
  	EndMacro
  	Macro _DatabaseQuery(DataBase, Request)
  		DatabaseQuery(DataBase, Request)
  	EndMacro
  	Macro _FinishDatabaseQuery(DataBase)
  		FinishDatabaseQuery(DataBase)
  	EndMacro
  	Macro _DatabaseUpdate(DataBase, Request)
  		DatabaseUpdate(DataBase, Request)
  	EndMacro
  	Macro _DatabaseError(Database)
  		DatabaseError()
  	EndMacro
  	Macro _DatabaseColumns(Database)
  		DatabaseColumns(Database)
  	EndMacro
  	Macro _DatabaseColumnName(Database, Column)
  		DatabaseColumnName(Database, Column)
  	EndMacro
  	Macro _DatabaseColumnType(Database, Column)
  		DatabaseColumnType(Database, Column)
  	EndMacro
  	Macro _DatabaseColumnSize(Database, Column)
  		DatabaseColumnSize(Database, Column)
  	EndMacro
  	Macro _NextDatabaseRow(Database)
  		NextDatabaseRow(Database)
  	EndMacro
  	Macro _GetDatabaseDouble(Database, Column)
  		GetDatabaseDouble(Database, Column)
  	EndMacro
  	Macro _GetDatabaseFloat(Database, Column)
  		GetDatabaseFloat(Database, Column)
  	EndMacro
  	Macro _GetDatabaseLong(Database, Column)
  		GetDatabaseLong(Database, Column)
  	EndMacro
  	Macro _GetDatabaseQuad(Database, Column)
  		GetDatabaseQuad(Database, Column)
  	EndMacro
  	Macro _GetDatabaseString(Database, Column)
  		GetDatabaseString(Database, Column)
  	EndMacro
  	Macro _GetDatabaseBLOB(Database, Column)
  		GetDatabaseBlob_(Database, Column)
  	EndMacro
  CompilerEndIf
  
  Structure sSQLiteDB
    *vt.SQLiteDB
  
    FileName.s
    Handle.i
    BatchActive.i
    TransactionActive.i
  EndStructure
  
  Procedure.i New()
    Protected *this.sSQLiteDB
  
    *this = AllocateStructure(sSQLiteDB)
    If *this
      *this\vt                = ?SQLiteDB_VT
      *this\Handle            = #Null
      *this\TransactionActive = #False
      *this\BatchActive       = #False
      *this\FileName          = ""
  
      ProcedureReturn *this
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i GetDatabaseBlob_(db.i, Column.i)
  	Protected bytes.i, *buf
  
		bytes = DatabaseColumnSize(db, Column)
		If bytes > 0
			*buf = AllocateMemory(bytes)
			If *buf
			  If GetDatabaseBlob(db, column, *buf, bytes)
			    ProcedureReturn *buf
			  EndIf
			  FreeMemory(*buf)
			EndIf
		EndIf
  
  	ProcedureReturn #Null
  EndProcedure  
  Procedure.i SQLiteDB_Query(*this.sSQLiteDB, sqlcmd.s)
  	If _IsDatabase(*this\Handle)
  		If _DatabaseQuery(*this\Handle, sqlcmd)
  			ProcedureReturn #True
  		EndIf
  	EndIf
  	ProcedureReturn #False
  EndProcedure
  Procedure.i SQLiteDB_Update(*this.sSQLiteDB, sqlcmd.s)
    If _IsDatabase(*this\Handle)
  		If _DatabaseUpdate(*this\Handle, sqlcmd)
  			ProcedureReturn #True
  		EndIf
  	EndIf
  	ProcedureReturn #False
  EndProcedure
  Procedure.i SQLiteDB_BeginTransaction(*this.sSQLiteDB)
    *this\TransactionActive = SQLiteDB_Update(*this, "BEGIN TRANSACTION")
    ProcedureReturn *this\TransactionActive
  EndProcedure
  Procedure.i SQLiteDB_EndTransaction(*this.sSQLiteDB)
    If *this\TransactionActive
      *this\TransactionActive = #False
      ProcedureReturn SQLiteDB_Update(*this, "COMMIT TRANSACTION")
    EndIf
  
    ProcedureReturn #True
  EndProcedure
  Procedure.i SQLiteDB_CancelTransaction(*this.sSQLiteDB)
    If *this\TransactionActive
      *this\TransactionActive = #False
      ProcedureReturn SQLiteDB_Update(*this, "ROLLBACK TRANSACTION")
    EndIf
  
    ProcedureReturn #True
  EndProcedure
  Procedure.i SQLiteDB_BeginBatch(*this.sSQLiteDB)
  	*this\BatchActive = SQLiteDB_BeginTransaction(*this)
  	If Not *this\BatchActive
  		MessageRequester("SQLite-Error", _DatabaseError(*this\Handle))
  	EndIf
  
  	ProcedureReturn *this\BatchActive
  EndProcedure
  Procedure.i SQLiteDB_Batch(*this.sSQLiteDB, sqlcmd.s)
    If Not *this\BatchActive : ProcedureReturn #False : EndIf
    If Not SQLiteDB_Update(*this, sqlcmd)
      MessageRequester("SQLite-Error", _DatabaseError(*this\Handle))
      ProcedureReturn #False
    EndIf
  
  	ProcedureReturn #True
  EndProcedure
  Procedure.i SQLiteDB_EndBatch(*this.sSQLiteDB, save.i)
    Protected res.i
  
    res = #True
    If *this\BatchActive
      *this\BatchActive = #False
  
  		If save
  			If Not SQLiteDB_EndTransaction(*this)
  			  MessageRequester("SQLite-Error", _DatabaseError(*this\Handle))
  			  res = #False
  			EndIf
  		Else
  			If Not SQLiteDB_CancelTransaction(*this)
  				MessageRequester("SQLite-Error", _DatabaseError(*this\Handle))
  				res = #False
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn res
  EndProcedure
  Procedure.i SQLiteDB_TableExists(*this.sSQLiteDB, tablename.s)
    Protected res.i
  
  	res = #False
  	If SQLiteDB_Query(*this, "SELECT [sql] From sqlite_master WHERE [type] = 'table' And LOWER(name) = '" + LCase(TableName) + "' ")
  	  If _NextDatabaseRow(*this\Handle)
  			res = #True
  		EndIf
  		_FinishDatabaseQuery(*this\Handle)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i SQLiteDB_TriggerExists(*this.sSQLiteDB, triggername.s)
    Protected res.i
  
  	res = #False
  	If SQLiteDB_Query(*this, "SELECT [sql] From sqlite_master WHERE [type] = 'trigger' And LOWER(name) = '" + LCase(triggername) + "' ")
  	  If _NextDatabaseRow(*this\Handle)
  			res = #True
  		EndIf
  		_FinishDatabaseQuery(*this\Handle)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure   SQLiteDB_CloseDB(*this.sSQLiteDB)
  	If _IsDatabase(*this\Handle)
      If *this\BatchActive       : SQLiteDB_EndBatch(*this, #False)  : EndIf
      If *this\TransactionActive : SQLiteDB_CancelTransaction(*this) : EndIf
  		_CloseDatabase(*this\Handle)
  	EndIf
  	*this\Handle   					= #Null
  	*this\FileName 					= ""
  	*this\BatchActive 			= #False
  	*this\TransactionActive = #False
  EndProcedure
  Procedure.i SQLiteDB_OpenDB(*this.sSQLiteDB, filename.s, user.s, pass.s)
  	Protected f.i
  
  	SQLiteDB_CloseDB(*this)
  
  	*this\FileName = filename
  	If filename <> ""
  		Select FileSize(filename)
  			Case -1:
  				f = CreateFile(#PB_Any, filename)
  				If IsFile(f)
  					CloseFile(f)
  				Else
  					ProcedureReturn #False
  				EndIf
  			Case -2:
  				ProcedureReturn #False
  		EndSelect
  	Else
  		ProcedureReturn #False
  	EndIf
  
  	*this\Handle = _OpenDatabase(#PB_Any, filename, user, pass, #PB_Database_SQLite)
  	If _IsDatabase(*this\Handle)
  	  ProcedureReturn #True
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i SQLiteDB_OpenMemoryDB(*this.sSQLiteDB)
  	SQLiteDB_CloseDB(*this)
  
  	*this\FileName = ":memory:"
  	*this\Handle   = _OpenDatabase(#PB_Any, *this\FileName, "", "", #PB_Database_SQLite)
  	If _IsDatabase(*this\Handle)
  	  ProcedureReturn #True
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure   SQLiteDB_Destroy(*this.sSQLiteDB)
    SQLiteDB_CloseDB(*this)
  
    FreeStructure(*this)
  EndProcedure
  Procedure.i SQLiteDB_Handle(*this.sSQLiteDB)
    ProcedureReturn *this\Handle
  EndProcedure
  Procedure.i SQLiteDB_GetBLOB(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseBLOB(*this\Handle, Column)
  EndProcedure
  Procedure.s SQLiteDB_GetString(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseString(*this\Handle, Column)
  EndProcedure
  Procedure.q SQLiteDB_GetQuad(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseQuad(*this\Handle, Column)
  EndProcedure
  Procedure.i SQLiteDB_GetLong(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseLong(*this\Handle, Column)
  EndProcedure
  Procedure.f SQLiteDB_GetFloat(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseFloat(*this\Handle, Column)
  EndProcedure
  Procedure.d SQLiteDB_GetDouble(*this.sSQLiteDB, Column.i)
    ProcedureReturn _GetDatabaseDouble(*this\Handle, Column)
  EndProcedure
  Procedure.i SQLiteDB_NextRow(*this.sSQLiteDB)
  	If _NextDatabaseRow(*this\Handle)
  		ProcedureReturn #True
  	EndIf
  	ProcedureReturn #False
  EndProcedure
  Procedure.s SQLiteDB_ColumnName(*this.sSQLiteDB, Column.i)
    ProcedureReturn _DatabaseColumnName(*this\Handle, Column)
  EndProcedure
  Procedure.i SQLiteDB_ColumnType(*this.sSQLiteDB, Column.i)
    ProcedureReturn _DatabaseColumnType(*this\Handle, Column)
  EndProcedure
  Procedure.i SQLiteDB_ColumnSize(*this.sSQLiteDB, Column.i)
    ProcedureReturn _DatabaseColumnSize(*this\Handle, Column)
  EndProcedure
  Procedure.i SQLiteDB_Columns(*this.sSQLiteDB)
    ProcedureReturn _DatabaseColumns(*this\Handle)
  EndProcedure
  Procedure.s SQLiteDB_DatabaseError(*this.sSQLiteDB)
    ProcedureReturn _DatabaseError(*this\Handle)
  EndProcedure
  Procedure   SQLiteDB_FinishQuery(*this.sSQLiteDB)
    ProcedureReturn _FinishDatabaseQuery(*this\Handle)
  EndProcedure
  Procedure.i SQLiteDB_PrepareStatement(*this.sSQLiteDB, sqlcmd.s)
    Protected *stat.AWSqlStatement::SQLStatement
  
    *stat = AWSqlStatement::New()
    If *stat
      *stat\SetSQL(sqlcmd)
    EndIf
  
    ProcedureReturn *stat
  EndProcedure
  Procedure.i SQLiteDB_IsReady(*this.sSQLiteDB)
    If _IsDatabase(*this\Handle)
      ProcedureReturn #True
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i SQLiteDB_SaveMemoryDB(*this.sSQLiteDB, filename.s)
  	Protected f.i, db.i, *pbackup, rc.i
  
  	If Not _IsDatabase(*this\Handle)
  		ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  
  	If filename <> ""
  		If FileSize(filename) >= -1
  			f = CreateFile(#PB_Any, filename)
  			If IsFile(f)
  				CloseFile(f)
  			Else
  				ProcedureReturn 1 ; SQLITE_ERROR
  			EndIf
  		EndIf
  	Else
  		ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  
  	db = _OpenDatabase(#PB_Any, filename, "", "", #PB_Database_SQLite)
  	If Not _IsDatabase(db)
  	  ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  	  
  	CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
  	  rc = AWDynSQLite::DynSQLite_Backup(db, *this\handle)
  	CompilerElse
  		*pbackup = sqlite3_backup_init(DatabaseID(db), "main", DatabaseID(*this\Handle), "main")
  		If *pbackup
  			sqlite3_backup_step(*pbackup, -1)
  			sqlite3_backup_finish(*pbackup)
  	  EndIf
  	  rc = sqlite3_errcode(DatabaseID(db))
  	CompilerEndIf
  
    _CloseDatabase(db)
  
    ProcedureReturn rc
  EndProcedure
  Procedure.i SQLiteDB_LoadMemoryDB(*this.sSQLiteDB, filename.s)
    Protected f, db, *pbackup, rc.i
    
  	If Not SQLiteDB_OpenMemoryDB(*this)
  		ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  
  	If filename <> ""
  		If FileSize(filename) >= 0
  			f = ReadFile(#PB_Any, filename)
  			If IsFile(f)
  				CloseFile(f)
  			Else
  				ProcedureReturn 1 ; SQLITE_ERROR
  			EndIf
  		EndIf
  	Else
  		ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  
  	db = _OpenDatabase(#PB_Any, filename, "", "", #PB_Database_SQLite)
  	If Not _IsDatabase(db)
  	  ProcedureReturn 1 ; SQLITE_ERROR
  	EndIf
  
  	CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
  	  rc = AWDynSQLite::DynSQLite_Backup(*this\handle, db)
  	CompilerElse
  		*pbackup = sqlite3_backup_init(DatabaseID(*this\Handle), "main", DatabaseID(db), "main")
  		If *pbackup
  			sqlite3_backup_step(*pbackup, -1)
  			sqlite3_backup_finish(*pbackup)
  	  EndIf
  	  rc = sqlite3_errcode(DatabaseID(db))
  	CompilerEndIf
  
    _CloseDatabase(db)
  
    ProcedureReturn rc
  EndProcedure
  
  DataSection
    SQLiteDB_VT:
      Data.i @SQLiteDB_Destroy()
      Data.i @SQLiteDB_Query()
      Data.i @SQLiteDB_Update()
      Data.i @SQLiteDB_BeginTransaction()
      Data.i @SQLiteDB_EndTransaction()
      Data.i @SQLiteDB_CancelTransaction()
      Data.i @SQLiteDB_BeginBatch()
      Data.i @SQLiteDB_Batch()
      Data.i @SQLiteDB_EndBatch()
      Data.i @SQLiteDB_TableExists()
      Data.i @SQLiteDB_TriggerExists()
      Data.i @SQLiteDB_OpenDB()
      Data.i @SQLiteDB_OpenMemoryDB()
      Data.i @SQLiteDB_CloseDB()
      Data.i @SQLiteDB_Handle()
      Data.i @SQLiteDB_GetBLOB()
      Data.i @SQLiteDB_GetString()
      Data.i @SQLiteDB_GetQuad()
      Data.i @SQLiteDB_GetLong()
      Data.i @SQLiteDB_GetFloat()
      Data.i @SQLiteDB_GetDouble()
      Data.i @SQLiteDB_NextRow()
      Data.i @SQLiteDB_ColumnName()
      Data.i @SQLiteDB_ColumnType()
      Data.i @SQLiteDB_ColumnSize()
      Data.i @SQLiteDB_Columns()
      Data.i @SQLiteDB_DatabaseError()
      Data.i @SQLiteDB_FinishQuery()
      Data.i @SQLiteDB_PrepareStatement()
      Data.i @SQLiteDB_IsReady()
      Data.i @SQLiteDB_SaveMemoryDB()
      Data.i @SQLiteDB_LoadMemoryDB()
  EndDataSection
  
  Procedure.s SQLiteLibVersion()
    Protected v.s
  
  	CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
  	  v = AWDynSQLite::DynSQLite_LibVersion()
  	CompilerElse
  		v = PeekS(sqlite3_libversion(), #PB_Any, #PB_Ascii)
  	CompilerEndIf
  
    ProcedureReturn v
  EndProcedure
  
  Procedure.i SQLiteAvailable()
    CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
      ProcedureReturn AWPBT_EnableDynSQLite3Lib::initialized
    CompilerElse
      ProcedureReturn #True
    CompilerEndIf
  EndProcedure
EndModule

CompilerIf Defined(AWPBT_EnableDynSQLite3Lib , #PB_Module)
  AWPBT_EnableDynSQLite3Lib::initialized = AWDynSQLite::Init_DynSQLite3("")
CompilerElse
  UseSQLiteDatabase()
CompilerEndIf


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 441
; FirstLine = 437
; Folding = --------------
; EnableUnicode
; EnableXP