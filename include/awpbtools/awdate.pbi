;{   awdate.pbi
;    Version 0.3 [2014/08/11]
;    Copyright (C) 2010-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWDate
  Declare.s DateToRFC2822(date.i)
  Declare.i RFC2822ToDate(rfc.s)
EndDeclareModule

Module AWDate
  EnableExplicit
  
  Procedure.s DateToRFC2822(date.i)
    ; RFC2822-konformen String erzeugen
    Protected rfcdate.s, timezone.s
    
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        Protected tzh.i, tzm.i, tz.s, tzi.TIME_ZONE_INFORMATION, tzir.i, bias.i
        
        tzir = GetTimeZoneInformation_(tzi.TIME_ZONE_INFORMATION)
        bias = tzi\Bias
        Select tzir
          Case #TIME_ZONE_ID_STANDARD
            If tzi\StandardDate\wMonth
              bias + tzi\StandardBias
            EndIf
          Case #TIME_ZONE_ID_DAYLIGHT
            If tzi\DaylightDate\wMonth
              bias + tzi\DaylightBias
            EndIf
        EndSelect 
        
        date = AddDate(date, #PB_Date_Minute, bias)
        If bias > 0 : timezone = "-" : Else : timezone = "+" : EndIf
        timezone + RSet(Str(Abs(bias / 0.6)), 4, "0")
      CompilerCase #PB_OS_Linux 
        Protected p.i
        
        p = RunProgram("date", "+%z", "", #PB_Program_Open | #PB_Program_Read)
        If p
          timezone = ReadProgramString(p)
          CloseProgram(p)
          date = AddDate(date, #PB_Date_Minute, -(Val(timezone) * 0.6))
        Else ; falls Programm nicht verf�gbar ist, oder Aufruf fehlschl�gt
          timezone = ""
        EndIf
    CompilerEndSelect
    
    rfcdate  = StringField("Sun|Mon|Tue|Wed|Thu|Fri|Sat", DayOfWeek(date) + 1, "|")
    rfcdate  + ", " + Str(Day(date)) + " "
    rfcdate  + StringField("Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec", Month(date), "|")
    rfcdate  + FormatDate(" %yyyy %hh:%ii:%ss", date)
    If timezone : rfcdate + " " + timezone : EndIf
  
    ProcedureReturn rfcdate
  EndProcedure
  Procedure.i RFC2822ToDate(rfc.s)
    ; RFC(2)822-konformen String in normales PB-Datumsformat umwandeln
    Protected p.i, tz.s, dat.i, d.s, z.s
    
    p = FindString(rfc, ",", 1)                 ; ggf Tag entfernen
    If p : rfc = Trim(Mid(rfc, p+1)) : EndIf
    
    rfc = ReplaceString(rfc, " Jan ", ".01.")   ; Monatsnamen durch .Ziffer. ersetzen
    rfc = ReplaceString(rfc, " Feb ", ".02.")
    rfc = ReplaceString(rfc, " Mar ", ".03.")
    rfc = ReplaceString(rfc, " Apr ", ".04.")
    rfc = ReplaceString(rfc, " May ", ".05.")
    rfc = ReplaceString(rfc, " Jun ", ".06.")
    rfc = ReplaceString(rfc, " Jul ", ".07.")
    rfc = ReplaceString(rfc, " Aug ", ".08.")
    rfc = ReplaceString(rfc, " Sep ", ".09.")
    rfc = ReplaceString(rfc, " Oct ", ".10.")
    rfc = ReplaceString(rfc, " Nov ", ".11.")
    rfc = ReplaceString(rfc, " Dec ", ".12.")
    d   = StringField(rfc, 1, " ")               ; Datum, Zeit und Zeitzone extrahieren
    z   = StringField(rfc, 2, " ")
    tz  = StringField(rfc, 3, " ")
    
    Select tz                                    ; RFC822-Zeitzonen anpassen               
      Case ""    : tz = "+0000"
      Case "UT"  : tz = "+0000"
      Case "GMT" : tz = "+0000"
      Case "EST" : tz = "-0500"
      Case "EDT" : tz = "-0400"
      Case "CST" : tz = "-0600"
      Case "CDT" : tz = "-0500"
      Case "MST" : tz = "-0700"
      Case "MDT" : tz = "-0600"
      Case "PST" : tz = "-0800"
      Case "PDT" : tz = "-0700"
      Case "A"   : tz = "-0100"
      Case "B"   : tz = "-0200"
      Case "C"   : tz = "-0300"
      Case "D"   : tz = "-0400"
      Case "E"   : tz = "-0500"
      Case "F"   : tz = "-0600"
      Case "G"   : tz = "-0700"
      Case "H"   : tz = "-0800"
      Case "I"   : tz = "-0900"
      Case "K"   : tz = "-1000"
      Case "L"   : tz = "-1100"
      Case "M"   : tz = "-1200"
      Case "N"   : tz = "+0100"
      Case "O"   : tz = "+0200"
      Case "P"   : tz = "+0300"
      Case "Q"   : tz = "+0400"
      Case "R"   : tz = "+0500"
      Case "S"   : tz = "+0600"
      Case "T"   : tz = "+0700"
      Case "U"   : tz = "+0800"
      Case "V"   : tz = "+0900"
      Case "W"   : tz = "+1000"
      Case "X"   : tz = "+1100"
      Case "Y"   : tz = "+1200"
    EndSelect
    
    dat = ParseDate("%dd.%mm.%yyyy %hh:%ii:%ss", d + " " + z)
    dat = AddDate(dat, #PB_Date_Minute, Val(tz) * 0.6)
  
    ProcedureReturn dat
  EndProcedure
EndModule


