;{   awziparc.pbi
;    Version 0.5 [2015/10/26]
;    Copyright (C) 2011-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awzip.pbi"

UseCRC32Fingerprint()

DeclareModule AWZipArc
  Interface ZIPArchive
    Destroy        ()
    Clear          (onlybuild.i = #False)
    SetComment     (comment.s)
    AddFile.i      (FileName.s, EntryName.s = "", Comment.s = "", CompressionRatio.i = AWZip::#COMP_NORMAL)
    AddBinary.i    (*binary, binsize.i, EntryName.s, Comment.s = "", CompressionRatio.i = AWZip::#COMP_NORMAL)
    Build.i        ()
    GetZIP.i       ()
    GetZIPSize.i   ()
    SaveZIP.i      (filename.s)
  EndInterface
  
  Declare.i New()
EndDeclareModule

Module AWZipArc
  EnableExplicit
  
  CompilerSelect #PB_Compiler_OS
    CompilerCase #PB_OS_Linux
      ImportC #PB_Compiler_Home + "purelibraries/linux/libraries/zlib.a"
    CompilerCase #PB_OS_MacOS
      ImportC "/usr/lib/libz.dylib"
    CompilerCase #PB_OS_Windows
      ImportC "zlib.lib"
    CompilerEndSelect
    compress2(*dest, *destLen, *source, sourceLen, level)
    uncompress(*dest, *destLen, *source, sourceLen)
  EndImport
  
  Structure LocalFile_Hdr
    Signature.l   ; 4	Local file header signature = $04034b50
    ExtractVer.w  ; 2	Version needed To extract (minimum)
    Flags.w       ;	2	General purpose bit flag
    Method.w      ; 2	Compression method
    ModTime.w     ; 2	File last modification time
    ModDate.w     ; 2	File last modification date
    Crc32.l       ;	4	CRC-32
    CompSize.l    ;	4	Compressed size (k)
    OrgSize.l     ; 4	Uncompressed size
    FNameLen.w    ;	2	File name length (n)
    ExFldLen.w    ; 2	Extra field length (m)
    
    FileName.s    ;	n	File name
    ExtraFld.s    ; m	Extra field  
    CompData.i    ; k Compressed Data
  
    FileFlags.l   ; Flags, private field, not written to file but needed for further usage
    Comment.s     ; Comment, private filed, needed for later usage
    Patched.b     ; #True if compressed, first 2 and last 4 bytes are removed. so decrease CompData by 2 before freeing it
  EndStructure
  Structure CentralDir_Hdr
    Signature.l   ; 4	Central directory file header signature = $02014b50
    MadeVer.w     ;	2	Version made by
    ExtractVer.w  ;	2	Version needed To extract (minimum)
    Flags.w       ; 2	General purpose bit flag
    Method.w      ; 2	Compression method
    ModTime.w     ;	2	File last modification time
    ModDate.w     ; 2	File last modification date
    Crc32.l       ; 4	CRC-32
    CompSize.l    ; 4	Compressed size
    OrgSize.l     ; 4	Uncompressed size
    FNameLen.w    ; 2	File name length (n)
    ExFldLen.w    ; 2	Extra field length (m)
    CommentLen.w  ; 2	File comment length (k)
    DiskStart.w   ; 2 Disk number where file starts
    IntAtt.w      ; 2	Internal file attributes
    ExtAtt.l      ; 4	External file attributes
    Offset.l      ; 4	Relative offset of local file header
    
    FileName.s    ; n	File name
    ExtraFld.s    ;	m	Extra field
    Comment.s     ; k	File comment
  EndStructure
  Structure EndOfCentralDir
    Signature.l   ;	4	End of central directory signature = $06054b50
    DiskNum.w     ; 2	Number of this disk
    DiskStart.w   ; 2	Disk where central directory starts
    DCDirCount.w  ; 2	Number of central directory records on this disk
    TCDirCount.w  ; 2	Total number of central directory records
    CDirSize.l    ; 4	Size of central directory (bytes)
    CDirOffset.l  ; 4	Offset of start of central directory, relative To start of archive
    CommentLen.w  ;	2	ZIP file comment length (n)
    
    Comment.s     ; n	ZIP file comment
  EndStructure
  Structure Archive
    List Files.LocalFile_Hdr()
    List CDir.CentralDir_Hdr()
    EOCDir.EndOfCentralDir
  EndStructure
  
  Interface iZIPArchive Extends ZIPArchive
    CalcArcSize.i ()
  EndInterface
  Structure sZIPArchive
    *vt.VT_ZIPArchive
    *arc.Archive
    *build
    buildsize.i
  EndStructure
  
  Procedure.w DateToDosDate(d.i)
    Protected res.w
    
    ; Bit 0-4:  Day   (5)
    ; Bit 5-8:  Month (4)
    ; Bit 9-15: Year  (7) (since 1980)
    
     res = ((Year(d) - 1980) << 9) + (Month(d) << 5) + Day(d)
    
    ProcedureReturn res
  EndProcedure
  Procedure.w TimeToDosTime(t.i)
    Protected res.w
    
    ; Bit 0-4:   Second  (5)
    ; Bit 5-10:  Minute  (6)
    ; Bit 11-15: Hour    (5)
    
    res = (Hour(t) << 11) + (Minute(t) << 5) + (Second(t) / 2)
    
    ProcedureReturn res
  EndProcedure
  
  Procedure.i New()
    Protected *this.sZIPArchive
    
    *this = AllocateStructure(sZIPArchive)
    If *this 
      *this\vt    = ?VT_ZIPArchive
      *this\build = #Null
      *this\arc   = AllocateStructure(Archive)
      If *this\arc
        ProcedureReturn *this
      EndIf
      FreeStructure(*this)
    EndIf
    
    ProcedureReturn #Null
  EndProcedure
  Procedure   ZipArc_Clear(*this.sZIPArchive, onlybuild.i)
    If Not onlybuild
      ForEach *this\arc\Files()
        If *this\arc\Files()\CompData
          If *this\arc\Files()\Patched
            FreeMemory(*this\arc\Files()\CompData - 2)
          Else
            FreeMemory(*this\arc\Files()\CompData)
          EndIf
        EndIf
      Next
      ClearList(*this\arc\Files())
      ClearList(*this\arc\CDir())  
      ClearStructure(*this\arc, Archive)
    EndIf
    If *this\build : FreeMemory(*this\build) : *this\build = #Null : EndIf
  EndProcedure
  Procedure.i ZipArc_CalcArcSize(*this.sZIPArchive)
    Protected res.i
    
    ; Berechnung der Gr��e des resultierenden Archives
    
    res = 0
    ForEach *this\arc\Files()
      res + 98
      res + (Len(*this\arc\Files()\FileName) * 2)
      res + (Len(*this\arc\Files()\ExtraFld) * 2)
      res + *this\arc\Files()\CompSize
      res + Len(*this\arc\Files()\Comment)
      res + Len(*this\arc\EOCDir\Comment)
    Next
    
    ProcedureReturn res
  EndProcedure
  Procedure   ZipArc_SetComment(*this.sZIPArchive, comment.s)
    If *this\arc
      *this\arc\EOCDir\Comment = comment
    EndIf
  EndProcedure
  Procedure.i ZipArc_AddFile(*this.sZIPArchive, FileName.s, EntryName.s, Comment.s, CompressionRatio.i)
    Protected file.i, length.i, *buf, ok.i, destLen.i, *dest, c2.i, store.b, filedate.i, fileflags.l
    
    If CompressionRatio < AWZip::#COMP_NONE : CompressionRatio = AWZip::#COMP_NONE : EndIf
    If CompressionRatio > AWZip::#COMP_MAX  : CompressionRatio = AWZip::#COMP_MAX  : EndIf
        
    ok   = #False
    *buf = #Null
    If FileName
      If EntryName = #Null$
        EntryName = GetFilePart(FileName)
      EndIf
      
      filedate  = GetFileDate(FileName, #PB_Date_Modified)        ; Dateiattribute und Zeit ermitteln
      fileflags = GetFileAttributes(FileName)
      file      = ReadFile(#PB_Any, FileName)                     ; Datei laden
      If IsFile(file)
        length = Lof(file)
        If length > 0
          *buf = AllocateMemory(length)
          If *buf
            If ReadData(file, *buf, length) = length
              ok = #True
            EndIf
          EndIf
        EndIf
        CloseFile(file)      
      EndIf
    EndIf
         
    If ok
      AddElement(*this\arc\Files())
      With *this\arc\Files()                                    ; Dateiheader ausf�llen
        \Signature  = $04034b50
        \ExtractVer = 20
        \Flags      = 0
        \Method     = 8                                         ; Deflate (or 0 if not compressed)
        \ModTime    = TimeToDosTime(filedate)
        \ModDate    = DateToDosDate(filedate)
        \Crc32      = Val("$" + Fingerprint(*buf, length, #PB_Cipher_CRC32))
        \CompSize   = 0
        \OrgSize    = length
        \FNameLen   = Len(EntryName)
        \ExFldLen   = 0        
        \FileName   = EntryName
        \ExtraFld   = ""
        \FileFlags  = fileflags
        \CompData   = #Null
        \Comment    = Comment
        \Patched    = #False
      EndWith
      
      store = #True
      *dest = #Null
      If CompressionRatio <> AWZip::#COMP_STORE
        destLen = length + 13 + (Int(length / 90))      ; Gr��e des Zielpuffers berechnen (ca)
        *dest   = AllocateMemory(destLen)
        If *dest
          c2 = compress2(*dest, @destLen, *buf, length, CompressionRatio)
          If c2 = 0             ; Compression successfull            
            store = #False
          EndIf
        EndIf
      EndIf
      If Not store
        FreeMemory(*buf)                            ; Compression successfull, release uncompressed data
        *this\arc\Files()\CompSize = destLen - 6    ; remove leading 2 and trailing 4 bytes to be compatible with zip archives
        *this\arc\Files()\CompData = *dest + 2
        *this\arc\Files()\Patched  = #True          ; set for freeing process
      Else
        If *dest
          FreeMemory(*dest)
        EndIf
        *this\arc\Files()\Method   = 0      ; no compression
        *this\arc\Files()\CompSize = length ; CompLen = OrgLen
        *this\arc\Files()\CompData = *buf
      EndIf      
    EndIf
    
    ProcedureReturn ok
  EndProcedure
  Procedure.i ZipArc_AddBinary(*this.sZIPArchive, *binary, binsize.i, EntryName.s, Comment.s, CompressionRatio.i)
    Protected file.i, length.i, *buf, ok.i, destLen.i, *dest, c2.i, store.b, filedate.i, fileflags.l
    
    If CompressionRatio < AWZip::#COMP_NONE : CompressionRatio = AWZip::#COMP_NONE : EndIf
    If CompressionRatio > AWZip::#COMP_MAX  : CompressionRatio = AWZip::#COMP_MAX  : EndIf
        
    ok   = #False
    *buf = #Null
    If *binary And binsize And EntryName <> #Null$
      filedate  = Date()
      fileflags = 0
      *buf      = *binary
      length    = binsize
      AddElement(*this\arc\Files())
      With *this\arc\Files()                                    ; Dateiheader ausf�llen
        \Signature  = $04034b50
        \ExtractVer = 20
        \Flags      = 0
        \Method     = 8                                         ; Deflate (or 0 if not compressed)
        \ModTime    = TimeToDosTime(filedate)
        \ModDate    = DateToDosDate(filedate)
        \Crc32      = Val("$" + Fingerprint(*buf, length, #PB_Cipher_CRC32))
        \CompSize   = 0
        \OrgSize    = length
        \FNameLen   = Len(EntryName)
        \ExFldLen   = 0        
        \FileName   = EntryName
        \ExtraFld   = ""
        \FileFlags  = fileflags
        \CompData   = #Null
        \Comment    = Comment
        \Patched    = #False
      EndWith
      
      store = #True
      *dest = #Null
      If CompressionRatio <> AWZip::#COMP_STORE
        destLen = length + 13 + (Int(length / 90))      ; Gr��e des Zielpuffers berechnen (ca)
        *dest   = AllocateMemory(destLen)
        If *dest
          c2 = compress2(*dest, @destLen, *buf, length, CompressionRatio)
          If c2 = 0             ; Compression successfull            
            store = #False
          EndIf
        EndIf
      EndIf
      If Not store
        *this\arc\Files()\CompSize = destLen - 6    ; remove leading 2 and trailing 4 bytes to be compatible with zip archives
        *this\arc\Files()\CompData = *dest + 2
        *this\arc\Files()\Patched  = #True          ; set for freeing process
        ok                         = #True
      Else
        If *dest : FreeMemory(*dest) : EndIf
        *this\arc\Files()\Method   = 0                      ; no compression
        *this\arc\Files()\CompSize = length                 ; CompLen = OrgLen
        *this\arc\Files()\CompData = AllocateMemory(length)
        If *this\arc\Files()\CompData
          CopyMemory(*buf, *this\arc\Files()\CompData, length)
          ok = #True
        Else
          ok = #False
          DeleteElement(*this\arc\Files())
        EndIf
      EndIf      
    EndIf
    
    ProcedureReturn ok
  EndProcedure
  Procedure.i ZipArc_Build(*this.sZIPArchive)
    Protected len.i, i.i, *buf
    
    ; erzeugt das ZIP-Archiv komplett im Speicher
    
    len = 0
    If *this\build : FreeMemory(*this\build) : *this\build = #Null : EndIf
    If ListSize(*this\arc\Files())
      *this\build = AllocateMemory(ZipArc_CalcArcSize(*this) + 2)
      *buf        = *this\build
      If *buf
        ClearList(*this\arc\CDir())                                ; CentralDirectory des Archivs erzeugen
        ForEach *this\arc\Files()
          AddElement(*this\arc\CDir())
          *this\arc\CDir()\Signature  = $02014b50
          *this\arc\CDir()\MadeVer    = 20
          *this\arc\CDir()\ExtractVer = *this\arc\Files()\ExtractVer
          *this\arc\CDir()\Flags      = *this\arc\Files()\Flags
          *this\arc\CDir()\Method     = *this\arc\Files()\Method
          *this\arc\CDir()\ModTime    = *this\arc\Files()\ModTime
          *this\arc\CDir()\ModDate    = *this\arc\Files()\ModDate
          *this\arc\CDir()\Crc32      = *this\arc\Files()\Crc32
          *this\arc\CDir()\CompSize   = *this\arc\Files()\CompSize
          *this\arc\CDir()\OrgSize    = *this\arc\Files()\OrgSize
          *this\arc\CDir()\FNameLen   = *this\arc\Files()\FNameLen
          *this\arc\CDir()\ExFldLen   = *this\arc\Files()\ExFldLen
          *this\arc\CDir()\CommentLen = Len(*this\arc\Files()\Comment)
          *this\arc\CDir()\DiskStart  = 0
          *this\arc\CDir()\IntAtt     = 0
          *this\arc\CDir()\ExtAtt     = *this\arc\Files()\FileFlags
          *this\arc\CDir()\Offset     = *buf - *this\build
          *this\arc\CDir()\FileName   = *this\arc\Files()\FileName
          *this\arc\CDir()\ExtraFld   = *this\arc\Files()\ExtraFld
          *this\arc\CDir()\Comment    = *this\arc\Files()\Comment
          
          CopyMemory(@*this\arc\Files(), *buf, 30)                                         : *buf + 30
          If *this\arc\Files()\FNameLen
            PokeS(*buf, *this\arc\Files()\FileName, *this\arc\Files()\FNameLen, #PB_Ascii) : *buf + *this\arc\Files()\FNameLen
          EndIf
          If *this\arc\Files()\ExFldLen
            PokeS(*buf, *this\arc\Files()\ExtraFld, *this\arc\Files()\ExFldLen, #PB_Ascii) : *buf + *this\arc\Files()\ExFldLen
          EndIf
          CopyMemory(*this\arc\Files()\CompData, *buf, *this\arc\Files()\CompSize)         : *buf + *this\arc\Files()\CompSize
        Next
        
        *this\arc\EOCDir\Signature  = $06054b50
        *this\arc\EOCDir\DiskNum    = 0
        *this\arc\EOCDir\DiskStart  = 0
        *this\arc\EOCDir\DCDirCount = ListSize(*this\arc\Files())
        *this\arc\EOCDir\TCDirCount = *this\arc\EOCDir\DCDirCount
        *this\arc\EOCDir\CDirOffset = *buf - *this\build
        *this\arc\EOCDir\CommentLen = Len(*this\arc\EOCDir\Comment)
        
        ForEach *this\arc\CDir()
          CopyMemory(@*this\arc\CDir(), *buf, 46)                                          : *buf + 46
          If *this\arc\CDir()\FNameLen
            PokeS(*buf, *this\arc\CDir()\FileName, *this\arc\CDir()\FNameLen,   #PB_Ascii) : *buf + *this\arc\CDir()\FNameLen
          EndIf
          If *this\arc\CDir()\ExFldLen
            PokeS(*buf, *this\arc\CDir()\ExtraFld, *this\arc\CDir()\ExFldLen,   #PB_Ascii) : *buf + *this\arc\CDir()\ExFldLen
          EndIf
          If *this\arc\CDir()\CommentLen
            PokeS(*buf, *this\arc\CDir()\Comment,  *this\arc\CDir()\CommentLen, #PB_Ascii) : *buf + *this\arc\CDir()\CommentLen
          EndIf
        Next
        
        *this\arc\EOCDir\CDirSize = *buf - *this\build - *this\arc\EOCDir\CDirOffset
        
        CopyMemory(@*this\arc\EOCDir, *buf, 22) : *buf + 22
        If *this\arc\EOCDir\CommentLen
          PokeS(*buf, *this\arc\EOCDir\Comment, *this\arc\EOCDir\CommentLen, #PB_Ascii)    : *buf + *this\arc\EOCDir\CommentLen
        EndIf
        
        len             = *buf - *this\build      
        *this\buildsize = len
        
        ProcedureReturn #True
      EndIf
    EndIf
    
    *this\buildsize = 0
    *this\build     = #Null
    
    ProcedureReturn #False
  EndProcedure
  Procedure.i ZipArc_GetZIP(*this.sZIPArchive)
    ProcedureReturn *this\build
  EndProcedure
  Procedure.i ZipArc_GetZIPSize(*this.sZIPArchive)
    ProcedureReturn *this\buildsize
  EndProcedure
  Procedure.i ZipArc_SaveZIP(*this.sZIPArchive, filename.s)
    Protected file.i
    
    If *this\build And *this\buildsize
      file = CreateFile(#PB_Any, filename)
      If IsFile(file)
        WriteData(file, *this\build, *this\buildsize)
        CloseFile(file)
        ProcedureReturn #True
      EndIf
    EndIf
    
    ProcedureReturn #False
  EndProcedure
  Procedure ZipArc_Destroy(*this.sZIPArchive)
    ZipArc_Clear(*this, #False)
    FreeStructure(*this\arc)
    FreeStructure(*this)
  EndProcedure
  
  DataSection
    VT_ZIPArchive:
      Data.i @ZipArc_Destroy()
      Data.i @ZipArc_Clear()
      Data.i @ZipArc_SetComment()
      Data.i @ZipArc_AddFile()
      Data.i @ZipArc_AddBinary()
      Data.i @ZipArc_Build()  
      Data.i @ZipArc_GetZIP()
      Data.i @ZipArc_GetZIPSize()
      Data.i @ZipArc_SaveZIP()
      Data.i @ZipArc_CalcArcSize()
  EndDataSection
EndModule



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 161
; FirstLine = 147
; Folding = ----
; EnableUnicode
; EnableXP