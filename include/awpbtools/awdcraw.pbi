﻿;{   awdcraw.pbi
;    Version 0.1 [2015/11/12]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

UseTIFFImageDecoder()
UseJPEGImageDecoder()

DeclareModule AWDcRaw
	;	#DCR_AverageRect            ; -A <x y w h>
	Structure sAverageRect
		left.w
		top.w
		width.w
		height.w
	EndStructure
	;	#DCR_CustomWhiteBalance			; -r <mul1 mul2 mul3 mul4>
	Structure sCustomWhiteBalance
		red.f
		green.f
		blue.f
		gamma.f
	EndStructure
	;	#DCR_CorrectChromaticAberration		; -C <r b>
	Structure sChromaticAberration
		red.f
		blue.f
	EndStructure
	;	#DCR_CustomGammaCurve							; -g <p ts>
	Structure sCustomGammaCurve
		power.f
		toe_slope.f
	EndStructure
	;	#DCR_SetHighlightMode							; -H [0-9]
	Enumeration
		#HLM_Clip = 0
		#HLM_UnClip
		#HLM_Blend
		#HLM_Rebuild_Level_1
		#HLM_Rebuild_Level_2
		#HLM_Rebuild_Level_3
		#HLM_Rebuild_Level_4
		#HLM_Rebuild_Level_5
		#HLM_Rebuild_Level_6
		#HLM_Rebuild_Level_7
	EndEnumeration
	;	#DCR_SetFlipMode									; -t [0-7,90,180,270]
	Enumeration
		#FM_Disable
		#FM_Mode_1
		#FM_Mode_2
		#FM_Mode_3
		#FM_Mode_4
		#FM_Mode_5
		#FM_Mode_6
		#FM_Mode_7
		#FM_90_Degree		= 90
		#FM_180_Degree	= 180
		#FM_270_Degree	= 270
	EndEnumeration
	;	#DCR_SetOutputColorspace					; -o [0-5]
	Enumeration
		#CS_RAW
		#CS_sRGB
		#CS_Adobe_RGB
		#CS_Wide_Gamut_RGB
		#CS_Kodak_ProPhoto
		#CS_XYZ
	EndEnumeration
	;	#DCR_SetInterpolationQuality			; -q [0-3]
	Enumeration
		#IQ_Bilinear
		#IQ_VNG
		#IQ_PPG
		#IQ_AHD
	EndEnumeration
	
	EnumerationBinary
		#DCR_CameraWhiteBalance						; -w
		#DCR_AverageWhiteBalance					; -a
		#DCR_AverageRect									; -A <x y w h>
		#DCR_CustomWhiteBalance						; -r <r g b g>
		#DCR_UseEmbeddedColorMatrix				;	+M
		#DCR_DoNotUseEmbeddedColorMatrix	;	-M
		#DCR_CorrectChromaticAberration		; -C <r b>
		#DCR_FixDeadPixelsByFile					; -P <file>
		#DCR_SubtractDarkFrameFile				; -K <file>
		#DCR_SetDarknessLevel							; -k <num>
		#DCR_SetSaturationLevel						; -S <num>
		#DCR_SetHighlightMode							; -H [0-9]
		#DCR_SetFlipMode									; -t [0-7,90,180,270]
		#DCR_SetOutputColorspace					; -o [0-5]
		#DCR_DocumentMode									; -d
		#DCR_DocumentMode_NoScale					; -D
		#DCR_NoStretchOrRotate						; -j
		#DCR_AdjustBrightness							; -b num
		#DCR_DisableAutoBrighten					; -W
		#DCR_CustomGammaCurve							; -g <p ts>
		#DCR_SetInterpolationQuality			; -q [0-3]
		#DCR_HalfSizeColorImage						; -h
		#DCR_SetDenoiseThreshold					; -n <num>
		#DCR_InterpolateRGGBas4Colors			; -f
		#DCR_ApplyMedian3x3Filter					; -m <passes>
	EndEnumeration
	Structure DCRawFlags
		Flags.l
		DeadPixelFile.s
		DarkFrameFile.s
		AverageRect.sAverageRect
		CustomWhiteBalance.sCustomWhiteBalance
		ChromaticAberration.sChromaticAberration
		CustomGammaCurve.sCustomGammaCurve
		DarknessLevel.w
		SaturationLevel.w
		HighlightMode.b
		FlipMode.w
		OutputColorspace.b
		BrightnessLevel.f
		InterpolationQuality.b
		DenoiseThreshold.w
		MedianFilterPasses.w
	EndStructure
	
	Declare   SetExe(filename.s)
	Declare.i GetThumbnail(filename.s)
	Declare.i GetPreview(filename.s)
	Declare.i GetThumbnailData(filename.s, *bufsize)
	Declare.i GetPreviewData(filename.s, *bufsize)
	Declare.i GetImageEx(filename.s, *flags.DCRawFlags)
	Declare.i GetImageDataEx(filename.s, *bufsize, *flags.DCRawFlags)
EndDeclareModule

Module AWDcRaw
  EnableExplicit
  
	Macro IsSet(X, Y)
		(((X) & (Y)) = (Y))
	EndMacro
  
  Global DCRawCmd.s
  
  Enumeration
  	#BlockSize = 128 * 1024	; 128 KiB block increase on allocating memory
  EndEnumeration
  
  Procedure SetExe(filename.s)
  	DCRawCmd = filename
  EndProcedure
  
  Procedure.i ExecuteDCRaw(parameters.s, *datasize)
  	Protected app.i, dsize.i, msize.i, *buf, *nbuf, bavail.i, bsize.i, err.i, bytes.i
  	
		*buf = #Null
  	app  = RunProgram(DCRawCmd, parameters, "", #PB_Program_Open | #PB_Program_Read)
  	If IsProgram(app)
			dsize = 0
			msize = 0
			err   = #False
	
		  While ProgramRunning(app)
		  	bavail = AvailableProgramOutput(app)
		  	If bavail > 0
		  		If dsize + bavail > msize
			  		bsize = #BlockSize
			  		While bsize < bavail
			  			bsize + #BlockSize
			  		Wend
			  		
			  		If *buf
		  				msize = dsize + bsize
			  			*nbuf = ReAllocateMemory(*buf, msize, #PB_Memory_NoClear)
			  			If *nbuf
			  				*buf = *nbuf
			  			Else
			  				err = #True
			  				Break
			  			EndIf
			  		Else
			  			msize = bsize
			  			*buf  = AllocateMemory(msize, #PB_Memory_NoClear)
			  			If Not *buf
			  				err = #True
			  				Break
			  			EndIf
			  		EndIf
		  		EndIf
		  		bytes = ReadProgramData(app, *buf + dsize, bavail)
		  		dsize + bytes
		  	EndIf
		  Wend
		  If ProgramExitCode(app) <> 0
		  	err = #True
		  EndIf
		  
		  CloseProgram(app)
		  
		  If err
		  	If *buf
		  		FreeMemory(*buf)
			  	*buf = #Null
		  	EndIf
		  Else
		  	PokeI(*datasize, dsize)
		  EndIf
		EndIf
		  
	  ProcedureReturn *buf
	EndProcedure
	Procedure.i GetImage(*buf, size.i)
		Protected img.i
		
		If *buf And (size > 0)
			img = CatchImage(#PB_Any, *buf, size)
		EndIf
		
		ProcedureReturn img
	EndProcedure
	Procedure.i GetThumbnailData(filename.s, *bufsize)
		ProcedureReturn ExecuteDCRaw("-e -c " + filename, *bufsize)
	EndProcedure
	Procedure.i GetThumbnail(filename.s)
		Protected *buf, bufsize.i, img.i
		
		*buf = GetThumbnailData(filename, @bufsize)
		If *buf
			img = GetImage(*buf, bufsize)
			FreeMemory(*buf)
		EndIf
		If Not IsImage(img)
			ProcedureReturn #False
		EndIf
		
		ProcedureReturn img
	EndProcedure
	Procedure.i GetPreviewData(filename.s, *bufsize)
		ProcedureReturn ExecuteDCRaw("-T -h -j -o 1 -c " + filename, *bufsize)
	EndProcedure
  Procedure.i GetPreview(filename.s)
		Protected *buf, bufsize.i, img.i
		
		*buf = GetPreviewData(filename, @bufsize)
		If *buf
			img = GetImage(*buf, bufsize)
			FreeMemory(*buf)
		EndIf
		If Not IsImage(img)
			ProcedureReturn #False
		EndIf
		
		ProcedureReturn img
	EndProcedure
	Procedure.i GetImageDataEx(filename.s, *bufsize, *flags.DCRawFlags)
		Protected cmd.s
		
		cmd = ""
		With *flags
			If IsSet(\Flags, #DCR_CameraWhiteBalance)
				cmd + "-w "
			ElseIf IsSet(\Flags, #DCR_AverageWhiteBalance)
				cmd + "-a "
			ElseIf IsSet(\Flags, #DCR_AverageRect)
				cmd + "-A " + Str(\AverageRect\left) + " " + Str(\AverageRect\top) + " " + Str(\AverageRect\width) + " " + Str(\AverageRect\height) + " "
			ElseIf IsSet(\Flags, #DCR_CustomWhiteBalance)
				cmd + "-r " + StrF(\CustomWhiteBalance\red) + " " + StrF(\CustomWhiteBalance\green) + " " + StrF(\CustomWhiteBalance\blue) + " " + StrF(\CustomWhiteBalance\gamma) + " "
			ElseIf IsSet(\Flags, #DCR_UseEmbeddedColorMatrix)
				cmd + "+M "
			ElseIf IsSet(\Flags, #DCR_DoNotUseEmbeddedColorMatrix)
				cmd + "-M "
			ElseIf IsSet(\Flags, #DCR_CorrectChromaticAberration)
				cmd + "-C " + StrF(\ChromaticAberration\red) + " " + StrF(\ChromaticAberration\blue) + " "
			ElseIf IsSet(\Flags, #DCR_FixDeadPixelsByFile)
				If \DeadPixelFile <> ""
					cmd + "-P " + Chr(34) + \DeadPixelFile + Chr(34) + " "
				EndIf
			ElseIf IsSet(\Flags, #DCR_SubtractDarkFrameFile)
				If \DarkFrameFile <> ""
					cmd + "-K " + Chr(34) + \DarkFrameFile + Chr(34) + " "
				EndIf
			ElseIf IsSet(\Flags, #DCR_SetDarknessLevel)
				cmd + "-k " + Str(\DarknessLevel) + " "
			ElseIf IsSet(\Flags, #DCR_SetSaturationLevel)
				cmd + "-S " + Str(\SaturationLevel) + " "
			ElseIf IsSet(\Flags, #DCR_SetHighlightMode)
				cmd + "-H " + Str(\HighlightMode) + " "
			ElseIf IsSet(\Flags, #DCR_SetFlipMode)
				cmd + "-t " + Str(\FlipMode) + " "
			ElseIf IsSet(\Flags, #DCR_SetOutputColorspace)
				cmd + "-o " + Str(\OutputColorspace) + " "
			ElseIf IsSet(\Flags, #DCR_DocumentMode)
				cmd + "-d "
			ElseIf IsSet(\Flags, #DCR_DocumentMode_NoScale)
				cmd + "-D "
			ElseIf IsSet(\Flags, #DCR_NoStretchOrRotate)
				cmd + "-j "
			ElseIf IsSet(\Flags, #DCR_AdjustBrightness)
				cmd + "-b " + StrF(\BrightnessLevel) + " "
			ElseIf IsSet(\Flags, #DCR_DisableAutoBrighten)
				cmd + "-W "
			ElseIf IsSet(\Flags, #DCR_CustomGammaCurve)
				cmd + "-g " + StrF(\CustomGammaCurve\power) + " " + StrF(\CustomGammaCurve\toe_slope) + " "
			ElseIf IsSet(\Flags, #DCR_SetInterpolationQuality)
				cmd + "-q " + Str(\InterpolationQuality) + " "
			ElseIf IsSet(\Flags, #DCR_HalfSizeColorImage)
				cmd + "-h "
			ElseIf IsSet(\Flags, #DCR_SetDenoiseThreshold)
				cmd + "-n " + Str(\DenoiseThreshold) + " "
			ElseIf IsSet(\Flags, #DCR_InterpolateRGGBas4Colors)
				cmd + "-f "
			ElseIf IsSet(\Flags, #DCR_ApplyMedian3x3Filter)
				cmd + "-m " + Str(\MedianFilterPasses) + " "
			EndIf
		EndWith
		cmd + " -T " + filename
		
		ProcedureReturn ExecuteDCRaw(cmd, *bufsize)
	EndProcedure
	Procedure.i GetImageEx(filename.s, *flags.DCRawFlags)
		Protected *buf, bufsize.i, img.i
		
		*buf = GetImageDataEx(filename, @bufsize, *flags)
		If *buf
			img = GetImage(*buf, bufsize)
			FreeMemory(*buf)
		EndIf
		If Not IsImage(img)
			ProcedureReturn #False
		EndIf
		
		ProcedureReturn img
	EndProcedure
  
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 93
; Folding = DABA-
; EnableUnicode
; EnableXP