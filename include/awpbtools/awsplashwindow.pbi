﻿;{   awpbtools.pbi
;    Version 0.1 [2015/10/29]
;    Copyright (C) 2014-15 Ronny Krüger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;} 

DeclareModule AWSplashWindow
	Interface SplashWindow
		Destroy       		()
		Show          		()
		Hide          		()
		GetWindowID.i 		()
		SetWindowSpecs		(left.i, top.i, width.i, height.i, title.s)
		SetVersionSpecs		(left.i, top.i, width.i, height.i, text.s, flags.i, frontcolor.i, backcolor.i)
		SetProgressSpecs	(left.i, top.i, width.i, height.i, percent.i, flags.i, frontcolor.i, backcolor.i)
		SetStatusSpecs		(left.i, top.i, width.i, height.i, text.s, flags.i, frontcolor.i, backcolor.i)
		SetImage					(imageID.i)
		UpdateStatus			(text.s)
		UpdateProgress		(percent.i)

	EndInterface
	
	Declare.i New()
EndDeclareModule

Module AWSplashWindow
	EnableExplicit
	
	Structure sSizeInfo
		w.i
		h.i
		x.i
		y.i
	EndStructure
		
	Structure sSplashWindow
		*vt.VT
		
		mUiWindow.i
		mTitle.s
		mVersion.s
		mProgress.i
		mStatus.s
		mImage.i
		mWindowPos.sSizeInfo
		mVersionPos.sSizeInfo
		mProgressPos.sSizeInfo
		mStatusPos.sSizeInfo
		mStatusFlags.i
		mVersionFlags.i
		mProgressFlags.i
		mStatusFont.i
		mVersionFont.i
		mProgressFC.i
		mProgressBC.i
		mStatusFC.i
		mStatusBC.i
		mVersionFC.i
		mVersionBC.i
	EndStructure
	
	NewList *instances.sSplashWindow()

	CompilerIf #PB_Compiler_OS = #PB_OS_Windows
		Procedure HideFromTaskBar(hWnd, Flag)
			Protected TBL.ITaskbarList
			
			CoInitialize_(0)
			If CoCreateInstance_(?CLSID_TaskBarList, 0, 1, ?IID_ITaskBarList, @TBL) = #S_OK
				TBL\HrInit()
				If Flag
					TBL\DeleteTab(hWnd)
				Else
					TBL\AddTab(hWnd)
				EndIf
				TBL\Release()
			EndIf
			CoUninitialize_()
		EndProcedure		
		
		DataSection
			CLSID_TaskBarList:
				Data.l $56FDF344
				Data.w $FD6D, $11D0
				Data.b $95, $8A, $00, $60, $97, $C9, $A0, $90
			IID_ITaskBarList:
				Data.l $56FDF342
				Data.w $FD6D, $11D0
				Data.b $95, $8A, $00, $60, $97, $C9, $A0, $90
		EndDataSection
	CompilerEndIf
	
	Macro GenerateHandler(CLASS, EVENT)
		Procedure EVENT()
			Protected *this.s#CLASS
			*this = getThis(EventWindow())
			If *this : CLASS#_#EVENT(*this) : EndIf
		EndProcedure
	EndMacro
	
	Procedure.i GetThis(eventWin.i)
		Protected *this.sSplashWindow
		Shared *instances()
		
		*this = #Null
		
		ResetList(*instances())
		While NextElement(*instances())
			If *instances()\mUiWindow = eventWin
				*this = *instances()
				Break
			EndIf
  	Wend
  	
  	ProcedureReturn *this
	EndProcedure

	Procedure  UpdateImage(*this.sSplashWindow)
		Protected img.i, x.i
		
		img = CopyImage(*this\mImage, #PB_Any)
		If IsImage(img)
			StartDrawing(ImageOutput(img))
				DrawingMode(#PB_2DDrawing_AlphaBlend)
				If *this\mProgress = 0
					Box(*this\mProgressPos\x, *this\mProgressPos\y, *this\mProgressPos\w, *this\mProgressPos\h, *this\mProgressBC)
				ElseIf *this\mProgress = 100
					Box(*this\mProgressPos\x, *this\mProgressPos\y, *this\mProgressPos\w, *this\mProgressPos\h, *this\mProgressFC)
				Else
					If (*this\mProgressFlags & #PB_ProgressBar_Vertical) = #PB_ProgressBar_Vertical
						x = Int(*this\mProgressPos\h * *this\mProgress / 100)
						Box(*this\mProgressPos\x, *this\mProgressPos\y + x, *this\mProgressPos\w, *this\mProgressPos\h - x, *this\mProgressBC)
						Box(*this\mProgressPos\x, *this\mProgressPos\y, *this\mProgressPos\w, x, *this\mProgressFC)					
					Else				
						x = Int(*this\mProgressPos\w * *this\mProgress / 100)
						Box(*this\mProgressPos\x + x, *this\mProgressPos\y, *this\mProgressPos\w - x, *this\mProgressPos\h, *this\mProgressBC)
						Box(*this\mProgressPos\x, *this\mProgressPos\y, x, *this\mProgressPos\h, *this\mProgressFC)
					EndIf
				EndIf				
				
				If Alpha(*this\mVersionBC) > 0
					BackColor(*this\mVersionBC)
					DrawingMode(#PB_2DDrawing_AlphaBlend)
				Else
					DrawingMode(#PB_2DDrawing_Transparent)
				EndIf
				FrontColor(*this\mVersionFC)
				DrawingFont(FontID(*this\mVersionFont))
				If (*this\mVersionFlags & #PB_Text_Right) = #PB_Text_Right
					x = *this\mVersionPos\x + *this\mVersionPos\w - TextWidth(*this\mVersion)
				ElseIf (*this\mVersionFlags & #PB_Text_Center) = #PB_Text_Center
					x = *this\mVersionPos\x + ((*this\mVersionPos\w - TextWidth(*this\mVersion)) / 2)
				Else
					x = *this\mVersionPos\x
				EndIf
				DrawText(x, *this\mVersionPos\y, *this\mVersion)
				
				If Alpha(*this\mStatusBC) > 0
					BackColor(*this\mStatusBC)
					DrawingMode(#PB_2DDrawing_AlphaBlend)
				Else
					DrawingMode(#PB_2DDrawing_Transparent)
				EndIf
				FrontColor(*this\mStatusFC)
				DrawingFont(FontID(*this\mStatusFont))
				If (*this\mStatusFlags & #PB_Text_Right) = #PB_Text_Right
					x = *this\mStatusPos\x + *this\mStatusPos\w - TextWidth(*this\mStatus)
				ElseIf (*this\mStatusFlags & #PB_Text_Center) = #PB_Text_Center
					x = *this\mStatusPos\x + ((*this\mStatusPos\w - TextWidth(*this\mStatus)) / 2)
				Else
					x = *this\mStatusPos\x
				EndIf
				DrawText(x, *this\mStatusPos\y, *this\mStatus)
			StopDrawing()
				
 			StartDrawing(WindowOutput(*this\mUiWindow))
 				DrawImage(ImageID(img), 0, 0, *this\mWindowPos\w, *this\mWindowPos\h)
   	  StopDrawing()
  	  
  	  FreeImage(img)
		EndIf
	EndProcedure
	
	Procedure  SplashWindow_OnRepaintWindow(*this.sSplashWindow)
		UpdateImage(*this)
	EndProcedure
	
	GenerateHandler(SplashWindow, OnRepaintWindow)
	
	Procedure.i New()
		Protected *this.sSplashWindow
		Shared *instances()
		
		*this = AllocateStructure(sSplashWindow)
		If *this
			With *this
				\vt    	  	= ?VT
				\mUiWindow	= #Null
			EndWith
			
			AddElement(*instances())
			*instances() = *this
			
			ProcedureReturn *this
		EndIf	
		
		ProcedureReturn #Null
	EndProcedure
	Procedure   Create(*this.sSplashWindow)
		Protected flags.i
		
		*this\mVersionFont = LoadFont(#PB_Any, "Arial", Int(*this\mVersionPos\h * 0.8), #PB_Font_Italic)
		*this\mStatusFont  = LoadFont(#PB_Any, "Arial", Int(*this\mStatusPos\h * 0.8))
		
 		flags = #PB_Window_NoActivate
		If *this\mTitle = ""
			flags | #PB_Window_BorderLess
		Else
			flags | #PB_Window_Tool
		EndIf
		If (*this\mWindowPos\x = #PB_Ignore) And (*this\mWindowPos\y = #PB_Ignore)
			flags | #PB_Window_ScreenCentered
		EndIf
		
		*this\mUiWindow = OpenWindow(#PB_Any, *this\mWindowPos\x, *this\mWindowPos\y, *this\mWindowPos\w, *this\mWindowPos\h, *this\mTitle, flags)
		If IsWindow(*this\mUiWindow)
			CompilerIf #PB_Compiler_OS = #PB_OS_Windows
				If *this\mTitle = ""
					HideFromTaskBar(WindowID(*this\mUiWindow), #True)
				EndIf
			CompilerEndIf
			StickyWindow(*this\mUiWindow, #True)
			SmartWindowRefresh(*this\mUiWindow, #True)
			BindEvent(#PB_Event_Repaint, @OnRepaintWindow(), *this\mUiWindow)
		Else
			*this\mUiWindow = #Null
		EndIf
	EndProcedure
	
	Procedure   AWSplashWindow_Destroy(*this.sSplashWindow)	
		Shared *instances()
		
		ResetList(*instances())
		While NextElement(*instances())
			If *instances() = *this
				DeleteElement(*instances())
				Break
			EndIf
  	Wend
  	
  	If IsWindow(*this\mUiWindow)
			UnbindEvent(#PB_Event_Repaint, @OnRepaintWindow(), *this\mUiWindow)
			CloseWindow(*this\mUiWindow)
		EndIf
		
		If IsFont(*this\mStatusFont)
			FreeFont(*this\mStatusFont)
		EndIf
		
		If IsFont(*this\mVersionFont)
			FreeFont(*this\mVersionFont)
		EndIf
		
		FreeStructure(*this)
	EndProcedure
	Procedure   AWSplashWindow_Show(*this.sSplashWindow)
		If Not IsWindow(*this\mUiWindow)
			Create(*this)
			UpdateImage(*this)
		EndIf
		HideWindow(*this\mUiWindow, #False)
	EndProcedure
	Procedure   AWSplashWindow_Hide(*this.sSplashWindow)
		If IsWindow(*this\mUiWindow)
			HideWindow(*this\mUiWindow, #True)
		EndIf
	EndProcedure
	Procedure.i AWSplashWindow_GetWindowID(*this.sSplashWindow)
		ProcedureReturn *this\mUiWindow
	EndProcedure
	Procedure   AWSplashWindow_SetWindowSpecs(*this.sSplashWindow, left.i, top.i, width.i, height.i, title.s)
		*this\mWindowPos\x = left
		*this\mWindowPos\y = top
		*this\mWindowPos\w = width
		*this\mWindowPos\h = height
		*this\mTitle 			 = title
		If IsWindow(*this\mUiWindow)
			ResizeWindow(*this\mUiWindow, *this\mWindowPos\x, *this\mWindowPos\y, *this\mWindowPos\w, *this\mWindowPos\h)
			SetWindowTitle(*this\mUiWindow, *this\mTitle)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_SetVersionSpecs(*this.sSplashWindow, left.i, top.i, width.i, height.i, text.s, flags.i, frontcolor.i, backcolor.i)
		*this\mVersionPos\x = left
		*this\mVersionPos\y = top
		*this\mVersionPos\w = width
		*this\mVersionPos\h = height
		*this\mVersion      = text
		*this\mVersionFlags = flags
		*this\mVersionFC    = frontcolor
		*this\mVersionBC    = backcolor
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_SetProgressSpecs(*this.sSplashWindow, left.i, top.i, width.i, height.i, percent.i, flags.i, frontcolor.i, backcolor.i)
		*this\mProgressPos\x = left
		*this\mProgressPos\y = top
		*this\mProgressPos\w = width
		*this\mProgressPos\h = height
		*this\mProgress			 = percent
		*this\mProgressFlags = flags
		*this\mProgressFC    = frontcolor
		*this\mProgressBC    = backcolor
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_SetStatusSpecs(*this.sSplashWindow, left.i, top.i, width.i, height.i, text.s, flags.i, frontcolor.i, backcolor.i)
		*this\mStatusPos\x = left
		*this\mStatusPos\y = top
		*this\mStatusPos\w = width
		*this\mStatusPos\h = height
		*this\mStatus			 = text
		*this\mStatusFlags = flags
		*this\mStatusFC    = frontcolor
		*this\mStatusBC    = backcolor
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_UpdateStatus(*this.sSplashWindow, text.s)
		*this\mStatus	= text
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_UpdateProgress(*this.sSplashWindow, percent.i)
		*this\mProgress = percent
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure
	Procedure   AWSplashWindow_SetImage(*this.sSplashWindow, imageID.i)
		*this\mImage = imageID
		If IsWindow(*this\mUiWindow)
			UpdateImage(*this)
		EndIf
	EndProcedure

	
	DataSection ;{ Virtual Function Table
		VT:
			Data.i @AWSplashWindow_Destroy()
			Data.i @AWSplashWindow_Show()
			Data.i @AWSplashWindow_Hide()
			Data.i @AWSplashWindow_GetWindowID()
			Data.i @AWSplashWindow_SetWindowSpecs()
			Data.i @AWSplashWindow_SetVersionSpecs()
			Data.i @AWSplashWindow_SetProgressSpecs()
			Data.i @AWSplashWindow_SetStatusSpecs()
			Data.i @AWSplashWindow_SetImage()
			Data.i @AWSplashWindow_UpdateStatus()
			Data.i @AWSplashWindow_UpdateProgress()
			
		EndDataSection ;}
EndModule
; IDE Options = PureBasic 5.40 LTS (Linux - x64)
; CursorPosition = 189
; FirstLine = 147
; Folding = -----
; EnableUnicode
; EnableXP