;    awdynsqlite3.pbi
;    Version 0.4 [2014/11/07]
;    Copyright (C) 2012-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.

XIncludeFile "awinternal.pbi"

DeclareModule AWDynSQLite
  Structure SQLiteDyn_Handle
  	*db
  	*statement
  	ActiveQuery.w
  EndStructure
  
  Declare   Free_DynSQLite3()
  Declare.i Init_DynSQLite3(alternatepath.s = "")
  Declare.i DynSQLite_OpenDatabase(DatabaseName.s, User.s, Password.s)
  Declare   DynSQLite_CloseDatabase(*db.SQLiteDyn_Handle)
  Declare.i DynSQLite_IsDatabase(*db.SQLiteDyn_Handle) 
  Declare.i DynSQLite_DatabaseQuery(*db.SQLiteDyn_Handle, Request.s)
  Declare.i DynSQLite_FinishDatabaseQuery(*db.SQLiteDyn_Handle)
  Declare.i DynSQLite_DatabaseUpdate(*db.SQLiteDyn_Handle, Request.s)
  Declare.s DynSQLite_DatabaseError(*db.SQLiteDyn_Handle)
  Declare.i DynSQLite_DatabaseColumns(*db.SQLiteDyn_Handle)
  Declare.s DynSQLite_DatabaseColumnName(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_DatabaseColumnType(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_DatabaseColumnSize(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_NextDatabaseRow(*db.SQLiteDyn_Handle)
  Declare.d DynSQLite_GetDatabaseDouble(*db.SQLiteDyn_Handle, Column.i)
  Declare.f DynSQLite_GetDatabaseFloat(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_GetDatabaseLong(*db.SQLiteDyn_Handle, Column.i)
  Declare.q DynSQLite_GetDatabaseQuad(*db.SQLiteDyn_Handle, Column.i)
  Declare.s DynSQLite_GetDatabaseString(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_GetDatabaseBLOB(*db.SQLiteDyn_Handle, Column.i)
  Declare.i DynSQLite_Backup(*von.SQLiteDyn_Handle, *nach.SQLiteDyn_Handle)
  Declare.i DynSQLite_Restore(*von.SQLiteDyn_Handle, *nach.SQLiteDyn_Handle)
  Declare.s DynSQLite_LibVersion()
EndDeclareModule

Module AWDynSQLite
  EnableExplicit
  
  Enumeration
  	#SQLITE_OK = 0
  	#SQLITE_ERROR
  	#SQLITE_INTERNAL
  	#SQLITE_PERM
  	#SQLITE_ABORT
  	#SQLITE_BUSY
  	#SQLITE_LOCKED
  	#SQLITE_NOMEM
  	#SQLITE_READONLY
  	#SQLITE_INTERRUPT
  	#SQLITE_IOERR
  	#SQLITE_CORRUPT
  	#SQLITE_NOTFOUND
  	#SQLITE_FULL
  	#SQLITE_CANTOPEN
  	#SQLITE_PROTOCOL
  	#SQLITE_EMPTY
  	#SQLITE_SCHEMA
  	#SQLITE_TOOBIG
  	#SQLITE_CONSTRAINT
  	#SQLITE_MISMATCH
  	#SQLITE_MISUSE
  	#SQLITE_NOLFS
  	#SQLITE_AUTH
  	#SQLITE_FORMAT
  	#SQLITE_RANGE
  	#SQLITE_NOTADB
  	#SQLITE_NOTICE
  	#SQLITE_WARNING
  	#SQLITE_ROW = 100
  	#SQLITE_DONE
  EndEnumeration
  Enumeration
  	#SQLITE_INTEGER = 1
  	#SQLITE_FLOAT
  	#SQLITE_TEXT
  	#SQLITE_BLOB
  	#SQLITE_NULL
  EndEnumeration
  
  PrototypeC.i Proto_sqlite3_open(filename.p-utf8, *db)
  PrototypeC   Proto_sqlite3_close(*db)
  PrototypeC.i Proto_sqlite3_errmsg(*db)		; returns utf8 string
  PrototypeC.i Proto_sqlite3_errcode(*db)
  PrototypeC   Proto_sqlite3_libversion()
  PrototypeC   Proto_sqlite3_prepare_v2(*db, sql.p-utf8, sqllen.i, *statementadr, *sqltail)
  PrototypeC   Proto_sqlite3_finalize(*statement)
  PrototypeC   Proto_sqlite3_step(*statement)
  PrototypeC   Proto_sqlite3_exec(*db, sql.p-utf8, *callback, cbdata.i, *errmsg)
  PrototypeC   Proto_sqlite3_backup_init(*pDest, zDestName.p-ascii, *pSource, zSourceName.p-ascii)
  PrototypeC   Proto_sqlite3_backup_step(*sqlite3_backup, nPage)
  PrototypeC   Proto_sqlite3_backup_finish(*sqlite3_backup)
  PrototypeC.i Proto_sqlite3_column_type(*statement, column.i)
  PrototypeC.i Proto_sqlite3_column_name(*statement, column.i)
  PrototypeC.i Proto_sqlite3_column_count(*statement)
  PrototypeC.i Proto_sqlite3_column_bytes(*statement, column.i)
  PrototypeC.i Proto_sqlite3_column_int(*statement, column.i)
  PrototypeC.q Proto_sqlite3_column_int64(*statement, column.i)
  PrototypeC.d Proto_sqlite3_column_double(*statement, column.i)
  PrototypeC.i Proto_sqlite3_column_text(*statement, column.i)
  PrototypeC.i Proto_sqlite3_column_blob(*statement, column.i)
  
  Structure SQLiteDyn_lib_table
  	LibHandle.i
  	SQLite3_Open.Proto_sqlite3_open
  	SQLite3_Close.Proto_sqlite3_close
  	SQLite3_ErrMsg.Proto_sqlite3_errmsg
  	SQLite3_ErrCode.Proto_sqlite3_errcode
  	SQLite3_LibVersion.Proto_sqlite3_libversion
  	SQLite3_Prepare_V2.Proto_sqlite3_prepare_v2
  	SQLite3_Finalize.Proto_sqlite3_finalize
  	SQLite3_Step.Proto_sqlite3_step
  	SQLite3_Exec.Proto_sqlite3_exec
  	SQLite3_Backup_Init.Proto_sqlite3_backup_init
  	SQLite3_Backup_Step.Proto_sqlite3_backup_step
  	SQLite3_Backup_Finish.Proto_sqlite3_backup_finish
  	SQLite3_Column_Type.Proto_sqlite3_column_type
  	SQLite3_Column_Name.Proto_sqlite3_column_name
  	SQLite3_Column_Count.Proto_sqlite3_column_count
  	SQLite3_Column_Bytes.Proto_sqlite3_column_bytes
  	SQLite3_Column_Int.Proto_sqlite3_column_int
  	SQLite3_Column_Int64.Proto_sqlite3_column_int64
  	SQLite3_Column_Double.Proto_sqlite3_column_double
  	SQLite3_Column_Text.Proto_sqlite3_column_text
  	SQLite3_Column_BLOB.Proto_sqlite3_column_blob
  EndStructure
  
  Global SQLiteDyn_lib.SQLiteDyn_lib_table
  
  Procedure.i DynSQLite_TestLibStructure(*ptr, entries.i)
  	Protected res.i, a.i, *p
  	
  	res = #True
  	*p  = *ptr
  	For a = 1 To entries
  		If PeekI(*p) = #Null
  			res = #False
  			Break
  		EndIf
  		*p + SizeOf(a)
  	Next
  	
  	ProcedureReturn res
  EndProcedure
  Procedure   Free_DynSQLite3()
    If IsLibrary(SQLiteDyn_lib\LibHandle) : CloseLibrary(SQLiteDyn_lib\LibHandle) : EndIf
  
    ClearStructure(@SQLiteDyn_lib, SQLiteDyn_lib_table)
  EndProcedure
  Procedure.i Init_DynSQLite3(alternatepath.s = "")
    ClearStructure(@SQLiteDyn_lib, SQLiteDyn_lib_table)
    
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
      	SQLiteDyn_lib\LibHandle = AWInternal::OpenSharedLibrary("sqlite3.dll", alternatepath)
      CompilerCase #PB_OS_Linux
      	SQLiteDyn_lib\LibHandle = AWInternal::OpenSharedLibrary("libsqlite3.so", alternatepath)
    CompilerEndSelect
  
    If IsLibrary(SQLiteDyn_lib\LibHandle)
    	With SQLiteDyn_lib
    		\SQLite3_Open     					= GetFunction(\LibHandle, "sqlite3_open")
  			\SQLite3_Close     					= GetFunction(\LibHandle, "sqlite3_close")
  			\SQLite3_ErrMsg    					= GetFunction(\LibHandle, "sqlite3_errmsg")
  			\SQLite3_ErrCode    				= GetFunction(\LibHandle, "sqlite3_errcode")
  			\SQLite3_LibVersion     		= GetFunction(\LibHandle, "sqlite3_libversion")
  			\SQLite3_Prepare_V2  				= GetFunction(\LibHandle, "sqlite3_prepare_v2")
  			\SQLite3_Finalize     			= GetFunction(\LibHandle, "sqlite3_finalize")
  			\SQLite3_Step     					= GetFunction(\LibHandle, "sqlite3_step")
  			\SQLite3_Exec     					= GetFunction(\LibHandle, "sqlite3_exec")
  			\SQLite3_Backup_Init     		= GetFunction(\LibHandle, "sqlite3_backup_init")
  			\SQLite3_Backup_Step     		= GetFunction(\LibHandle, "sqlite3_backup_step")
  			\SQLite3_Backup_Finish     	= GetFunction(\LibHandle, "sqlite3_backup_finish")
  			\SQLite3_Column_Type     		= GetFunction(\LibHandle, "sqlite3_column_type")
  			\SQLite3_Column_Name     		= GetFunction(\LibHandle, "sqlite3_column_name")
  			\SQLite3_Column_Count    		= GetFunction(\LibHandle, "sqlite3_column_count")
  			\SQLite3_Column_Bytes     	= GetFunction(\LibHandle, "sqlite3_column_bytes")
  			\SQLite3_Column_Int     		= GetFunction(\LibHandle, "sqlite3_column_int")
  			\SQLite3_Column_Int64     	= GetFunction(\LibHandle, "sqlite3_column_int64")
  			\SQLite3_Column_Double     	= GetFunction(\LibHandle, "sqlite3_column_double")
  			\SQLite3_Column_Text     		= GetFunction(\LibHandle, "sqlite3_column_text")
  			\SQLite3_Column_BLOB     		= GetFunction(\LibHandle, "sqlite3_column_blob")
  		EndWith
  		
      If DynSQLite_TestLibStructure(@SQLiteDyn_lib, SizeOf(SQLiteDyn_lib) / SizeOf(SQLiteDyn_lib\LibHandle))
  			ProcedureReturn #True
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i DynSQLite_OpenDatabase(DatabaseName.s, User.s, Password.s)
  	Protected *db.SQLiteDyn_Handle, dbh.i
  
  	*db = AllocateMemory(SizeOf(SQLiteDyn_Handle))
  	If *db <> #Null
  		*db\ActiveQuery = #False
  		*db\statement   = #Null
  		If SQLiteDyn_lib\SQLite3_Open(DatabaseName, @dbh) = #SQLITE_OK
  			*db\db = dbh
  			ProcedureReturn *db
  		EndIf
  		FreeMemory(*db)
  	EndIf
  
  	ProcedureReturn #Null
  EndProcedure
  Procedure   DynSQLite_CloseDatabase(*db.SQLiteDyn_Handle)
  	If *db
  		If *db\db
  			If SQLiteDyn_lib\SQLite3_Close(*db\db) = #SQLITE_OK
  				FreeMemory(*db)
  
  				ProcedureReturn #True
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i DynSQLite_IsDatabase(*db.SQLiteDyn_Handle) 
  	If *db <> #Null
  		If *db\db <> #Null
  			ProcedureReturn #True
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i DynSQLite_DatabaseQuery(*db.SQLiteDyn_Handle, Request.s)
  	Protected stmt.i
  
  	If *db
  		If *db\db
  			If SQLiteDyn_lib\SQLite3_Prepare_V2(*db\db, request, -1, @stmt, #Null) = #SQLITE_OK
  				*db\statement 	= stmt
  				*db\ActiveQuery = #True
  
  				ProcedureReturn #True
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i DynSQLite_FinishDatabaseQuery(*db.SQLiteDyn_Handle)
  	If *db
  		If *db\db
  			*db\ActiveQuery = #False
  			If SQLiteDyn_lib\SQLite3_Finalize(*db\statement) = #SQLITE_OK
  				*db\statement = #Null
  
  				ProcedureReturn #True
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i DynSQLite_DatabaseUpdate(*db.SQLiteDyn_Handle, Request.s)
  	If *db
  		If *db\db
  			If SQLiteDyn_lib\SQLite3_Exec(*db\db, Request, #Null, #Null, #Null) = #SQLITE_OK
  				ProcedureReturn #True
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.s DynSQLite_DatabaseError(*db.SQLiteDyn_Handle)
  	If *db
  		If *db\db
  			ProcedureReturn PeekS(SQLiteDyn_lib\SQLite3_ErrMsg(*db\db), #PB_Any, #PB_UTF8)
  		EndIf
  	EndIf
  
  	ProcedureReturn "Database handle is missing"
  EndProcedure
  Procedure.i DynSQLite_DatabaseColumns(*db.SQLiteDyn_Handle)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Count(*db\statement)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.s DynSQLite_DatabaseColumnName(*db.SQLiteDyn_Handle, Column.i)
  	Protected *res
  
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					*res = SQLiteDyn_lib\SQLite3_Column_Name(*db\statement, Column)
  					If *res <> #Null
  						ProcedureReturn PeekS(*res, #PB_Any, #PB_UTF8)
  					EndIf
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #Null$
  EndProcedure
  Procedure.i DynSQLite_DatabaseColumnType(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Type(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #SQLITE_NULL
  EndProcedure
  Procedure.i DynSQLite_DatabaseColumnSize(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Bytes(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.i DynSQLite_NextDatabaseRow(*db.SQLiteDyn_Handle)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					If SQLiteDyn_lib\SQLite3_Step(*db\statement) = #SQLITE_ROW
  						ProcedureReturn #True
  					EndIf
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.d DynSQLite_GetDatabaseDouble(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Double(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.f DynSQLite_GetDatabaseFloat(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Double(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.i DynSQLite_GetDatabaseLong(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Int(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.q DynSQLite_GetDatabaseQuad(*db.SQLiteDyn_Handle, Column.i)
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					ProcedureReturn SQLiteDyn_lib\SQLite3_Column_Int64(*db\statement, Column)
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn 0
  EndProcedure
  Procedure.s DynSQLite_GetDatabaseString(*db.SQLiteDyn_Handle, Column.i)
  	Protected *res
  
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					*res = SQLiteDyn_lib\SQLite3_Column_Text(*db\statement, Column)
  					If *res
  						ProcedureReturn PeekS(*res, #PB_Any, #PB_UTF8)
  					EndIf
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #Null$
  EndProcedure
  Procedure.i DynSQLite_GetDatabaseBLOB(*db.SQLiteDyn_Handle, Column.i)
  	Protected *res, bytes.i, *buf
  
  	If *db
  		If *db\db
  			If *db\ActiveQuery
  				If *db\statement
  					*res = SQLiteDyn_lib\SQLite3_Column_Text(*db\statement, Column)
  					If *res
  						bytes = SQLiteDyn_lib\SQLite3_Column_Bytes(*db\statement, Column)
  						If bytes > 0
  							*buf = AllocateMemory(bytes)
  							If *buf
  								CopyMemory(*res, *buf, bytes)
  								ProcedureReturn *buf
  							EndIf
  						EndIf
  					EndIf
  				EndIf
  			EndIf
  		EndIf
  	EndIf
  
  	ProcedureReturn #Null
  EndProcedure
	Procedure.i DynSQLite_Backup(*von.SQLiteDyn_Handle, *nach.SQLiteDyn_Handle)
	  Protected *pbackup
	  
	  *pbackup = SQLiteDyn_lib\SQLite3_Backup_Init(*von\db, "main", *nach\db, "main")
		If *pbackup
			SQLiteDyn_lib\SQLite3_Backup_Step(*pbackup, -1)
			SQLiteDyn_lib\SQLite3_Backup_Finish(*pbackup)
	  EndIf
	  ProcedureReturn SQLiteDyn_lib\SQLite3_ErrCode(*von\db)
	EndProcedure
	Procedure.i DynSQLite_Restore(*von.SQLiteDyn_Handle, *nach.SQLiteDyn_Handle)
	  Protected *pbackup
	  
	  *pbackup = SQLiteDyn_lib\SQLite3_Backup_Init(*von\db, "main", *nach\db, "main")
		If *pbackup
			SQLiteDyn_lib\SQLite3_Backup_Step(*pbackup, -1)
			SQLiteDyn_lib\SQLite3_Backup_Finish(*pbackup)
	  EndIf
	  ProcedureReturn SQLiteDyn_lib\SQLite3_ErrCode(*nach\db)
	EndProcedure
  Procedure.s DynSQLite_LibVersion()
    ProcedureReturn PeekS(SQLiteDyn_lib\SQLite3_LibVersion(), #PB_Any, #PB_Ascii)
  EndProcedure
  
EndModule



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 20
; Folding = ------
; EnableUnicode
; EnableXP