;{   awzip.pbi
;    Version 0.3 [2015/10/26]
;    Copyright (C) 2011-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWZip
  #COMP_0          = 0                               ;* keine Kompression
  #COMP_1          = 1                               ;* Kompressionsstufe 1
  #COMP_2          = 2                               ;* Kompressionsstufe 2
  #COMP_3          = 3                               ;* Kompressionsstufe 3
  #COMP_4          = 4                               ;* Kompressionsstufe 4
  #COMP_5          = 5                               ;* Kompressionsstufe 5
  #COMP_6          = 6                               ;* Kompressionsstufe 6
  #COMP_7          = 7                               ;* Kompressionsstufe 7
  #COMP_8          = 8                               ;* Kompressionsstufe 8
  #COMP_9          = 9                               ;* Kompressionsstufe 9
  #COMP_STORE      = #COMP_0                ;* keine Kompression, entspricht #COMP_0 
  #COMP_NONE       = #COMP_0                ;* keine Kompression, entspricht #COMP_0 
  #COMP_FAST       = #COMP_1                ;* schnellste Kompression, entspricht #COMP_1
  #COMP_NORMAL     = #COMP_5                ;* normale Kompression, entspricht #COMP_5
  #COMP_MAX        = #COMP_9                ;* h�chste Kompression, entspricht #COMP_9
  
  Declare.i PackMemory(*source, sourceLen.i, *destLen, level.i = #COMP_MAX)
  Declare.i UnpackMemory(*source, sourceLen.i, *destLen)
  Declare   FreeBuffer(*blockadr)
EndDeclareModule

Module AWZip
  EnableExplicit
  
  CompilerSelect #PB_Compiler_OS
    CompilerCase #PB_OS_Linux
      ImportC #PB_Compiler_Home + "purelibraries/linux/libraries/zlib.a"
    CompilerCase #PB_OS_MacOS
      ImportC "/usr/lib/libz.dylib"
    CompilerCase #PB_OS_Windows
      ImportC "zlib.lib"
    CompilerEndSelect
    compress2(*dest, *destLen, *source, sourceLen, level)
    uncompress(*dest, *destLen, *source, sourceLen)
  EndImport
  
  Procedure.i PackMemory(*source, sourceLen.i, *destLen, level.i = #COMP_MAX)
    Protected *dest, destLen.i, c2.i, *buf
    
    *buf = #Null
    If level < #COMP_NONE : level = #COMP_NONE : EndIf
    If level > #COMP_MAX  : level = #COMP_MAX  : EndIf
    If *source And sourceLen And *destLen
      destLen = sourceLen + 13 + (Int(sourceLen / 90)) + 8      ; Gr��e des Zielpuffers berechnen (ca)
      *dest   = AllocateMemory(destLen)
      If *dest
        *buf = *dest
        PokeL(*dest, $505a5741) : *dest + 4 ; "AWZP" Header schreiben (kann irgendein 32-Bit-Wert sein)
        PokeL(*dest, sourceLen) : *dest + 4 ; Originall�nge Length (32 Bit) 
        c2 = compress2(*dest, @destLen, *source, sourceLen, level)
        If c2
          FreeMemory(*buf)
          destLen = 0
          *buf   = #Null
        EndIf
      EndIf
    EndIf
    If *buf
      PokeI(*destLen, destLen + 8)
    Else
      PokeI(*destLen, 0)
    EndIf  
      
    ProcedureReturn *buf
  EndProcedure
  Procedure.i UnpackMemory(*source, sourceLen.i, *destLen)
    Protected *dest, dlen.i
  
    *dest = #Null
    If *source And sourceLen
      If PeekL(*source) = $505a5741   ; "AWZP"  Header auslesen und �berpr�fen; wenn nicht der selbe Wert, dann wird kein entpacken versucht
        *source   + 4
        dLen = PeekL(*source)         ; Originall�nge auslesen (32 Bit)
        *source   + 4
        sourceLen - 8
        *dest = AllocateMemory(dlen)
        If *dest
          If uncompress(*dest, @dlen, *source, sourceLen)
            FreeMemory(*dest)
            *dest = #Null
          EndIf
        EndIf
      EndIf   
    EndIf
    If *dest
      PokeI(*destLen, dlen)
    Else
      PokeI(*destLen, 0)
    EndIf
    
    ProcedureReturn *dest
  EndProcedure
  Procedure   FreeBuffer(*blockadr)
    If *blockadr
      FreeMemory(*blockadr)
    EndIf
  EndProcedure
EndModule



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 2
; Folding = --
; EnableUnicode
; EnableXP