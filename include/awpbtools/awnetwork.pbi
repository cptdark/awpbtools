;{   awnetwork.pbi
;    Version 0.8 [2015/10/26]
;    Copyright (C) 2013-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
  XIncludeFile "awopenssl.pbi"
CompilerEndIf

XIncludeFile "awsupport.pbi"

DeclareModule AWNetwork
  Enumeration 0
    #ERR_NONE
    #ERR_LOGIN_FAILED
    #ERR_SEND_FAIL
    #ERR_RECV_DATA
    #ERR_TIMEOUT
    #ERR_NO_DATA
    #ERR_ERR
    #ERR_NO_CONNECTION
    #ERR_OUT_OF_MEMORY
    #ERR_UNKNOWN_PROTOCOL
    #ERR_NO
    #ERR_BYE
  EndEnumeration
  CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
    #SSL_ERR_NONE				          = #ERR_NONE
    #SSL_ERR_UNKNOWN_METHOD	      = -1
    #SSL_ERR_CTX_NEW_FAIL		      = -2
    #SSL_ERR_SSL_NEW_FAIL		      = -3
    #SSL_ERR_SET_FD_FAIL		      = -4
    #SSL_ERR_CONNECT_FAIL		      = -5
    #SSL_ERR_WRITE                = -6
    #SSL_ERR_METHOD_NOT_AVAILABLE = -7
    
    Enumeration 1
    	#SSL_METHOD_V2
    	#SSL_METHOD_V3
    	#SSL_METHOD_V23
    	#SSL_METHOD_TLSV1
    	#SSL_METHOD_TLSV1_1
    	#SSL_METHOD_TLSV1_2
    EndEnumeration
  CompilerEndIf
  Enumeration 0
    #SSL_NONE
    #SSL_TRY_STARTTLS
    #SSL_FORCE_STARTTLS
    #SSL_FULL
  EndEnumeration
  Enumeration 0
    #PROXY_NONE
    #PROXY_SOCKS4
    #PROXY_SOCKS5
  EndEnumeration
 	#SSL_METHOD_NONE = 0
 	#CRLF            = Chr(13) + Chr(10)
 	#ERR_OK          = #ERR_NONE
 	#ERR_BAD         = #ERR_ERR
 	
  Enumeration
    #LE_POP3_SL
    #LE_POP3_ML
    #LE_SMTP
    #LE_IMAP
    #LE_IMAP_DT
  EndEnumeration

  Structure ConnInfo
    Host.s
    Port.i
    User.s
    Pass.s
    TimeOut.i
    SSLMode.i
    ProxyHost.s
    ProxyPort.i
    ProxyUser.s
    ProxyPass.s
    ProxyMode.i
    Protocol.i
    StructureUnion
      IMAP_Seq.i
    EndStructureUnion
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      SSLMethod.i
    CompilerEndIf
  EndStructure
  
  Enumeration
    #PR_NONE
    #PR_POP3
    #PR_IMAP
    #PR_SMTP
  EndEnumeration

  Interface Connection
    Destroy                 ()
    GetConnInfo.i           ()
    EnableLog               (enabled.i)
    ClearLog                ()
    GetLog.s                ()
    Connect.i               ()
    DisConnect              ()
    Connected.i             ()
    SendNetworkData.i       (*buf, bufsize.i)
    SendNetworkString.i     (text.s, nl.i = #False)
    ResetReceiveBuffer      ()
    GetReceivedString.s     ()
    GetReceivedDataSize.i   ()
    GetReceivedData.i       ()
    ReceiveNetworkData.i    ()
    GetResultCode.i         ()
    GetLastError.s          ()
    ClearLastError          ()
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      UpgradeSSL.i          ()
    	DowngradeSSL.i        ()
    CompilerEndIf
    ReadResult              (CheckEndMode.i)
    Execute.i               (cmd.s, CheckEndMode.i, retries.i = 1)
  EndInterface
  
  Declare.i New()
  
  Define initialized.i
EndDeclareModule
 
Module AWNetwork
  EnableExplicit
  
  #BUF_SIZE = 20480
  
  Global FCiphers.q
  
  Structure sConnection
    *vt.Connection
    *Buffer
    *Received
    ReceivedSize.i
    ConnID.i
    ResultCode.i
    LogEnabled.i
    LogData.s
    LastErr.s
    Info.ConnInfo
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
    	TLSactive.i
    	sslHandle.i		; SSL*
    	sslContext.i  ; SSL_CTX*
    	sslMethod.i
    	socket.i
    	lastErrSSL.l
  	CompilerEndIf
  EndStructure
  
  #SOCKS5_Method_NoAuthenticationRequired = 0
  #SOCKS5_Method_UserNamePassword         = 2
  
  #SOCKS5_Version = 5
  #SOCKS4_Version = 4
  
  #SOCKS_AUTH_Ok      = #ERR_NONE
  #SOCKS_AUTH_Failure = #ERR_LOGIN_FAILED
  
  #SOCKS_CMD_Connect  = 1
  
  #SOCKS_ATYP_DomainName = 3
  
  #SOCKS_REP_Succeeded = 0
  
  Structure SOCKS4Info
    ver.a
    rsv.a
    port.w
    ip.l
  EndStructure
  Structure SOCKS5Info
    ver.a
    method.a
    methods.a[255]
  EndStructure
  Structure SOCKS5Cmd
    ver.a
    cmd.a
    rsv.a
    atyp.a
  EndStructure
  Structure SOCKS5Reply
    ver.a
    rep.a
    rsv.a
    atyp.a
    addr.l
    port.w
  EndStructure
  
  Macro PortOct(X)
    Int(X / 256) + (Int(Mod(X, 256)) << 8)
  EndMacro
  
  Procedure.i New()
    Protected *this.sConnection
  
    *this = AllocateStructure(sConnection)
    If *this
      *this\vt               = ?Connection_VT
      *this\Received         = #Null
      *this\ReceivedSize     = 0
      *this\LastErr          = ""
      *this\Info\TimeOut     = 15000
      *this\info\Protocol    = #PR_NONE
      *this\info\IMAP_Seq    = 0
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        *this\socket         = #Null
        *this\TLSactive      = #False
        *this\sslContext     = #Null
        *this\sslHandle      = #Null
        *this\lastErrSSL	   = #SSL_ERR_NONE
        *this\info\SSLMethod = #SSL_METHOD_NONE
        *this\info\SSLMode   = #SSL_NONE
      CompilerEndIf
      *this\Buffer           = AllocateMemory(#BUF_SIZE)
      If *this\Buffer <> #Null
        ProcedureReturn *this
      Else
        FreeStructure(*this)
      EndIf
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure   EnableLog(*this.sConnection, enabled.i)
    *this\LogEnabled = enabled
  EndProcedure
  Procedure   ClearLog(*this.sConnection)
    *this\LogData = ""
  EndProcedure
  Procedure.s GetLog(*this.sConnection)
    ProcedureReturn *this\LogData
  EndProcedure
  Procedure   AddLog(*this.sConnection, text.s)
	  If *this\LogEnabled
	    *this\LogData + text + #CRLF
  	EndIf
Debug text
  EndProcedure
  Procedure   AddLogErr(*this.sConnection, text.s, errcode.i)
  	If *this\LogEnabled
      *this\LogData + text + " (Error Code: " + Str(errcode) + ")" + #CRLF
  	EndIf
Debug text + " (Error Code: " + Str(errcode) + ")"
  EndProcedure  
  Procedure.i GetConnInfo(*this.sConnection)
    ProcedureReturn @*this\Info
  EndProcedure
  Procedure.i ReceiveProxyData(*this.sConnection, *buf, bufsize.i)
    Protected timeout.i, res.i
    
    res     = -1
    timeout = ElapsedMilliseconds() + *this\Info\TimeOut
    Repeat
      Delay(5)
      If NetworkClientEvent(*this\ConnID) = #PB_NetworkEvent_Data
        res = ReceiveNetworkData(*this\ConnID, *buf, bufsize)
        Break
      EndIf
    Until ElapsedMilliseconds() >= timeout
    
    ProcedureReturn res
  EndProcedure
  Procedure.i ProxySOCKS4(*this.sConnection)
    Protected *cmd.SOCKS4Info, cmdsize.i, res.i
    
    res = #ERR_NO_CONNECTION
    If AWSupport::IsIP(*this\Info\Host) = AWSupport::#IP_V4
      cmdsize = SizeOf(SOCKS4Info) + Len(*this\Info\ProxyUser) + SizeOf(Byte)
      *cmd    = AllocateMemory(cmdsize)
      If *cmd <> #Null
        *this\ConnID = OpenNetworkConnection(*this\Info\ProxyHost, *this\Info\ProxyPort, #PB_Network_TCP | #PB_Network_IPv4, *this\Info\TimeOut)
        If *this\ConnID <> #Null
          *cmd\ver  = #SOCKS4_Version
          *cmd\rsv  = 1
          *cmd\port = PortOct(*this\Info\Port)
          *cmd\ip   = MakeIPAddress(Val(StringField(*this\Info\Host, 1, ".")), Val(StringField(*this\Info\Host, 2, ".")), Val(StringField(*this\Info\Host, 3, ".")), Val(StringField(*this\Info\Host, 4, ".")))
          PokeS(*Cmd + SizeOf(SOCKS4Info), *this\Info\ProxyUser, #PB_Ascii)
          
          If cmdsize = SendNetworkData(*this\ConnID, *cmd, cmdsize)
            res = ReceiveProxyData(*this, *cmd, cmdsize)
            If (res > 0) And (*cmd\ver = #Null) And (*cmd\rsv = $5a)
              res = #ERR_NONE
            Else
              res = #ERR_NO_CONNECTION
            EndIf
          EndIf
          
          If Not res
            CloseNetworkConnection(*this\ConnID)
            *this\ConnID = #Null
          EndIf
        EndIf
      Else
        res = #ERR_OUT_OF_MEMORY
        
        FreeMemory(*cmd)
      EndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i ProxySOCKS5(*this.sConnection)
    Protected res.i, cmdsize.i, *cmd.SOCKS5Cmd, info.SOCKS5Info, rep.SOCKS5Reply, timeout.i, *cmd2.SOCKS5Cmd
    
    res     = #ERR_NO_CONNECTION
    cmdsize = Len(*this\Info\ProxyUser) + Len(*this\Info\ProxyPass) + SizeOf(SOCKS5Info)
    *cmd    = AllocateMemory(cmdsize)
    If *cmd <> #Null
      info\Ver = #SOCKS5_Version
      If *this\Info\ProxyUser = ""
        info\Method     = 1
        info\Methods[0] = #SOCKS5_Method_NoAuthenticationRequired
      Else
        info\Method     = 2
        info\Methods[0] = #SOCKS5_Method_NoAuthenticationRequired
        info\Methods[1] = #SOCKS5_Method_UserNamePassword
      EndIf
      
      *this\ConnID = OpenNetworkConnection(*this\Info\ProxyHost, *this\Info\ProxyPort, #PB_Network_TCP | #PB_Network_IPv4, *this\Info\TimeOut)
      If *this\ConnID <> #Null
        SendNetworkData(*this\ConnID, @info, SizeOf(Word) + info\method * SizeOf(byte))
        res = ReceiveProxyData(*this, @info, SizeOf(Word))
        
        If (res > 0) Or (info\ver <> #SOCKS5_Version)
          Select info\method
            Case #SOCKS5_Method_NoAuthenticationRequired
              info\method = #SOCKS_AUTH_Ok
            Case #SOCKS5_Method_UserNamePassword
              *cmd\ver = #SOCKS5_Version
              *cmd\cmd = Len(*this\Info\ProxyUser)
              PokeS(*Cmd + 2 * SizeOf(Byte), *this\Info\ProxyUser, Len(*this\Info\ProxyUser), #PB_Ascii | #PB_String_NoZero)
              PokeA(*cmd + 2 * SizeOf(Byte) + Len(*this\Info\ProxyUser), Len(*this\Info\ProxyPass))
              PokeS(*cmd + 2 * SizeOf(Byte) + Len(*this\Info\ProxyUser), *this\Info\ProxyPass, Len(*this\Info\ProxyPass), #PB_Ascii | #PB_String_NoZero)
              
              SendNetworkData(*this\ConnID, *cmd, cmdsize)
              res = ReceiveProxyData(*this, @info, SizeOf(SOCKS5Info))
              If (res > 0) And (info\ver = #SOCKS5_Version)
                info\method = #SOCKS_AUTH_FAILURE
              EndIf
          EndSelect
          
          If info\method = #SOCKS_AUTH_Ok
            cmdsize = SizeOf(SOCKS5Cmd) + 3 * SizeOf(Byte) + Len(*this\info\Host)
            *cmd2 = ReAllocateMemory(*cmd, cmdsize)
            If *cmd2 <> #Null
              *cmd      = *cmd2
              *cmd\ver  = #SOCKS5_Version
              *cmd\cmd  = #SOCKS_CMD_Connect
              *cmd\rsv  = #Null
              *cmd\atyp = #SOCKS_ATYP_DomainName
              PokeA(*cmd + SizeOf(SOCKS5Cmd), Len(*this\Info\Host))
              PokeS(*cmd + SizeOf(SOCKS5Cmd) + SizeOf(Byte), *this\Info\Host, Len(*this\Info\Host), #PB_Ascii | #PB_String_NoZero)
              PokeW(*cmd + SizeOf(SOCKS5Cmd) + SizeOf(Byte) + Len(*this\Info\Host), PortOct(*this\Info\Port))
              
              SendNetworkData(*this\ConnID, *cmd, cmdsize)
              res = ReceiveProxyData(*this, @rep, SizeOf(SOCKS5Reply))
              If (res > 0) And (rep\ver = #SOCKS5_Version)
                If rep\rep = #SOCKS_REP_Succeeded
                  res = #ERR_NONE
                EndIf
              EndIf
            EndIf
          EndIf
        EndIf
        
        If res <> #ERR_NONE
          CloseNetworkConnection(*this\ConnID)
          *this\ConnID = #Null
        EndIf
      EndIf
      FreeMemory(*cmd)
    EndIf
    
    ProcedureReturn res
  EndProcedure    
  Procedure.i ProxyConnect(*this.sConnection)
    Select *this\info\ProxyMode
      Case #PROXY_SOCKS4
        ProcedureReturn ProxySOCKS4(*this)
      Case #PROXY_SOCKS5
        ProcedureReturn ProxySOCKS5(*this)
    EndSelect
    
    ProcedureReturn #ERR_NO_CONNECTION
  EndProcedure
  CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
  	Procedure.i DowngradeSSL(*this.sConnection)
  		Protected res.i, erg.i
  
  		erg = #SSL_ERR_NONE
  		If *this\sslHandle <> #Null
  			res = AWOpenSSL::SSL_shutdown(*this\sslHandle)
  			If res < 0
  			  erg = AWOpenSSL::SSL_get_error(*this\sslHandle, res)
  			  *this\LastErr = AWOpenSSL::ERR_error_string(erg)
  			  AddLogErr(*this, "SSL: Unable to shutdown SSL", erg)
  			EndIf
  			AWOpenSSL::SSL_free(*this\sslHandle)
  		EndIf
  
  		If *this\sslContext <> #Null
  			AWOpenSSL::SSL_CTX_free(*this\sslContext)
  		EndIf
    	*this\TLSactive  = #False
    	*this\sslHandle  = #Null
    	*this\sslContext = #Null
    	*this\socket     = #Null
    	
    	*this\lastErrSSL = erg
    	
    	ProcedureReturn erg
    EndProcedure
    Procedure.i UpgradeSSL(*this.sConnection)
      Protected res.i
      
      *this\socket     = ConnectionID(*this\ConnID)
      *this\TLSactive  = #False
      *this\sslContext = #Null
      *this\sslHandle  = #Null
      *this\sslMethod  = #Null
      
      Select *this\Info\SSLMethod
        Case #SSL_METHOD_TLSV1:   *this\sslMethod = AWOpenSSL::TLSv1_client_method()
        Case #SSL_METHOD_TLSV1_1: *this\sslMethod = AWOpenSSL::TLSv1_1_client_method()
        Case #SSL_METHOD_TLSV1_2: *this\sslMethod = AWOpenSSL::TLSv1_2_client_method()
        Case #SSL_METHOD_V2:      *this\sslMethod = AWOpenSSL::SSLv2_client_method()
        Case #SSL_METHOD_V3:      *this\sslMethod = AWOpenSSL::SSLv3_client_method()
        Case #SSL_METHOD_V23:     *this\sslMethod = AWOpenSSL::SSLv23_client_method()
        Default:
          AddLog(*this, "SSL: Unknown SSL Method requested")
          *this\LastErr    = "Unknown SSL Method requested"
          *this\lastErrSSL = #SSL_ERR_UNKNOWN_METHOD
          ProcedureReturn #SSL_ERR_UNKNOWN_METHOD
      EndSelect
      
      If *this\sslMethod = #Null
        AddLog(*this, "SSL: SSL Method available")
        *this\LastErr    = "SSL Method not available"
        *this\lastErrSSL = #SSL_ERR_METHOD_NOT_AVAILABLE
        ProcedureReturn #SSL_ERR_METHOD_NOT_AVAILABLE
      EndIf
      
      *this\sslContext = AWOpenSSL::SSL_CTX_new(*this\sslMethod)
		  If *this\sslContext = #Null
			  AddLog(*this, "SSL: Unable To create SSL Context")
			  DowngradeSSL(*this)
        *this\LastErr = "Unable To create SSL Context"
			  *this\lastErrSSL = #SSL_ERR_CTX_NEW_FAIL
			  ProcedureReturn #SSL_ERR_CTX_NEW_FAIL
		  EndIf
		  
		  ;- TODO check for 32 bit
		  FCiphers = $00544c5541464544  ; // DEFAULT\0
      AWOpenSSL::SSL_CTX_set_cipher_list(*this\sslContext, @FCiphers)
      AWOpenSSL::SSL_CTX_set_verify(*this\sslContext, AWOpenSSL::#SSL_VERIFY_NONE, #Null)
    
		  *this\sslHandle = AWOpenSSL::SSL_new(*this\sslContext)
		  If *this\sslHandle = #Null
			  AddLog(*this, "SSL: Unable to obtain SSL Handle")
			  DowngradeSSL(*this)
			  *this\LastErr = "Unable to obtain SSL Handle"
			  *this\lastErrSSL = #SSL_ERR_SSL_NEW_FAIL
			  ProcedureReturn #SSL_ERR_SSL_NEW_FAIL
		  EndIf

		  If Not AWOpenSSL::SSL_set_fd(*this\sslHandle, *this\socket)
			  AddLog(*this, "SSL: Unable to connect socket to SSL")
			  DowngradeSSL(*this)
			  *this\LastErr = "Unable to connect socket to SSL"
			  *this\lastErrSSL = #SSL_ERR_SET_FD_FAIL
			  ProcedureReturn #SSL_ERR_SET_FD_FAIL
		  EndIf
		  
		  AWOpenSSL::SSL_set_mode(*this\sslHandle, AWOpenSSL::#SSL_MODE_AUTO_RETRY);
		  
		  res = AWOpenSSL::SSL_connect(*this\sslHandle)
		  If res <> 1
  			If res < 0
  				res = AWOpenSSL::SSL_get_error(*this\sslHandle, res)
  				*this\LastErr = AWOpenSSL::ERR_error_string(res)
  				AddLogErr(*this, "SSL: Unable to connect with SSL", res)
  			  DowngradeSSL(*this)
  				*this\lastErrSSL = res
  				ProcedureReturn res
  			EndIf
  			
  			DowngradeSSL(*this)
			  *this\LastErr    = "Unable to connect with SSL"
			  *this\lastErrSSL = #SSL_ERR_CONNECT_FAIL
  			ProcedureReturn #SSL_ERR_CONNECT_FAIL
  		EndIf
  		
  		*this\TLSactive  = #True
  		*this\lastErrSSL = #SSL_ERR_NONE
  		*this\LastErr    = ""
  		
  		ProcedureReturn #SSL_ERR_NONE
  	EndProcedure
    Procedure.i _SendNetworkDataSSL(*this.sConnection, *buf, bufsize)
      Protected *ptr, datasize.i, sentsize.i, timeout.i, ctime.i, maxlen.i, err.i
      
      *ptr             = *buf
      datasize         = bufsize
      timeout          = *this\Info\TimeOut + ElapsedMilliseconds()
      *this\lastErrSSL = #SSL_ERR_NONE
      *this\LastErr    = ""     
      
      Repeat
        maxlen = datasize
        If maxlen > 51200 : maxlen = 51200 : EndIf
        
  	    Repeat
		      sentsize =  AWOpenSSL::SSL_write(*this\sslHandle, *ptr, maxlen)
		      err      =  AWOpenSSL::SSL_get_error(*this\sslHandle, sentsize)
			  Until (err <> AWOpenSSL::#SSL_ERROR_WANT_WRITE) And err <> (AWOpenSSL::#SSL_ERROR_WANT_READ)
			  
			  AddLog(*this, "SSL: " + Str(sentsize) + " from " + Str(maxlen) + " bytes written")
       
  	    If err = AWOpenSSL::#SSL_ERROR_ZERO_RETURN
  				AddLogErr(*this, "SSL: SSL_write", err)
  				*this\LastErr    = "SSL_write"
  				*this\lastErrSSL = #SSL_ERR_WRITE
  				ProcedureReturn #SSL_ERR_WRITE
  	    ElseIf err <> 0
  	    	*this\lastErrSSL = err
  	    	*this\LastErr    = AWOpenSSL::ERR_error_string(err)
  	    	AddLogErr(*this, "SSL: SSL_write", err)
  	    	ProcedureReturn #SSL_ERR_WRITE
  	    Else
  	    	If sentsize > 0
  	    	  *ptr     + sentsize
  	    	  datasize - sentsize
            timeout  = *this\Info\TimeOut + ElapsedMilliseconds()   ; reset timeout since data was sent
  	  	  Else
  	    	  Delay(50)
  	    	EndIf
  	    EndIf
  	  Until (datasize <= 0) Or (timeout < ElapsedMilliseconds())
  	     
  	  If datasize > 0
  	    AddLog(*this, "SSL: _SendNetworkDataSSL not written all bytes")
				*this\LastErr    = "_SendNetworkDataSSL not written all bytes"
  	    *this\lastErrSSL = #ERR_TIMEOUT
  	    ProcedureReturn #ERR_TIMEOUT
  	  EndIf
      
      ProcedureReturn #ERR_NONE
    EndProcedure  	
    Procedure.i _ReceiveNetworkDataSSL(*this.sConnection)
      Protected rlen.i, *buf, res.i, timeout.i, err.i
      
  	  Repeat
  	  	rlen = AWOpenSSL::SSL_read(*this\sslHandle, *this\Buffer, #BUF_SIZE)
  	  	err  = AWOpenSSL::SSL_get_error(*this\sslHandle, rlen)
  	  Until (err <> AWOpenSSL::#SSL_ERROR_WANT_READ) And (err <> AWOpenSSL::#SSL_ERROR_WANT_WRITE)
  	  *this\lastErrSSL = err
      
	    AddLog(*this, "SSL: " + Str(rlen) + " bytes read")
	    
      If rlen > 0
        *buf = ReAllocateMemory(*this\Received, *this\ReceivedSize + rlen + 2)
        If *buf 
          *this\Received = *buf
          CopyMemory(*this\Buffer, *this\Received + *this\ReceivedSize, rlen)
          PokeW(*this\Received + *this\ReceivedSize + rlen, 0)
          *this\ReceivedSize + rlen
          res                = #ERR_NONE
        Else
          *this\LastErr = "Out of Memory"
          res           = #ERR_OUT_OF_MEMORY
        EndIf
      Else
        *this\LastErr = AWOpenSSL::ERR_error_string(*this\lastErrSSL)
    	  AddLogErr(*this, "SSL: SSL_read", *this\lastErrSSL)
        ProcedureReturn #ERR_NO_DATA
      EndIf
      
      ProcedureReturn res
    EndProcedure
  CompilerEndIf
  Procedure.i Connect(*this.sConnection)
    Protected timeout.i
    
    timeout = *this\info\TimeOut + ElapsedMilliseconds()
    
    If *this\Info\ProxyMode = #PROXY_NONE
      *this\ConnID = OpenNetworkConnection(*this\Info\Host, *this\Info\Port, #PB_Network_TCP, *this\Info\TimeOut)
      If *this\ConnID <> 0
        ProcedureReturn #ERR_NONE
      EndIf
    Else
      ProcedureReturn ProxyConnect(*this)        
    EndIf
    
    If timeout < ElapsedMilliseconds()
      ProcedureReturn #ERR_TIMEOUT
    Else
      ProcedureReturn #ERR_NO_CONNECTION
    EndIf
  EndProcedure
  Procedure   DisConnect(*this.sConnection)
    If *this\ConnID <> #Null
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        If *this\TLSactive
          DowngradeSSL(*this)
        EndIf
      CompilerEndIf
      CloseNetworkConnection(*this\ConnID)
      *this\ConnID   = #Null
      If *this\Received <> #Null
        FreeMemory(*this\Received)
        *this\Received     = #Null
        *this\ReceivedSize = 0
      EndIf
    EndIf
  EndProcedure
  Procedure.i Connected(*this.sConnection)
    ProcedureReturn Bool(*this\ConnID <> #Null)
  EndProcedure
  Procedure.i _SendNetworkData(*this.sConnection, *buf, bufsize.i)
    Protected *ptr, datasize.i, sentsize.i, timeout.i, ctime.i, maxlen.i
    
    If *this\ConnID = #Null : ProcedureReturn #ERR_NO_CONNECTION : EndIf
    If (*buf = #Null) Or (bufsize = 0) : ProcedureReturn #ERR_NO_DATA : EndIf
    
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      If *this\TLSactive
        ProcedureReturn _SendNetworkDataSSL(*this, *buf, bufsize)
      EndIf
    CompilerEndIf
    
    *ptr          = *buf
    datasize      = bufsize
    timeout       = *this\Info\TimeOut + ElapsedMilliseconds()
    *this\LastErr = ""
    
    Repeat
      maxlen   = datasize
      If maxlen > 51200 : maxlen = 51200 : EndIf
      sentsize = SendNetworkData(*this\ConnID, *ptr, maxlen)
      If sentsize > 0
        *ptr     + sentsize
        datasize - sentsize
        timeout  = *this\Info\TimeOut + ElapsedMilliseconds()   ; reset timeout since data was sent
      Else
        Delay(50)
      EndIf
    Until (datasize <= 0) Or (timeout < ElapsedMilliseconds())
    
    If datasize > 0
      *this\LastErr = "Timeout, not all data sent"
      ProcedureReturn #ERR_TIMEOUT
    EndIf
    
    ProcedureReturn #ERR_NONE
  EndProcedure
  Procedure.i _SendNetworkString(*this.sConnection, text.s, nl.i = #False)
    Protected *buf, bufsize.i, res.i, slen.i, nllen.i, cmdlen.i
    
    If Len(text) = 0 : ProcedureReturn #ERR_NO_DATA : EndIf
    
    slen    = StringByteLength(text, #PB_Ascii)
    nllen   = StringByteLength(#CRLF, #PB_Ascii)
    cmdlen  = Len(text)
    bufsize = slen + 20
    If nl : bufsize + nllen : EndIf
    *buf    = AllocateMemory(bufsize)    
    
    If *buf = #Null : ProcedureReturn #ERR_OUT_OF_MEMORY : EndIf
      
    PokeS(*buf, text, -1, #PB_Ascii)
    If nl : cmdlen + nllen : PokeS(*buf + slen, #CRLF, -1, #PB_Ascii) : EndIf
    
    res = _SendNetworkData(*this, *buf, cmdlen)
    
    FreeMemory(*buf)
    
    ProcedureReturn res
  EndProcedure
  Procedure   ResetReceiveBuffer(*this.sConnection)
    If *this\Received <> #Null : FreeMemory(*this\Received) : EndIf
    *this\Received     = #Null
    *this\ReceivedSize = 0
  EndProcedure
  Procedure.s GetReceivedString(*this.sConnection)
    If *this\Received <> #Null
      ProcedureReturn PeekS(*this\Received, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure  
  Procedure.i GetReceivedDataSize(*this.sConnection)
    ProcedureReturn *this\ReceivedSize
  EndProcedure
  Procedure.i GetReceivedData(*this.sConnection)
    ProcedureReturn *this\Received
  EndProcedure
  Procedure.s ReceivedRight(*this.sConnection, count.i)
    Protected res.s, *ptr
    
    If *this\Received <> #Null
      If count > *this\ReceivedSize : count = *this\ReceivedSize : EndIf
      *ptr = *this\Received + *this\ReceivedSize - count
      ProcedureReturn PeekS(*ptr, count, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.i _ReceiveNetworkData(*this.sConnection)
    Protected rlen.i, *buf, res.i, timeout.i
    
    *this\LastErr = ""
    If *this\ConnID = #Null : ProcedureReturn #ERR_NO_CONNECTION : EndIf
    
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      If *this\TLSactive
        ProcedureReturn _ReceiveNetworkDataSSL(*this)
      EndIf
    CompilerEndIf
    
    rlen = ReceiveNetworkData(*this\ConnID, *this\Buffer, #BUF_SIZE)
    If rlen > 0
      *buf = ReAllocateMemory(*this\Received, *this\ReceivedSize + rlen + 2)
      If *buf 
        *this\Received = *buf
        CopyMemory(*this\Buffer, *this\Received + *this\ReceivedSize, rlen)
        PokeW(*this\Received + *this\ReceivedSize + rlen, 0)
        *this\ReceivedSize + rlen
        res                = #ERR_NONE
      Else
        *this\LastErr = "Out of Memory"
        res           = #ERR_OUT_OF_MEMORY
      EndIf
    ElseIf rlen = 0
      *this\LastErr = "No data received"
      res           = #ERR_NO_DATA
    Else
      *this\LastErr = "No Connection"
      res           = #ERR_NO_CONNECTION
    EndIf
    
    ProcedureReturn res
  EndProcedure  
  Procedure.i GetResultCode(*this.sConnection)
    Protected code.i, res.s, *p, *buf, blen.i, cmp.s, cmp2.s
    
    If *this\Received = #Null : ProcedureReturn #ERR_UNKNOWN_PROTOCOL : EndIf
    If *this\ReceivedSize = 0 : ProcedureReturn #ERR_UNKNOWN_PROTOCOL : EndIf
    
    Select *this\Info\Protocol
      Case #PR_POP3
        res = PeekS(*this\Received, 4, #PB_Ascii)
        If LCase(Left(res, 3)) = "+ok"
          code = #ERR_OK
        ElseIf LCase(res) = "-err"
          code = #ERR_ERR
        EndIf
      Case #PR_SMTP
        code = Val(Left(res, 3))
      Case #PR_IMAP
        code = #ERR_NONE
        cmp  = "a" + Str(*this\Info\IMAP_Seq) + " "
        *buf = *this\Received
        blen = *this\ReceivedSize
        *p   = *buf + blen - 2
        If *p >= *buf
          If (PeekA(*p) = $0D) And (PeekA(*p + 1) = $0A)
            *p - 1
            While (*p > *buf)
              If PeekA(*p) = $0A 
                *p + 1
                Break
              EndIf
              *p - 1
            Wend
            ; *p sollte nun auf x bei Axxx blabla zeigen
            If *p < (*buf + blen)
              cmp2 = LCase(PeekS(*p, Len(cmp) + 3, #PB_Ascii))
              If cmp + "ok " = cmp2
                code = #ERR_OK
              ElseIf cmp + "no " = cmp2
                code = #ERR_NO
              Else
                cmp2 = LCase(PeekS(*p, Len(cmp) + 4, #PB_Ascii))
                If cmp + "bad " = cmp2
                  code = #ERR_BAD
                ElseIf cmp + "bye " = cmp2
                  code = #ERR_BYE
                EndIf
              EndIf
            EndIf
          EndIf
        EndIf
      Default:
        code = #ERR_UNKNOWN_PROTOCOL
    EndSelect
  
    ProcedureReturn code
  EndProcedure
  Procedure.s GetLastError(*this.sConnection)
    ProcedureReturn *this\LastErr
  EndProcedure
  Procedure   ClearLastError(*this.sConnection)
    *this\LastErr = ""
  EndProcedure
  Procedure.i IsResultLastLine(*this.sConnection, CheckEndMode.i)
    Protected p.i, *buf, blen.i, *p
    
    *buf = *this\Received
    blen = *this\ReceivedSize
    
    Select CheckEndMode
      Case #LE_POP3_SL  ; test for CRLF on lineende
        *p = *buf + blen - 2
        If *p >= *buf
          If (PeekA(*p) = $0D) And (PeekA(*p + 1) = $0A)
            ProcedureReturn #True
          EndIf
        EndIf
      Case #LE_POP3_ML  ; test for CRLF.CRLF on lineend
        *p = *buf + blen - 5
        If *p >= *buf
          If (PeekA(*p) = $0D) And (PeekA(*p+1) = $0A) And (PeekA(*p+2) = '.') And (PeekA(*p) = $0D) And (PeekA(*p+1) = $0A)
            ProcedureReturn #True
          EndIf
        EndIf
      Case #LE_SMTP  ; test for xxx blabla on linebeginning UND CRLF am Ende
        *p = *buf + blen - 2
        If *p >= *buf
          If (PeekA(*p) = $0D) And (PeekA(*p + 1) = $0A)
            *p - 1
            While (*p > *buf)
              If PeekA(*p) = $0A 
                *p + 1
                Break
              EndIf
              *p - 1
            Wend
            ; *p sollte nun auf x bei xyz#blabla zeigen
            *p + 3  ; zeigt nun auf # -> oder ins nirvana
            If *p < (*buf + blen)
              If PeekA(*p) = ' '
                ProcedureReturn #True
              EndIf
            EndIf
          EndIf
        EndIf
      Case #LE_IMAP_DT      ; + ... CRLF
        If PeekA(*buf) = '+'
          *p = *buf + blen - 2
          If *p >= *buf
            If (PeekA(*p) = $0D) And (PeekA(*p + 1) = $0A)
              ProcedureReturn #True
            EndIf
          EndIf
        EndIf
      Case #LE_IMAP  ; test for Axxx blabla on linebeginning UND CRLF am Ende
        *p = *buf + blen - 2
        If *p >= *buf
          If (PeekA(*p) = $0D) And (PeekA(*p + 1) = $0A)
            Select GetResultCode(*this)
              Case #ERR_BAD, #ERR_BYE, #ERR_OK, #ERR_NO
                ProcedureReturn #True
            EndSelect
          EndIf
        EndIf

    EndSelect
    
    ProcedureReturn #False
  EndProcedure
  Procedure   ReadResult(*this.sConnection, CheckEndMode.i)
    Protected done.i, err.i, timeout.i
    
    *this\LastErr = ""
    timeout       = *this\Info\TimeOut + ElapsedMilliseconds()
    ResetReceiveBuffer(*this)
    
    Repeat
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        If *this\TLSactive
          Repeat
            *this\lastErrSSL = #SSL_ERR_NONE
            err              = _ReceiveNetworkData(*this)
            If (err = AWOpenSSL::#SSL_ERROR_NONE) And (AWOpenSSL::SSL_pending(*this\sslHandle) < 1)
          	  done = #True
            Else
            	done = #False
            EndIf
            While Not done
              err = _ReceiveNetworkData(*this)
            	If err = AWOpenSSL::#SSL_ERROR_ZERO_RETURN
        	  		done = #True
        	  	ElseIf err = AWOpenSSL::#SSL_ERROR_NONE
        	  		If AWOpenSSL::SSL_pending(*this\sslHandle) < 1
        	  			done = #True
        	  		EndIf
        	  	EndIf
        	  Wend
        	Until done
      	Else
      CompilerEndIf
      
        err = _ReceiveNetworkData(*this)
        
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        EndIf
      CompilerEndIf    
      
      If (err = #ERR_NONE) Or (err = #ERR_NO_DATA)
        done = IsResultLastLine(*this, CheckEndMode)
        If err = #ERR_NONE
          timeout = *this\Info\TimeOut + ElapsedMilliseconds()
        EndIf
      Else
        done = #True
      EndIf
  
    Until done Or (timeout < ElapsedMilliseconds())

  EndProcedure  
  Procedure.i Execute(*this.sConnection, cmd.s, CheckEndMode.i, retries.i = 1)
    Protected res.i
    
    If *this\ConnID = #Null : ProcedureReturn #ERR_NO_CONNECTION : EndIf
    
;-DEBUG    Debug "EXEC: " + cmd
      
    Repeat
      AddLog(*this, "EXEC: " + cmd)
    	Delay(50)
    	res = _SendNetworkString(*this, cmd, #True)
      Delay(50)
      ReadResult(*this, CheckEndMode)
      If res <> #ERR_NONE
        retries - 1
      EndIf
    Until res = #ERR_NONE Or (retries <= 0)

;-DEBUG    Debug "RECV: " + Left(GetReceivedString(*this), 80) + "..."

    ProcedureReturn res
  EndProcedure

  Procedure   Destroy(*this.sConnection)
    ResetReceiveBuffer(*this)
    If *this\Buffer : FreeMemory(*this\Buffer) : EndIf
    FreeStructure(*this)
  EndProcedure

  DataSection
    Connection_VT:
      Data.i @Destroy()
      Data.i @GetConnInfo()
      Data.i @EnableLog()
      Data.i @ClearLog()
      Data.i @GetLog()
      Data.i @Connect()
      Data.i @DisConnect()
      Data.i @Connected()
      Data.i @_SendNetworkData()
      Data.i @_SendNetworkString()
      Data.i @ResetReceiveBuffer()
      Data.i @GetReceivedString()
      Data.i @GetReceivedDataSize()
      Data.i @GetReceivedData()
      Data.i @_ReceiveNetworkData()
      Data.i @GetResultCode()
      Data.i @GetLastError()
      Data.i @ClearLastError()
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        Data.i @UpgradeSSL()
      	Data.i @DowngradeSSL()
      CompilerEndIf
      Data.i @ReadResult()
      Data.i @Execute()
      
  EndDataSection
EndModule

;{ automatic initialization
AWNetwork::initialized = InitNetwork()
;}


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 972
; FirstLine = 959
; Folding = -----------
; EnableUnicode
; EnableXP
