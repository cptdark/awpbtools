﻿;{   awsettings.pbi
;    Version 0.1 [2015/11/10]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awcode.pbi"

DeclareModule AWSettings
  Interface Settings
  	Destroy  		 			 ()
  	SetSection	 			 (section.s)
		SetDefaultSection	 ()
    SetByte		 	 			 (key.s, value.b)
    SetWord		 	 			 (key.s, value.w)
    SetLong		 	 			 (key.s, value.l)
    SetQuad		 				 (key.s, value.q)
    SetFloat	 	 			 (key.s, value.f)
    SetDouble	 	 			 (key.s, value.d)
    SetString	 	 			 (key.s, value.s)
    SetBinary		 			 (key.s, *value, length.i)
    SetStructData      (key.s, *buffer, structsize.i)
    GetByte.b		 			 (key.s, def.b)
    GetWord.w		 			 (key.s, def.w)
    GetLong.l		 			 (key.s, def.l)
    GetQuad.q		 			 (key.s, def.q)
    GetFloat.f	 			 (key.s, def.f)
    GetDouble.d	 			 (key.s, def.d)
    GetString.s	 			 (key.s, def.s)
    GetBinary.i	 			 (key.s, *binlen)
    GetStructData.i    (key.s, *buffer, structsize.i)
		Open.i						 (filename.s)
		Close							 ()
		Write.i					   ()
		GetSections				 (List sections.s())
		GetKeys						 (List keys.s())
		GetType.i					 (key.s)
		DeleteKey					 (key.s)
		DeleteSection			 (section.s)

  EndInterface
 
  Declare.i New()
EndDeclareModule

Module AWSettings
  EnableExplicit
   	
  Structure sItem
  	type.i
  	StringValue.s
  	StructureUnion
	  	WordValue.w
	  	LongValue.l
	  	QuadValue.q
	  	FloatValue.f
	  	DoubleValue.d
	  	ByteValue.b
	  EndStructureUnion
	EndStructure
	Structure sSection
		Map items.sItem()
	EndStructure
	Structure sSettings
    *vt.Settings
    
    Map sections.sSection()
    
    flushed.i
    section.s
    mFile.i
    mFilename.s
    mErrorLine.i
  EndStructure
 
  Declare Settings_SetSection(*this.sSettings, section.s)

  Procedure.i New()
    Protected *this.sSettings
    
    *this = AllocateStructure(sSettings)
    If *this
      *this\vt        = ?Settings_VT
     	*this\flushed 	= #True
     	*this\mFile   	= #Null
     	*this\mFilename = ""
     	
	    ProcedureReturn *this
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  
  Procedure   Clear(*this.sSettings)
  	ResetMap(*this\sections())
		While NextMapElement(*this\sections())
			ClearMap(*this\sections()\items())
		Wend
		ClearMap(*this\sections())  	
  EndProcedure
  Procedure   SetValue(*this.sSettings, key.s, type.i, value.s)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = value
			\type        = type
  		Select type
  			Case #PB_Byte
  				\ByteValue = Val(value)
  			Case #PB_Word
  				\WordValue = Val(value)
  			Case #PB_Long
  				\LongValue = Val(value)
  			Case #PB_Quad
  				\QuadValue = Val(value)
  			Case #PB_Float
  				\FloatValue = ValF(value)
  			Case #PB_Double
  				\DoubleValue = ValD(value)
  		EndSelect
  	EndWith
  EndProcedure
  Procedure.i ParseFile(*this.sSettings)
  	Protected row.s, p.i, section.s, key.s, value.s, type.s, rownum.i, typeval.i
  	
  	Clear(*this)
  	*this\mErrorLine = 0
  	rownum           = 0
  	While Not Eof(*this\mFile)
  		row    = Trim(ReadString(*this\mFile, #PB_UTF8))
  		rownum + 1
  		
  		If row = ""
  			; nothing, empty line
  		ElseIf Left(row, 1) = "["
  			p = FindString(row, "]", 2)
  			If Not p
  				*this\mErrorLine = rownum
  				ProcedureReturn #False
  			EndIf
 				section = Mid(row, 2, p - 2)
 				Settings_SetSection(*this, section)
  		ElseIf Left(row, 3) = ";%%"
  			p = FindString(row, "%%", 4)
  			If Not p
  				*this\mErrorLine = rownum
  				ProcedureReturn #False
  			EndIf
 				type = Mid(row, 4, p - 4)
  		ElseIf Left(row, 1) = ";"
  			; nothing, comment
  		Else
 				p = FindString(row, "=")
 				If Not p 
  				*this\mErrorLine = rownum
  				ProcedureReturn #False
 				EndIf
 				
 				key = Trim(Left(row, p - 1))
 				value = UnescapeString(Trim(Mid(row, p + 1)))

  			If type ="BYTE"
  				SetValue(*this, key, #PB_Byte, value)
  			ElseIf type ="WORD"
  				SetValue(*this, key, #PB_Word, value)
  			ElseIf type ="LONG"
  				SetValue(*this, key, #PB_Long, value)
  			ElseIf type ="QUAD"
  				SetValue(*this, key, #PB_Quad, value)
  			ElseIf type ="FLOAT"
  				SetValue(*this, key, #PB_Float, value)
  			ElseIf type ="DOUBLE"
  				SetValue(*this, key, #PB_Double, value)
  			ElseIf type ="BIN"
  				SetValue(*this, key, #PB_Any, value)
  			Else
  				SetValue(*this, key, #PB_String, value)
				EndIf
  		EndIf
  	Wend
  	
		*this\flushed = #True

  	ProcedureReturn #True
  EndProcedure
  
  Procedure   Settings_SetDefaultSection(*this.sSettings)
  	Settings_SetSection(*this, "default")
  EndProcedure
  Procedure   Settings_SetSection(*this.sSettings, section.s)
  	*this\section = section
  	If Not FindMapElement(*this\sections(), *this\section)
  		AddMapElement(*this\sections(), *this\section)
  	EndIf  	
  EndProcedure  
  
  Procedure   Settings_SetByte(*this.sSettings, key.s, value.b)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = Str(value)
  		\ByteValue   = value
  		\type        = #PB_Byte
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetWord(*this.sSettings, key.s, value.w)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = Str(value)
  		\WordValue   = value
  		\type        = #PB_Word
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetLong(*this.sSettings, key.s, value.l)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = Str(value)
  		\LongValue   = value
  		\type        = #PB_Long
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetQuad(*this.sSettings, key.s, value.q)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = Str(value)
  		\QuadValue   = value
  		\type        = #PB_Quad
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetFloat(*this.sSettings, key.s, value.f)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = StrF(value)
  		\FloatValue  = value
  		\type        = #PB_Float
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetDouble(*this.sSettings, key.s, value.d)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = StrD(value)
  		\DoubleValue = value
  		\type        = #PB_Double
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetString(*this.sSettings, key.s, value.s)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = value
  		\type        = #PB_String
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetBinary(*this.sSettings, key.s, *value, length.i)
  	AddMapElement(*this\sections()\items(), key)
  	With *this\sections()\items()
  		\StringValue = AWCode::EncodeBase64BinToStr(*value, length, #PB_Any)
  		\StringValue = Left(\StringValue, Len(\StringValue) - 2)
  		\type        = #PB_Any
  	EndWith  	
  	*this\flushed  = #False
  EndProcedure
  Procedure   Settings_SetStructData(*this.sSettings, key.s, *buffer, structsize.i)
  	Settings_SetBinary(*this, key, *buffer, structsize)
  EndProcedure
  
  Procedure.b Settings_GetByte(*this.sSettings, key.s, def.b)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Byte
		  			ProcedureReturn \ByteValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn Val(\StringValue)	
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.w Settings_GetWord(*this.sSettings, key.s, def.w)
  	If FindMapElement(*this\sections(), *this\section)
   		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Word
		  			ProcedureReturn \WordValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn Val(\StringValue)	
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.l Settings_GetLong(*this.sSettings, key.s, def.l)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Long
		  			ProcedureReturn \LongValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn Val(\StringValue)	
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	

		ProcedureReturn def
  EndProcedure
  Procedure.q Settings_GetQuad(*this.sSettings, key.s, def.q)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Quad
		  			ProcedureReturn \QuadValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn Val(\StringValue)	
		  		EndIf	
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.f Settings_GetFloat(*this.sSettings, key.s, def.f)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Float
		  			ProcedureReturn \FloatValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn ValF(\StringValue)	
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.d Settings_GetDouble(*this.sSettings, key.s, def.d)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Double
		  			ProcedureReturn \DoubleValue
		  		ElseIf (\type <> #Null) And (\type <> #PB_Any)
		  			ProcedureReturn ValD(\StringValue)	
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.s Settings_GetString(*this.sSettings, key.s, def.s)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
	  			ProcedureReturn \StringValue
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn def
  EndProcedure
  Procedure.i Settings_GetBinary(*this.sSettings, key.s, *binlen)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	With *this\sections()\items()
		  		If \type = #PB_Any
		  			ProcedureReturn AWCode::DecodeBase64StrToBin(\StringValue, *binlen)
		  		EndIf
		  	EndWith
		  EndIf	
		EndIf	
		
		ProcedureReturn #Null
	EndProcedure
  Procedure.i Settings_GetStructData(*this.sSettings, key.s, *buffer, structsize.i)
  	Protected *buf, size.i
  	
  	If Not *buffer Or (structsize = 0)
  		ProcedureReturn #False
  	EndIf
  	
  	*buf = Settings_GetBinary(*this, key, @size)
  	
  	If *buf And (size = structsize)
  		CopyMemory(*buf, *buffer, size)
  		FreeMemory(*buf)
  		
  		ProcedureReturn #True
  	EndIf

  	ProcedureReturn #False
  EndProcedure
  
	Procedure   Settings_DeleteKey(*this.sSettings, key.s)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
  			DeleteMapElement(*this\sections()\items())
		  EndIf
		EndIf
  EndProcedure
 	Procedure   Settings_DeleteSection(*this.sSettings, section.s)
 		If FindMapElement(*this\sections(), *this\section)
 			ClearMap(*this\sections()\items())
 			DeleteMapElement(*this\sections())
		EndIf
  EndProcedure

  Procedure.i Settings_Write(*this.sSettings)
  	Protected type.s, value.s, key.s, entry.s, section.s
  	
  	If Not *this\flushed
  		If IsFile(*this\mFile)
  			FileSeek(*this\mFile, 0, #PB_Absolute)
  			TruncateFile(*this\mFile)
				PushMapPosition(*this\sections())
				ForEach *this\sections()
					section = MapKey(*this\sections())
					If Trim(section) <> ""
			  		WriteStringN(*this\mFile, "[" + section + "]", #PB_UTF8)
		  		
			  		ForEach *this\sections()\items()
			  			key = MapKey(*this\sections()\items())
			  			If Trim(key) <> ""
				  			With *this\sections()\items()
						  		value = \StringValue
					  			Select \type
					  				Case #PB_Byte
					  					type  = "BYTE"
					  				Case #PB_Word		
					  					type  = "WORD"
					  				Case #PB_Long		
					  					type  = "LONG"
								  	Case #PB_Quad		
								  		type  = "QUAD"
								  	Case #PB_Float		
								  		type  = "FLOAT"
								  	Case #PB_Double	
								  		type  = "DOUBLE"
								  	Case #PB_Any
								  		type  = "BIN"
								  	Case #PB_String
								  		type = "STRING"
								  	Default
								  		type  = "NULL"
								  		value = ""
								  EndSelect
								EndWith
								value = EscapeString(value)
								WriteStringN(*this\mFile, ";%%" + type + "%%", #PB_UTF8)
								WriteStringN(*this\mFile, key + " = " + value, #PB_UTF8)								
							EndIf
						Next
					EndIf
				Next
				PopMapPosition(*this\sections())
			EndIf
			*this\flushed = Bool(FlushFileBuffers(*this\mFile) <> 0)  		 
		EndIf
		ProcedureReturn *this\flushed
  EndProcedure
  Procedure   Settings_Close(*this.sSettings)
  	If IsFile(*this\mFile)
  		Settings_Write(*this)
  		CloseFile(*this\mFile)
  	EndIf
  	
  	Clear(*this)
  EndProcedure
  Procedure   Settings_Destroy(*this.sSettings)
  	Settings_Close(*this)
  	FreeStructure(*this)
  EndProcedure
  Procedure.i Settings_Open(*this.sSettings, filename.s)
  	Settings_Close(*this)
  	
  	*this\mFilename = filename
  	*this\mFile     = OpenFile(#PB_Any, *this\mFilename, #PB_UTF8) 
  	If IsFile(*this\mFile)
  		If ParseFile(*this)
  			Settings_SetSection(*this, "default")
  			ProcedureReturn #True
  		EndIf
  		CloseFile(*this\mFile)
  		*this\mFile = #Null
  	EndIf
  	ProcedureReturn #False  	
  EndProcedure
  
  Procedure   Settings_GetSections(*this.sSettings, List sections.s())
  	Protected section.s
  	
  	ClearList(sections())
  	
  	PushMapPosition(*this\sections())
  	ForEach *this\sections()
  		section = MapKey(*this\sections())
  		If Trim(section) <> ""
  			AddElement(sections())
  			sections() = section
  		EndIf
  	Next
  	PopMapPosition(*this\sections())

		EndProcedure
	Procedure   Settings_GetKeys(*this.sSettings, List keys.s())
		Protected key.s
		
		ClearList(keys())
		
  	PushMapPosition(*this\sections()\items())
  	ForEach *this\sections()\items()
  		key = MapKey(*this\sections()\items())
  		If Trim(key) <> ""
  			AddElement(keys())
  			keys() = key
  		EndIf
  	Next
  	PopMapPosition(*this\sections()\items())
		
	EndProcedure
  Procedure.i Settings_GetType(*this.sSettings, key.s)
  	If FindMapElement(*this\sections(), *this\section)
  		If FindMapElement(*this\sections()\items(), key)
		  	ProcedureReturn *this\sections()\items()\type
		  EndIf	
		EndIf	
		
		ProcedureReturn #Null
  EndProcedure
	
  DataSection
    Settings_VT:
      Data.i @Settings_Destroy()
      Data.i @Settings_SetSection()
			Data.i @Settings_SetDefaultSection()
      Data.i @Settings_SetByte()
      Data.i @Settings_SetWord()
      Data.i @Settings_SetLong()
      Data.i @Settings_SetQuad()
      Data.i @Settings_SetFloat()
      Data.i @Settings_SetDouble()
      Data.i @Settings_SetString()
      Data.i @Settings_SetBinary()
      Data.i @Settings_SetStructData()
      Data.i @Settings_GetByte()
      Data.i @Settings_GetWord()
      Data.i @Settings_GetLong()
      Data.i @Settings_GetQuad()
      Data.i @Settings_GetFloat()
      Data.i @Settings_GetDouble()
      Data.i @Settings_GetString()
      Data.i @Settings_GetBinary()
      Data.i @Settings_GetStructData()
			Data.i @Settings_Open()
			Data.i @Settings_Close()
			Data.i @Settings_Write()
			Data.i @Settings_GetSections()
			Data.i @Settings_GetKeys()
			Data.i @Settings_GetType()
			Data.i @Settings_DeleteKey()
			Data.i @Settings_DeleteSection()
			
    EndDataSection
EndModule
