;{   awlocale.pbi
;    Version 0.3 [2014/08/11]
;    Copyright (C) 2011-14 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awsupport.pbi"

DeclareModule AWLocale
  Declare.i LoadCatalog(filename.s, format.i = #PB_Ascii)
  Declare.i AppendCatalog(filename.s, format.i = #PB_Ascii)  
  Declare.i CatchCatalog(text.s)
  Declare   ClearCatalog()
  Declare.s Text(key.s, defval.s)
  Declare   ReplaceText(key.s, val.s)
EndDeclareModule

Module AWLocale 
  EnableExplicit
  
  Global NewMap Catalog.s()
  
  Procedure.s DeEscapeString(str.s)
    Protected esc.i, *str, a.i, target.s, *dst, len.i, c.c, b.i, m.i, d.i
    
    len    = Len(str)
    target = Space(len)
    *str   = @str
    *dst   = @target
    esc    = 0
    For a = 1 To len
      c = PeekC(*str) : *str + SizeOf(character)
      If c = '\'
        esc + 1
      Else
        If esc
          m = esc % 2
          d = Int(esc / 2)
          For b = 1 To d : PokeC(*dst, '\') : *dst + SizeOf(character) : Next b
          If m  ; ungrade anzahl an "\" -> esc-sequenz
            Select c
              Case 't': PokeC(*dst, $09)
              Case 'n': PokeC(*dst, $0a)
              Case 'r': PokeC(*dst, $0d)
              Default : PokeC(*dst, ' ')
            EndSelect
            *dst + SizeOf(character)
          Else  ; grade anzahl an "\" -> keine esc-sequenz; c übernehmen
            PokeC(*dst, c) : *dst + SizeOf(character)
          EndIf
          esc = 0
        Else ; keine esc-sequenz, c übernehmen
          PokeC(*dst, c) : *dst + SizeOf(character)
        EndIf
      EndIf
    Next a
    If len : PokeC(*dst, $00) : EndIf
    
    ProcedureReturn target
  EndProcedure
  Procedure   ClearCatalog()
    ClearMap(Catalog())
  EndProcedure
  Procedure.i AppendCatalog(filename.s, format.i = #PB_Ascii)
    Protected file.i, key.s, value.s, p.i
    
    file = ReadFile(#PB_Any, filename)
    If IsFile(file)
      While Not Eof(file)
        key   = ReadString(file, format)
        If Left(key, 1) <> "#"                    ; mit # werden kommentare eingeleitet
          p = FindString(key, "=", 2)
          If p                                    ; = in der Zeile, mindestens 2. Stelle
            value = Trim(Mid(key, p + 1))
            key   = LCase(Trim(Left(key, p - 1)))
            Catalog(key) = DeEscapeString(value)
          EndIf
        EndIf
      Wend
      CloseFile(file)
    Else
      ProcedureReturn #False
    EndIf
    
    ProcedureReturn #True
  EndProcedure  
  Procedure.i LoadCatalog(filename.s, format.i = #PB_Ascii)
    ClearCatalog()
    ProcedureReturn AppendCatalog(filename, format)
  EndProcedure
  Procedure.i CatchCatalog(text.s)
    Protected key.s, value.s, start.i, newstart.i, l.i, p.i
    
    ClearCatalog()
    l     = Len(text)
    start = 1
    While (start <= l) And (start <> -1)
      key = AWSupport::GetLine(text, start, @newstart)
      If Left(key, 1) <> "#"
        p = FindString(key, "=", 2)
        If p
          value = AWSupport::TrimWS(Mid(key, p + 1))
          key   = LCase(AWSupport::TrimWS(Left(key, p - 1)))
          Catalog(key) = DeEscapeString(value)
        EndIf
      EndIf
    start = newstart
    Wend
    
    ProcedureReturn #True
  EndProcedure
  Procedure   ReplaceText(key.s, val.s)
    Catalog(LCase(key)) = val
  EndProcedure
  Procedure.s Text(key.s, defval.s)
    Protected res.s
    
    If FindMapElement(Catalog(), LCase(key))
      res = Catalog()
    Else
      res = defval
    EndIf
    
    ProcedureReturn res
  EndProcedure
EndModule



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; FirstLine = 27
; Folding = --
; EnableUnicode
; EnableXP