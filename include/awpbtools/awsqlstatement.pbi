;{   awsqlstatement.pbi
;    Version 0.5 [2015/10/26]
;    Copyright (C) 2012-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWSqlStatement
  Interface SQLStatement
    Destroy    ()
    SetSQL     (sqlcmd.s)
    BindQuad   (index.i, value.q)
    BindLong   (index.i, value.l)
    BindFloat  (index.i, value.f)
    BindDouble (index.i, value.d)
    BindString (index.i, value.s)
    BindBLOB	 (index.i, *value, length.i)
    GetSQL.s   ()
    Reset      ()
  EndInterface
  
  Declare.i New()
EndDeclareModule

Module AWSqlStatement
  EnableExplicit
  
  Structure sSQLStatement
    *vt.SQLStatement
    Map bound.s()
    sql.s
    compiled.s
  EndStructure
  
  Procedure.i New()
    Protected *this.sSQLStatement
  
    *this = AllocateStructure(sSQLStatement)
    If *this
      *this\vt       = ?VT_SQLStatement
      *this\sql      = ""
      *this\compiled = ""
  
      ProcedureReturn *this
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure SQLStatement_Destroy(*this.sSQLStatement)
    ClearMap(*this\bound())
    FreeStructure(*this)
  EndProcedure
  Procedure   SQLStatement_SetSQL(*this.sSQLStatement, sqlcmd.s)
    *this\sql      = sqlcmd
    *this\compiled = ""
  EndProcedure
  Procedure   SQLStatement_BindQuad(*this.sSQLStatement, index.i, value.q)
    *this\bound(Str(index)) = Str(value)
  EndProcedure
  Procedure   SQLStatement_BindLong(*this.sSQLStatement, index.i, value.l)
    *this\bound(Str(index)) = Str(value)
  EndProcedure
  Procedure   SQLStatement_BindFloat(*this.sSQLStatement, index.i, value.f)
    *this\bound(Str(index)) = StrF(value)
  EndProcedure
  Procedure   SQLStatement_BindDouble(*this.sSQLStatement, index.i, value.d)
    *this\bound(Str(index)) = StrD(value)
  EndProcedure
  Procedure   SQLStatement_BindString(*this.sSQLStatement, index.i, value.s)
    *this\bound(Str(index)) = Chr(34) + ReplaceString(value, Chr(34), Chr(34)+Chr(34)) + Chr(34)
  EndProcedure
  Procedure.i SQLStatement_BindBLOB(*this.sSQLStatement, index.i, *value, length.i)
  	Protected *blobdata, blobsize.i, *ptr, i.i, b.b, h1.b, h2.b
  	Static Dim hexval.b(16)
  	
  	For i =  0 To  9 : hexval(i) = i + 48 : Next
  	For i = 10 To 15 : hexval(i) = i + 55 : Next
  	
  	blobsize = (length * 2) + 4
  	*blobdata = AllocateMemory(blobsize)
  	If *blobdata 
  		*ptr = *blobdata
  		PokeB(*ptr, $78) : *ptr + 1
  		PokeB(*ptr, $27) : *ptr + 1
  		For i = 1 To length
  			b = PeekB(*value) : *value + 1
  			PokeB(*ptr, hexval((b & $F0) >> 4)) : *ptr + 1
  			PokeB(*ptr, hexval( b & $F)       ) : *ptr + 1
  		Next
  		PokeB(*ptr, $27) : *ptr + 1
  		PokeB(*ptr,   0) : *ptr + 1
  		
  		*this\bound(Str(index)) = PeekS(*blobdata, blobsize, #PB_Ascii)
  		
  		ProcedureReturn #True
  	EndIf
  	
  	ProcedureReturn #False
  EndProcedure
  Procedure.s SQLStatement_GetSQL(*this.sSQLStatement)
    Protected k.i, c.i
  
    *this\compiled = ""
    c              = CountString(*this\sql, "?")
  
    For k = 1 To c + 1
      *this\compiled + StringField(*this\sql, k, "?")
      If k <= c : *this\compiled + *this\bound(Str(k)) : EndIf
    Next k
    
    ProcedureReturn *this\compiled
  EndProcedure
  Procedure   SQLStatement_Reset(*this.sSQLStatement)
    ClearMap(*this\bound())
    *this\compiled = ""
  EndProcedure
  
  DataSection
    VT_SQLStatement:
      Data.i @SQLStatement_Destroy()
      Data.i @SQLStatement_SetSQL()
      Data.i @SQLStatement_BindQuad()
      Data.i @SQLStatement_BindLong()
      Data.i @SQLStatement_BindFloat()
      Data.i @SQLStatement_BindDouble()
      Data.i @SQLStatement_BindString()
      Data.i @SQLStatement_BindBLOB()
      Data.i @SQLStatement_GetSQL()
      Data.i @SQLStatement_Reset()
  EndDataSection
EndModule

  
	
; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 2
; Folding = ---
; EnableUnicode
; EnableXP