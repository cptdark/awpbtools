;{   awchunkyfile.pbi
;    Version 0.5 [2015/10/26]
;    Copyright (C) 2011-15 Ronny Kr�ger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awzip.pbi"

UseCRC32Fingerprint()

CompilerIf Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
  XIncludeFile "awcustomcrypt.pbi"
CompilerEndIf

DeclareModule AWChunkyFile
  #MODE_READ  = 1
  #MODE_WRITE = 2
  
  Interface ChunkyFile
    Destroy               ()
    CloseChunk.i          ()
    Close                 ()
    Open.i                (filename.s, hdr.s, mode.i)
    OpenChunk.i           (hdr.s)
    WriteQuad.i           (value.q)
    WriteLong.i           (value.l)
    WriteWord.i           (value.w)
    WriteByte.i           (value.b)
    WriteDouble.i         (value.d)
    WriteFloat.i          (value.f)
    WriteBlock.i          (*src, slen.i, complevel.i = AWZip::#COMP_STORE)
    WriteString.i         (str.s, format.b, complevel.i = AWZip::#COMP_STORE)
    NextChunk.i           ()
    GetChunkHeader.s      ()
    GetChunkSize.i        ()
    ReadBlock.i           ()
    GetBlockSize.i        ()
    ReadString.s          ()
  	ReadBlockDirect.i			(*buffer, bufsize.i)
    ReadQuad.q            ()
    ReadLong.l            ()
    ReadWord.w            ()
    ReadByte.b            ()
    ReadDouble.d          ()
    ReadFloat.f           ()
    CheckChunkIntegrity.i ()
    FreeBlock             (*block)
    CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
    	InitAES.i							(*key, *initializationvector, bits.i = 128)
    	FreeAES								()
    	EnableAES.i						()
    	DisableAES						()
    CompilerEndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileJSON, #PB_Module)
      WriteJSON.i           (json.i, complevel.i = AWZip::#COMP_STORE)
      ReadJSON.i            (json.i)
    CompilerEndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileXML, #PB_Module)
      WriteXML.i           (xml.i, complevel.i = AWZip::#COMP_STORE)
      ReadXML.i            (xml.i)
    CompilerEndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
      WriteCustomCrypt.i         (*lib.AWCustomCrypt::CustomCrypt, *src, slen.i, *userdata = #Null)
      ReadCustomCrypt.i          (*lib.AWCustomCrypt::CustomCrypt, *userdata = #Null)
      WriteCustomCryptString.i   (*lib.AWCustomCrypt::CustomCrypt, text.s, format.b = #PB_Ascii, *userdata = #Null)
      ReadCustomCryptString.s    (*lib.AWCustomCrypt::CustomCrypt, *userdata = #Null)
    CompilerEndIf
  EndInterface
  
  Declare.i New()
EndDeclareModule

Module AWChunkyFile
  EnableExplicit
  
  #MODE_NONE = 0
  
  Structure sHeader
    Header.l
    UserHeader.l
    Flags.l
  EndStructure
  Structure sChunkHeader
    Header.l
    Offset.l
    NextChunkOffset.l
    CRC32.l
    DataSize.l
  EndStructure
  Structure sCompressedBlockHdr
    OriginalLength.l
    OriginalCRC32.l
    PackedLength.l
    PackedCRC32.l
    Packed.b
    Cipher.b
  EndStructure
  
  Interface iChunkyFile Extends ChunkyFile
    WriteData.i          (*Binary, BinLen.i)
    CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
    	WriteCipherData.i		(*Binary, BinLen.i)
    	ReadCipherData.i		(*Buffer, BinLen.i)
    CompilerEndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
      WriteCCData.i       (*Buffer, BinLen.i, *userdata)
      ReadCCData.i        (*Buffer, BinLen.i, *userdata)
    CompilerEndIf
  EndInterface
  
  Structure sChunkyFile
    *vt.VT_ChunkyFile
    ChunkOffset.i
    Handle.i
    Mode.b
    FileHdr.sHeader
    ChunkHdr.sChunkHeader
    ChunkOpen.b
    LastBlockLen.i
    ChunkCRC.i
    CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
    	UseAES.b
     	*AESKey
    	*AESInitializationVector
    	AESBits.i
    CompilerEndIf    
  EndStructure
  
  ;- BUG-Workaround
  Procedure.i StringByteLengthUTF8(*UTF8String, CharLength)
    Protected  *b.Byte = *UTF8String, CharLen
    While *b\b <> 0
      If     *b\b & %10000000 = 0         : *b + 1 : CharLen + 1
      ElseIf *b\b & %11000000 = %11000000 : *b + 2 : CharLen + 1
      ElseIf *b\b & %11100000 = %11100000 : *b + 3 : CharLen + 1
      ElseIf *b\b & %11110000 = %11110000 : *b + 4 : CharLen + 1
      Else                                : *b + 1
      EndIf
      If CharLen = CharLength : Break : EndIf
    Wend
    ProcedureReturn *b - *UTF8String
  EndProcedure
  Procedure.s PeekS_(*buf, maxlen.i, format)
    If format = #PB_UTF8
      maxlen = StringByteLengthUTF8(*buf, maxlen)
    EndIf
    
    ProcedureReturn PeekS(*buf, maxlen, format)
  EndProcedure  
  
  Procedure.l MakeHdr(hdr.s)
    Protected h.s, res.l
  
    h   = LSet(hdr, 4, " ")
    res = (Asc(Left(h, 1))) + (Asc(Mid(h, 2, 1)) << 8) + (Asc(Mid(h, 3, 1)) << 16) + (Asc(Mid(h, 4, 1)) << 24)
  
    ProcedureReturn res
  EndProcedure
  Procedure.s DecodeHdr(hdr.l)
    Protected res.s
  
    res = Chr((hdr & $000000FF)) + Chr((hdr & $0000FF00) >> 8) + Chr((hdr & $00FF0000) >> 16) + Chr((hdr & $FF000000) >> 24)
  
    ProcedureReturn res
  EndProcedure
  Procedure.i New()
    Protected *this.sChunkyFile
  
    *this = AllocateStructure(sChunkyFile)
    If *this
      *this\vt                       = ?VT_ChunkyFile
      *this\Mode                     = #MODE_NONE
      *this\Handle                   = #Null
      *this\ChunkCRC								 = #Null
      *this\ChunkOffset              = -1
      *this\FileHdr\Flags            = 0
      *this\FileHdr\Header           = MakeHdr("####")
      *this\FileHdr\UserHeader       = MakeHdr("####")
      *this\ChunkHdr\CRC32           = 0
      *this\ChunkHdr\DataSize        = 0
      *this\ChunkHdr\Header          = MakeHdr("####")
      *this\ChunkHdr\NextChunkOffset = -1
      *this\ChunkOpen                = #False
      CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
      	*this\UseAES									 = #False
    		*this\AESKey									 = #Null
    		*this\AESBits									 = 0
    		*this\AESInitializationVector	 = #Null
    	CompilerEndIf
  
      ProcedureReturn *this
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  	Procedure.i EnableAES(*this.sChunkyFile)
  		If (*this\AESInitializationVector <> #Null) And (*this\AESKey <> #Null)
  			*this\UseAES = #True
  		Else
  			*this\UseAES = #False
  		EndIf
  
  		ProcedureReturn *this\UseAES
  	EndProcedure
  	Procedure   DisableAES(*this.sChunkyFile)
  		*this\UseAES = #False
  	EndProcedure
  	Procedure   FreeAES(*this.sChunkyFile)
  		DisableAES(*this)
    	If *this\AESInitializationVector : FreeMemory(*this\AESInitializationVector) : EndIf
    	If *this\AESKey									 : FreeMemory(*this\AESKey)									 : EndIf
    	*this\UseAES									= #False
    	*this\AESKey									= #Null
    	*this\AESBits									= 0
    	*this\AESInitializationVector	= #Null
  	EndProcedure
  	Procedure.i InitAES(*this.sChunkyFile, *key, *initializationvector, bits.i = 128)
  		FreeAES(*this)
  		If (*key <> #Null) And (*initializationvector <> #Null) And ((bits = 128) Or (bits = 192) Or (bits = 256))
  			*this\AESInitializationVector = AllocateMemory(16)
  			If *this\AESInitializationVector <> #Null
  				CopyMemory(*initializationvector, *this\AESInitializationVector, 16)
  				*this\AESKey = AllocateMemory(bits >> 3)
  				If *this\AESKey <> #Null
  					CopyMemory(*key, *this\AESKey, bits >> 3)
  					*this\AESBits = bits
  					*this\UseAES = #False
  
  					ProcedureReturn #True
  				EndIf
  				FreeMemory(*this\AESInitializationVector)
  				*this\AESInitializationVector = #Null
  			EndIf
  		EndIf
  		ProcedureReturn #False
  	EndProcedure
  CompilerEndIf
  Procedure.i CloseChunk(*this.sChunkyFile)
    If *this\ChunkOpen
    	If IsFile(*this\Handle)
    		*this\ChunkHdr\CRC32 					 = Val("$" + FinishFingerprint(*this\ChunkCRC))
        *this\ChunkHdr\NextChunkOffset = Loc(*this\Handle)
        FileSeek(*this\Handle, *this\ChunkOffset)
        WriteData(*this\Handle, @*this\ChunkHdr, SizeOf(sChunkHeader))
        FileSeek(*this\Handle, *this\ChunkHdr\NextChunkOffset)
        *this\ChunkOpen = #False
        *this\ChunkCRC  = #Null
        ProcedureReturn #True
      EndIf
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure   Close(*this.sChunkyFile)
    If IsFile(*this\Handle)
      If *this\Mode = #MODE_READ
        CloseFile(*this\Handle)
      ElseIf *this\Mode = #MODE_WRITE
        If *this\ChunkOpen : CloseChunk(*this) : EndIf
        CloseFile(*this\Handle)
      EndIf
    EndIf
  	*this\ChunkCRC 								 = #Null
    *this\Mode                     = #MODE_NONE
    *this\Handle                   = #Null
    *this\ChunkOffset              = -1
    *this\FileHdr\Flags            = 0
    *this\FileHdr\Header           = MakeHdr("####")
    *this\FileHdr\UserHeader       = MakeHdr("####")
    *this\ChunkHdr\CRC32           = 0
    *this\ChunkHdr\DataSize        = 0
    *this\ChunkHdr\Header          = MakeHdr("####")
    *this\ChunkHdr\NextChunkOffset = -1
    *this\ChunkOpen                = #False
  EndProcedure
  Procedure.i Open(*this.sChunkyFile, filename.s, hdr.s, mode.b)
    If IsFile(*this\Handle) : Close(*this) : EndIf
    If filename
      *this\Mode = mode
      If mode = #MODE_WRITE
        *this\Handle = CreateFile(#PB_Any, filename)
        If IsFile(*this\Handle)
        	*this\ChunkCRC								 = #Null
          *this\FileHdr\Flags            = 0
          *this\FileHdr\Header           = MakeHdr("AWCF")
          *this\FileHdr\UserHeader       = MakeHdr(hdr)
          *this\ChunkHdr\CRC32           = 0
          *this\ChunkHdr\DataSize        = 0
          *this\ChunkHdr\Header          = MakeHdr("####")
          *this\ChunkHdr\NextChunkOffset = -1
          WriteData(*this\Handle, @*this\FileHdr, SizeOf(sHeader))
          *this\ChunkOffset              = Loc(*this\Handle)
          *this\ChunkOpen                = #False
          ProcedureReturn #True
        EndIf
        *this\Handle = #Null
      ElseIf mode = #MODE_READ
        *this\Handle = ReadFile(#PB_Any, filename)
        If IsFile(*this\Handle)
          If ReadData(*this\Handle, @*this\FileHdr, SizeOf(sHeader)) = SizeOf(sHeader)
            If *this\FileHdr\UserHeader = MakeHdr(hdr)
              If Not Eof(*this\Handle)
                *this\ChunkOffset = Loc(*this\Handle)
                *this\ChunkOpen   = #False
                ProcedureReturn #True
              EndIf
            EndIf
          EndIf
          CloseFile(*this\Handle)
          *this\Handle = #Null
        EndIf
      EndIf
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i OpenChunk(*this.sChunkyFile, hdr.s)
    If IsFile(*this\Handle) And (*this\mode = #MODE_WRITE)
    	If *this\ChunkOpen : CloseChunk(*this) : EndIf
    	*this\ChunkCRC                 = StartFingerprint(#PB_Any, #PB_Cipher_CRC32)
      *this\ChunkHdr\Offset          = Loc(*this\Handle)
      *this\ChunkHdr\CRC32           = 0
      *this\ChunkHdr\DataSize        = 0
      *this\ChunkHdr\Header          = MakeHdr(hdr)
      *this\ChunkHdr\NextChunkOffset = -1
      *this\ChunkOffset              = *this\ChunkHdr\Offset
      *this\ChunkOpen                = #True
      WriteData(*this\Handle, @*this\ChunkHdr, SizeOf(sChunkHeader))
      ProcedureReturn #True
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i WriteData_(*this.sChunkyFile, *Binary, BinLen.i)
    If *Binary And IsFile(*this\Handle) And (*this\mode = #MODE_WRITE)
      If *this\ChunkOpen
        AddFingerprintBuffer(*this\ChunkCRC, *Binary, BinLen)
        *this\ChunkHdr\DataSize + BinLen
        If WriteData(*this\Handle, *Binary, BinLen) = BinLen
        	ProcedureReturn #True
       	EndIf
      EndIf
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  	Procedure.i WriteCipherData(*this.sChunkyFile, *Binary, BinLen.i)
  		Protected *dst, *src, srclen.i, res.i
  
  		res = #False
  		If *Binary And IsFile(*this\Handle) And (*this\mode = #MODE_WRITE)
  			If BinLen >= 16
  				*src   = *Binary
  				srclen = BinLen
  			Else
  				*src   = AllocateMemory(16)
  				srclen = 16
  				If *src <> #Null
  					FillMemory(*src, 16)
  					CopyMemory(*Binary, *src, BinLen)
  				EndIf
  			EndIf
  			If *src
  				*dst = AllocateMemory(srclen)
  				If *dst
  					If AESEncoder(*src, *dst, srclen, *this\AESKey, *this\AESBits, *this\AESInitializationVector) <> 0
  						res = WriteData_(*this, *dst, srclen)
  					EndIf
  					FreeMemory(*dst)
  				EndIf
  				If *src <> *Binary
  					FreeMemory(*src)
  				EndIf
  			EndIf
  		EndIf
  
  	  ProcedureReturn res
  	EndProcedure
  CompilerEndIf
  Procedure.i WriteQuad_(*this.sChunkyFile, value.q)
  	ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteLong_(*this.sChunkyFile, value.l)
    ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteWord_(*this.sChunkyFile, value.w)
    ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteByte_(*this.sChunkyFile, value.b)
    ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteDouble_(*this.sChunkyFile, value.d)
    ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteFloat_(*this.sChunkyFile, value.f)
    ProcedureReturn WriteData_(*this, @value, SizeOf(value))
  EndProcedure
  Procedure.i WriteBlock(*this.sChunkyFile, *src, slen.i, complevel.i)
    Protected hdr.sCompressedBlockHdr, *dst, dlen.i, scrc.l, dcrc.l
  
    If *src And slen
      If *this\ChunkOpen And IsFile(*this\Handle)
        dlen = 0
        *dst = #Null
        scrc = Val("$" + Fingerprint(*src, slen, #PB_Cipher_CRC32))		; Pr�fsumme der Originaldaten berechnen
        If complevel < AWZip::#COMP_NONE : complevel = AWZip::#COMP_NONE : EndIf
        If complevel > AWZip::#COMP_MAX  : complevel = AWZip::#COMP_MAX  : EndIf
        If complevel
          *dst = AWZip::PackMemory(*src, slen, @dlen, complevel)
        EndIf
        If (dlen = 0) Or (dlen > slen)         ; wenn nicht gepackt oder nach dem packen gr��er
          hdr\OriginalCRC32  = scrc
          hdr\OriginalLength = slen
          hdr\PackedCRC32    = scrc
          hdr\PackedLength   = slen
          hdr\Packed         = 0
  				CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  					If *this\UseAES : hdr\Cipher = 1 : Else : hdr\Cipher = 0 : EndIf
  				CompilerElse
  					hdr\Cipher = 0
  				CompilerEndIf
          WriteData_(*this, @hdr, SizeOf(sCompressedBlockHdr))
  				CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  					If *this\UseAES
  						WriteCipherData(*this, *src, slen)  ; data
  					Else
  						WriteData_(*this, *src, slen)  ; data
  					EndIf
  				CompilerElse
  					WriteData_(*this, *src, slen)  ; data
  				CompilerEndIf
        Else                                                ; erfolgreich komprimiert
        	dcrc = Val("$" + Fingerprint(*dst, dlen, #PB_Cipher_CRC32))	; Pr�fsumme der gepackten Daten berechnen
          hdr\OriginalCRC32  = scrc
          hdr\OriginalLength = slen
          hdr\PackedCRC32    = dcrc
          hdr\PackedLength   = dlen
          hdr\Packed         = 1
  				CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  					If *this\UseAES : hdr\Cipher = 1 : Else : hdr\Cipher = 0 : EndIf
  				CompilerElse
  					hdr\Cipher = 0
  				CompilerEndIf
          WriteData_(*this, @hdr, SizeOf(sCompressedBlockHdr))
  				CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  					If *this\UseAES
  		        WriteCipherData(*this, *dst, dlen)  ; data
  					Else
  		        WriteData_(*this, *dst, dlen)  ; data
  					EndIf
  				CompilerElse
  	        WriteData_(*this, *dst, dlen)  ; data
  				CompilerEndIf
        EndIf
  
        If *dst : FreeMemory(*dst) : EndIf
  
        ProcedureReturn #True
      EndIf
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i WriteString_(*this.sChunkyFile, str.s, format.b, complevel.i)
    Protected *buf, len.l, res.i, blen.i
  
    res = #False
    If str
      ; Speicher anfordern f�r den String im gew�nschten Format und anschlie�end inkl. Chr(0)-Abschluss in die Datei schreiben, falls gew�nscht gepackt
      len  = Len(str)
      blen = StringByteLength(str, format) + 2
      *buf = AllocateMemory(blen)
      If *buf
        PokeS(*buf, str, len, format)
        PokeW(*buf + blen - 2, 0)
        res = WriteLong_(*this, len)
        res & WriteByte_(*this, format)
        res & WriteBlock(*this, *buf, blen, complevel)
        FreeMemory(*buf)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i NextChunk(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And (*this\ChunkOffset > 0)
      FileSeek(*this\Handle, *this\ChunkOffset)
      If Not Eof(*this\Handle)
        ReadData(*this\Handle, @*this\ChunkHdr, SizeOf(sChunkHeader))
        *this\ChunkOffset = *this\ChunkHdr\NextChunkOffset
        *this\ChunkOpen   = #True
        ProcedureReturn #True
      EndIf
    EndIf
    *this\ChunkOffset = -1
    *this\ChunkOpen   = #False
    ProcedureReturn #False
  EndProcedure
  Procedure.s GetChunkHeader(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn DecodeHdr(*this\ChunkHdr\Header)
    EndIf
  
    ProcedureReturn ""
  EndProcedure
  Procedure.i GetChunkSize(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn *this\ChunkHdr\DataSize
    EndIf
  
    ProcedureReturn -1
  EndProcedure
  CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  	Procedure.i ReadCipherData(*this.sChunkyFile, *Buffer, BinLen.i)
  		Protected *dst, *src, srclen.i, res.i
  
  		res = #False
  		If IsFile(*this\Handle) And (*Buffer <> #Null) And (BinLen > 0)
  			If BinLen >= 16
  				*src   = AllocateMemory(BinLen)
  				*dst   = *Buffer
  				srclen = BinLen
  			Else
  				*src   = AllocateMemory(16)
  				*dst   = AllocateMemory(16)
  				srclen = 16
  			EndIf
  			If (*src <> #Null) And (*dst <> #Null)
  				If ReadData(*this\Handle, *src, srclen) = srclen
  					If AESDecoder(*src, *dst, srclen, *this\AESKey, *this\AESBits, *this\AESInitializationVector) <> 0
  						If *dst <> *Buffer : CopyMemory(*dst, *Buffer, BinLen) : EndIf
  						res = #True
  					EndIf
  				EndIf
  			EndIf
  			If *dst <> *Buffer
  				If *dst <> #Null : FreeMemory(*dst) : EndIf
  			EndIf
  			If *src <> #Null : FreeMemory(*src) : EndIf
  		EndIf
  
  	  ProcedureReturn res
  	EndProcedure
  CompilerEndIf
  Procedure.i ReadBlock(*this.sChunkyFile)
    Protected hdr.sCompressedBlockHdr, *src, *dst, slen.i, dlen.i, packed.b, pos.i, len.i, scrc.l, dcrc.l, crc.l, rlen.i
  
    *this\lastblocklen = -1
    *dst               = #Null
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ReadData(*this\Handle, @hdr, SizeOf(sCompressedBlockHdr))
      If hdr\Packed
        dlen = hdr\OriginalLength
        slen = hdr\PackedLength
        dcrc = hdr\OriginalCRC32
        scrc = hdr\PackedCRC32
        If slen > 0
        	*src = AllocateMemory(slen)
  	      If *src
  	      	If hdr\Cipher = 1
  	      		CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  	      			If *this\UseAES
  	      				If ReadCipherData(*this, *src, slen)
  	      					rlen = slen
  	      				Else
  	      					rlen = 0
  	      				EndIf
  	      			Else
  	      				rlen = 0
  	      			EndIf
  	      		CompilerElse
  	      			rlen = 0
  	      		CompilerEndIf
  	      	Else
  	      		rlen = ReadData(*this\handle, *src, slen)
  	      	EndIf
  
  	      	If rlen = slen
  	          crc = Val("$" + Fingerprint(*src, slen, #PB_Cipher_CRC32))
  	          If crc = scrc
  	            *dst = AWZip::UnpackMemory(*src, slen, @len)
  	            FreeMemory(*src)
  	            *src = #Null
  	          EndIf
  	        EndIf
  	      EndIf
        EndIf
      Else
        dlen = hdr\OriginalLength
        dcrc = hdr\OriginalCRC32
        If dlen > 0
        	*dst = AllocateMemory(dlen)
  	      *src = #Null
  	      If *dst
  	      	If hdr\Cipher = 1
  	      		CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
  	      			If *this\UseAES
  	      				If ReadCipherData(*this, *dst, dlen)
  	      					rlen = dlen
  	      				Else
  	      					rlen = 0
  	      				EndIf
  	      			Else
  	      				rlen = 0
  	      			EndIf
  	      		CompilerElse
  	      			rlen = 0
  	      		CompilerEndIf
  	      	Else
  	      		rlen = ReadData(*this\handle, *dst, dlen)
  	      	EndIf
  
  	      	If rlen = dlen
  	          len = dlen
  	        EndIf
  	      EndIf
  	    EndIf
  	  EndIf
  
      If (len = dlen) And (len <> 0) And (dlen <> 0)
        crc = Val("$" + Fingerprint(*dst, dlen, #PB_Cipher_CRC32))
        If crc = dcrc
          *this\lastblocklen = len
        Else
          FreeMemory(*dst)
          *dst = #Null
        EndIf
      Else
      	If *dst : FreeMemory(*dst) : EndIf
        *dst = #Null
      EndIf
    EndIf
  
    ProcedureReturn *dst
  EndProcedure
  Procedure.i GetBlockSize(*this.sChunkyFile)
    ProcedureReturn *this\LastBlockLen
  EndProcedure
  Procedure   FreeBlock(*this.sChunkyFile, *block)
    If *block
      FreeMemory(*block)
    EndIf
  EndProcedure
  Procedure.s ReadString_(*this.sChunkyFile)
    Protected res.s, format.b, slen.l, *src
  
    res = ""
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      slen   = ReadLong(*this\Handle)
      format = ReadByte(*this\Handle)
      *src   = ReadBlock(*this)
      If *src
        If slen > 0
          res = PeekS_(*src, slen, format)
        EndIf
        FreeBlock(*this, *src)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i ReadBlockDirect(*this.sChunkyFile, *buffer, bufsize.i)
    Protected res.i, slen.i, *src
  
    res = #False
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen And (*buffer <> #Null) And (bufsize > 0)
      *src   = ReadBlock(*this)
      If *src
      	slen = *this\LastBlockLen
      	If slen > bufsize : slen = bufsize : EndIf
      	CopyMemory(*src, *buffer, slen)
      	FreeBlock(*this, *src)
      	res = #True
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.q ReadQuad_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadQuad(*this\handle)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure.l ReadLong_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadLong(*this\handle)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure.w ReadWord_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadWord(*this\handle)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure.b ReadByte_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadByte(*this\handle)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure.d ReadDouble_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadDouble(*this\handle)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure.f ReadFloat_(*this.sChunkyFile)
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      ProcedureReturn ReadFloat(*this\handle)
    EndIf
  
  
    ProcedureReturn 0
  EndProcedure
  Procedure.i CheckChunkIntegrity(*this.sChunkyFile)
    Protected p.i, *src, scrc.l, res.b
  
    res = #False
    If IsFile(*this\Handle) And (*this\mode = #MODE_READ) And *this\ChunkOpen
      p = Loc(*this\Handle)
      FileSeek(*this\Handle, *this\ChunkHdr\Offset + SizeOf(sChunkHeader))
      *src = AllocateMemory(*this\ChunkHdr\DataSize)
      If *src
        ReadData(*this\Handle, *src, *this\ChunkHdr\DataSize)
        scrc = Val("$" + Fingerprint(*src, *this\ChunkHdr\DataSize, #PB_Cipher_CRC32))
        FreeMemory(*src)
        If scrc = *this\ChunkHdr\CRC32
          res = #True
        EndIf
      EndIf
      FileSeek(*this\Handle, p)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure   Destroy(*this.sChunkyFile)
    If IsFile(*this\Handle) : Close(*this) : EndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
    	FreeAES(*this)
    CompilerEndIf
    FreeStructure(*this)
  EndProcedure
  CompilerIf Defined(AWPBT_EnableChunkyFileJSON, #PB_Module)
    Procedure.i WriteJSON(*this.sChunkyFile, json.i, complevel.i = AWZip::#COMP_STORE)
      Protected res.i, *buf, bufsize.i, size.i
      
      res = #False
      If IsJSON(json)
        bufsize = ExportJSONSize(json)
        If bufsize > 0
          *buf = AllocateMemory(bufsize)
          If *buf <> #Null
            size = ExportJSON(json, *buf, bufsize)
            If size > 0
              res = WriteBlock(*this, *buf, size, complevel)
            EndIf
            FreeMemory(*buf)
          EndIf
        EndIf
      EndIf
      
      ProcedureReturn res
    EndProcedure
    Procedure.i ReadJSON(*this.sChunkyFile, json.i)
      Protected *buf, bufsize.i, res.i
      
      res  = 0
      *buf = ReadBlock(*this)
      If *buf <> #Null
        bufsize = GetBlockSize(*this)
        If bufsize > 0
          res = CatchJSON(json, *buf, bufsize)
        EndIf
        FreeBlock(*this, *buf)
      EndIf
      
      ProcedureReturn res
    EndProcedure
  CompilerEndIf
  CompilerIf Defined(AWPBT_EnableChunkyFileXML, #PB_Module)
    Procedure.i WriteXML(*this.sChunkyFile, xml.i, complevel.i = AWZip::#COMP_STORE)
      Protected res.i, *buf, bufsize.i, size.i
      
      res = #False
      If IsXML(xml)
        bufsize = ExportXMLSize(xml)
        If bufsize > 0
          *buf = AllocateMemory(bufsize)
          If *buf <> #Null
            size = ExportXML(xml, *buf, bufsize)
            If size > 0
              res = WriteBlock(*this, *buf, size, complevel)
            EndIf
            FreeMemory(*buf)
          EndIf
        EndIf
      EndIf
      
      ProcedureReturn res
    EndProcedure
    Procedure.i ReadXML(*this.sChunkyFile, xml.i)
      Protected *buf, bufsize.i, res.i
      
      res  = 0
      *buf = ReadBlock(*this)
      If *buf <> #Null
        bufsize = GetBlockSize(*this)
        If bufsize > 0
          res = CatchXML(xml, *buf, bufsize)
        EndIf
        FreeBlock(*this, *buf)
      EndIf
      
      ProcedureReturn res
    EndProcedure
  CompilerEndIf
    CompilerIf Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
      Procedure.i WriteCustomCrypt(*this.sChunkyFile, *lib.AWCustomCrypt::CustomCrypt, *src, slen.i, *userdata = #Null)
        Protected *enc, encsize.i, res.i
        
        res = #False
        If (*lib <> #Null) And (*src <> #Null) And (slen > 0)
          *enc = *lib\Encrypt(*src, slen, @encsize, *userdata)
          If *enc <> #Null
            If encsize > 0
              res = WriteBlock(*this, *enc, encsize, AWZip::#COMP_STORE)
            EndIf
            *lib\FreeBuffer(*enc, *userdata)
          EndIf
        EndIf
        
        ProcedureReturn res
      EndProcedure
      Procedure.i ReadCustomCrypt(*this.sChunkyFile, *lib.AWCustomCrypt::CustomCrypt, *userdata = #Null)
        Protected *buf, bufsize, *dec, decsize.i, *res
        
        *res = #Null
        If *lib <> #Null
          *buf = ReadBlock(*this)
          If *buf <> #Null
            bufsize = GetBlockSize(*this)
            If bufsize > 0
              *dec = *lib\Decrypt(*buf, bufsize, @decsize, *userdata)
              If *dec <> #Null
                If decsize > 0
                  *res = AllocateMemory(decsize)
                  If *res <> #Null
                    CopyMemory(*dec, *res, decsize)
                  EndIf
                EndIf
                *lib\FreeBuffer(*dec, *userdata)
              EndIf
            EndIf
            FreeBlock(*this, *buf)
          EndIf
        EndIf
        
        ProcedureReturn *res
      EndProcedure
      Procedure.i WriteCustomCryptString(*this.sChunkyFile, *lib.AWCustomCrypt::CustomCrypt, text.s, format.b = #PB_Ascii, *userdata = #Null)
        Protected *enc, encsize.i, res.i
        
        res = #False
        If text <> ""
          *enc = *lib\EncodeStr(text, @encsize, format, *userdata)
          If *enc <> #Null
            If encsize > 0
              res = WriteLong_(*this, Len(text))
              res & WriteByte_(*this, format)
              res & WriteBlock(*this, *enc, encsize, AWZip::#COMP_STORE)
            EndIf
            *lib\FreeBuffer(*enc, *userdata)
          EndIf
        EndIf
        
        ProcedureReturn res
      EndProcedure
      Procedure.s ReadCustomCryptString(*this.sChunkyFile, *lib.AWCustomCrypt::CustomCrypt, *userdata = #Null)
        Protected *buf, bufsize, *dec, decsize.i, res.s, length.l, format.b
        
        res = ""
        If *lib <> #Null
          length = ReadLong_(*this)
          format = ReadByte_(*this)
          *buf   = ReadBlock(*this)
          If *buf <> #Null
            bufsize = GetBlockSize(*this)
            If bufsize > 0
              *dec = *lib\Decrypt(*buf, bufsize, @decsize, *userdata)
              If *dec <> #Null
                If decsize > 0
                  res = PeekS_(*dec, length, format)
                EndIf
                *lib\FreeBuffer(*dec, *userdata)
              EndIf
            EndIf
            FreeBlock(*this, *buf)
          EndIf
        EndIf
        
        ProcedureReturn res
      EndProcedure
    CompilerEndIf
  
  DataSection ;{ Virtual Function Table
    VT_ChunkyFile:
      Data.i @Destroy()
      Data.i @CloseChunk()
      Data.i @Close()
      Data.i @Open()
      Data.i @OpenChunk()
      Data.i @WriteQuad_()
      Data.i @WriteLong_()
      Data.i @WriteWord_()
      Data.i @WriteByte_()
      Data.i @WriteDouble_()
      Data.i @WriteFloat_()
      Data.i @WriteBlock()
      Data.i @WriteString_()
      Data.i @NextChunk()
      Data.i @GetChunkHeader()
      Data.i @GetChunkSize()
      Data.i @ReadBlock()
      Data.i @GetBlockSize()
      Data.i @ReadString_()
      Data.i @ReadBlockDirect()
      Data.i @ReadQuad_()
      Data.i @ReadLong_()
      Data.i @ReadWord_()
      Data.i @ReadByte_()
      Data.i @ReadDouble_()
      Data.i @ReadFloat_()
      Data.i @CheckChunkIntegrity()
      Data.i @FreeBlock()
      CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
      	Data.i @InitAES()
      	Data.i @FreeAES()
      	Data.i @EnableAES()
      	Data.i @DisableAES()
      CompilerEndIf
      CompilerIf Defined(AWPBT_EnableChunkyFileJSON, #PB_Module)
        Data.i @WriteJSON()
        Data.i @ReadJSON()
      CompilerEndIf
      CompilerIf Defined(AWPBT_EnableChunkyFileXML, #PB_Module)
        Data.i @WriteXML()
        Data.i @ReadXML()
      CompilerEndIf
      Data.i @WriteData_()
      CompilerIf Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
      	Data.i @WriteCipherData()
      	Data.i @ReadCipherData()
      CompilerEndIf
      CompilerIf Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
        Data.i @WriteCustomCrypt()
        Data.i @ReadCustomCrypt()
        Data.i @WriteCustomCryptString()
        Data.i @ReadCustomCryptString()
      CompilerEndIf
  EndDataSection
EndModule
  
	
	
; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 764
; FirstLine = 755
; Folding = --------------
; EnableUnicode
; EnableXP