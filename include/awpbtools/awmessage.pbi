;{   awmessage.pbi
;    Version 0.12 [2015/10/26]
;    Copyright (C) 2014-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.

XIncludeFile "awsupport.pbi"
XIncludeFile "awregexsup.pbi"
XIncludeFile "awchunkyfile.pbi"
XIncludeFile "awdate.pbi"
XIncludeFile "awcode.pbi"
XIncludeFile "awcharset.pbi"

DeclareModule AWMessage
  ;{ Constants
  ; Priorities
  Enumeration 0
    #PRIO_NONE             ;* keine Priorit�t
    #PRIO_HIGHEST          ;* h�chste Priorit�t
    #PRIO_HIGH             ;* hohe Priorit�t
    #PRIO_NORMAL           ;* normale Priorit�t
    #PRIO_LOW              ;* niedrige Priorit�t
    #PRIO_LOWEST           ;* niedrigste Priorit�t
  EndEnumeration
  
  ; Content-Types
  Enumeration 0
    #TYPE_AUTO
    #TYPE_PLAIN
    #TYPE_HTML
    #TYPE_MP_RELATED                      ;* multipart/related als Content-Type
    #TYPE_MP_MIXED                        ;* multipart/mixed als Content-Type
    #TYPE_MP_ALTERNATIVE                  ;* multipart/alternative als Content-Type
    #TYPE_BINARY
    #TYPE_ATTACHMENT
    #TYPE_INLINE
  EndEnumeration
  
  ; Encodings
  Enumeration 0
    #ENCODE_AUTO
    #ENCODE_QP
    #ENCODE_B64
  EndEnumeration
  
  ; Header-Fields
  Enumeration 1
    #HDR_SUBJECT
    #HDR_FROM
    #HDR_FROM_MAIL
    #HDR_FROM_MAIL_AB
    #HDR_CC
    #HDR_CC_MAILS
    #HDR_CC_MAILS_AB
    #HDR_TO
    #HDR_TO_MAILS
    #HDR_TO_MAILS_AB
    #HDR_BCC
    #HDR_BCC_MAILS
    #HDR_BCC_MAILS_AB
    #HDR_RETURNPATH
    #HDR_MESSAGEID
    #HDR_DATE
    #HDR_DISPOSITIONNOTIFICATIONTO
    #HDR_REPLYTO
    #HDR_CONTENTTYPE
    #HDR_CONTENTSUBTYPE
    #HDR_CONTENTCHARSET
    #HDR_CONTENTTRANSFERENCODING
    #HDR_BOUNDARY
    #HDR_CONTENTDISPOSITION
    #HDR_CONTENTNAME
    #HDR_CONTENTFILENAME
    #HDR_CONTENTID
    #HDR_XPRIORITY
    #HDR_FULLCONTENTTYPE
  EndEnumeration
  
  #CRLF 											   = Chr(13) + Chr(10)
  ;}
  
  Structure HeaderField             ; HeaderField
    key.s                           ; Key, also das was vorm : bezw vom = steht
    value.s                         ; der jeweilige Eintrag, max bis zum ;
  EndStructure
  Structure Headers
    Map dat.HeaderField()          ; Eine Headerzeile = eine Map von Eintr�gen
  EndStructure
  Structure MimePart
    Map  Header.Headers()           ; Message Header (alle Headerzeilen dieses Parts)
    List SubPart.MimePart()        ; Message SubParts (sofern multipart, alle subparts darin)
    body.i                          ; Pointer to BodyData, RAW - including all CR/LF
    bodylen.i                       ; Length of BodyData in bytes (including all CR/LF)
    justptr.b                       ; if false FreeMem, if true body is part of msg-src
  EndStructure
  Structure sMsg Extends MimePart
    src.i                           ; source, ascii-format, 1 byte / char, with CR/LF
    bcc.s                           ; BCC Empf�ngerliste
    decoded.b                       ; Flag, #True wenn dekodiert, sonst #False
    mhdecoded.b                     ; Flag, #True wenn Mainheader dekodiert, sonst #False
  EndStructure
  Structure AttList
    *Next.AttList
    *Att.MimePart
  EndStructure
  
  Interface Message
    Destroy                 ()
    Clear                   ()
    Decode.i                ()
    DecodeHeader.i          ()
    BuildAttachmentList.i   (*part.MimePart = #Null)
    FreeAttachmentList      (*AttList.AttList)
    Encode.i                ()
    SetBodyBinary.i         (*src, srclen.i, name.s, filename.s, id.s = "", *part.MimePart = #Null)
    SetBodyText.i           (body.s, type.i, charset.s = "", encode.i = #ENCODE_AUTO, *part.MimePart = #Null)
    SetMultipart            (type.i, *part.MimePart = #Null)
    AddMultipart.i          (type.i, *part.MimePart = #Null)
    AddPartHTML.i           (html.s, charset.s = "", encode.i = #ENCODE_AUTO, *part.MimePart = #Null)
    AddPartText.i           (text.s, charset.s = "", encode.i = #ENCODE_AUTO, *part.MimePart = #Null)
    AddPartBinary.i         (*src, srclen.i, name.s, filename.s, id.s = "", *part.MimePart = #Null)
    AddPartBinaryFile.i     (name.s, filename.s, id.s = "", *part.MimePart = #Null)
    SetMailSource.i         (src.s)
    SetMailSourceBin.i      (*src, srclen.i)
    GetMailSource.s         ()
    LoadEML.i               (filename.s)
    SaveEML.i               (filename.s)
    Load.i                  (filename.s)
    Save.i                  (filename.s, complevel.i = AWZip::#COMP_MAX)
    GetHeaderField.s        (field.i, *part.MimePart = #Null)
    SetHeaderField          (field.i, value.s, *part.MimePart = #Null)
    SetContentType          (contenttype.s, contentsubtype.s, *part.MimePart = #Null)
    FindPartCID.i           (cid.s, casesensitive.i = #False, *part.MimePart = #Null)
    FindPartName.i          (name.s, casesensitive.i = #False, *part.MimePart = #Null)
    FindPartFileName.i      (filename.s, casesensitive.i = #False, *part.MimePart = #Null)
    GetDate.i               (*part.MimePart = #Null)
    SetDate                 (datevalue.i, *part.MimePart = #Null)
    SetDateNow              (*part.MimePart = #Null)
    GetXPriority.i          (*part.MimePart = #Null)
    SetXPriority            (priority.i, *part.MimePart = #Null)
    GetHTML.s               ()
    GetPlain.s              ()
    GetPartAsString.s       (*part.MimePart)
    GetPartAsBin.i          (*part.MimePart, *partlen)
    SavePartToFile.i        (*part.MimePart, filename.s)
    GetPlainAsHTML.s        ()
    GetSize.i               () 
    CompilerIf Defined(AWPBT_EnableMessageAES, #PB_Module)
      LoadAESCrypted.i      (filename.s, *key, *initializationvector, bits.i = 128)
    	SaveAESCrypted.i      (filename.s, *key, *initializationvector, bits.i = 128, complevel.i = AWZip::#COMP_MAX)
  	CompilerEndIf
  	GetMailSourceBin.i      ()
    IsDecoded.i             ()
    IsHeaderDecoded.i       ()
    ClearPart               (*msgpart.MimePart)
    GetHeaderValue.s        (*msgpart.MimePart, Key1.s, Key2.s)
    GetHeaderData.s         (*msgpart.MimePart, Key.s)
    AnalyzeMimePart.i       (*msgpart.MimePart, *src, maxlen.i, onlyheader.i = #False)
    AddHeader               (*msgpart.MimePart, hdr.s, value.s)
    AddHeaderValue          (*msgpart.MimePart, hdr.s, key.s, value.s)
    BuildHeader.s           (*msgpart.MimePart)
    CalcHeaderSize.i        (*msgpart.MimePart)
    CalcSize.i              (*msgpart.MimePart)
    BuildPart.i             (*msgpart.MimePart, *partlen, addbufsize.i)
    GetHTMLPart.i           (*part.MimePart)
    GetPlainPart.i          (*part.MimePart)
  EndInterface
  
  Declare.i New()
EndDeclareModule

Module AWMessage
  EnableExplicit
  
  Global LastCharset.s
  
  Structure sMessage
    *vt.Message
    *msg.sMsg
  EndStructure
    
  Procedure.s GenBoundary()         ; Boundary-Generator f�r Multipart-Mails
    Protected dt.i, bnd.s
  
    ; die Boundary erstellen
    ; Format: Boundary_YYYMDDhhmmss_RND_
    ; YYY Jahr, M Monat, DD Tag, hh Stunde, mm Minute, ss Sekunde - jeweils in HEX-Format mit entsprechender Zeichenanzahl
    ; RND zuf�llige 3-Stellige HEX-Zahl) zwischen 000 und FFF
    ; dadurch wird gesichert, das in einer Mail, die mehrere Parts enth�lt, jeder Part eine eigene Boundary erh�lt
  
    dt  = Date()
    bnd = "Boundary_" + RSet(Hex(Year(dt)), 3, "0") + RSet(Hex(Month(dt)), 1) + RSet(Hex(Day(dt)), 2, "0") + RSet(Hex(Hour(dt)), 2, "0") + RSet(Hex(Minute(dt)), 2, "0") + RSet(Hex(Second(dt)), 2, "0") + "_" + RSet(Hex(Random($FFF)), 3, "0") + "_"
  
    ProcedureReturn bnd
  EndProcedure
  Procedure.i NeedQuotes(txt.s)                                   ; Test, ob der �bergebene String in "" eingeschlossen werden muss
    Protected c.c, *a, i.i, l.i
  
    *a = @txt
    l  = Len(txt)
    If (PeekC(*a) = 34) And (PeekC(*a + l - 1) = 34)
      ; already surrounded by quotes
      ProcedureReturn #False
    EndIf
    For i = 1 To l
      c = PeekC(*a) : *a + SizeOf(c)
      If Not (((c >= 'A') And (c <= 'Z')) Or ((c >= 'a') And (c <= 'z')) Or ((c >= '0') And (c <= '9')))
        ProcedureReturn #True
      EndIf
    Next
  
    ProcedureReturn #False
  EndProcedure
  Procedure.s AddQuotesIfNeeded(txt.s)                            ; falls der �bergebene String in "" eingeschlossen werden muss dieses tun und das ergebnis zur�ckgeben
    If NeedQuotes(txt)
      txt = Chr(34) + txt + Chr(34)
    EndIf
    ProcedureReturn txt
  EndProcedure
  Procedure.i CountCharsToEncode(text.s, encodews.i = #True)      ; Anzahl der Zeichen, die QP-Encoded werden m�ssen ermitteln
    Protected *b, i.i, l.i, c.c, cnt.i
  
    l   = Len(text)
    *b  = @text
    cnt = 0
    For i = 1 To l
      c = PeekC(*b) : *b + 1
      If (c < 33) Or (c > 126) Or (c = 61) Or (c = '!') Or (c = '"') Or (c = '#') Or (c = '$') Or (c = '@') Or (c = '[') Or (c = '\') Or (c = ']') Or (c = '^') Or (c = '`') Or (c = '{') Or (c = '|') Or (c = '}') Or (c = '~')
        If (c = 9) Or (c = 32)
          If (PeekC(*b) = $0d) Or (PeekC(*b - 2) = $0a) Or encodews
            cnt + 1
          EndIf
        Else
          If (c <> 10) And (c <> 13)
            cnt + 1
          EndIf
        EndIf
      EndIf
    Next i
  
    ProcedureReturn cnt
  EndProcedure
  Procedure.i MaxCharCountForQPLineLen(text.s, linelen.i, startpos.i, encodews.i = #True)   ; maximale Anzahl der Zeichen ermitteln, die in eine Zeile codiert werden k�nnen
    Protected *b, len.i, c.c, cnt.i, tl.i
  
    tl  = Len(text)
    cnt = 0
    If startpos < tl
      *b  = @text + ((startpos - 1) * SizeOf(c))
      len = 0
      tl  - startpos + 1
      While tl > 0
        c = PeekC(*b)
        If (c < 33) Or (c > 126) Or (c = 61) Or (c = '!') Or (c = '"') Or (c = '#') Or (c = '$') Or (c = '@') Or (c = '[') Or (c = '\') Or (c = ']') Or (c = '^') Or (c = '`') Or (c = '{') Or (c = '|') Or (c = '}') Or (c = '~')
          ; encode char
          If (c = 9) Or (c = 32)            ; need only be encoded before/after CRLF
            If (PeekC(*b) = $0d) Or (PeekC(*b - 2) = $0a) Or encodews
              len + 3
            Else
              len + 1
            EndIf
          Else
            If (c = 10) Or (c = 13)
              len + 1
            Else
              len + 3
            EndIf
          EndIf
        Else
          len + 1
        EndIf
        If len < linelen : cnt + 1 : Else : Break : EndIf
        *b + SizeOf(Character)
        tl - 1
      Wend
    EndIf
    ProcedureReturn cnt
  EndProcedure
  Procedure.i SingleDataExpected(hdrname.s)         ; Header testen, ob mehrere Daten (mit ; getrennte key=value-Paare) erwartet werden (#False) oder nicht (#True)
    ;- probably incomplete
    If hdrname = "CONTENT-TYPE"
      ProcedureReturn #False
    ElseIf hdrname = "CONTENT-DISPOSITION"
      ProcedureReturn #False
  
    EndIf
    ProcedureReturn #True
  EndProcedure
  Procedure.i NeedEncoding(text.s, ignoretab.i = #True, ignorespc.i = #True)            ; Test, ob der Text kodiert werden muss, also ob er f�r 7-Bit eMails ung�ltige Zeichen enth�lt
    Protected *a, i.i, c.c
  
    *a = @text
    If ignoretab And ignorespc
      For i = 1 To Len(text)
        c = PeekC(*a)    
        If (c > 126) Or ((c < 32) And (c <> 9)) Or (c = 34)
          ProcedureReturn #True
        EndIf
        *a + SizeOf(c)
      Next
    ElseIf ignoretab And Not ignorespc
      For i = 1 To Len(text)
        c = PeekC(*a)  
        If (c > 126) Or ((c < 33) And (c <> 9)) Or (c = 34)
          ProcedureReturn #True
        EndIf
        *a + SizeOf(c)
      Next
    ElseIf Not ignoretab And ignorespc
      For i = 1 To Len(text)
        c = PeekC(*a)    
        If (c > 126) Or (c < 32) Or (c = 34)
          ProcedureReturn #True
        EndIf
        *a + SizeOf(c)
      Next
    Else
      For i = 1 To Len(text)
        c = PeekC(*a)
        If (c > 126) Or (c < 33) Or (c = 34)
          ProcedureReturn #True
        EndIf
        *a + SizeOf(c)
      Next
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.s EncodeInline(text.s, charset.s = "", encode.i = #ENCODE_AUTO, ignoretab.i = #False, ignorespc.i = #False) ; Inline-Encoder; erzeugt =?...?.?...?= Strings sofern n�tig
    Protected res.s, enclen.i, *encbin, a.i, f.s, tlen.i, lines.i, llen.i, codelen.i, pos.i, txt.s, fl.i
    If encode = #ENCODE_AUTO
      If NeedEncoding(text, ignoretab, ignorespc)
        encode = #ENCODE_QP
      EndIf
    EndIf
  
    If charset = #Null$ : charset = AWCharset::GetSysCharsetName() : EndIf
    codelen = Len(charset) + 7
    pos     = 1
    tlen    = Len(text)
    res     = ""
    fl      = #True
  
    While pos <= tlen
      Select encode
        Case #ENCODE_AUTO
          llen = 60
          If llen + pos > tlen
            txt = Mid(text, pos)
            pos = tlen + 1
          Else
            txt = Mid(text, pos, llen)
            pos + llen
          EndIf
        Case #ENCODE_B64
          llen = 40 - codelen
          If llen + pos > tlen
            txt = Mid(text, pos)
            pos = tlen + 1
          Else
            txt = Mid(text, pos, llen)
            pos + llen
          EndIf
          *encbin = AWCode::EncodeBase64Str(txt, Len(txt), @enclen, #PB_Any)
          If *encbin
            txt = "=?" + charset + "?B?" + AWSupport::TrimWS(PeekS(*encbin, enclen, #PB_Ascii)) + "?="
            FreeMemory(*encbin)
          EndIf
        Case #ENCODE_QP
          llen = MaxCharCountForQPLineLen(text, 60 - codelen, pos)
          If llen
            txt  = Mid(text, pos, llen)
            pos  + llen
            *encbin = AWCode::EncodeQuotedPrintableStr(txt, Len(txt), @enclen, #PB_Any, #True)
            If *encbin
              txt = "=?" + charset + "?Q?" + AWSupport::TrimWS(ReplaceString(PeekS(*encbin, enclen, #PB_Ascii), Chr(32), "_")) + "?="
              FreeMemory(*encbin)
            EndIf
          Else
            txt  = Mid(text, pos)
          EndIf
      EndSelect
      If fl
        fl = #False
        res + txt
      Else
        res + Chr(13) + Chr(10) + Chr(9) + txt
      EndIf
    Wend
    ProcedureReturn res
  EndProcedure
  Procedure.s DecodeInline(txt.s) ; Decoder f�r Inline-Encodiertes ( =?charset?c?daten?= )
    Shared LastCharset
    Protected charset.s, codec.s, p1.i, p2.i, p3.i, p4.i, buf.s, blen.i, tmp.s
  
    ; wird nach jedem dekodierten block rekursiv aufgerufen, da mehr als ein block enthalten sein kann
    ; =?CHARSET?CODIERUNG?TEXT?=
    ; Codierung = B f�r Base64 oder Q f�r Quoted Printable
    LastCharset = ""
    If txt
      p1 = FindString(txt, "=?", 1)                           ; =? suchen
      If p1                                                   ; gefunden, string enth�lt kodierten bereich
        p2   = FindString(txt, "?", p1 + 2)                   ; ? suchen
        If p2                                                 ; gefunden
          LastCharset = Mid(txt, p1 + 1, p2 - p1 - 1)  ; Charset auslesen (wird aktuell noch nicht benutzt)
          p3      = FindString(txt, "?", p2 + 1)              ; ? suchen
          If p3                                               ; gefunden
            codec = UCase(Mid(txt, p2 + 1, p3 - p2 - 1))      ; Codec auslesen
            p4    = FindString(txt, "?=", p3 + 1)             ; Ende des kodierten Textes suchen
            If p4                                             ; gefunden
              blen = p4 - p3 - 1                              ; L�nge des Textes
              If codec = "B"
                buf  = Space(blen)
                PokeS(@buf, Mid(txt, p3 + 1, blen), blen, #PB_Ascii)    ; Text als ASCII auslesen und dekodieren (Base64)
                ProcedureReturn AWCode::DecodeBase64Str(@buf, blen) + DecodeInline(Mid(txt, p4 + 2))      ; dekodierten Text mit dem dekodierten Rest der Zeile zur�ckgeben
              ElseIf codec = "Q"
                buf  = Space(blen)
                PokeS(@buf, ReplaceString(Mid(txt, p3 + 1, blen), "_", " "), blen, #PB_Ascii)   ; Text als ASCII auslesen und dekodieren (QuotedPrintable)
                ProcedureReturn AWCode::DecodeQuotedPrintableStr(@buf, blen) + DecodeInline(Mid(txt, p4 + 2))    ; dekodierten Text mit dem dekodierten Rest der Zeile zur�ckgeben
              Else
                ProcedureReturn Mid(txt, p3 + 1, blen) + DecodeInline(Mid(txt, p4 + 2))    ; unbekannter codec, Text mit dem dekodierten Rest der Zeile zur�ckgeben
              EndIf
            EndIf
          EndIf
        EndIf
      EndIf
    EndIf
  
    ProcedureReturn txt
  EndProcedure
  Procedure.s GetLastInlineCharset()
    ProcedureReturn LastCharset
  EndProcedure
  
  Procedure.i New()
    Protected *this.sMessage
  
    *this = AllocateStructure(sMessage)
    If *this
      *this\vt  = ?Message_VT
      *this\msg = AllocateStructure(sMsg)
      If *this\msg
        *this\msg\bcc       = ""
        *this\msg\decoded   = #False
        *this\msg\mhdecoded = #False
        *this\msg\src       = #Null
  
        ProcedureReturn *this
      EndIf
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure   ClearPart(*this.sMessage, *msgpart.MimePart)                                  ; Messagestruktur (ein Part + seine subparts) bereinigen
    Protected bl.i
  
    If *msgpart
      ForEach *msgpart\Header()
        ForEach *msgpart\Header()\dat()
          *msgpart\Header()\dat()\key   = ""
          *msgpart\Header()\dat()\value = ""
        Next
        ClearMap(*msgpart\Header()\dat())
      Next
      ClearMap(*msgpart\Header())
      ForEach *msgpart\SubPart()
        ClearPart(*this, *msgpart\SubPart())
      Next
      ClearList(*msgpart\SubPart())
      If Not *msgpart\justptr
        If *msgpart\body
          FreeMemory(*msgpart\body)
          *msgpart\body = #Null
        EndIf
      EndIf
      *msgpart\bodylen = 0
    EndIf
  EndProcedure
  Procedure   Clear(*this.sMessage)
    ClearPart(*this, *this\msg)
    If *this\msg\src : FreeMemory(*this\msg\src) : *this\msg\src = #Null : EndIf
    *this\msg\bcc       = ""
    *this\msg\decoded   = #False
    *this\msg\mhdecoded = #False
  EndProcedure
  Procedure.s GetHeaderValue(*this.sMessage, *msgpart.MimePart, Key1.s, Key2.s)
    Protected res.s
  
    res = ""
    If Not *msgpart : *msgpart = *this\msg : EndIf
    If FindMapElement(*msgpart\Header(), Key1)
      If FindMapElement(*msgpart\Header()\dat(), Key2)
        res = *msgpart\Header()\dat()\value
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.s GetHeaderData(*this.sMessage, *msgpart.MimePart, Key.s)
    ProcedureReturn GetHeaderValue(*this, *msgpart, Key, Key)
  EndProcedure
  Procedure.i AnalyzeMimePart(*this.sMessage, *msgpart.MimePart, *src, maxlen.i, onlyheader.i = #False)            ; Message-Sub-Part zerlegen
    Protected *start, *p, *ende, *lstart, *lende, *dstart, *dende, *d, *ep, *dstart2, *bodystart, *bodyend, *boundary, *bstart, *bende, *safe
    Protected len.i, c.a, pos.i, llen.i, tmp.i, blen.i
    Protected hdrname.s, hdrdatal.s, hdrdatar.s, boundary.s, test.c, firstbound.i, lastbound.i, res.i, t.l, *m.Headers, uhdrname.s, uhdrdatal.s
  
    ; Header ermitteln
    ; -> CRLFCRLF suchen (Leerzeile)
    res      = #False
    *start   = *src                                      ; *start == start des headers
    len      = -1
    *bodyend = *src + maxlen - 1
    *ende    = #Null
    For pos = 1 To maxlen
      If PeekL(*src) = $0a0d0a0d
        len   = *src - *start + 2                       ; len    == l�nge des headers (inkl. einem CRLF)
        *ende = *src + 1                                ; *ende  == letztes Zeichen des headers
        *src  + 4                                       ; *src   == start des n�chsten Blockes nach der Leerzeile
        Break
      EndIf
      *src + 1
    Next pos
    *bodystart = *src
  
    ; Header parsen
    *lstart = *start                                    ; *lstart == anfang der zeile
    For *p = *start To *ende
      If PeekW(*p) = $0a0d
        *p + 2                                          ; *p      == start der n�chsten zeile
        c = PeekA(*p)
        If (c <> 9) And (c <> 32)                       ; KEIN zeilenumbruch
          *lende = *p - 1                               ; *lende  == letztes zeichen der zeile
          llen   = *lende - *lstart + 1                 ; llen    == l�nge der zeile
          For *dstart = *lstart To *lende
            If PeekA(*dstart) = Asc(":")
              *dstart + 1
              Break
            EndIf
          Next *dstart
          hdrname  = AWSupport::TrimWS(PeekS(*lstart, *dstart - *lstart - 1, #PB_Ascii))
          uhdrname = UCase(hdrname)
          *dstart + 1
          With *msgpart\header(uhdrname)
            If SingleDataExpected(uhdrname)             ; keine unterteilung
              hdrdatar             = AWSupport::TrimWS(PeekS(*dstart, *lende - *dstart, #PB_Ascii))
              \dat(uhdrname)\key   = hdrname
              \dat(uhdrname)\value = hdrdatar
            Else                                          ; parameter sammeln
              *ep      = #Null
              *dstart2 = *dstart
              For *d = *dstart To *lende
                c = PeekA(*d)
                If c = 34
                  Repeat : *d + 1 : Until PeekA(*d) = 34
                ElseIf (c = Asc("=")) And (*ep = #Null)
                  *ep = *d
                ElseIf (c = Asc(";")) Or ((c = $0d) And ((*ep <> #Null) Or (*d = *lende - 1)))
                  If *ep                                ; = gefunden
                    hdrdatal              = AWSupport::TrimWS(PeekS(*dstart2, *ep - *dstart2, #PB_Ascii))
                    uhdrdatal             = UCase(hdrdatal)
                    hdrdatar              = AWSupport::TrimWSQ(PeekS(*ep + 1, *d - *ep - 1, #PB_Ascii))
                    *ep                   = #Null
                    \dat(uhdrdatal)\key   = hdrdatal
                    \dat(uhdrdatal)\value = hdrdatar
                  Else
                    hdrdatar              = AWSupport::TrimWSQ(PeekS(*dstart, *d - *dstart2, #PB_Ascii))
                    \dat(uhdrname)\key    = hdrname
                    \dat(uhdrname)\value  = hdrdatar
                  EndIf
                  *d       + 1
                  *dstart2 = *d
                EndIf
              Next *d
            EndIf
          EndWith
          *lstart = *p                                  ; *lstart == anfang der n�chsten zeile
        EndIf
      EndIf
    Next *p
    
    If onlyheader : ProcedureReturn #True : EndIf
    
    boundary = GetHeaderValue(*this, *msgpart, "CONTENT-TYPE", "BOUNDARY")
    If boundary ; erste boundary suchen
      blen       = Len(boundary)
      *boundary  = AllocateMemory(blen + 1)
      If *boundary = #Null : ProcedureReturn #False : EndIf
      PokeS(*boundary, boundary, blen, #PB_Ascii)
      *bstart = *bodystart
      For *d = *bodystart To *bodyend
        If PeekW(*d) = $2d2d
          *bende = *d - 1
          If CompareMemoryString(*d + 2, *boundary, 0, blen, #PB_Ascii) = #PB_String_Equal
            *safe  = *d
            *d     + 2
            *d     + blen
            If PeekW(*d) = $2d2d        ; das war die letzte boundary
              If PeekW(*d + 2) = $0a0d  ; und der zeilenumbruch danach; wenn nicht, kann es nicht die boundary sein
                *msgpart\body    = *bstart
                *msgpart\bodylen = *bende - *bstart + 2
                *msgpart\justptr = #True
                *bstart      = *d + 4
                res          = #True
                Break
              Else
                *d = *safe
              EndIf
            ElseIf PeekW(*d) = $0a0d    ; normaler Zeilenumbruch; wenn nicht, kann es nicht die boundary sein
              *msgpart\body    = *bstart
              *msgpart\bodylen = *bende - *bstart + 2
              *msgpart\justptr = #True
              *bstart      = *d + 2
              res          = #True
              Break
            Else
              *d = *safe
            EndIf
          EndIf
        EndIf
      Next *d
      *bodystart = *bstart  ; *bodystart == erstes zeichen nach der boundary
  
      For *d = *bodystart To *bodyend
        If PeekW(*d) = $2d2d
          *bende = *d - 1
          If CompareMemoryString(*d + 2, *boundary, 0, blen, #PB_Ascii) = #PB_String_Equal
            *safe  = *d
            *d     + 2
            *d     + blen
            If PeekW(*d) = $2d2d        ; das war die letzte boundary
              If PeekW(*d + 2) = $0a0d  ; und der zeilenumbruch danach; wenn nicht, kann es nicht die boundary sein
                AddElement(*msgpart\SubPart())
                res = AnalyzeMimePart(*this, @*msgpart\SubPart(), *bstart, *bende - *bstart + 2)
                *bstart      = *d + 4
              Else
                *d = *safe
              EndIf
            ElseIf PeekW(*d) = $0a0d    ; normaler Zeilenumbruch; wenn nicht, kann es nicht die boundary sein
              AddElement(*msgpart\SubPart())
              res = AnalyzeMimePart(*this, @*msgpart\SubPart(), *bstart, *bende - *bstart + 2)
              *bstart = *d + 2
            Else
              *d = *safe
            EndIf
          EndIf
        EndIf
      Next *d
    Else
      ; keine boundary, rest als body speichern
      *msgpart\body    = *bodystart
      *msgpart\bodylen = *bodyend - *bodystart - 2
      *msgpart\justptr = #True
    EndIf
  
    ProcedureReturn #True
  EndProcedure
  Procedure.i Decode(*this.sMessage)                                                                 ; Message zerlegen
    If Not *this\msg\decoded    
      *this\msg\decoded   = AnalyzeMimePart(*this, *this\msg, *this\msg\src, MemorySize(*this\msg\src))
      *this\msg\mhdecoded = *this\msg\decoded 
    EndIf
    
    ProcedureReturn *this\msg\decoded
  EndProcedure
  Procedure.i DecodeHeader(*this.sMessage)                                                                 ; Message zerlegen (nur den ersten header)
    If Not *this\msg\mhdecoded
      *this\msg\mhdecoded = AnalyzeMimePart(*this, *this\msg, *this\msg\src, MemorySize(*this\msg\src), #True)
    EndIf
    
    ProcedureReturn *this\msg\mhdecoded
  EndProcedure
  Procedure.i BuildAttachmentList(*this.sMessage, *part.MimePart)                           ; eine Liste aller speicherbaren Attachments erstellen (Inline-Sachen wie z.B. bilder in gmx-werbungen werden hier nicht eingef�gt)
    Protected cd.s, *al.AttList, *al2.AttList, *al3.AttList
    If Not *part : *part = *this\msg : EndIf
    cd  = GetHeaderData(*this, *part, "CONTENT-DISPOSITION")
    *al = #Null
    If UCase(cd) = "ATTACHMENT"
      *al = AllocateMemory(SizeOf(AttList))
      If *al
        *al\Att  = *part
        *al\Next = #Null
      Else
        ProcedureReturn #Null
      EndIf
    Else
      ForEach *part\SubPart()
        *al2 = BuildAttachmentList(*this, *part\SubPart())
        If *al2
          If *al
            *al3 = *al
            While *al3\Next : *al3 = *al3\Next : Wend
            *al3\Next = *al2
          Else
            *al = *al2
          EndIf
        EndIf
      Next
    EndIf
  
    ProcedureReturn *al
  EndProcedure
  Procedure   FreeAttachmentList(*this.sMessage, *AttList.AttList)
    Protected *al.AttList
  
    While *AttList
      *al = *AttList\Next
      FreeMemory(*AttList)
      *AttList = *al
    Wend
  EndProcedure
  Procedure   AddHeader(*this.sMessage, *msgpart.MimePart, hdr.s, value.s)                  ; Header hinzuf�gen hdr=ABC value=GHI -> "ABC: GHI"
    Protected uhdr.s
  
    If Not *msgpart : *msgpart = *this\msg : EndIf
    uhdr                                  = UCase(hdr)
    *msgpart\Header(uhdr)\dat(uhdr)\key   = hdr
    *msgpart\Header(uhdr)\dat(uhdr)\value = value
  EndProcedure
  Procedure   AddHeaderValue(*this.sMessage, *msgpart.MimePart, hdr.s, key.s, value.s)      ; Header hinzuf�gen hdr=ABC key=DEF value=GHI -> "ABC: DEF=GHI;"
    Protected uhdr.s, ukey.s
  
    If Not *msgpart : *msgpart = *this\msg : EndIf
    uhdr                                  = UCase(hdr)
    ukey                                  = UCase(key)
    *msgpart\Header(uhdr)\dat(ukey)\key   = key
    *msgpart\Header(uhdr)\dat(ukey)\value = value
  EndProcedure
  Procedure.s BuildHeader(*this.sMessage, *msgpart.MimePart)                                ; f�gt eine Header-MAP zu einem Headerstring zusammen
    Protected hdr.s, key.s, subkey.s, dat.s, subhdr.s, usubkey.s, ukey.s
  
    hdr = ""
    If Not *msgpart : *msgpart = *this\msg : EndIf
    ForEach *msgpart\Header()
      ukey   = MapKey(*msgpart\Header())
      key    = *msgpart\Header()\dat(ukey)\key
      subhdr = ""
      ForEach *msgpart\Header()\dat()
        usubkey = MapKey(*msgpart\Header()\dat())
        subkey  = *msgpart\Header()\dat()\key
        dat     = *msgpart\Header()\dat()\value
        If usubkey = ukey
          If subhdr
            subhdr = dat + ";" + Chr(13) + Chr(10) + Chr(9) + subhdr
          Else
            subhdr = dat
          EndIf
        Else
          If subhdr
            subhdr + ";" + Chr(13) + Chr(10) + Chr(9)
          EndIf
          subhdr + subkey + "=" + dat
        EndIf
      Next
      If hdr : hdr + Chr(13) + Chr(10) : EndIf
      hdr + key + ": " + subhdr
    Next
  
    ProcedureReturn hdr
  EndProcedure
  Procedure.i CalcHeaderSize(*this.sMessage, *msgpart.MimePart)                             ; Berechnet die Gr��e des Headers in Zeichenanzahl
    Protected key.i, dat.i, subkey.i, ukey.s, usubkey.s, subhdr.i, hdr.i
  
    hdr = 0
    If Not *msgpart : *msgpart = *this\msg : EndIf
    ForEach *msgpart\Header()
      ukey   = MapKey(*msgpart\Header())
      key    = Len(*msgpart\Header()\dat(ukey)\key)
      subhdr = 0
      ForEach *msgpart\Header()\dat()
        usubkey = MapKey(*msgpart\Header()\dat())
        subkey  = Len(*msgpart\Header()\dat()\key)
        dat     = Len(*msgpart\Header()\dat()\value)
        If usubkey = ukey
          If subhdr
            subhdr + dat + 4
          Else
            subhdr = dat
          EndIf
        Else
          If subhdr
            subhdr + 4
          EndIf
          subhdr + subkey + 1 + dat
        EndIf
      Next
      If hdr : hdr + 2 : EndIf
      hdr + key + 2 + subhdr
    Next
  
    ProcedureReturn hdr
  EndProcedure
  Procedure.i CalcSize(*this.sMessage, *msgpart.MimePart)                                   ; Berechnet die Gr��e der encodierten Message in Bytes, inkl. Header und allen Steuercodes
    Protected res.i, bound.s, bl.i
  
    If Not *msgpart : *msgpart = *this\msg : EndIf
    bound = GetHeaderValue(*this, *msgpart, "CONTENT-TYPE", "BOUNDARY")
    res   + CalcHeaderSize(*this, *msgpart)
    If *msgpart\body
      res + *msgpart\bodylen + 4
    Else
      res + 2
    EndIf
    If bound
      bl  = Len(bound)
      res + 4 + bl
      ForEach *msgpart\SubPart()
        res + CalcSize(*this, *msgpart\SubPart()) + 6 + bl
      Next
      res + 4
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i BuildPart(*this.sMessage, *msgpart.MimePart, *partlen, addbufsize.i)          ; Generiert einen Messagepart inkl. Header und Body
    Protected *src, newlen.i, header.s, *partsrc, bound.s, *srcpos, bl.i, hl.i, plen.i
  
    header = ""
    bound  = ""
    plen   = 0
    newlen = CalcSize(*this, *msgpart) + addbufsize
    If newlen
      *src = AllocateMemory(newlen)
      If *src
        bound   = GetHeaderValue(*this, *msgpart, "CONTENT-TYPE", "BOUNDARY")
        *srcpos = *src
        header  = BuildHeader(*this, *msgpart)
        bl      = Len(bound)
        hl      = Len(header)
        PokeS(*srcpos, header, hl, #PB_Ascii)          : *srcpos + hl
        If *msgpart\body
          PokeL(*srcpos, $0a0d0a0d)                    : *srcpos + 4
          CopyMemory(*msgpart\body, *srcpos, *msgpart\bodylen) : *srcpos + *msgpart\bodylen
        Else
          PokeW(*srcpos, $0a0d)                        : *srcpos + 2
        EndIf
        If bound
          PokeL(*srcpos, $2d2d0a0d)                    : *srcpos + 4
          PokeS(*srcpos, bound, bl, #PB_Ascii)         : *srcpos + bl
          ForEach *msgpart\SubPart()
            *partsrc = BuildPart(*this, *msgpart\SubPart(), @plen, 0)
            If *partsrc
              PokeW(*srcpos, $0a0d)                    : *srcpos + 2
              CopyMemory(*partsrc, *srcpos, plen)      : *srcpos + plen
              PokeL(*srcpos, $2d2d0a0d)                : *srcpos + 4
              PokeS(*srcpos, bound, bl, #PB_Ascii)     : *srcpos + bl
              FreeMemory(*partsrc)
            EndIf
          Next
          PokeL(*srcpos, $0a0d2d2d)                    : *srcpos + 4
        EndIf
        plen = *srcpos - *src
      EndIf
    EndIf
    If *src
      PokeL(*partlen, plen)
    Else
      PokeL(*partlen, 0)
    EndIf
    ProcedureReturn *src
  EndProcedure
  Procedure.i Encode(*this.sMessage)                                                                 ; Generiert die Message inkl CRLF Abschluss
    Protected newlen.i, *src
    
    If *this\msg\src <> #Null
      FreeMemory(*this\msg\src)
      *this\msg\src = #Null
    EndIf
    
    *src = BuildPart(*this, *this\msg, @newlen, 3)
    If *src
      PokeW(*src + newlen, $0a0d) : newlen + 2
      PokeA(*src + newlen, 0)     : newlen + 1
      *this\msg\src = *src
      ProcedureReturn #True
    EndIf
    ProcedureReturn #False
  EndProcedure
  Procedure.i SetBodyBinary(*this.sMessage, *src, srclen.i, name.s, filename.s, id.s, *part.MimePart)          ; setzt den Body eines Parts mit Bin�rdaten (diese werden automatisch Base64 encodiert)
    Protected len.i
  
    If Not *part : *part = *this\msg : EndIf
    If *src And srclen
      AddHeader(*this, *part, "Content-Type", "application/force-download")
      AddHeader(*this, *part, "Content-Transfer-Encoding", "base64")
      If name     : AddHeaderValue(*this, *part, "Content-Type", "name", AddQuotesIfNeeded(name))                : EndIf
      If filename : AddHeaderValue(*this, *part, "Content-Disposition", "filename", AddQuotesIfNeeded(filename)) : EndIf
      If id
        AddHeader(*this, *part, "Content-Disposition", "inline")
        AddHeader(*this, *part, "Content-ID", "<"+id+">")
      Else
        AddHeader(*this, *part, "Content-Disposition", "attachment")
      EndIf
  
      *part\body    = AWCode::EncodeBase64(*src, srclen, @len)
      *part\bodylen = len
      *part\justptr = #False
  
      If *part\bodylen : ProcedureReturn #True : EndIf
    EndIf
    ProcedureReturn #False
  EndProcedure
  Procedure.i SetBodyText(*this.sMessage, body.s, type.i, charset.s, encode.i, *part.MimePart)                 ; setzt den Body eines Parts als ASCII-String, der n�tigenfalls/erzwungen codiert wird
    Protected len.i, body2.s
  
    If Not *part : *part = *this\msg : EndIf
    Select type
      Case #TYPE_PLAIN :   AddHeader(*this, *part, "Content-Type", "text/plain")
      Case #TYPE_HTML  :   AddHeader(*this, *part, "Content-Type", "text/html")
      Default                   :   AddHeader(*this, *part, "Content-Type", "text/plain")
    EndSelect
  
    If charset = #Null$: charset = AWCharset::GetSysCharsetName() : EndIf
  
    AddHeaderValue(*this, *part, "Content-Type", "charset", AddQuotesIfNeeded(charset))
  
    If encode = #ENCODE_AUTO
      If NeedEncoding(body)
        encode = #ENCODE_QP
      EndIf
    EndIf
  
    Select encode
      Case #ENCODE_B64
        AddHeader(*this, *part, "Content-Transfer-Encoding", "base64")
        *part\body    = AWCode::EncodeBase64Str(body, Len(body), @len)
        *part\bodylen = len
        *part\justptr = #False
      Case #ENCODE_QP
        AddHeader(*this, *part, "Content-Transfer-Encoding", "quoted-printable")
        *part\body    = AWCode::EncodeQuotedPrintableStr(body, Len(body), @len)
        *part\bodylen = len
        *part\justptr = #False
      Default
      	; replace all CRLF. by CRLF..
        AddHeader(*this, *part, "Content-Transfer-Encoding", "8bit")
        If Left(body, 1) = "." : body2 = "." : Else : body2 = "" : EndIf
        body2 + ReplaceString(body, #CRLF + ".", #CRLF + "..")
  
        len        = Len(body2)
        *part\body = AllocateMemory(len + 4)
        If *part\body
          *part\bodylen = len + 2
          PokeS(*part\body, body2, len, #PB_Ascii)
          PokeW(*part\body + len, $0a0d)
        Else
          *part\bodylen = 0
        EndIf
        *part\justptr = #False
    EndSelect
    If *part\bodylen : ProcedureReturn #True : EndIf
    ProcedureReturn #False
  EndProcedure
  Procedure   SetMultipart(*this.sMessage, type.i, *part.MimePart)                                             ; setzt den Multipart-Header + Boundary eines Parts
    If Not *part : *part = *this\msg : EndIf
    Select type
      Case #TYPE_MP_ALTERNATIVE  : AddHeader(*this, *part, "Content-Type", "multipart/alternative")
      Case #TYPE_MP_RELATED      : AddHeader(*this, *part, "Content-Type", "multipart/related")
      Default                        : AddHeader(*this, *part, "Content-Type", "multipart/mixed")
    EndSelect
    AddHeaderValue(*this, *part, "Content-Type", "boundary", GenBoundary())
    *part\body    = #Null
    *part\bodylen = 0
    *part\justptr = #False
  EndProcedure
  Procedure.i AddMultipart(*this.sMessage, type.i, *part.MimePart)                                             ; h�ngt einen neuen Part an die Mail an und setzt den Multipart-Header + Boundary
    Protected *msgpart.MimePart
  
    *msgpart = #Null
    If Not *part : *part = *this\msg : EndIf
    AddElement(*part\SubPart())
    *msgpart = *part\SubPart()
    If *msgpart
      SetMultipart(*this, *msgpart, type)
      ProcedureReturn *msgpart
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i AddPartHTML(*this.sMessage, html.s, charset.s, encode.i, *part.MimePart)                         ; f�gt einen html-part der Mail hinzu und setzt gleich den (ggf codierten) Body
    Protected *msgpart.MimePart
  
    *msgpart = #Null
    If Not *part : *part = *this\msg : EndIf
    AddElement(*part\SubPart())
    *msgpart = *part\SubPart()
    If *msgpart
      If charset = "" : charset = AWCharset::GetHTMLCharsetName(html) : EndIf
      If SetBodyText(*this, html, #TYPE_HTML, charset, encode, *msgpart)
        ProcedureReturn #True
      EndIf
      DeleteElement(*part\SubPart())
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i AddPartText(*this.sMessage, text.s, charset.s, encode.i, *part.MimePart)                         ; f�gt einen PlainText-part der Mail hinzu und setzt gleich den (ggf codierten) Body
    Protected *msgpart.MimePart
  
    *msgpart = #Null
    If Not *part : *part = *this\msg : EndIf
    AddElement(*part\SubPart())
    *msgpart = *part\SubPart()
    If *msgpart
      If SetBodyText(*this, text, #TYPE_PLAIN, charset, encode, *msgpart)
        ProcedureReturn #True
      EndIf
      DeleteElement(*part\SubPart())
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i AddPartBinary(*this.sMessage, *src, srclen.i, name.s, filename.s, id.s, *part.MimePart)  ; f�gt einen attachment-part der Mail hinzu und setzt gleich den base64 codierten Body
    Protected *msgpart.MimePart
  
    *msgpart = #Null
    If Not *part : *part = *this\msg : EndIf
    AddElement(*part\SubPart())
    *msgpart = *part\SubPart()
    If *msgpart
      If SetBodyBinary(*this, *src, srclen, name, GetFilePart(filename), id, *msgpart)
        ProcedureReturn #True
      EndIf
      DeleteElement(*part\SubPart())
    EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i AddPartBinaryFile(*this.sMessage, name.s, filename.s, id.s, *part.MimePart)              ; f�gt einen attachment-part aus einer datei der Mail hinzu und setzt gleich den base64 codierten Body
    Protected file.i, length.i, bytes.i, *txt, res.i
  
    res = #False
    If Not *part : *part = *this\msg : EndIf
    file     = ReadFile(#PB_Any, filename)
    If IsFile(file)
      length = Lof(file)
      If length > 0
        *txt = AllocateMemory(length)
        If *txt
          bytes = ReadData(file, *txt, length)
          If bytes = length
            res = AddPartBinary(*this, *txt, length, name, filename, id, *part)
          EndIf
          FreeMemory(*txt)
        EndIf
      EndIf
      CloseFile(file)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i SetMailSource(*this.sMessage, src.s)                                                   ; Mailquellcode setzen; es wird ein String �bergeben
    Protected len.i
  
    Clear(*this)
    If src
      len                 = Len(src)
      *this\msg\decoded   = #False
      *this\msg\mhdecoded = #False
      *this\msg\src       = AllocateMemory(len + 1)
      If *this\msg\src
        PokeS(*this\msg\src, src, len, #PB_Ascii)
        ProcedureReturn #True
      Else
        ProcedureReturn #False
      EndIf
    EndIf
  
    ProcedureReturn #True
  EndProcedure
  Procedure.i SetMailSourceBin(*this.sMessage, *src, srclen.i)                                       ; Mailquellcode setzen; es wird ein Bin�rer ASCII-String �bergeben
    Clear(*this)
    If *src And srclen
      *this\msg\mhdecoded = #False
      *this\msg\decoded   = #False
      *this\msg\src       = AllocateMemory(srclen + 1)
      If *this\msg\src
        CopyMemory(*src, *this\msg\src, srclen)
        PokeA(*this\msg\src + srclen, 0)
        ProcedureReturn #True
      Else
        ProcedureReturn #False
      EndIf
    EndIf
  
    ProcedureReturn #True
  EndProcedure
  Procedure.s GetMailSource(*this.sMessage)                                                          ; Mailquellcode zur�ckgeben. es wird ein String zur�ckgegeben
    If *this\msg\src
      ProcedureReturn PeekS(*this\msg\src, MemorySize(*this\msg\src), #PB_Ascii)
    EndIf
  
    ProcedureReturn ""
  EndProcedure
  Procedure.i GetMailSourceBin(*this.sMessage)                                                       ; Mailquellcode zur�ckgeben. es wird ein Zeiger auf die Originaldaten zur�ckgegeben (NICHT FREIGEBEN)
    ProcedureReturn *this\msg\src
  EndProcedure
  Procedure.i LoadEML(*this.sMessage, filename.s)                                                    ; eine Mail im Plain-Text-Format lesen (EML, ASCII); OHNE bcc daten
    Protected file.i, length.i, bytes.i
  
    Clear(*this)
    If filename <> #Null$
      *this\msg\mhdecoded = #False
      *this\msg\decoded   = #False
      file                = ReadFile(#PB_Any, filename)
      If IsFile(file)
        length = Lof(file)
        If length > 0
          *this\msg\src = AllocateMemory(length + 1)
          If *this\msg\src
            bytes = ReadData(file, *this\msg\src, length)
            If bytes = length
              PokeA(*this\msg\src + length, 0)
            Else
              FreeMemory(*this\msg\src)
              *this\msg\src = #Null
            EndIf
          EndIf
        EndIf
        CloseFile(file)
      EndIf
    EndIf
  
    If *this\msg\src : ProcedureReturn #True : EndIf
    ProcedureReturn #False
  EndProcedure
  Procedure.i SaveEML(*this.sMessage, filename.s)                                                    ; eine Mail im Plain-Text-Format screiben (EML, ASCII); OHNE bcc daten
    Protected file.i
  
    If filename <> #Null$
  	  If *this\msg\src
  	    file = CreateFile(#PB_Any, filename)
  	    If IsFile(file)
  	      WriteData(file, *this\msg\src, MemorySize(*this\msg\src) - 1)   ; im Memorysize ist das abschlie�ende Chr(0) drin, das weglasen
  	      CloseFile(file)
  	      ProcedureReturn #True
  	    EndIf
  	  EndIf
  	EndIf
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i Load(*this.sMessage, filename.s)                                                       ; eine Mail im bin�ren format laden; MIT bcc daten
    Protected *f.AWChunkyFile::ChunkyFile, res.i, hdr.s
  
    Clear(*this)
    *this\msg\mhdecoded = #False
    *this\msg\decoded   = #False
    res                 = #False
    If filename <> #Null$
      *f = AWChunkyFile::New()
      If *f
        If *f\Open(FileName, "MAIL", AWChunkyFile::#MODE_READ)
          While *f\NextChunk()
            If Not *f\CheckChunkIntegrity()
              res = #False
              Break
            EndIf
            hdr = *f\GetChunkHeader()
            If hdr = "BCC "
              *this\msg\bcc = *f\ReadString()
            ElseIf hdr = "SRC "
              If Not *this\msg\src
                *this\msg\src = *f\ReadBlock()
                res = #True
              Else
                res = #False
                Break
              EndIf
            EndIf
          Wend
          *f\Close()
        EndIf
        *f\Destroy()
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i Save(*this.sMessage, filename.s, complevel.i)                                          ; eine Mail im bin�ren format speichern (komprimiert); MIT bcc daten
    Protected *f.AWChunkyFile::ChunkyFile, res.i
  
    res = #False
    If *this\msg\src And (filename <> #Null$)
      *f = AWChunkyFile::New()
      If *f
        If *f\Open(FileName, "MAIL", AWChunkyFile::#MODE_WRITE)
          If *this\msg\bcc
            If *f\OpenChunk("BCC ")
              res = *f\WriteString(*this\msg\bcc, #PB_Ascii, complevel)
              res & *f\CloseChunk()
            EndIf
          Else
            res = #True
          EndIf
          If res
            If *f\OpenChunk("SRC ")
              res = *f\WriteBlock(*this\msg\src, MemorySize(*this\msg\src), complevel)
              res & *f\CloseChunk()
            EndIf
          EndIf
          *f\Close()
        EndIf
        *f\Destroy()
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.s GetHeaderField(*this.sMessage, field.i, *part.MimePart)
    Protected res.s
  
    If Not *part : *part = *this\msg : EndIf
    Select field
      Case #HDR_SUBJECT:                   res = DecodeInline(GetHeaderData(*this, *part, "SUBJECT"))
      Case #HDR_FROM:                      res = DecodeInline(GetHeaderData(*this, *part, "FROM"))
      Case #HDR_FROM_MAIL:                 res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "FROM")), ", ")
      Case #HDR_FROM_MAIL_AB:              res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "FROM")), ", ", AWRegExSup::#ANGLE_BRACKETS)
      Case #HDR_CC:                        res = DecodeInline(GetHeaderData(*this, *part, "CC"))
      Case #HDR_CC_MAILS:                  res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "CC")), ", ")
      Case #HDR_CC_MAILS_AB:               res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "CC")), ", ", AWRegExSup::#ANGLE_BRACKETS)
      Case #HDR_TO:                        res = DecodeInline(GetHeaderData(*this, *part, "TO"))
      Case #HDR_TO_MAILS:                  res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "TO")), ", ")
      Case #HDR_TO_MAILS_AB:               res = AWRegExSup::CreateMailAdrList(DecodeInline(GetHeaderData(*this, *part, "TO")), ", ", AWRegExSup::#ANGLE_BRACKETS)
      Case #HDR_BCC:                       res = *this\msg\bcc
      Case #HDR_BCC_MAILS:                 res = AWRegExSup::CreateMailAdrList(*this\msg\bcc, ", ")
      Case #HDR_BCC_MAILS_AB:              res = AWRegExSup::CreateMailAdrList(*this\msg\bcc, ", ", AWRegExSup::#ANGLE_BRACKETS)
      Case #HDR_RETURNPATH:                res = DecodeInline(GetHeaderData(*this, *part, "RETURN-PATH"))
      Case #HDR_MESSAGEID:                 res = GetHeaderData(*this, *part, "MESSAGE-ID")
      Case #HDR_DATE:                      res = GetHeaderData(*this, *part, "DATE")
      Case #HDR_DISPOSITIONNOTIFICATIONTO: res = DecodeInline(GetHeaderData(*this, *part, "DISPOSITION-NOTIFICATION-TO"))
      Case #HDR_REPLYTO:                   res = DecodeInline(GetHeaderData(*this, *part, "REPLY-TO"))
      Case #HDR_CONTENTTYPE:               res = GetHeaderData(*this, *part, "CONTENT-TYPE") : If res : res = StringField(res, 1, "/") : EndIf
      Case #HDR_CONTENTSUBTYPE:            res = GetHeaderData(*this, *part, "CONTENT-TYPE") : If res : res = StringField(res, 2, "/") : EndIf
      Case #HDR_CONTENTCHARSET:            res = GetHeaderValue(*this, *part, "CONTENT-TYPE", "CHARSET")
      Case #HDR_CONTENTTRANSFERENCODING:   res = GetHeaderData(*this, *part, "CONTENT-TRANSFER-ENCODING")
      Case #HDR_BOUNDARY:                  res = GetHeaderValue(*this, *part, "CONTENT-TYPE", "BOUNDARY")
      Case #HDR_CONTENTDISPOSITION:        res = GetHeaderData(*this, *part, "CONTENT-DISPOSITION")
      Case #HDR_CONTENTNAME:               res = GetHeaderValue(*this, *part, "CONTENT-TYPE", "NAME")
      Case #HDR_CONTENTFILENAME:           res = GetHeaderValue(*this, *part, "CONTENT-DISPOSITION", "FILENAME")
      Case #HDR_CONTENTID:                 res = GetHeaderData(*this, *part, "CONTENT-ID")
      Case #HDR_XPRIORITY:                 res = GetHeaderData(*this, *part, "X-PRIORITY")
      Default:                                  res = ""
    EndSelect
  
    ProcedureReturn res
  EndProcedure
  Procedure   SetHeaderField(*this.sMessage, field.i, value.s, *part.MimePart)
    If Not *part : *part = *this\msg : EndIf
    Select field
      Case #HDR_SUBJECT:                   AddHeader(*this, *part, "Subject", EncodeInline(value, "", #ENCODE_AUTO, #False, #True))
      Case #HDR_FROM, #HDR_FROM_MAIL:      AddHeader(*this, *part, "From", value)
      Case #HDR_CC, #HDR_CC_MAILS:         AddHeader(*this, *part, "CC", value)
      Case #HDR_TO, #HDR_TO_MAILS:         AddHeader(*this, *part, "To", value)
      Case #HDR_BCC, #HDR_BCC_MAILS:       *this\msg\bcc = value
      Case #HDR_DATE:                      AddHeader(*this, *part, "Date", value)
      Case #HDR_DISPOSITIONNOTIFICATIONTO: AddHeader(*this, *part, "Disposition-Notification-To", value)
      Case #HDR_REPLYTO:                   AddHeader(*this, *part, "Reply-To", value)
      Case #HDR_CONTENTCHARSET:            AddHeaderValue(*this, *part, "Content-Type", "charset", value)
      Case #HDR_CONTENTTRANSFERENCODING:   AddHeader(*this, *part, "Content-Transfer-Encoding", value)
      Case #HDR_BOUNDARY:                  AddHeaderValue(*this, *part, "Content-Type", "boundary", AddQuotesIfNeeded(value))
      Case #HDR_CONTENTDISPOSITION:        AddHeader(*this, *part, "Content-Disposition", value)
      Case #HDR_CONTENTNAME:               AddHeaderValue(*this, *part, "Content-Type", "name", value)
      Case #HDR_CONTENTFILENAME:           AddHeaderValue(*this, *part, "Content-Disposition", "filename", value)
      Case #HDR_CONTENTID:                 AddHeader(*this, *part, "Content-ID", value)
      Case #HDR_XPRIORITY:                 AddHeader(*this, *part, "X-Priority", value)
      Case #HDR_FULLCONTENTTYPE:           AddHeader(*this, *part, "Content-Type", value)
    EndSelect
  EndProcedure
  Procedure   SetContentType(*this.sMessage, contenttype.s, contentsubtype.s, *part.MimePart)
    SetHeaderField(*this, #HDR_FULLCONTENTTYPE, contenttype + "/" + contentsubtype, *part)
  EndProcedure
  Procedure.i FindPartCID(*this.sMessage, cid.s, casesensitive.i, *part.MimePart)
    Protected res.i
  
    If Not *part : *part = *this\msg : EndIf
    If casesensitive
      If GetHeaderField(*this, #HDR_CONTENTID, *part) = cid
        ProcedureReturn *part
      EndIf
    Else
      If UCase(GetHeaderField(*this, #HDR_CONTENTID, *part)) = UCase(cid)
        ProcedureReturn *part
      EndIf
    EndIf
    If UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "MULTIPART"
      ForEach *part\SubPart()
        res = FindPartCID(*this, cid, casesensitive, *part\SubPart())
        If res : ProcedureReturn res : EndIf
      Next
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i FindPartName(*this.sMessage, name.s, casesensitive.i, *part.MimePart)
    Protected res.i
  
    If Not *part : *part = *this\msg : EndIf
    If casesensitive
      If GetHeaderField(*this, #HDR_CONTENTNAME, *part) = name
        ProcedureReturn *part
      EndIf
    Else
      If UCase(GetHeaderField(*this, #HDR_CONTENTNAME, *part)) = UCase(name)
        ProcedureReturn *part
      EndIf
    EndIf
    If UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "MULTIPART"
      ForEach *part\SubPart()
        res = FindPartName(*this, name, casesensitive, *part\SubPart())
        If res : ProcedureReturn res : EndIf
      Next
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i FindPartFileName(*this.sMessage, filename.s, casesensitive.i, *part.MimePart)
    Protected res.i, fn.s
  
    If Not *part : *part = *this\msg : EndIf
    If casesensitive
      If GetHeaderField(*this, #HDR_CONTENTFILENAME, *part) = filename
        ProcedureReturn *part
      EndIf
    Else
      If UCase(GetHeaderField(*this, #HDR_CONTENTFILENAME, *part)) = UCase(filename)
        ProcedureReturn *part
      EndIf
    EndIf
    If UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "MULTIPART"
      ForEach *part\SubPart()
        res = FindPartFileName(*this, filename, casesensitive, *part\SubPart())
        If res : ProcedureReturn res : EndIf
      Next
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i GetDate(*this.sMessage, *part.MimePart)
    ProcedureReturn AWDate::RFC2822ToDate(GetHeaderField(*this, #HDR_DATE, *part))
  EndProcedure
  Procedure   SetDate(*this.sMessage, datevalue.i, *part.MimePart)
    SetHeaderField(*this, #HDR_DATE, AWDate::DateToRFC2822(datevalue), *part)
  EndProcedure
  Procedure   SetDateNow(*this.sMessage, *part.MimePart)
    SetHeaderField(*this, #HDR_DATE, AWDate::DateToRFC2822(Date()), *part)
  EndProcedure
  Procedure.i GetXPriority(*this.sMessage, *part.MimePart)
    ProcedureReturn Val(AWSupport::TrimWS(GetHeaderField(*this, #HDR_XPRIORITY, *part)))
  EndProcedure
  Procedure   SetXPriority(*this.sMessage, priority.i, *part.MimePart)
    SetHeaderField(*this, #HDR_XPRIORITY, Str(priority), *part)
  EndProcedure
  Procedure.i GetHTMLPart(*this.sMessage, *part.MimePart)                                   ; den text/html-Part suchen und zur�ckgeben
    Protected res.i
  
    If Not *part : *part = *this\msg : EndIf
    If UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "TEXT"
      If UCase(GetHeaderField(*this, #HDR_CONTENTSUBTYPE, *part)) = "HTML"
        ProcedureReturn *part
      EndIf
    ElseIf UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "MULTIPART"
      ForEach *part\SubPart()
        res = GetHTMLPart(*this, *part\SubPart())
        If res : ProcedureReturn res : EndIf
      Next
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i GetPlainPart(*this.sMessage, *part.MimePart)                                  ; den text/plain-Part suchen und zur�ckgeben
    Protected res.i
  
    If Not *part : *part = *this\msg : EndIf
    If UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "TEXT"
      If UCase(GetHeaderField(*this, #HDR_CONTENTSUBTYPE, *part)) = "PLAIN"
        ProcedureReturn *part
      EndIf
    ElseIf UCase(GetHeaderField(*this, #HDR_CONTENTTYPE, *part)) = "MULTIPART"
      ForEach *part\SubPart()
        res = GetPlainPart(*this, *part\SubPart())
        If res : ProcedureReturn res : EndIf
      Next
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.s GetHTML(*this.sMessage)                                                       ; den text/html-Part suchen und den Body als String zur�ckgeben
    Protected *part.MimePart, encoding.s, res.s
  
    *part = GetHTMLPart(*this, #Null)
    If *part
      encoding = UCase(GetHeaderField(*this, #HDR_CONTENTTRANSFERENCODING, *part))
      If encoding  = "QUOTED-PRINTABLE"
        ProcedureReturn AWCode::DecodeQuotedPrintableStr(*part\body, *part\bodylen)
      ElseIf encoding = "BASE64"
        ProcedureReturn AWCode::DecodeBase64Str(*part\body, *part\bodylen)
      Else
      	If PeekW(*part\body) = $2E2E
      		res = PeekS(*part\body + 1, *part\bodylen - 1, #PB_Ascii)
      	Else
      		res = PeekS(*part\body, *part\bodylen, #PB_Ascii)
      	EndIf
      	res = ReplaceString(res, #CRLF + "..", #CRLF + ".")	; replace all CRLF.. by CRLF.
  
        ProcedureReturn res
      EndIf
    EndIf
  
    ProcedureReturn ""
  EndProcedure
  Procedure.s GetPlain(*this.sMessage)                                                      ; den text/plain-Part suchen und den Body als String zur�ckgeben
    Protected *part.MimePart, encoding.s, res.s
  
    *part = GetPlainPart(*this, #Null)
    If *part
      encoding = UCase(GetHeaderField(*this, #HDR_CONTENTTRANSFERENCODING, *part))
      If encoding = "QUOTED-PRINTABLE"
        ProcedureReturn AWCode::DecodeQuotedPrintableStr(*part\body, *part\bodylen)
      ElseIf encoding = "BASE64"
        ProcedureReturn AWCode::DecodeBase64Str(*part\body, *part\bodylen)
      Else
      	If PeekW(*part\body) = $2E2E
      		res = PeekS(*part\body + 1, *part\bodylen - 1, #PB_Ascii)
      	Else
      		res = PeekS(*part\body, *part\bodylen, #PB_Ascii)
      	EndIf
      	res = ReplaceString(res, #CRLF + "..", #CRLF + ".")	; replace all CRLF.. by CRLF.
  
        ProcedureReturn res
      EndIf
    EndIf
  
    ProcedureReturn ""
  EndProcedure
  Procedure.s GetPartAsString(*this.sMessage, *part.MimePart)                               ; den Body des �bergebenen Parts auslesen (ggf dekodieren) und als String zur�ckgeben
    Protected encoding.s, res.s
  
    If Not *part : *part = *this\msg : EndIf
    encoding = UCase(GetHeaderField(*this, #HDR_CONTENTTRANSFERENCODING, *part))
    If encoding = "QUOTED-PRINTABLE"
      ProcedureReturn AWCode::DecodeQuotedPrintableStr(*part\body, *part\bodylen)
    ElseIf encoding = "BASE64"
      ProcedureReturn AWCode::DecodeBase64Str(*part\body, *part\bodylen)
    Else
    	If PeekW(*part\body) = $2E2E
    		res = PeekS(*part\body + 1, *part\bodylen - 1, #PB_Ascii)
    	Else
    		res = PeekS(*part\body, *part\bodylen, #PB_Ascii)
    	EndIf
    	res = ReplaceString(res, #CRLF + "..", #CRLF + ".")	; replace all CRLF.. by CRLF.
  
      ProcedureReturn res
    EndIf
  EndProcedure
  Procedure.i GetPartAsBin(*this.sMessage, *part.MimePart, *partlen)                        ; den Body des �bergebenen Parts auslesen (ggf dekodieren) und als Zeiger auf Speicheradresse zur�ckgeben; die L�nge wird in PartLen gespeichert
    Protected *res, len.i, encoding.s, a.i, *p
  
    *res = #Null
    If Not *part : *part = *this\msg : EndIf
    encoding = UCase(GetHeaderField(*this, #HDR_CONTENTTRANSFERENCODING, *part))
    If encoding = "QUOTED-PRINTABLE"
      *res = AWCode::DecodeQuotedPrintable(*part\body, *part\bodylen, @len)
    ElseIf encoding = "BASE64"
      *res = AWCode::DecodeBase64(*part\body, *part\bodylen, @len)
    Else
      len  = *part\bodylen
      *res = AllocateMemory(len)
      If *res
      	If PeekW(*part\body) = $2E2E
  				len -1
  				CopyMemory(*part\body + 1, *res, len)
  			Else
  				CopyMemory(*part\body, *res, len)
  			EndIf
  
  			*p = *res
  			For a = 0 To len - 1
  				If PeekL(*p) = $0A0D2E2E												; Zeilenumbruch gefolgt von zwei Punkten
  					MoveMemory(*p + 3, *p + 2, len - a - 3)				;- stabilit�t testen
  				EndIf
  			Next
  		EndIf
    EndIf
    If *res
      PokeI(*partlen, len)
    Else
      PokeI(*partlen, 0)
    EndIf
  
    ProcedureReturn *res
  EndProcedure
  Procedure.i SavePartToFile(*this.sMessage, *part.MimePart, filename.s)                    ; den Body des �bergebenen Parts in die angegebene Date (inkl, Pfad) speichern (ggf vorher dekodieren)
    Protected *bin, len.i, res.i, file.i
  
    If filename
      res  = #False
      *bin = GetPartAsBin(*this, *part, @len)
      If *bin
        file = CreateFile(#PB_Any, filename)
        If IsFile(file)
          WriteData(file, *bin, len)
          CloseFile(file)
          res = #True
        EndIf
        FreeMemory(*bin)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.s GetPlainAsHTML(*this.sMessage)                                                ; den text/plain-Part suchen und den Body als in HTML umgewandelten String zur�ckgeben
    Protected plain.s, html.s, *part.MimePart
  
    html  = ""
    *part = GetPlainPart(*this, #Null)
    If *part
      plain = GetPartAsString(*this, *part)
      If plain
        html = "<!DOCTYPE html PUBLIC "+Chr(34)+"-//W3C//DTD html 4.01 Transitional//EN"+Chr(34)+">"+Chr(10)
        html + "<html><head><meta content="+Chr(34)+"text/html; charset=" + html + GetHeaderField(*this, #HDR_CONTENTCHARSET, *part) + Chr(34)
        html + " http-equiv="+Chr(34)+"content-type"+Chr(34)+">"
        html + "<title></title></head><body><pre><code>"
        html + plain
        html + "</code></pre></body></html>"
      EndIf
    EndIf
  
    ProcedureReturn html
  EndProcedure
  Procedure.i IsDecoded(*this.sMessage)
    ProcedureReturn *this\msg\decoded
  EndProcedure
  Procedure.i IsHeaderDecoded(*this.sMessage)
    ProcedureReturn *this\msg\mhdecoded
  EndProcedure
  Procedure.i GetSize(*this.sMessage)
    If *this\msg\src
      ProcedureReturn (MemorySize(*this\msg\src) - 1)
    EndIf
  
    ProcedureReturn 0
  EndProcedure
  Procedure   Destroy(*this.sMessage)
    Clear(*this)
    FreeStructure(*this\msg)
    FreeStructure(*this)
  EndProcedure
  CompilerIf Defined(AWPBT_EnableMessageAES, #PB_Module)
  	Procedure.i LoadAESCrypted(*this.sMessage, filename.s, *key, *initializationvector, bits.i)                                                       ; eine Mail im bin�ren format laden; MIT bcc daten
  	  Protected *f.AWChunkyFile::ChunkyFile, res.i, hdr.s
  
  	  Clear(*this)
  	  *this\msg\decoded   = #False
  	  *this\msg\mhdecoded = #False
  	  res                 = #False
  	  If (filename <> #Null$) And (*key <> #Null) And (*initializationvector <> #Null)
  	    *f = AWChunkyFile::New()
  	    If *f
  	    	If *f\InitAES(*key, *initializationvector, bits)
  	    		If *f\EnableAES()
  						If *f\Open(FileName, "MAIL", AWChunkyFile::#MODE_READ)
  			        While *f\NextChunk()
  			          If Not *f\CheckChunkIntegrity()
  			            res = #False
  			            Break
  			          EndIf
  			          hdr = *f\GetChunkHeader()
  			          If hdr = "BCC "
  			            *this\msg\bcc = *f\ReadString()
  			          ElseIf hdr = "SRC "
  			            If Not *this\msg\src
  			              *this\msg\src = *f\ReadBlock()
  			              res = #True
  			            Else
  			              res = #False
  			              Break
  			            EndIf
  			          EndIf
  			        Wend
  			        *f\Close()
  			      EndIf
  			    EndIf
  			  EndIf
  	      *f\Destroy()
  	    EndIf
  	  EndIf
  
  	  ProcedureReturn res
  	EndProcedure
  	Procedure.i SaveAESCrypted(*this.sMessage, filename.s, *key, *initializationvector, bits.i, complevel.i)                                          ; eine Mail im bin�ren format speichern (komprimiert); MIT bcc daten
  	  Protected *f.AWChunkyFile::ChunkyFile, res.i
  
  	  res = #False
  	  If (*this\msg\src <> #Null) And (filename <> #Null$) And (*key <> #Null) And (*initializationvector <> #Null)
  	    *f = AWChunkyFile::New()
  	    If *f
  	    	If *f\InitAES(*key, *initializationvector, bits)
  	    		If *f\EnableAES()
  			      If *f\Open(FileName, "MAIL", AWChunkyFile::#MODE_WRITE)
  			        If *this\msg\bcc
  			          If *f\OpenChunk("BCC ")
  			            res = *f\WriteString(*this\msg\bcc, #PB_Ascii, complevel)
  			            res & *f\CloseChunk()
  			          EndIf
  			        Else
  			          res = #True
  			        EndIf
  			        If res
  			          If *f\OpenChunk("SRC ")
  			            res = *f\WriteBlock(*this\msg\src, MemorySize(*this\msg\src), complevel)
  			            res & *f\CloseChunk()
  			          EndIf
  			        EndIf
  			        *f\Close()
  			      EndIf
  			    EndIf
  			  EndIf
  	      *f\Destroy()
  	    EndIf
  	  EndIf
  
  	  ProcedureReturn res
  	EndProcedure
  CompilerEndIf
  
  DataSection
    Message_VT:
      Data.i @Destroy()
      Data.i @Clear()
      Data.i @Decode()
      Data.i @DecodeHeader()
      Data.i @BuildAttachmentList()
      Data.i @FreeAttachmentList()
      Data.i @Encode()
      Data.i @SetBodyBinary()
      Data.i @SetBodyText()
      Data.i @SetMultipart()
      Data.i @AddMultipart()
      Data.i @AddPartHTML()
      Data.i @AddPartText()
      Data.i @AddPartBinary()
      Data.i @AddPartBinaryFile()
      Data.i @SetMailSource()
      Data.i @SetMailSourceBin()
      Data.i @GetMailSource()
      Data.i @LoadEML()
      Data.i @SaveEML()
      Data.i @Load()
      Data.i @Save()
      Data.i @GetHeaderField()
      Data.i @SetHeaderField()
      Data.i @SetContentType()
      Data.i @FindPartCID()
      Data.i @FindPartName()
      Data.i @FindPartFileName()
      Data.i @GetDate()
      Data.i @SetDate()
      Data.i @SetDateNow()
      Data.i @GetXPriority()
      Data.i @SetXPriority()
      Data.i @GetHTML()
      Data.i @GetPlain()
      Data.i @GetPartAsString()
      Data.i @GetPartAsBin()
      Data.i @SavePartToFile()
      Data.i @GetPlainAsHTML()
      Data.i @GetSize()
      CompilerIf Defined(AWPBT_EnableMessageAES, #PB_Module)
    	  Data.i @LoadAESCrypted()
    	  Data.i @SaveAESCrypted()
      CompilerEndIf
  	  Data.i @GetMailSourceBin()
      Data.i @IsDecoded()    
      Data.i @IsHeaderDecoded()    
      Data.i @ClearPart()
      Data.i @GetHeaderValue()
      Data.i @GetHeaderData()
      Data.i @AnalyzeMimePart()
      Data.i @AddHeader()
      Data.i @AddHeaderValue()
      Data.i @BuildHeader()
      Data.i @CalcHeaderSize()
      Data.i @CalcSize()
      Data.i @BuildPart()
      Data.i @GetHTMLPart()
      Data.i @GetPlainPart()
  EndDataSection
EndModule  
 
 
 
; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 1607
; FirstLine = 1604
; Folding = ---------------
; EnableUnicode
; EnableXP