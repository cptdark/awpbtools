;{   awsmtp.pbi
;    Version 0.10 [2015/10/26]
;    Copyright (C) 2011-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;
;}

XIncludeFile "awpbtools.pbi"
XIncludeFile "awnetwork.pbi"
XIncludeFile "awsupport.pbi"
XIncludeFile "awmessage.pbi"
XIncludeFile "awcode.pbi"
XIncludeFile "awcipher.pbi"

DeclareModule AWSmtp
  #SSL_NONE     = $00000000
  #SSL_TRY      = $00000001
  #SSL_FORCE    = $00000002
  #SSL_FULL     = $00000004
  #CONN_PLAIN   = $00000008
  #CONN_LOGIN   = $00000010
  #CONN_CRAMMD5 = $00000020
  #CONN_AUTO    = #CONN_PLAIN | #CONN_LOGIN | #CONN_CRAMMD5
  #CONN_DEFAULT = #SSL_TRY | #CONN_AUTO
  
  #ERR_NONE     =  0
  #ERR_FAIL     = -1
  #ERR_LOGIN    = -2
  #ERR_USER     = -3
  #ERR_PASS     = -4
  #ERR_TIMEOUT  = AWNetwork::#ERR_TIMEOUT
  
  Interface SMTPClient
    Destroy               ()
    SetTimeout            (timeout.i = 15000)
    SetHost               (host.s, port.i)
    SetLogin              (user.s, pass.s)
    SetProxyHost          (type.i, host.s, port.i)
    SetProxyLogin         (user.s, pass.s)    
    Connect.i             (flags.i = #CONN_DEFAULT)
    DisConnect            ()
    SendMail.i            (*msg.AWMessage::Message)
    GetLog.s							()
    ClearLog							()
    EnableLog							(enabled.i)
    GetLastError.s        ()
    ClearLastError        ()
    NOOP.i                ()
    RSET.i                ()
    UserCommand.s         (cmd.s, *rc)
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      SetSSLMethod        (sslmethod.i)
    CompilerEndIf
    
  EndInterface
  
  Declare.i New()
  Declare.i OpenSSLAvailable()
EndDeclareModule

Module AWSmtp
  EnableExplicit
  
  Macro IsSet(X, Y)
    (((X) & (Y)) = (Y))
  EndMacro
   
  Structure sSMTPClient
    *vt.SMTPClient
    *Conn.AWNetwork::Connection
    Caps.s
  EndStructure
  
  Procedure.i New()
    Protected *this.sSMTPClient, *ci.AWNetwork::ConnInfo
  
    *this = AllocateStructure(sSMTPClient)
    If *this
      *this\vt   = ?SMTPClient_VT
      *this\Caps = ""
      *this\Conn = AWNetwork::New()
      If *this\Conn
        *this\Conn\ClearLog()
        *ci          = *this\Conn\GetConnInfo()
        *ci\Timeout  = 15000
        *ci\Protocol = AWNetwork::#PR_SMTP
        CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
          *ci\SSLMethod = AWNetwork::#SSL_METHOD_V23
        CompilerEndIf
        
        ProcedureReturn *this
      EndIf
      FreeStructure(*this)
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i OpenSSLAvailable()
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      ProcedureReturn AWPBT_EnableOpenSSL::initialized
    CompilerElse
      ProcedureReturn #False
    CompilerEndIf
  EndProcedure
  
  Procedure   SetTimeout(*this.sSMTPClient, timeout.i = 15000)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci         = *this\Conn\GetConnInfo()
    *ci\Timeout = timeout
  EndProcedure
  Procedure   SetHost(*this.sSMTPClient, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\Host = host
    *ci\Port = port
  EndProcedure
  Procedure   SetLogin(*this.sSMTPClient, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\User = user
    *ci\Pass = pass
  EndProcedure
  Procedure   SetProxyHost(*this.sSMTPClient, type.i, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyHost = host
    *ci\ProxyPort = port
    *ci\ProxyMode = type
  EndProcedure
  Procedure   SetProxyLogin(*this.sSMTPClient, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyUser = user
    *ci\ProxyPass = pass
  EndProcedure
  CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
    Procedure   SetSSLMethod(*this.sSMTPClient, sslmethod.i)
      Protected *ci.AWNetwork::ConnInfo
    
      *ci           = *this\Conn\GetConnInfo()
      *ci\SSLMethod = sslmethod
    EndProcedure
  CompilerEndIf
  
  Procedure.s FindCapa(*this.sSMTPClient, capa.s)             ; testen, ob der Server ein Feature unterst�tzt (suche im CAPS-String)
    Protected res.s, s.l, e.l
  
    res = ""
    s   = FindString(UCase(*this\Caps), UCase(capa), 1)
    If s
      e   = FindString(*this\Caps, AWNetwork::#CRLF, s)
      If e = 0
        e = Len(*this\Caps)
      EndIf
      res = Trim(Mid(*this\Caps, s, e - s))
    EndIf
  
    ProcedureReturn res
  EndProcedure
  
  Procedure.i _EHLO(*this.sSMTPClient, host.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("EHLO " + host, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250 To 259:   res = #ERR_NONE : done = #True
          Default:           res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _HELO(*this.sSMTPClient, host.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("EHLO " + host, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250 To 259:   res = #ERR_NONE : done = #True
          Default:           res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _QUIT(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("QUIT", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 221:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _RSET(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("RSET", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _NOOP(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("NOOP", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _ETRN(*this.sSMTPClient, value.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("ETRN " + value, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250 To 259:   res = #ERR_NONE : done = #True
          Default:           res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _VRFY(*this.sSMTPClient, value.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("VRFY " + value, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250 To 259:   res = #ERR_NONE : done = #True
          Default:           res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _MAIL_FROM(*this.sSMTPClient, value.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("MAIL FROM: " + value, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _RCPT_TO(*this.sSMTPClient, value.s)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1

    Repeat
      If *this\Conn\Execute("RCPT TO: " + value, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _AUTH_PLAIN(*this.sSMTPClient)
    Protected res.i, retries.i, done.i, *aup, aupl.i, ul.i, pl.i, userpassb64.s, *ci.AWNetwork::ConnInfo
    
    *ci     = *this\Conn\GetConnInfo()
    res     = #ERR_FAIL
    done    = #False
    retries = 1
    ul      = Len(*ci\User)
    pl      = Len(*ci\Pass)
    aupl    = ul + pl + 2
    *aup    = AllocateMemory(aupl + 2)
    If *aup

      PokeA(*aup,          0)
      PokeS(*aup + 1,      *ci\User, ul, #PB_Ascii)
      PokeA(*aup + ul + 1, 0)
      PokeS(*aup + ul + 2, *ci\Pass, pl, #PB_Ascii)
      userpassb64 = AWSupport::TrimWS(AWCode::EncodeBase64BinToStr(*aup, aupl, #PB_Any))
      FreeMemory(*aup)
      Repeat
        If *this\Conn\Execute("AUTH PLAIN " + userpassb64, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case 235:   res = #ERR_NONE  : done = #True
            Case 535:   res = #ERR_LOGIN : done = #True
            Default:    res = #ERR_FAIL  : retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _AUTH_LOGIN(*this.sSMTPClient)
    Protected res.i, retries.i, done.i, userb64.s, passb64.s, *ci.AWNetwork::ConnInfo
    
    *ci     = *this\Conn\GetConnInfo()
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    userb64 = AWSupport::TrimWS(AWCode::EncodeBase64StrToStr(*ci\User))
    passb64 = AWSupport::TrimWS(AWCode::EncodeBase64StrToStr(*ci\Pass))
    Repeat
      If *this\Conn\Execute("AUTH LOGIN", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 334:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    If res = #ERR_NONE
      retries = 1
      done    = #False
      Repeat
        If *this\Conn\Execute(userb64, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case 334:   res = #ERR_NONE : done = #True
            Case 535:   res = #ERR_USER : done = #True
            Default:    res = #ERR_FAIL : retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)
    EndIf

    If res = #ERR_NONE
      retries = 1
      done    = #False
      Repeat
        If *this\Conn\Execute(passb64, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case 235:   res = #ERR_NONE : done = #True
            Case 535:   res = #ERR_PASS : done = #True
            Default:    res = #ERR_FAIL : retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i _AUTH_CRAM_MD5(*this.sSMTPClient)
    Protected challenge.s, p.i, *ci.AWNetwork::ConnInfo, chal.s, cmd.s, retries.i, res.i, done.i
    
    *ci     = *this\Conn\GetConnInfo()
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If *this\Conn\Execute("AUTH CRAM-MD5", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 334:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    If res = #ERR_NONE
      res = #ERR_FAIL
      challenge = AWCode::DecodeBase64StrToStr(AWSupport::TrimWS(Mid(*this\Conn\GetReceivedString(), 5)))
      If challenge <> ""
        chal = AWCipher::HMAC_MD5(challenge, *ci\Pass)
        If chal <> ""
          cmd  = AWSupport::TrimWS(AWCode::EncodeBase64StrToStr(*ci\User + " " + chal))
          done = #False
          Repeat
            If *this\Conn\Execute(cmd, AWNetwork::#LE_SMTP, retries) = #ERR_NONE
              Select *this\Conn\GetResultCode()
                Case 235:   res = #ERR_NONE : done = #True
                Default:    res = #ERR_FAIL : retries - 1
              EndSelect
            Else
              retries - 1
            EndIf
          Until done Or (retries <= 0)
        EndIf
      EndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _DATA(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If *this\Conn\Execute("DATA", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 354:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
  
    ProcedureReturn res
  EndProcedure
  Procedure.i _STARTTLS(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
    
    res = #ERR_FAIL
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      retries = 1
      done    = #False
      Repeat
        If *this\Conn\Execute("STARTTLS", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case 220:   res = #ERR_NONE : done = #True
            Default:    res = #ERR_FAIL : retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)

      If res = #ERR_NONE
      	If *this\Conn\UpgradeSSL() <> AWNetwork::#SSL_ERR_NONE
      		res = #ERR_FAIL
      	EndIf
      EndIf
    CompilerEndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i SendData(*this.sSMTPClient, value.s)
    ProcedureReturn *this\Conn\SendNetworkString(value, #False)
  EndProcedure
  Procedure.i SendDataBin(*this.sSMTPClient, *buf, bufsize.i)
    ProcedureReturn *this\Conn\SendNetworkData(*buf, bufsize)
  EndProcedure
  Procedure.i FinishData(*this.sSMTPClient)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    Repeat
      If *this\Conn\Execute(AWNetwork::#CRLF + ".", AWNetwork::#LE_SMTP, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case 250:   res = #ERR_NONE : done = #True
          Default:    res = #ERR_FAIL : retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
  
    ProcedureReturn res
  EndProcedure
  Procedure.i DoLogin(*this.sSMTPClient, flags.i)
    Protected res.i, auths.s
    
    res   = #ERR_FAIL
    auths = UCase(FindCapa(*this, "AUTH "))
    If auths = "" : auths = UCase(FindCapa(*this, "AUTH=")) : EndIf
    If auths <> ""
      If IsSet(flags, #CONN_CRAMMD5) And (FindString(auths, "CRAM-MD5", 1) > 0)
        res = _AUTH_CRAM_MD5(*this)
      EndIf
      If (res <> #ERR_NONE) And IsSet(flags, #CONN_PLAIN) And (FindString(auths, "PLAIN", 1) > 0)   ; falls noch nicht eingeloggt, n�chste unsicherere methode versuchen
        res = _AUTH_PLAIN(*this)
      EndIf
      If (res <> #ERR_NONE) And IsSet(flags, #CONN_LOGIN) And (FindString(auths, "LOGIN", 1) > 0)   ; falls noch nicht eingeloggt, n�chste unsicherere methode versuchen
        res = _AUTH_LOGIN(*this)
      EndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i DoSecure(*this.sSMTPClient, flags.i)
    Protected res.i
    
    res = #ERR_NONE
    If IsSet(flags, #SSL_FORCE) Or IsSet(flags, #SSL_TRY)
      res = #ERR_FAIL
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
  		  If OpenSSLAvailable()
  		    If FindString(*this\Caps, "STARTTLS", 1, #PB_String_NoCase) > 0
  					; TLS starten; OpenSSL erforderlich 
            res = _STARTTLS(*this)
            If res = #ERR_NONE
          		*this\Caps = ""
            	res = _EHLO(*this, Hostname())
              If res = #ERR_NONE
              	*this\Caps = *this\Conn\GetReceivedString()
              EndIf
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If (res <> #ERR_NONE) And IsSet(flags, #SSL_TRY)
        res = #ERR_NONE
      EndIf
    EndIf

    ProcedureReturn res
  EndProcedure
  Procedure.i Connect(*this.sSMTPClient, flags.i = #CONN_DEFAULT)
    Protected res.i, rc.i, *ci.AWNetwork::ConnInfo, loggedin.i, secured.i
    
    *ci           = *this\Conn\GetConnInfo()
    res           = #ERR_FAIL
    *this\Caps    = ""
    loggedin      = #False
    secured       = #False
    With *this\Conn
      res = \Connect() 
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        If res = AWNetwork::#ERR_NONE
          If IsSet(flags, #SSL_FULL)
            res = #ERR_FAIL
            If OpenSSLAvailable()
              res = *this\Conn\UpgradeSSL()
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If res = AWNetwork::#ERR_NONE
        \ReadResult(AWNetwork::#LE_SMTP)
        If \GetResultCode() = 220
          res = _EHLO(*this, Hostname())
          If res = #ERR_FAIL
            res = _HELO(*this, Hostname())
          EndIf
          If res = #ERR_NONE
            *this\Caps = \GetReceivedString()                       ; Features-String des Servers (nur bei EHLO verf�gbar) speichern
            secured    = DoSecure(*this, flags)
            loggedin   = DoLogin(*this, flags)
            If secured  <> #ERR_NONE : secured  = DoSecure(*this, flags) : EndIf
            If loggedin <> #ERR_NONE : loggedin = DoLogin(*this, flags)  : EndIf
            If (secured <> #ERR_NONE) Or (loggedin <> #ERR_NONE)
              res = #ERR_LOGIN
            EndIf
          EndIf
        EndIf
        If res <> #ERR_NONE                   ; nicht eingeloggt? Verbindung komplett trennen
          \Disconnect()
        EndIf
      EndIf
    EndWith
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _SendMail(*this.sSMTPClient, *msg.AWMessage::Message)
    Protected von.s, an.s, *src, res.i, srclen.i, erg.i, k.i, i.i, an2.s
    
    erg = #ERR_FAIL
    If *msg <> #Null
      If *msg\DecodeHeader()
        *src   = *msg\GetMailSourceBin()
        srclen = *msg\GetSize()
        von    = *msg\GetHeaderField(AWMessage::#HDR_FROM_MAIL)
        an     = *msg\GetHeaderField(AWMessage::#HDR_TO_MAILS)
        an     + "," + *msg\GetHeaderField(AWMessage::#HDR_CC_MAILS)
        an     + "," + *msg\GetHeaderField(AWMessage::#HDR_BCC_MAILS)
        
        res    = _MAIL_FROM(*this, von)
        If res = #ERR_NONE
          k = CountString(an, ",") + 1
          For i = 1 To k
            an2 = StringField(an, i, ",")
            If an2 <> ""
              res = _RCPT_TO(*this, an2)
              If res <> #ERR_NONE : Break : EndIf
            EndIf
          Next
          
          If res = #ERR_NONE
            res = _DATA(*this)
            If res = #ERR_NONE
              res = SendDataBin(*this, *src, srclen)
              If res = #ERR_NONE
                res = FinishData(*this)
              EndIf
            EndIf
          EndIf
        EndIf
      EndIf
    EndIf
        
    ProcedureReturn res
  EndProcedure
  
  Procedure   DisConnect(*this.sSMTPClient)
    Protected res.i
    
    res = _QUIT(*this)
    If res = #ERR_NONE
      *this\Conn\DisConnect()
    EndIf
    
    ProcedureReturn res
  EndProcedure
  
  Procedure.s UserCommand(*this.sSMTPClient, cmd.s, *rc)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1
    If *rc = #Null : ProcedureReturn "" : EndIf
    PokeI(*rc, 0)
    
    Repeat
      res = *this\Conn\Execute(cmd, AWNetwork::#LE_SMTP, retries)
      If res = #ERR_NONE
        PokeI(*rc, *this\Conn\GetResultCode())
        done = #True
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ProcedureReturn *this\Conn\GetReceivedString()
    EndIf

    ProcedureReturn ""
  EndProcedure

  Procedure   Destroy(*this.sSMTPClient)
    DisConnect(*this)
    If *this\Conn : *this\Conn\Destroy() : EndIf
    FreeStructure(*this)
  EndProcedure
  Procedure.s GetLog(*this.sSMTPClient)
		ProcedureReturn *this\Conn\GetLog()
  EndProcedure
  Procedure   ClearLog(*this.sSMTPClient)
  	*this\Conn\ClearLog()
  EndProcedure
  Procedure   EnableLog(*this.sSMTPClient, enabled.i)
  	*this\Conn\EnableLog(enabled)
  EndProcedure
  Procedure.s GetLastError(*this.sSMTPClient)
    ProcedureReturn *this\Conn\GetLastError()
  EndProcedure
  Procedure   ClearLastError(*this.sSMTPClient)
    *this\Conn\ClearLastError()
  EndProcedure
  
  DataSection
    SMTPClient_VT:
      Data.i @Destroy()
      Data.i @SetTimeout()
      Data.i @SetHost()
      Data.i @SetLogin()
      Data.i @SetProxyHost()
      Data.i @SetProxyLogin()
      Data.i @Connect()
      Data.i @DisConnect()
      Data.i @_SendMail()
      Data.i @GetLog()
      Data.i @ClearLog()
      Data.i @EnableLog()
      Data.i @GetLastError()
      Data.i @ClearLastError()
      Data.i @_NOOP()
      Data.i @_RSET()
      Data.i @UserCommand()
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        Data.i @SetSSLMethod()
      CompilerEndIf
      
      
  EndDataSection
EndModule


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 26
; Folding = ---------
; EnableUnicode
; EnableXP