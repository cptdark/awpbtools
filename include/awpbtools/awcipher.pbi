﻿;{ awcipher.pbi
;  Version 0.1 [2015/10/28]
;  Copyright (C) 2015 Ronny Krueger
;
;  This file is part of AWPB-Tools.
;
;  AWPB-Tools is free software: you can redistribute it and/or modify
;  it under the terms of the GNU Lesser General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  AWPB-Tools is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU Lesser General Public License for more details.
;
;  You should have received a copy of the GNU Lesser General Public License
;  along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

 CompilerIf Defined(AWPBT_EnableCipherMD5, #PB_Module)
	UseMD5Fingerprint()
CompilerEndIf
CompilerIf Defined(AWPBT_EnableCipherSHA1, #PB_Module)
	UseSHA1Fingerprint()
CompilerEndIf

DeclareModule AWCipher
	CompilerIf Defined(AWPBT_EnableCipherMD5, #PB_Module)
	  Declare.s HMAC_MD5(str.s, key.s)
	CompilerEndIf
	CompilerIf Defined(AWPBT_EnableCipherSHA1, #PB_Module)
	  Declare.s HMAC_SHA1(str.s, key.s)
	CompilerEndIf
EndDeclareModule

Module AWCipher
  EnableExplicit
   
  Structure hmac_data
		msgLen.i
		keyLen.i
		*msg
		key.a[66]
		iPad.a[64]
		oPad.a[64]
	EndStructure
	
	Procedure.s HMAC(msg.s, key.s, method.i, hashsize.i)
		Protected hmac.hmac_data, fp.i, digest.s, k.i, i.i

		With hmac
			digest         = ""
			\msgLen = StringByteLength(msg, #PB_UTF8)
			\keyLen = StringByteLength(key, #PB_UTF8)
			\msg    = AllocateMemory(\msgLen + 2)
			
			If \msg
				PokeS(\msg, msg, -1, #PB_UTF8)
				
				If \keyLen > 64
					\keyLen = hashsize
					digest     = StringFingerprint(key, method, 0, #PB_UTF8)
					i       = 0
					For k = 1 To hashsize * 2 Step 2
						\key[i] = Val("$" + Mid(digest, k, 2))
						i + 1
					Next k
				Else
					PokeS(@\key, key, -1, #PB_Ascii)
				EndIf
				
				CopyMemory(@\key, @\iPad, \keyLen)
				CopyMemory(@\key, @\oPad, \keyLen)
			
				For k = 0 To 63
					\iPad[k] ! $36 
					\oPad[k] ! $5c 
				Next k
			
				fp = StartFingerprint(#PB_Any, method)
				AddFingerprintBuffer(fp, @\iPad, 64)
				If \msgLen > 0
					AddFingerprintBuffer(fp, \msg, \msgLen)
				EndIf
				digest = FinishFingerprint(fp)
				i   = 0
				For k = 1 To hashsize * 2 Step 2
					\key[i] = Val("$" + Mid(digest, k, 2))
					i + 1
				Next k
				
				fp = StartFingerprint(#PB_Any, method)
				AddFingerprintBuffer(fp, @\oPad, 64)
				AddFingerprintBuffer(fp, @\key, hashsize)
				digest = FinishFingerprint(fp)
				
				FreeMemory(\msg)
			EndIf
		EndWith
	
		ProcedureReturn digest
	EndProcedure
	
	CompilerIf Defined(AWPBT_EnableCipherMD5, #PB_Module)
		Procedure.s HMAC_MD5(str.s, key.s)
			ProcedureReturn HMAC(str, key, #PB_Cipher_MD5, 16)	
		EndProcedure
	CompilerEndIf
	CompilerIf Defined(AWPBT_EnableCipherSHA1, #PB_Module)
		Procedure.s HMAC_SHA1(str.s, key.s)
			ProcedureReturn HMAC(str, key, #PB_Cipher_SHA1, 20)	
		EndProcedure
	CompilerEndIf
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 114
; FirstLine = 65
; Folding = ---
; EnableUnicode
; EnableXP