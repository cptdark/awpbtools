﻿;{   awpbtools.pbi
;    Version 0.5 [2015/10/28]
;    Copyright (C) 2014-15 Ronny Krüger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;} 

; This must be declared before including this file to use the AWPBTools library.
; DeclareModule AWPBTools_Settings
;   #AdditionalLibPath = ""
;   #Enable_DynamicSQLite3Lib = 1
;   #Enable_ChunkyFileCustomEncryption = 1
;   #Enable_ChunkyFileAES = 1
;   #Enable_ChunkyFileJSON = 1
;   #Enable_ChunkyFileXML = 1
;   #Enable_OpenSSL = 1
;   #Enable_EnableMessageAES = 1
; 	#Enable_Cipher_MD5 = 1
; 	#Enable_Cipher_SHA1 = 1
;   
;   #Use_AWCharset = 1
;   #Use_AWChunkyFile = 1
;   #Use_AWCustomCrypt = 1
;   #Use_AWDate = 1
;   #Use_AWCode = 1
;   #Use_AWGnuPG = 1
;   #Use_AWLocale = 1
;   #Use_AWMessage = 1
;   #Use_AWRegExSup = 1
;   #Use_AWSQLiteDB = 1
;   #Use_AWSqlStatement = 1
;   #Use_AWSupport = 1
;   #Use_AWZip = 1
;   #Use_AWZipArc = 1
;   #Use_AWSmtp = 1
;   #Use_AWPop3 = 1
;   #Use_AWMediaInfo = 1
;   #Use_AWImap = 1
;   #Use_AWClamAV = 1
;   #Use_AWRandom = 1
;   #Use_AWCipher = 1
;		#Use_AWSplashWindow = 1
;		#Use_AWSettings = 1
;		#Use_AWDcRaw = 1
;		#Use_AWFFMpeg = 1
;   #Use_AWMouseOver = 1
; 
; EndDeclareModule

XIncludeFile "awmacros.pbi"
   
CompilerIf Not Defined(AWPBTools_Settings, #PB_Module)
  DeclareModule AWPBTools_Settings
  EndDeclareModule
CompilerEndIf

Module AWPBTools_Settings 
EndModule 

UseModule AWPBTools_Settings

Macro EnableChunkyFileCustomEncryption
  CompilerIf Not Defined(AWPBT_EnableChunkyFileCustomEncryption, #PB_Module)
    DeclareModule AWPBT_EnableChunkyFileCustomEncryption
    EndDeclareModule
    Module AWPBT_EnableChunkyFileCustomEncryption
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableChunkyFileAES
  CompilerIf Not Defined(AWPBT_EnableChunkyFileAES, #PB_Module)
    DeclareModule AWPBT_EnableChunkyFileAES
    EndDeclareModule
    Module AWPBT_EnableChunkyFileAES
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableChunkyFileJSON
  CompilerIf Not Defined(AWPBT_EnableChunkyFileJSON, #PB_Module)
    DeclareModule AWPBT_EnableChunkyFileJSON
    EndDeclareModule
    Module AWPBT_EnableChunkyFileJSON
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableChunkyFileXML
  CompilerIf Not Defined(AWPBT_EnableChunkyFileXML, #PB_Module)
    DeclareModule AWPBT_EnableChunkyFileXML
    EndDeclareModule
    Module AWPBT_EnableChunkyFileXML
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableDynSQLite3Lib
  CompilerIf Not Defined(AWPBT_EnableDynSQLite3Lib, #PB_Module)
    DeclareModule AWPBT_EnableDynSQLite3Lib
      Define altpath.s, initialized.i
    EndDeclareModule
    Module AWPBT_EnableDynSQLite3Lib 
      CompilerIf Defined(AWPBTools_Settings::AdditionalLibPath, #PB_Constant)
        altpath = AWPBTools_Settings::#AdditionalLibPath
      CompilerElse
        altpath = ""
      CompilerEndIf
      initialized = #False
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableMessageAES
  CompilerIf Not Defined(AWPBT_EnableMessageAES, #PB_Module)
    EnableChunkyFileAES
    DeclareModule AWPBT_EnableMessageAES
    EndDeclareModule
    Module AWPBT_EnableMessageAES
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableOpenSSL
  CompilerIf Not Defined(AWPBT_EnableOpenSSL, #PB_Module)
    DeclareModule AWPBT_EnableOpenSSL
      Define altpath.s, initialized.i
    EndDeclareModule
    Module AWPBT_EnableOpenSSL
      CompilerIf Defined(AWPBTools_Settings::AdditionalLibPath, #PB_Constant)
        altpath = AWPBTools_Settings::#AdditionalLibPath
      CompilerElse
        altpath = ""
      CompilerEndIf
      initialized = #False
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableCipherMD5
  CompilerIf Not Defined(AWPBT_EnableCipherMD5, #PB_Module)
    DeclareModule AWPBT_EnableCipherMD5
    EndDeclareModule
    Module AWPBT_EnableCipherMD5
    EndModule 
  CompilerEndIf
EndMacro
Macro EnableCipherSHA1
  CompilerIf Not Defined(AWPBT_EnableCipherSHA1, #PB_Module)
    DeclareModule AWPBT_EnableCipherSHA1
    EndDeclareModule
    Module AWPBT_EnableCipherSHA1
    EndModule 
  CompilerEndIf
EndMacro

CompilerIf Defined(Enable_ChunkyFileCustomEncryption, #PB_Constant)
  EnableChunkyFileCustomEncryption
CompilerEndIf
CompilerIf Defined(Enable_ChunkyFileAES, #PB_Constant)
  EnableChunkyFileAES
CompilerEndIf
CompilerIf Defined(Enable_ChunkyFileJSON, #PB_Constant)
  EnableChunkyFileJSON
CompilerEndIf
CompilerIf Defined(Enable_ChunkyFileXML, #PB_Constant)
  EnableChunkyFileXML
CompilerEndIf
CompilerIf Defined(Enable_DynamicSQLite3Lib, #PB_Constant)
  EnableDynSQLite3Lib
CompilerEndIf
CompilerIf Defined(Enable_OpenSSL, #PB_Constant)
  EnableOpenSSL
CompilerEndIf
CompilerIf Defined(Enable_EnableMessageAES, #PB_Constant)
  EnableMessageAES
CompilerEndIf
CompilerIf Defined(Enable_Cipher_MD5, #PB_Constant)
  EnableCipherMD5
CompilerEndIf
CompilerIf Defined(Enable_Cipher_SHA1, #PB_Constant)
  EnableCipherSHA1
CompilerEndIf

CompilerIf Defined(Use_AWCharset, #PB_Constant)
  XIncludeFile "awcharset.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWChunkyFile, #PB_Constant)
  XIncludeFile "awchunkyfile.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWDate, #PB_Constant)
  XIncludeFile "awdate.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWCode, #PB_Constant)
  XIncludeFile "awcode.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWGnuPG, #PB_Constant)
  XIncludeFile "awgnupg.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWLocale, #PB_Constant)
  XIncludeFile "awlocale.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWMessage, #PB_Constant)
  XIncludeFile "awmessage.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWRegExSup, #PB_Constant)
  XIncludeFile "awregexsup.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSqlStatement, #PB_Constant)
  XIncludeFile "awsqlstatement.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSQLiteDB, #PB_Constant)
  XIncludeFile "awsqlitedb.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSupport, #PB_Constant)
  XIncludeFile "awsupport.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWZip, #PB_Constant)
  XIncludeFile "awzip.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWZipArc, #PB_Constant)
  XIncludeFile "awziparc.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWPop3, #PB_Constant)
  XIncludeFile "awpop3.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSmtp, #PB_Constant)
	EnableCipherMD5
	XIncludeFile "awsmtp.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWMediaInfo, #PB_Constant)
  XIncludeFile "awmediainfo.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWImap, #PB_Constant)
  XIncludeFile "awimap.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWCustomCrypt, #PB_Constant)
  XIncludeFile "awcustomcrypt.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWClamAV, #PB_Constant)
  XIncludeFile "awclamav.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWRandom, #PB_Constant)
  XIncludeFile "awrandom.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWCipher, #PB_Constant)
  XIncludeFile "awcipher.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSplashWindow, #PB_Constant)
  XIncludeFile "awsplashwindow.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWSettings, #PB_Constant)
  XIncludeFile "awsettings.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWDcRaw, #PB_Constant)
  XIncludeFile "awdcraw.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWFFMpeg, #PB_Constant)
  XIncludeFile "awffmpeg.pbi"
CompilerEndIf
CompilerIf Defined(Use_AWMouseOver, #PB_Constant)
  XIncludeFile "awmouseover.pbi"
CompilerEndIf

UnuseModule AWPBTools_Settings

; IDE Options = PureBasic 5.42 Beta 1 LTS (Linux - x64)
; CursorPosition = 268
; FirstLine = 128
; Folding = HAAg-----
; EnableUnicode
; EnableThread
; EnableXP
; EnablePurifier