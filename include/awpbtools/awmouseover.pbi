﻿;    awmouseover.pbi
;    Version 0.1 [2016/02/03]
;    Copyright (C) 2016 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.

DeclareModule AWMouseOver
	Interface MouseOver
		Destroy ()
		Enable  ()
		Disable ()
	EndInterface
	
	Declare.i New(window.i, timer.i, timeout.i = 100)
EndDeclareModule

Module AWMouseOver
  EnableExplicit
  
  CompilerIf #PB_Compiler_OS = #PB_OS_Windows
    Import ""
  CompilerElse
    ImportC ""
  CompilerEndIf
      PB_Object_EnumerateStart( PB_Objects )
      PB_Object_EnumerateNext( PB_Objects, *ID.Integer )
      PB_Object_EnumerateAbort( PB_Objects )
      PB_Window_Objects.i
      PB_Gadget_Objects.i
    EndImport
    
  Structure sMouseOver
    *vt.VT_MouseOver
    
    mWindow.i
    mTimer.i
    mTimeOut.i
    mLastGadget.i
    mLastX.i
    mLastY.i
    mIsActive.i
  EndStructure
  
	NewList *instances.sMouseOver()
	
	Procedure.i GetThis(eventWin.i)
		Protected *this.sMouseOver
		Shared *instances()
		
		*this = #Null
		
		ResetList(*instances())
		While NextElement(*instances())
			If *instances()\mWindow = eventWin
				*this = *instances()
				Break
			EndIf
  	Wend
  	
  	ProcedureReturn *this
	EndProcedure

  Procedure   EventHandler()
  	Protected *this.sMouseOver
  	Protected eventwin.i, found.i, x.i, y.i, gadget.i
    
    eventwin = EventWindow()
  	*this    = GetThis(eventwin)
  	
  	If Not *this           : ProcedureReturn : EndIf
  	If Not *this\mIsActive : ProcedureReturn : EndIf
  	
    x = WindowMouseX(eventwin)
    y = WindowMouseY(eventwin)
    If (x <> *this\mLastX) Or (y <> *this\mLastY)
    	*this\mLastX = x
    	*this\mLastY = y
      found        = #False

      PB_Object_EnumerateStart(PB_Gadget_Objects)
    	While PB_Object_EnumerateNext(PB_Gadget_Objects, @gadget)
    		If (x >= GadgetX(gadget)) And (x <= GadgetWidth(gadget) + GadgetX(gadget)) And (y >= GadgetY(gadget)) And (y <= GadgetHeight(gadget) + GadgetY(gadget))
    			found = #True
    			PB_Object_EnumerateAbort(PB_Gadget_Objects)
          Break
        EndIf
      Wend
            
      If found
      	If *this\mLastGadget <> gadget
      		If *this\mLastGadget <> #PB_Any
      			PostEvent(#PB_Event_Gadget, *this\mWindow, *this\mLastGadget, #PB_EventType_MouseLeave)
      		EndIf
	      	PostEvent(#PB_Event_Gadget, *this\mWindow, gadget, #PB_EventType_MouseEnter)
      		*this\mLastGadget = gadget
      	EndIf
      	ProcedureReturn
      Else
      	; should this really be done?
      	If *this\mLastGadget <> #PB_Any
      		PostEvent(#PB_Event_Gadget, *this\mWindow, *this\mLastGadget, #PB_EventType_MouseLeave)
      		*this\mLastGadget = #PB_Any
      	EndIf      	
      EndIf
    EndIf
  EndProcedure
  
  Procedure   MouseOver_Enable(*this.sMouseOver)
		If Not *this\mIsActive
			AddWindowTimer(*this\mWindow, *this\mTimer, *this\mTimeOut)
			BindEvent(#PB_Event_Timer , @EventHandler(), *this\mWindow)
      
      *this\mIsActive = #True
		EndIf
	EndProcedure	
  Procedure   MouseOver_Disable(*this.sMouseOver)
		If *this\mIsActive
      *this\mIsActive = #False
      
      UnbindEvent(#PB_Event_Timer, @EventHandler(), *this\mWindow)
      RemoveWindowTimer(*this\mWindow, *this\mTimer)
		EndIf
	EndProcedure
	Procedure   MouseOver_Destroy(*this.sMouseOver)
		Shared *instances()
		
		ResetList(*instances())
		While NextElement(*instances())
			If *instances() = *this
				DeleteElement(*instances())
				Break
			EndIf
  	Wend
  	
  	MouseOver_Disable(*this)
		
		FreeStructure(*this)
	EndProcedure
	
  Procedure.i New(window.i, timer.i, timeout.i = 100)
    Protected *this.sMouseOver
		Shared *instances()

    If Not IsWindow(window) : ProcedureReturn #Null : EndIf
    
    *this = AllocateStructure(sMouseOver)
    If *this
    	*this\vt          = ?VT_MouseOver
    	*this\mLastGadget = #PB_Any
    	*this\mLastX      = 0
    	*this\mLastY      = 0
    	*this\mTimeOut    = timeout
    	*this\mTimer      = timer
    	*this\mWindow     = window
    	*this\mIsActive   = #False
    	
    	AddElement(*instances())
			*instances() = *this

      ProcedureReturn *this
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
	
  DataSection ;{ Virtual Function Table
    VT_MouseOver:
      Data.i @MouseOver_Destroy()
      Data.i @MouseOver_Enable()
      Data.i @MouseOver_Disable()
    
  EndDataSection
EndModule

; IDE Options = PureBasic 5.42 Beta 1 LTS (Linux - x64)
; CursorPosition = 92
; FirstLine = 68
; Folding = -5
; EnableUnicode
; EnableXP