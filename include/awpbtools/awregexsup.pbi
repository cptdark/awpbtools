﻿;{   awregexsup.pbi
;    Version 0.3 [2014/09/24]
;    Copyright (C) 2014 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWRegExSup
  Enumeration 0
    #NO_BRACKETS
    #ANGLE_BRACKETS
    #ROUND_BRACKETS
    #SQUARE_BRACKETS
    #CURLY_BRACKETS
    #SINGLE_QUOTE
    #DOUBLE_QUOTES
  EndEnumeration
  
  Declare.s CreateMailAdrList(adr.s, separator.s = ":", brackets.i = #NO_BRACKETS)
EndDeclareModule

Module AWRegExSup
  EnableExplicit

  Procedure.s CreateMailAdrList(adr.s, separator.s = ":", brackets.i = #NO_BRACKETS)
    Protected k.i, ex.i, res.s, a.i
    Protected NewMap al.s()
    Protected Dim result$(0)
    
    res = ""
    ex  = CreateRegularExpression(#PB_Any, "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"+Chr(34)+"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"+Chr(34)+")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])")
    If ex
      k = ExtractRegularExpression(ex, adr, result$())
      If k
        For a = 0 To k - 1
          res = LCase(result$(a))
          Select brackets
            Case #NO_BRACKETS:      al(res) = res
            Case #ANGLE_BRACKETS:   al(res) = "<" + res + ">"
            Case #ROUND_BRACKETS:   al(res) = "(" + res + ")"
            Case #SQUARE_BRACKETS:  al(res) = "[" + res + "]"
            Case #CURLY_BRACKETS:   al(res) = "{" + res + "}"
            Case #SINGLE_QUOTE:     al(res) = "'" + res + "'"
            Case #DOUBLE_QUOTES:    al(res) = Chr(34) + res + Chr(34)
          EndSelect
        Next a
        
        ResetMap(al())
        If NextMapElement(al()) : res = al() : Else : res = "" : EndIf
        While NextMapElement(al())
          res + separator + al()
        Wend
        
        ClearMap(al())
      EndIf
      FreeRegularExpression(ex)
    EndIf

    ProcedureReturn res
  EndProcedure

EndModule


