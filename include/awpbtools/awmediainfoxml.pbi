﻿;{   awmediainfoxml.pbi
;    Version 0.1 [2015/11/13]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWMediaInfoXML
  Interface MediaInfoXML
    Destroy      		()
    GetValueList.i	(track.s, trackid.i, field.s, List values.s())
  EndInterface
  
  Declare.i New(xmlsrc.s)
EndDeclareModule

Module AWMediaInfoXML
	EnableExplicit
	
  Structure sMediaInfoXML
    *vt.VT
    
    xml.i
  EndStructure

  Procedure.i New(xmlsrc.s)
    Protected *this.sMediaInfoXML
    
    *this = AllocateStructure(sMediaInfoXML)
    If *this
    	*this\vt  = ?VT
    	*this\xml = ParseXML(#PB_Any, xmlsrc)
    	
    	If *this\xml
	  		If XMLStatus(*this\xml) = #PB_XML_Success
	  			ProcedureReturn *this
	  		EndIf
	  		FreeXML(*this\xml)
	  	EndIf
      
      FreeStructure(*this)
    EndIf
    
    ProcedureReturn #Null
  EndProcedure
  Procedure   AWMediaInfoXML_Destroy(*this.sMediaInfoXML)
  	If IsXML(*this\xml)
  		FreeXML(*this\xml)
  	EndIf
    
    FreeStructure(*this)
  EndProcedure
  Procedure.i AWMediaInfoXML_GetValueList(*this.sMediaInfoXML, track.s, trackid.i, field.s, List values.s())
  	Protected main.i, child.i, node.i, ok.i
  	
  	ClearList(values())
  	
  	track = LCase(track)
  	field = LCase(field)
  	
  	If Not IsXML(*this\xml)
  		ProcedureReturn #False
  	EndIf
  	
  	main = MainXMLNode(*this\xml)
  	If LCase(GetXMLNodeName(main)) <> "file"
  		ProcedureReturn #False
  	EndIf
  	
  	ok    = #False
 		child = ChildXMLNode(main)
  	While child
  		If LCase(GetXMLNodeName(child)) = "track"
  			If LCase(GetXMLAttribute(child, "type")) = track
  				If trackid <> #PB_Any
			  		node = ChildXMLNode(child)
			  		While node
			  			If LCase(GetXMLNodeName(node)) = "id"
			  				If Val(GetXMLNodeText(node)) = trackid
			  					ok = #True
			  					Break 2
			  				EndIf
			  			EndIf
			  			node = NextXMLNode(node)
			  		Wend
  				Else
	  				ok = #True
	  				Break
	  			EndIf
	  		EndIf		  		
  		EndIf
  		child = NextXMLNode(child)
  	Wend
  	
  	If ok
  		node = ChildXMLNode(child)
  		While node
  			If LCase(GetXMLNodeName(node)) = field
  				AddElement(values())
  				values() = GetXMLNodeText(node)
  			EndIf
  			node = NextXMLNode(node)
  		Wend
  	EndIf
  	
  	ProcedureReturn ok
  EndProcedure
  
  DataSection ;{ Virtual Function Table
  	VT:
    	Data.i @AWMediaInfoXML_Destroy()
    	Data.i @AWMediaInfoXML_GetValueList()
    	
  EndDataSection ;}
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 78
; FirstLine = 38
; Folding = P-
; EnableUnicode
; EnableXP