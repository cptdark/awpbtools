;{   awpop3.pbi
;    Version 0.10 [2015/10/26]
;    Copyright (C) 2011-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awpbtools.pbi"
XIncludeFile "awnetwork.pbi"
XIncludeFile "awsupport.pbi"

DeclareModule AWPop3
  #SSL_NONE     = $00000000
  #SSL_TRY      = $00000001
  #SSL_FORCE    = $00000002
  #SSL_FULL     = $00000004
  #CONN_USER    = $00000040
  #CONN_APOP    = $00000080
  #CONN_AUTO    = #CONN_USER | #CONN_APOP
  #CONN_DEFAULT = #SSL_TRY | #CONN_AUTO
  
  #ERR_NONE     =  0
  #ERR_FAIL     = -1
  #ERR_LOGIN    = -2
  #ERR_USER     = -3
  #ERR_PASS     = -4
  #ERR_TIMEOUT  = AWNetwork::#ERR_TIMEOUT
  
  #RES_MULTILINE  = 1
  #RES_SINGLELINE = 2
  
  #ERR_OK   = AWNetwork::#ERR_OK
  #ERR_ERR  = AWNetwork::#ERR_ERR

  Structure sID
    uid.s
    index.l
  EndStructure
  
  Interface POP3Client
    Destroy               ()
    SetTimeout            (timeout.i = 15000)
    SetHost               (host.s, port.i)
    SetLogin              (user.s, pass.s)
    SetProxyHost          (type.i, host.s, port.i)
    SetProxyLogin         (user.s, pass.s)    
    Connect.i             (flags.i = #CONN_DEFAULT)
    DisConnect            ()
    GetMailCount.i        ()
    GetMailHeader.s       (index.i)
    GetFullMail.s         (index.i)
    RemoveMail.i          (index.i)
    GetUID.s              (index.i)
    GetUIDList.i          (List ID.sID())
    GetLog.s							()
    ClearLog							()
    EnableLog							(enabled.i)
    GetLastError.s        ()
    ClearLastError        ()
    NOOP                  ()
    RSet                  ()
    UserCommand.s         (cmd.s, *rc, restype.i)
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      SetSSLMethod        (sslmethod.i)
    CompilerEndIf
    
  EndInterface
  
  Declare.i New()
  Declare.i OpenSSLAvailable()
EndDeclareModule

Module AWPop3
  EnableExplicit
  
  Macro IsSet(X, Y)
    (((X) & (Y)) = (Y))
  EndMacro
    
  Structure sPOP3Client
    *vt.POP3Client
    *Conn.AWNetwork::Connection
    Caps.s
    TimeStamp.s
  EndStructure
  
  Procedure.i New()
    Protected *this.sPOP3Client, *ci.AWNetwork::ConnInfo
  
    *this = AllocateStructure(sPOP3Client)
    If *this
      *this\vt   = ?POP3Client_VT
      *this\Caps = ""
      *this\Conn = AWNetwork::New()
      If *this\Conn
        *this\Conn\ClearLog()
        *ci          = *this\Conn\GetConnInfo()
        *ci\Timeout  = 15000
        *ci\Protocol = AWNetwork::#PR_POP3
        CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
          *ci\SSLMethod = AWNetwork::#SSL_METHOD_V23
        CompilerEndIf
        
        ProcedureReturn *this
      EndIf
      FreeStructure(*this)
    EndIf
  
    ProcedureReturn #Null
  EndProcedure
  Procedure.i OpenSSLAvailable()
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      ProcedureReturn AWPBT_EnableOpenSSL::initialized
    CompilerElse
      ProcedureReturn #False
    CompilerEndIf
  EndProcedure
  
  Procedure   SetTimeout(*this.sPOP3Client, timeout.i = 15000)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci         = *this\Conn\GetConnInfo()
    *ci\Timeout = timeout
  EndProcedure
  Procedure   SetHost(*this.sPOP3Client, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\Host = host
    *ci\Port = port
  EndProcedure
  Procedure   SetLogin(*this.sPOP3Client, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci      = *this\Conn\GetConnInfo()
    *ci\User = user
    *ci\Pass = pass
  EndProcedure
  Procedure   SetProxyHost(*this.sPOP3Client, type.i, host.s, port.i)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyHost = host
    *ci\ProxyPort = port
    *ci\ProxyMode = type
  EndProcedure
  Procedure   SetProxyLogin(*this.sPOP3Client, user.s, pass.s)
    Protected *ci.AWNetwork::ConnInfo
    
    *ci           = *this\Conn\GetConnInfo()
    *ci\ProxyUser = user
    *ci\ProxyPass = pass
  EndProcedure
  CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
    Procedure   SetSSLMethod(*this.sPOP3Client, sslmethod.i)
      Protected *ci.AWNetwork::ConnInfo
    
      *ci           = *this\Conn\GetConnInfo()
      *ci\SSLMethod = sslmethod
    EndProcedure
  CompilerEndIf
    
  Procedure.i _NOOP(*this.sPOP3Client)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("NOOP", AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.i _QUIT(*this.sPOP3Client)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("QUIT", AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.i _RSET(*this.sPOP3Client)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("RSET", AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.i _DELE(*this.sPOP3Client, index.i)
    Protected res.i, retries.i, done.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("DELE " + Str(index), AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.s _CAPA(*this.sPOP3Client)
    Protected res.i, retries.i, done.i, ret.s, p.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    ret     = ""
    Repeat
      If *this\Conn\Execute("CAPA", AWNetwork::#LE_POP3_ML, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ret = *this\Conn\GetReceivedString()
      p   = FindString(ret, Chr(10))
      If p = 0 : p   = FindString(ret, Chr(13))       : EndIf
      If p > 0 : ret = AWSupport::TrimWS(Mid(ret, p)) : EndIf
    EndIf
    
    ProcedureReturn ret
  EndProcedure
  Procedure.i _STAT(*this.sPOP3Client)
    Protected res.i, retries.i, done.i
  
    retries = -1
    res     = 0
    done    = #False
    Repeat
      If *this\Conn\Execute("STAT", AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = Val(StringField(*this\Conn\GetReceivedString(), 2, " ")) : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    ProcedureReturn res
  EndProcedure
  Procedure.s _TOP(*this.sPOP3Client, index.i)
    Protected res.i, retries.i, done.i, ret.s, p.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    ret     = ""
    Repeat
      If *this\Conn\Execute("TOP " + Str(index) + " 0", AWNetwork::#LE_POP3_ML, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ret = *this\Conn\GetReceivedString()
      p   = FindString(ret, Chr(10))
      If p = 0 : p   = FindString(ret, Chr(13))       : EndIf
      If p > 0 : ret = AWSupport::TrimWS(Mid(ret, p, Len(ret) - p - 3)) : EndIf
    EndIf
    
    ProcedureReturn ret
  EndProcedure
  Procedure.s _RETR(*this.sPOP3Client, index.i)
    Protected res.i, retries.i, done.i, ret.s, p.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    ret     = ""
    Repeat
      If *this\Conn\Execute("RETR " + Str(index), AWNetwork::#LE_POP3_ML, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ret = *this\Conn\GetReceivedString()
      p   = FindString(ret, Chr(10))
      If p = 0 : p   = FindString(ret, Chr(13))       : EndIf
      If p > 0 : ret = AWSupport::TrimWS(Mid(ret, p, Len(ret) - p - 3)) : EndIf
    EndIf
    
    ProcedureReturn ret
  EndProcedure
  Procedure.s _LIST(*this.sPOP3Client)
    Protected res.i, retries.i, done.i, ret.s, p.i
  
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    ret     = ""
    Repeat
      If *this\Conn\Execute("LIST", AWNetwork::#LE_POP3_ML, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ret = *this\Conn\GetReceivedString()
      p   = FindString(ret, Chr(10))
      If p = 0 : p   = FindString(ret, Chr(13))       : EndIf
      If p > 0 : ret = AWSupport::TrimWS(Mid(ret, p, Len(ret) - p - 3)) : EndIf
    EndIf
    
    ProcedureReturn ret
  EndProcedure
  Procedure.i _USER(*this.sPOP3Client)
    Protected res.i, retries.i, done.i, *ci.AWNetwork::ConnInfo
    
    *ci     = *this\Conn\GetConnInfo()
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("USER " + *ci\User, AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE  : done = #True
          Case #ERR_ERR:  res = #ERR_LOGIN : done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.i _PASS(*this.sPOP3Client)
    Protected res.i, retries.i, done.i, *ci.AWNetwork::ConnInfo
    
    *ci     = *this\Conn\GetConnInfo()
    retries = 1
    res     = #ERR_FAIL
    done    = #False
    
    Repeat
      If *this\Conn\Execute("PASS " + *ci\Pass, AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE  : done = #True
          Case #ERR_ERR:  res = #ERR_LOGIN : done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)

    ProcedureReturn res
  EndProcedure
  Procedure.i _APOP(*this.sPOP3Client)
    Protected res.i, retries.i, done.i, *ci.AWNetwork::ConnInfo, digest.s
    
    res = #ERR_FAIL
    If *this\TimeStamp <> ""
      *ci     = *this\Conn\GetConnInfo()
      retries = 1
      done    = #False
      digest  = StringFingerprint(*this\TimeStamp + *ci\pass, #PB_Cipher_MD5, 0, #PB_Ascii)
      
      Repeat
        If *this\Conn\Execute("APOP " + *ci\user + " " + digest, AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case #ERR_OK:   res = #ERR_NONE  : done = #True
            Case #ERR_ERR:  res = #ERR_LOGIN : done = #True
            Default:        retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i _STLS(*this.sPOP3Client)
    Protected res.i, retries.i, done.i
  
    res = #ERR_FAIL
    CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
      retries = 1
      done    = #False
      Repeat
        If *this\Conn\Execute("STLS", AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
          Select *this\Conn\GetResultCode()
            Case #ERR_OK:   res = #ERR_NONE : done = #True
            Case #ERR_ERR:  done = #True
            Default:        retries - 1
          EndSelect
        Else
          retries - 1
        EndIf
      Until done Or (retries <= 0)

      If res = #ERR_NONE
      	If *this\Conn\UpgradeSSL() <> AWNetwork::#SSL_ERR_NONE
      		res = #ERR_FAIL
      	EndIf
      EndIf
    CompilerEndIf

    ProcedureReturn res
  EndProcedure
  Procedure.s _UIDL(*this.sPOP3Client, index.i)
    Protected res.i, retries.i, done.i, ret.s, p.i
  
    retries = -1
    ret     = ""
    res     = #ERR_NONE
    done    = #False
    Repeat
      If *this\Conn\Execute("UIDL " + Str(index), AWNetwork::#LE_POP3_SL, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      p = FindString(ret, " ")
      If p <> 0
        p = FindString(ret, " ", p + 1)
        If p <> 0
          ret = Mid(ret, p + 1)
        EndIf
      EndIf
    EndIf
    
    ProcedureReturn ret
  EndProcedure
  Procedure.i _UIDL_LIST(*this.sPOP3Client, List ID.sID())
    Protected res.i, retries.i, done.i, p.i, np.i, l.s, s.s, ret.s
    
    retries = -1
    res     = 0
    done    = #False
    ClearList(ID())
    Repeat
      If *this\Conn\Execute("UIDL", AWNetwork::#LE_POP3_ML, retries) = #ERR_NONE
        Select *this\Conn\GetResultCode()
          Case #ERR_OK:   res = #ERR_NONE : done = #True
          Case #ERR_ERR:  done = #True
          Default:        retries - 1
        EndSelect
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ret = *this\Conn\GetReceivedString()
      l   = AWSupport::GetLine(ret, 1, @np)
      While np <> -1
        s = AWSupport::TrimWS(AWSupport::GetLine(ret, np, @np))
        p = FindString(s, " ")
        If p > 1
          AddElement(ID())
          ID()\index = Val(Left(s, p - 1))
          ID()\uid  = AWSupport::TrimWS(Mid(s, p))
        EndIf
      Wend
    EndIf
    
    ProcedureReturn res
  EndProcedure
  
  Procedure.i DoLogin(*this.sPOP3Client, flags.i)
    Protected res.i
    
    res = #ERR_FAIL
    If IsSet(flags, #CONN_APOP) And (FindString(*this\Caps, "APOP", 1) > 0)
      res = _APOP(*this)
    EndIf
    If (res <> #ERR_NONE) And IsSet(flags, #CONN_USER) And (FindString(*this\Caps, "USER", 1) > 0)   ; falls noch nicht eingeloggt, n�chste unsicherere methode versuchen
      res = _USER(*this)
      If res = #ERR_NONE
        res = _PASS(*this)
      EndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i DoSecure(*this.sPOP3Client, flags.i)
    Protected res.i
    
    res = #ERR_NONE
    If IsSet(flags, #SSL_FORCE) Or IsSet(flags, #SSL_TRY)
      res = #ERR_FAIL
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
  		  If OpenSSLAvailable()
  		    If FindString(*this\Caps, "STLS", 1, #PB_String_NoCase) > 0
  					; TLS starten; OpenSSL erforderlich 
            res = _STLS(*this)
            If res = #ERR_NONE
              *this\Caps = _CAPA(*this)
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If (res <> #ERR_NONE) And IsSet(flags, #SSL_TRY)
        res = #ERR_NONE
      EndIf
    EndIf

    ProcedureReturn res
  EndProcedure
  Procedure.i Connect(*this.sPOP3Client, flags.i = #CONN_DEFAULT)
    Protected res.i, rc.i, *ci.AWNetwork::ConnInfo, loggedin.i, secured.i, tsb.i, tse.i, ts.s
    
    *ci             = *this\Conn\GetConnInfo()
    res             = #ERR_FAIL
    *this\Caps      = ""
    *this\TimeStamp = ""
    loggedin        = #False
    secured         = #False
    With *this\Conn
      res = \Connect() 
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        If res = AWNetwork::#ERR_NONE
          If IsSet(flags, #SSL_FULL)
            res = #ERR_FAIL
            If OpenSSLAvailable()
              res = *this\Conn\UpgradeSSL()
            EndIf
          EndIf
        EndIf
      CompilerEndIf
      If res = AWNetwork::#ERR_NONE
        \ReadResult(AWNetwork::#LE_POP3_SL)
        If \GetResultCode() = #ERR_OK
          ts  = \GetReceivedString()
          tsb = FindString(ts, "<", 1)
          If tsb <> 0
            tse = AWSupport::FindLastChar(ts, '>')
            If tse > tsb
              *this\TimeStamp = Mid(ts, tsb, tse - tsb + 1)
            EndIf
          EndIf          
          *this\Caps = _CAPA(*this)
          secured    = DoSecure(*this, flags)
          loggedin   = DoLogin(*this, flags)
          If secured  <> #ERR_NONE : secured  = DoSecure(*this, flags) : EndIf
          If loggedin <> #ERR_NONE : loggedin = DoLogin(*this, flags)  : EndIf
          If (secured <> #ERR_NONE) Or (loggedin <> #ERR_NONE)
            res = #ERR_LOGIN
          EndIf
        EndIf
        If res <> #ERR_NONE                   ; nicht eingeloggt? Verbindung komplett trennen
          \Disconnect()
        EndIf
      EndIf
    EndWith
    
    ProcedureReturn res
  EndProcedure
  Procedure   DisConnect(*this.sPOP3Client)
    Protected res.i
    
    res = _QUIT(*this)
    If res = #ERR_NONE
      *this\Conn\DisConnect()
    EndIf
    
    ProcedureReturn res
  EndProcedure
  
  Procedure.i GetMailCount(*this.sPOP3Client)
    ProcedureReturn _STAT(*this)
  EndProcedure
  Procedure.s GetMailHeader(*this.sPOP3Client, index.i)
    ProcedureReturn _TOP(*this, index)
  EndProcedure
  Procedure.s GetFullMail(*this.sPOP3Client, index.i)
    ProcedureReturn _RETR(*this, index)
  EndProcedure
  Procedure.i RemoveMail(*this.sPOP3Client, index.i)
    ProcedureReturn _DELE(*this, index)
  EndProcedure
  Procedure.s GetUID(*this.sPOP3Client, index.i)
    ProcedureReturn _UIDL(*this, index)
  EndProcedure
  Procedure.i GetUIDList(*this.sPOP3Client, List ID.sID())
    ProcedureReturn _UIDL_LIST(*this, ID())
  EndProcedure
  
  Procedure   Destroy(*this.sPOP3Client)
    DisConnect(*this)
    If *this\Conn : *this\Conn\Destroy() : EndIf
    FreeStructure(*this)
  EndProcedure
  Procedure.s GetLog(*this.sPOP3Client)
		ProcedureReturn *this\Conn\GetLog()
  EndProcedure
  Procedure   ClearLog(*this.sPOP3Client)
  	*this\Conn\ClearLog()
  EndProcedure
  Procedure   EnableLog(*this.sPOP3Client, enabled.i)
  	*this\Conn\EnableLog(enabled)
  EndProcedure
  Procedure.s GetLastError(*this.sPOP3Client)
    ProcedureReturn *this\Conn\GetLastError()
  EndProcedure
  Procedure   ClearLastError(*this.sPOP3Client)
    *this\Conn\ClearLastError()
  EndProcedure
  
  Procedure.s UserCommand(*this.sPOP3Client, cmd.s, *rc, restype.i)
    Protected res.i, retries.i, done.i
  
    res     = #ERR_FAIL
    done    = #False
    retries = 1
    If *rc = #Null : ProcedureReturn "" : EndIf
    PokeI(*rc, 0)
    
    Repeat
      Select restype
        Case #RES_MULTILINE
          res = *this\Conn\Execute(cmd, AWNetwork::#LE_POP3_ML, retries)
        Case #RES_SINGLELINE
          res = *this\Conn\Execute(cmd, AWNetwork::#LE_POP3_ML, retries)
      EndSelect
      
      If res = #ERR_NONE
        PokeI(*rc, *this\Conn\GetResultCode())
        done = #True
      Else
        retries - 1
      EndIf
    Until done Or (retries <= 0)
    
    If res = #ERR_NONE
      ProcedureReturn *this\Conn\GetReceivedString()
    EndIf

    ProcedureReturn ""
  EndProcedure

  DataSection
    POP3Client_VT:
      Data.i @Destroy()
      Data.i @SetTimeout()
      Data.i @SetHost()
      Data.i @SetLogin()
      Data.i @SetProxyHost()
      Data.i @SetProxyLogin()
      Data.i @Connect()
      Data.i @DisConnect()
      Data.i @GetMailCount()
      Data.i @GetMailHeader()
      Data.i @GetFullMail()
      Data.i @RemoveMail()
      Data.i @GetUID()
      Data.i @GetUIDList()
      Data.i @GetLog()
      Data.i @ClearLog()
      Data.i @EnableLog()
      Data.i @GetLastError()
      Data.i @ClearLastError()
      Data.i @_NOOP()
      Data.i @_RSET()
      Data.i @UserCommand()
      CompilerIf Defined(AWPBT_EnableOpenSSL, #PB_Module)
        Data.i @SetSSLMethod()
      CompilerEndIf
  EndDataSection
EndModule

  
; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 443
; FirstLine = 439
; Folding = ----------
; EnableUnicode
; EnableXP