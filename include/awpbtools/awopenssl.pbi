;{   awopenssl.pbi
;    Version 0.8 [2014/11/18]
;    Copyright (C) 2012-2014 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;
;
; If using this interface in your project, please acknowledge following terms:
;
; License regarding OpenSSL
; =========================
;
;  LICENSE ISSUES
;  ==============
;
;  The OpenSSL toolkit stays under a dual license, i.e. both the conditions of
;  the OpenSSL License and the original SSLeay license apply to the toolkit.
;  See below for the actual license texts. Actually both licenses are BSD-style
;  Open Source licenses. In case of any license issues related to OpenSSL
;  please contact openssl-core@openssl.org.
;
;  OpenSSL License
;  ---------------
;
;/* ====================================================================
; * Copyright (c) 1998-2011 The OpenSSL Project.  All rights reserved.
; *
; * Redistribution and use in source and binary forms, with or without
; * modification, are permitted provided that the following conditions
; * are met:
; *
; * 1. Redistributions of source code must retain the above copyright
; *    notice, this list of conditions and the following disclaimer.
; *
; * 2. Redistributions in binary form must reproduce the above copyright
; *    notice, this list of conditions and the following disclaimer in
; *    the documentation and/or other materials provided with the
; *    distribution.
; *
; * 3. All advertising materials mentioning features or use of this
; *    software must display the following acknowledgment:
; *    "This product includes software developed by the OpenSSL Project
; *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
; *
; * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
; *    endorse or promote products derived from this software without
; *    prior written permission. For written permission, please contact
; *    openssl-core@openssl.org.
; *
; * 5. Products derived from this software may not be called "OpenSSL"
; *    nor may "OpenSSL" appear in their names without prior written
; *    permission of the OpenSSL Project.
; *
; * 6. Redistributions of any form whatsoever must retain the following
; *    acknowledgment:
; *    "This product includes software developed by the OpenSSL Project
; *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
; *
; * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
; * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
; * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
; * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
; * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
; * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
; * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
; * OF THE POSSIBILITY OF SUCH DAMAGE.
; * ====================================================================
; *
; * This product includes cryptographic software written by Eric Young
; * (eay@cryptsoft.com).  This product includes software written by Tim
; * Hudson (tjh@cryptsoft.com).
; *
; */
;
; Original SSLeay License
; -----------------------
;
;/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
; * All rights reserved.
; *
; * This package is an SSL implementation written
; * by Eric Young (eay@cryptsoft.com).
; * The implementation was written so as to conform with Netscapes SSL.
; *
; * This library is free for commercial and non-commercial use as long as
; * the following conditions are aheared to.  The following conditions
; * apply to all code found in this distribution, be it the RC4, RSA,
; * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
; * included with this distribution is covered by the same copyright terms
; * except that the holder is Tim Hudson (tjh@cryptsoft.com).
; *
; * Copyright remains Eric Young's, and as such any Copyright notices in
; * the code are not to be removed.
; * If this package is used in a product, Eric Young should be given attribution
; * as the author of the parts of the library used.
; * This can be in the form of a textual message at program startup or
; * in documentation (online or textual) provided with the package.
; *
; * Redistribution and use in source and binary forms, with or without
; * modification, are permitted provided that the following conditions
; * are met:
; * 1. Redistributions of source code must retain the copyright
; *    notice, this list of conditions and the following disclaimer.
; * 2. Redistributions in binary form must reproduce the above copyright
; *    notice, this list of conditions and the following disclaimer in the
; *    documentation and/or other materials provided with the distribution.
; * 3. All advertising materials mentioning features or use of this software
; *    must display the following acknowledgement:
; *    "This product includes cryptographic software written by
; *     Eric Young (eay@cryptsoft.com)"
; *    The word 'cryptographic' can be left out if the rouines from the library
; *    being used are not cryptographic related :-).
; * 4. If you include any Windows specific code (or a derivative thereof) from
; *    the apps directory (application code) you must include an acknowledgement:
; *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
; *
; * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
; * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
; * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
; * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
; * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
; * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
; * SUCH DAMAGE.
; *
; * The licence and distribution terms for any publically available version or
; * derivative of this code cannot be changed.  i.e. this code cannot simply be
; * copied and put under another distribution licence
; * [including the GNU Public Licence.]
; */
;
;
; Note: this is a reduced interface just to satisfy use with pop3/smtp
;}

XIncludeFile "awinternal.pbi"

DeclareModule AWOpenSSL
  #SSL_ERROR_NONE             = 0
  #SSL_ERROR_SSL              = 1
  #SSL_ERROR_WANT_READ        = 2
  #SSL_ERROR_WANT_WRITE       = 3
  #SSL_ERROR_WANT_X509_LOOKUP = 4
  #SSL_ERROR_SYSCALL          = 5 ; look at error stack/Return value/errno
  #SSL_ERROR_ZERO_RETURN      = 6
  #SSL_ERROR_WANT_CONNECT     = 7
  #SSL_ERROR_WANT_ACCEPT      = 8
  
  #SSL_MODE_AUTO_RETRY              = 4
  #SSL_VERIFY_NONE			            = $00
  #SSL_VERIFY_PEER			            = $01
  #SSL_VERIFY_FAIL_IF_NO_PEER_CERT	= $02
  #SSL_VERIFY_CLIENT_ONCE		        = $04
  
  Declare.i InitSSLInterface(alternatepath.s = "")
  Declare   DestroySSLInterface()
  Declare.i LibAvailable()
  Declare.i SSL_CTX_new(*meth)
  Declare   SSL_CTX_free(*arg0)
  Declare.i SSL_set_fd(*s, fd.i)
  Declare.i SSLv2_client_method()
  Declare.i SSLv3_client_method()
  Declare.i SSLv23_client_method()
  Declare.i TLSv1_client_method()
  Declare.i TLSv1_1_client_method()
  Declare.i TLSv1_2_client_method()
  Declare.i SSL_new(*ctx)
  Declare   SSL_free(*ssl)
  Declare.l SSL_connect(*ssl)
  Declare.l SSL_shutdown(*ssl)
  Declare.l SSL_read(*ssl, *buf, len.l)
  Declare.l SSL_peek(*ssl, *buf, len.l)
  Declare.l SSL_write(*ssl, *buf, len.l)
  Declare.l SSL_pending(*ssl)
  Declare.s SSL_get_version(*ssl)
  Declare.l SSL_get_error(*ssl, ret_code.l)
  Declare   SSL_set_connect_state(*ssl)
  Declare.s ERR_error_string(err.l)
  Declare.i SSL_set_mode(*ssl, larg.i)
  Declare   SSL_CTX_set_verify(*ctx, mode.l, *callback)
  Declare   SSL_set_verify(*ssl, mode.l, *callback)
  Declare.l SSL_CTX_set_cipher_list(*ctx, *str)
  Declare.l SSL_set_cipher_list(*ssl, *str)
  
EndDeclareModule

Module AWOpenSSL
  EnableExplicit

  PrototypeC.l Proto_SSL_get_error(*ssl, ret_code.l)
  PrototypeC.i Proto_SSL_library_init()
  PrototypeC   Proto_SSL_load_error_strings()
  PrototypeC.i Proto_SSL_CTX_new(*meth)
  PrototypeC   Proto_SSL_CTX_free(*arg0)
  PrototypeC.i Proto_SSL_set_fd(*s, fd.l)
;-  PrototypeC.i Proto_SSL_set_fd(*s, fd.l)
  PrototypeC.i Proto_SSLv2_client_method()
  PrototypeC.i Proto_SSLv3_client_method()
  PrototypeC.i Proto_TLSv1_client_method()
  PrototypeC.i Proto_TLSv1_1_client_method()
  PrototypeC.i Proto_TLSv1_2_client_method()
  PrototypeC.i Proto_SSLv23_client_method()
  PrototypeC.i Proto_SSL_new(*ctx)
  PrototypeC   Proto_SSL_free(*ssl)
  PrototypeC.l Proto_SSL_connect(*ssl)
  PrototypeC.l Proto_SSL_shutdown(*ssl)
  PrototypeC.l Proto_SSL_read(*ssl, *buf, num.l)
  PrototypeC.l Proto_SSL_peek(*ssl, *buf, num.l)
  PrototypeC.l Proto_SSL_write(*ssl, *buf, num.l)
  PrototypeC.l Proto_SSL_pending(*ssl)
  PrototypeC.i Proto_SSL_get_version(*ssl) ; returns char*
  PrototypeC   Proto_SSL_set_connect_state(*ssl)
  PrototypeC.i Proto_SSL_ctrl(*ssl, cmd.l, larg.i, *parg)
  PrototypeC   Proto_SSL_CTX_set_verify(*ctx, mode.l, *callback)
  PrototypeC   Proto_SSL_set_verify(*ssl, mode.l, *callback)
  PrototypeC.l Proto_SSL_CTX_set_cipher_list(*ctx, *str)
  PrototypeC.l Proto_SSL_set_cipher_list(*ssl, *str)
  
  PrototypeC.i Proto_ERR_error_string(err.l, *buf) 

  #SSL_CTRL_MODE                  = 33
  #OPTIONAL_LIBSSLUTIL_FUNCTIONS  = 0
  #OPTIONAL_LIBSSL_FUNCTIONS      = 6
  
  Structure libssl_table
    LibHandle.i
    SSL_get_error.Proto_SSL_get_error
    SSL_library_init.Proto_SSL_library_init
    SSL_load_error_strings.Proto_SSL_load_error_strings
    SSL_CTX_new.Proto_SSL_CTX_new
    SSL_CTX_free.Proto_SSL_CTX_free
    SSL_set_fd.Proto_SSL_set_fd
    SSL_new.Proto_SSL_new
    SSL_free.Proto_SSL_free
    SSL_connect.Proto_SSL_connect
    SSL_shutdown.Proto_SSL_shutdown
    SSL_read.Proto_SSL_read
    SSL_peek.Proto_SSL_peek
    SSL_write.Proto_SSL_write
    SSL_pending.Proto_SSL_pending
    SSL_get_version.Proto_SSL_get_version
    SSL_set_connect_state.Proto_SSL_set_connect_state
    SSL_ctrl.Proto_SSL_ctrl
    SSL_CTX_set_verify.Proto_SSL_CTX_set_verify
    SSL_set_verify.Proto_SSL_set_verify
    SSL_CTX_set_cipher_list.Proto_SSL_CTX_set_cipher_list
    SSL_set_cipher_list.Proto_SSL_set_cipher_list
    
    ; optional
    SSLv2_client_method.Proto_SSLv2_client_method
    SSLv3_client_method.Proto_SSLv3_client_method
    SSLv23_client_method.Proto_SSLv23_client_method
    TLSv1_client_method.Proto_TLSv1_client_method
    TLSv1_1_client_method.Proto_TLSv1_1_client_method
    TLSv1_2_client_method.Proto_TLSv1_2_client_method
  EndStructure
  Structure libsslutil_table
    LibHandle.i
    ERR_error_string.Proto_ERR_error_string
  EndStructure
  
  Global LibSSL.libssl_table, LibSSLUtil.libsslutil_table
  
  Procedure.i TestLibStructure(*ptr, entries.i)
  	Protected res.i, a.i, *p
  	
  	res = #True
  	*p  = *ptr
  	For a = 1 To entries
;-DEBUG		Debug "Entry " + Str(a) + " = " + Str(PeekI(*p))
  	  If PeekI(*p) = #Null
  			res = #False
;  			Break
  		EndIf
  		*p + SizeOf(a)
  	Next
  	
  	ProcedureReturn res
  EndProcedure
  Procedure   Free()
    If IsLibrary(LibSSL\LibHandle)     : CloseLibrary(LibSSL\LibHandle) : EndIf
    If IsLibrary(LibSSLUtil\LibHandle) : CloseLibrary(LibSSLUtil\LibHandle) : EndIf
  
    ClearStructure(@LibSSLUtil, libsslutil_table)
  EndProcedure
  Procedure.i Init(alternatepath.s = "")
    ClearStructure(@LibSSL, libssl_table)
    ClearStructure(@LibSSLUtil, libsslutil_table)
    
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
      	LibSSL\LibHandle = AWInternal::OpenSharedLibrary("ssleay32.dll", alternatepath)
      	LibSSLUtil\LibHandle = AWInternal::OpenSharedLibrary("libeay32.dll", alternatepath)
      CompilerCase #PB_OS_Linux
      	LibSSL\LibHandle = AWInternal::OpenSharedLibrary("libssl.so", alternatepath)
      	LibSSLUtil\LibHandle = AWInternal::OpenSharedLibrary("libcrypto.so", alternatepath)
    CompilerEndSelect
    
    If IsLibrary(LibSSL\LibHandle)
    	With LibSSL
        \SSL_get_error           = GetFunction(\LibHandle, "SSL_get_error")
        \SSL_library_init        = GetFunction(\LibHandle, "SSL_library_init")
        \SSL_load_error_strings  = GetFunction(\LibHandle, "SSL_load_error_strings")
        \SSL_CTX_new             = GetFunction(\LibHandle, "SSL_CTX_new")
        \SSL_CTX_free            = GetFunction(\LibHandle, "SSL_CTX_free")
        \SSL_set_fd              = GetFunction(\LibHandle, "SSL_set_fd")
        \SSL_new                 = GetFunction(\LibHandle, "SSL_new")
        \SSL_free                = GetFunction(\LibHandle, "SSL_free")
        \SSL_connect             = GetFunction(\LibHandle, "SSL_connect")
        \SSL_shutdown            = GetFunction(\LibHandle, "SSL_shutdown")
        \SSL_read                = GetFunction(\LibHandle, "SSL_read")
        \SSL_peek                = GetFunction(\LibHandle, "SSL_peek")
        \SSL_write               = GetFunction(\LibHandle, "SSL_write")
        \SSL_pending             = GetFunction(\LibHandle, "SSL_pending")
        \SSL_get_version         = GetFunction(\LibHandle, "SSL_get_version")
        \SSL_set_connect_state   = GetFunction(\LibHandle, "SSL_set_connect_state")
        \SSL_ctrl                = GetFunction(\LibHandle, "SSL_ctrl")
        \SSL_CTX_set_verify      = GetFunction(\LibHandle, "SSL_CTX_set_verify")
        \SSL_set_verify          = GetFunction(\LibHandle, "SSL_set_verify")
        \SSL_CTX_set_cipher_list = GetFunction(\LibHandle, "SSL_CTX_set_cipher_list")
        \SSL_set_cipher_list     = GetFunction(\LibHandle, "SSL_set_cipher_list")
        
        \SSLv2_client_method     = GetFunction(\LibHandle, "SSLv2_client_method")
        \SSLv3_client_method     = GetFunction(\LibHandle, "SSLv3_client_method")
        \SSLv23_client_method    = GetFunction(\LibHandle, "SSLv23_client_method")
        \TLSv1_client_method     = GetFunction(\LibHandle, "TLSv1_client_method")
        \TLSv1_1_client_method   = GetFunction(\LibHandle, "TLSv1_1_client_method")
        \TLSv1_2_client_method   = GetFunction(\LibHandle, "TLSv1_2_client_method")
      EndWith
    EndIf
    
    If IsLibrary(LibSSLUtil\LibHandle)
    	With LibSSLUtil
        \ERR_error_string       = GetFunction(\LibHandle, "ERR_error_string")
      EndWith
      
      If TestLibStructure(@LibSSL, SizeOf(LibSSL) / SizeOf(LibSSL\LibHandle) - #OPTIONAL_LIBSSL_FUNCTIONS) And TestLibStructure(@LibSSLUtil, SizeOf(LibSSLUtil) / SizeOf(LibSSLUtil\LibHandle) - #OPTIONAL_LIBSSLUTIL_FUNCTIONS)
  			ProcedureReturn #True
   		EndIf
    EndIf
  
    Free()
  
    ProcedureReturn #False
  EndProcedure
  Procedure.i InitSSLInterface(alternatepath.s = "")
    Protected result.i
  
    result = #False
    If Init(alternatepath)
      LibSSL\SSL_library_init()
      LibSSL\SSL_load_error_strings()
      result = #True
    EndIf
  
    ProcedureReturn result
  EndProcedure
  Procedure   DestroySSLInterface()
    Free()
  EndProcedure
  
  Procedure.i LibAvailable()
  	ProcedureReturn Bool(IsLibrary(LibSSL\LibHandle) And IsLibrary(LibSSLUtil\LibHandle))
  EndProcedure
  Procedure.i SSL_CTX_new(*meth)
  	ProcedureReturn LibSSL\SSL_CTX_new(*meth)
  EndProcedure
  Procedure   SSL_CTX_free(*arg0)
    LibSSL\SSL_CTX_free(*arg0)
  EndProcedure
  Procedure.i SSL_set_fd(*s, fd.i)
    ProcedureReturn LibSSL\SSL_set_fd(*s, fd)
  EndProcedure
  Procedure.i SSLv2_client_method()
    If LibSSL\SSLv2_client_method
      ProcedureReturn LibSSL\SSLv2_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i SSLv3_client_method()
    If LibSSL\SSLv3_client_method
      ProcedureReturn LibSSL\SSLv3_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i SSLv23_client_method()
  	If LibSSL\SSLv23_client_method
      ProcedureReturn LibSSL\SSLv23_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i TLSv1_client_method()
    If LibSSL\TLSv1_client_method
      ProcedureReturn LibSSL\TLSv1_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i TLSv1_1_client_method()
    If LibSSL\TLSv1_1_client_method
      ProcedureReturn LibSSL\TLSv1_1_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i TLSv1_2_client_method()
    If LibSSL\TLSv1_2_client_method
      ProcedureReturn LibSSL\TLSv1_2_client_method()
    EndIf
    ProcedureReturn #Null
  EndProcedure
  Procedure.i SSL_new(*ctx)
    ProcedureReturn LibSSL\SSL_new(*ctx)
  EndProcedure
  Procedure   SSL_free(*ssl)
    LibSSL\SSL_free(*ssl)
  EndProcedure
  Procedure.l SSL_connect(*ssl)
    ProcedureReturn LibSSL\SSL_connect(*ssl)
  EndProcedure
  Procedure.l SSL_shutdown(*ssl)
    ProcedureReturn LibSSL\SSL_shutdown(*ssl)
  EndProcedure
  Procedure.l SSL_read(*ssl, *buf, len.l)
    ProcedureReturn LibSSL\SSL_read(*ssl, *buf, len)
  EndProcedure
  Procedure.l SSL_peek(*ssl, *buf, len.l)
    ProcedureReturn LibSSL\SSL_peek(*ssl, *buf, len)
  EndProcedure
  Procedure.l SSL_write(*ssl, *buf, len.l)
    ProcedureReturn LibSSL\SSL_write(*ssl, *buf, len)
  EndProcedure
  Procedure.l SSL_pending(*ssl)
    ProcedureReturn LibSSL\SSL_pending(*ssl)
  EndProcedure
  Procedure.s SSL_get_version(*ssl)
    Protected *res
    
    *res = LibSSL\SSL_get_version(*ssl)
    If *res
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.l SSL_get_error(*ssl, ret_code.l)
    ProcedureReturn LibSSL\SSL_get_error(*ssl, ret_code)
  EndProcedure
  Procedure   SSL_set_connect_state(*ssl)
    LibSSL\SSL_set_connect_state(*ssl)
  EndProcedure
  Procedure.i SSL_set_mode(*ssl, larg.i)
    ProcedureReturn LibSSL\SSL_ctrl(*ssl, #SSL_CTRL_MODE, larg, #Null)
  EndProcedure
  Procedure.s ERR_error_string(err.l)
    Protected *res
    
    *res = LibSSLUtil\ERR_error_string(err, #Null)
    If *res
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure   SSL_CTX_set_verify(*ctx, mode.l, *callback)
    LibSSL\SSL_CTX_set_verify(*ctx, mode, *callback)
  EndProcedure
  Procedure   SSL_set_verify(*ssl, mode.l, *callback)
    LibSSL\SSL_set_verify(*ssl, mode, *callback)
  EndProcedure
  Procedure.l SSL_CTX_set_cipher_list(*ctx, *str)
    LibSSL\SSL_CTX_set_cipher_list(*ctx, *str)
  EndProcedure  
  Procedure.l SSL_set_cipher_list(*ssl, *str)
    LibSSL\SSL_set_cipher_list(*ssl, *str)
  EndProcedure  

EndModule

;{ Automatic Initialization
AWPBT_EnableOpenSSL::initialized = AWOpenSSL::InitSSLInterface()
;}


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 498
; FirstLine = 451
; Folding = -------
; EnableUnicode
; EnableXP
