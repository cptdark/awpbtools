﻿;{ awinternal.pbi
;  Version 0.1 [2015/10/28]
;  Copyright (C) 2015 Ronny Krueger
;
;  This file is part of AWPB-Tools.
;
;  AWPB-Tools is free software: you can redistribute it and/or modify
;  it under the terms of the GNU Lesser General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  AWPB-Tools is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU Lesser General Public License for more details.
;
;  You should have received a copy of the GNU Lesser General Public License
;  along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awsupport.pbi"

DeclareModule AWInternal
	Declare.i OpenSharedLibrary(libname.s, altPaths.s = "")
EndDeclareModule

Module AWInternal
  EnableExplicit
  
  Procedure.i OpenSharedLibrary(libname.s, altPaths.s = "")
  	Protected paths.s, p.i, path.s, a.i, handle.i
  	
  	handle = #Null
  	paths  = altPaths
  	
    CompilerIf Defined(AWPBTools_Settings::AdditionalLibPath, #PB_Constant)
   		If paths <> ""
    		paths + Chr(10)
    	EndIf
      paths + AWPBTools_Settings::#AdditionalLibPath
    CompilerEndIf

    CompilerSelect #PB_Compiler_OS
    		CompilerCase #PB_OS_Linux
		    	If paths <> ""
		    		paths + Chr(10)
		    	EndIf
		    	paths + "./" + Chr(10) + "./lib/" + Chr(10) + "/usr/lib/"
		    CompilerCase #PB_OS_Windows
		    	paths + Chr(10)
	  CompilerEndSelect
	  
	  p.i = CountString(paths, Chr(10)) + 1
    For a = 1 To p
    	path   = Trim(StringField(paths, a, Chr(10)))
    	handle = OpenLibrary(#PB_Any, AWSupport::CombinePath(path, libname, #True))
   		
   		If IsLibrary(handle) 
   			Break
   		Else
   			handle = #Null
   		EndIf
   	Next
   	
   	ProcedureReturn handle
	EndProcedure
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 1
; Folding = -
; EnableUnicode
; EnableXP