﻿;{   awffmpeg.pbi
;    Version 0.1 [2015/11/16]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awsupport.pbi"

DeclareModule AWFFMpeg
  Structure FileInfo
  	FrameRate.d
  	FrameCount.l
  	Width.l
  	Height.l
  	Duration.d
  	AspectRatio.d
  	CodecName.s
		
  EndStructure
  ;{ Constants
  #FF_STREAM_VIDEO  				= "video"
  #FF_STREAM_AUDIO  				= "audio"

  #FF_CODEC_TYPE 						= "codec_type"
  #FF_FRAME_COUNT						= "nb_frames"
  #FF_CODEC_NAME						= "codec_name"
  #FF_WIDTH									= "width"
  #FF_HEIGHT								= "height"
  #FF_DURATION							= "duration"
  #FF_AVG_FRAME_RATE				= "avg_frame_rate"
  #FF_INDEX									= "index"
  #FF_CODEC_LONG_NAME				= "codec_long_name"
  #FF_PROFILE								= "profile"
  #FF_CODED_WIDTH						= "coded_width"
  #FF_CODED_HEIGHT					= "coded_height"
	#FF_CODEC_TAG_STRING			= "codec_tag_string"  
  #FF_CODEC_TAG							= "codec_tag"
  #FF_SAMPLE_ASPECT_RATIO		= "sample_aspect_ratio"
  #FF_DISPLAY_ASPECT_RATIO	= "display_aspect_ratio"
  #FF_HAS_B_FRAMES					= "has_b_frames"
  #FF_PIX_FMT								= "pix_fmt"
  #FF_LEVEL									= "level"
  #FF_COLOR_RANGE						= "color_range"
  #FF_COLOR_SPACE						= "color_space"
  #FF_COLOR_TRANSFER				= "color_transfer"
  #FF_COLOR_PRIMARIES				= "color_primaries"
  #FF_CHROMA_LOCATION				= "chroma_location"
  #FF_TIMECODE							= "timecode"
  #FF_REFS									= "refs"
  #FF_IS_AVC								= "is_avc"
  #FF_NAL_LENGTH_SIZE				= "nal_length_size"
  #FF_ID										= "id"
  #FF_R_FRAME_RATE					= "r_frame_rate"
  #FF_TIME_BASE							= "time_base"
  #FF_START_PTS							= "start_pts"
  #FF_START_TIME						= "start_time"
  #FF_DURATION_TS						= "duration_ts"
  #FF_BIT_RATE							= "bit_rate"
  #FF_MAX_BIT_RATE					= "max_bit_rate"
  #FF_BITS_PER_RAW_SAMPLE		= "bits_per_raw_sample"
  #FF_NB_FRAMES							= "nb_frames"
  #FF_NB_READ_FRAMES				= "nb_read_frames"
  #FF_NB_READ_PACKETS				= "nb_read_packets"
  #FF_DIS_DEFAULT						= "DISPOSITION:default"
  #FF_DIS_DUB								= "DISPOSITION:dub"
  #FF_DIS_ORIGINAL					= "DISPOSITION:original"
  #FF_DIS_COMMENT						= "DISPOSITION:comment"
  #FF_DIS_LYRICS						= "DISPOSITION:lyrics"
  #FF_DIS_KARAOKE						= "DISPOSITION:karaoke"
  #FF_DIS_FORCED						= "DISPOSITION:forced"
  #FF_DIS_HEARING_IMPAIRED	= "DISPOSITION:hearing_impaired"
  #FF_DIS_VISUAL_IMPAIRED		= "DISPOSITION:visual_impaired"
  #FF_DIS_CLEAN_EFFECTS			= "DISPOSITION:clean_effects"
  #FF_DIS_ATTACHED_PIC			= "DISPOSITION:attached_pic"
  #FF_TAG_LANGUAGE					= "TAG:language"
  #FF_TAG_HANDLER_NAME			= "TAG:handler_name"
  #FF_SAMPLE_FMT						= "sample_fmt"
  #FF_SAMPLE_RATE						= "sample_rate"
  #FF_CHANNELS							= "channels"
  #FF_CHANNEL_LAYOUT				= "channel_layout"
  #FF_BITS_PER_SAMPLE				= "bits_per_sample"
  ;}
  Enumeration
  	#IMG_TYPE_PNG
  	#IMG_TYPE_JPG
  EndEnumeration
  
  Declare   SetExe(ffmpeg.s, ffprobe.s)
  Declare.i AnalyzeVideoFile(filename.s, *errorcode = #Null)
  Declare.s GetInfo(*info.FileInfo, streamtype.s, field.s)
  Declare   FreeFileInfo(*info.FileInfo)
  Declare.i GenerateThumbnailsTiled(filename.s, imgcount.i, imgwidth.i, imgheight.i, tilex.i, tiley.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
  Declare.i GenerateThumbnailsTiledFill(filename.s, tilewidth.i, tileheight.i, tilex.i, tiley.i, tilecount.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
  Declare.i GenerateThumbnails(filename.s, imgcount.i, imgwidth.i, imgheight.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
EndDeclareModule

Module AWFFMpeg
  EnableExplicit
  
  Structure sStreamData
  	Map value.s()
  EndStructure
  Structure sFileInfo Extends FileInfo
  	List streams.sStreamData()
  EndStructure
  
  Global FFMpegCmd.s, FFProbeCmd.s
  
 	Procedure.i ExecuteFFMpeg(parameters.s, *errorcode)
  	Protected app.i, err.i, el.s
  	
  	app  = RunProgram(FFMpegCmd, parameters, "", #PB_Program_Open | #PB_Program_Error)
  	If IsProgram(app)
  		While ProgramRunning(app)
  			If WaitProgram(app, 1000)
  				Break
  			EndIf
  		Wend
  		err = ProgramExitCode(app)
	    PokeI(*errorcode, err)
	    CloseProgram(app)
	    
	    If Not err
	    	ProcedureReturn #True
	    EndIf
  	EndIf
  	
  	ProcedureReturn #False
  EndProcedure

  Procedure   SetExe(ffmpeg.s, ffprobe.s)
  	FFMpegCmd  = ffmpeg
  	FFProbeCmd = ffprobe
  EndProcedure
  Procedure.s GetInfo(*info.FileInfo, streamtype.s, field.s)
  	Protected res.s, *fi.sFileInfo
  	
  	res = ""
  	If *info
	  	*fi = *info
	  	
	  	ForEach *fi\streams()
	  		If *fi\streams()\value(#FF_CODEC_TYPE) = streamtype
	  			res = *fi\streams()\value(field)
	  			Break
	  		EndIf
	  	Next
	  EndIf
	  
  	ProcedureReturn res
  EndProcedure
  Procedure.i AnalyzeVideoFile(filename.s, *errorcode = #Null)
  	Protected app.i, res.s, err.i, *fi.sFileInfo
  	Protected p.i, sstart.i, send.i, lines.s, l.s, np.i
  	Protected r.s, fc.s
  	
  	*fi = #Null
  	err = 0
  	app = RunProgram(FFProbeCmd, "-v quiet -show_streams -i " + Chr(34) + filename + Chr(34), "", #PB_Program_Open | #PB_Program_Read | #PB_Program_Error)
  	If IsProgram(app)
  		While ProgramRunning(app)
  			If AvailableProgramOutput(app)
  				res + ReadProgramString(app) + Chr(10)
  			EndIf
	    Wend
	    res + Chr(10)
	    err = ProgramExitCode(app)
	    If *errorcode 
	    	PokeI(*errorcode, err)
	    EndIf
	    CloseProgram(app)
  	EndIf
  	
  	If Not err
  		*fi = AllocateStructure(sFileInfo)
  		If *fi
		  	sstart = 0
		  	send   = 1
		  	Repeat
		  		sstart = FindString(res, "[STREAM]", send)
		  		If sstart
		  			send = FindString(res, "[/STREAM]", sstart)
		  			If send
		  				lines = Mid(res, sstart + 9, send - sstart - 10)
		  				If lines <> ""
		  					AddElement(*fi\streams())
		  					; lines is one block -> make map
								np = 1
								While np <> -1
		  						l = AWSupport::GetLine(lines, np, @np)
		  						p = FindString(l, "=")
		  						If p
		  							*fi\streams()\value(LCase(Left(l, p - 1))) = Mid(l, p + 1)
		  						EndIf
		  					Wend
		  				EndIf
		  			EndIf
		  		EndIf
		  	Until Not sstart Or Not send
		  	
		  	*fi\FrameCount = Val(GetInfo(*fi, #FF_STREAM_VIDEO, #FF_FRAME_COUNT))
		  	*fi\CodecName  = GetInfo(*fi, #FF_STREAM_VIDEO, #FF_CODEC_NAME)
		  	*fi\Width			 = Val(GetInfo(*fi, #FF_STREAM_VIDEO, #FF_WIDTH))
		  	*fi\Height		 = Val(GetInfo(*fi, #FF_STREAM_VIDEO, #FF_HEIGHT))
		  	*fi\Duration	 = ValD(GetInfo(*fi, #FF_STREAM_VIDEO, #FF_DURATION))
		  	
		  	fc = GetInfo(*fi, #FF_STREAM_VIDEO, #FF_DISPLAY_ASPECT_RATIO)
  			If fc <> "n/a"
  				p = FindString(fc, ":")
  				If p
  					l = Left(fc, p - 1)
  					r = Mid(fc, p + 1)
  					If r <> "0"
  						*fi\AspectRatio = ValD(l) / ValD(r)
  					EndIf
  				Else
  					*fi\AspectRatio = ValD(fc)
  				EndIf
  			EndIf
  			
  			fc = GetInfo(*fi, #FF_STREAM_VIDEO, #FF_AVG_FRAME_RATE)
  			If fc <> "n/a" And fc <> "0/0"
  				p = FindString(fc, "/")
  				If p
  					l = Left(fc, p - 1)
  					r = Mid(fc, p + 1)
  					If r <> "0"
  						*fi\FrameRate = ValD(l) / ValD(r)
  					EndIf
  				Else
  					*fi\FrameRate = ValD(fc)
  				EndIf
  			EndIf
		  EndIf
  	EndIf

  	ProcedureReturn *fi
  EndProcedure
  Procedure   FreeFileInfo(*info.FileInfo)
  	Protected *fi.sFileInfo
  	
  	*fi = *info
  	ForEach *fi\streams()
			ClearMap(*fi\streams()\value())
		Next
		ClearList(*fi\streams())
		
		FreeStructure(*fi)
  EndProcedure
  Procedure.i GenerateThumbnailsTiled(filename.s, imgcount.i, imgwidth.i, imgheight.i, tilex.i, tiley.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
  	Protected target.s, cmd.s, err.i, *fi.sFileInfo, it.s
  	
		Select imagetype
			Case #IMG_TYPE_JPG
				it = "jpg"
			Case #IMG_TYPE_PNG
				it = "png"
			Default
				ProcedureReturn #Null
		EndSelect
		
  	If *info
  		*fi = *info
  	Else
  		*fi = AnalyzeVideoFile(filename)
  	EndIf
  	  	
  	If *fi
			If *fi\FrameCount
  			target = AWSupport::CombinePath(folder, basename + "." + it)
  			cmd    = "-i " + Chr(34) + filename + Chr(34)
  			cmd    + " -vsync 0 -vf "
  			cmd    + Chr(34) + "select='not(mod(n,floor(" + Str(*fi\FrameCount) + "/" + Str(imgcount - 1) + ")))',scale=" + Str(imgwidth) + ":" + Str(imgheight) + ",tile=" + Str(tilex) + "x" + Str(tiley) + Chr(34)
  			cmd    + " " + Chr(34) + target + Chr(34)
  			
  			If ExecuteFFMpeg(cmd, @err)
  				If Not *info
  					FreeFileInfo(*fi)
  				EndIf
  				
  				ProcedureReturn #True
	  		EndIf
  		EndIf
			If Not *info
				FreeFileInfo(*fi)
			EndIf
  	EndIf
  	
  	ProcedureReturn #False
  EndProcedure
  Procedure.i GenerateThumbnailsTiledFill(filename.s, tilewidth.i, tileheight.i, tilex.i, tiley.i, tilecount.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
  	ProcedureReturn GenerateThumbnailsTiled(filename, tilex * tiley * tilecount, tilewidth, tileheight, tilex, tiley, folder, basename, imagetype, *info)
  EndProcedure
  Procedure.i GenerateThumbnails(filename.s, imgcount.i, imgwidth.i, imgheight.i, folder.s, basename.s, imagetype.i, *info.FileInfo = #Null)
  	ProcedureReturn GenerateThumbnailsTiled(filename, imgcount, imgwidth, imgheight, 1, 1, folder, basename, imagetype, *info)
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 95
; FirstLine = 48
; Folding = -Dw
; EnableUnicode
; EnableXP