;{   awmediainfo.pbi
;    Version 0.6 [2015/11/13]
;    Copyright (C) 2011-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awinternal.pbi"
XIncludeFile "awmediainfoxml.pbi"

DeclareModule AWMediaInfo
  Enumeration
    #Stream_General
    #Stream_Video
    #Stream_Audio
    #Stream_Text
    #Stream_Chapters
    #Stream_Image
    #Stream_Menu
    #Stream_Max
  EndEnumeration
  Enumeration
    #Info_Name
    #Info_Text
    #Info_Measure
    #Info_Options
    #Info_NameText
    #Info_MeasureText
    #Info_Info
    #Info_HowTo
    #Info_Max
  EndEnumeration
  Enumeration
    #Option_ShowInInform
    #Option_Reserved
    #Option_ShowInSupported
    #Option_TypeOfValue
    #Option_Max
  EndEnumeration
  
  Interface MediaInfo
    Destroy     				()
    Open.i      				(FileName.s)
    Close       				()
    Version.s   				()
    GetInfoBasePlain.s	()
    GetInfoFullPlain.s	()
    GetInfoBaseXML.s		()
    GetInfoFullXML.s		()
    GetInfoBaseHTML.s		()
    GetInfoFullHTML.s		()
    GetFileSize.i				()
    MediaInfoA_Get.s    (StreamKind.i, StreamNumber.i, Parameter.s, InfoKind.i, SearchKind.i)
    MediaInfoA_GetI.s   (StreamKind.i, StreamNumber.i, Parameter.i, InfoKind.i)
  	MediaInfoA_Option.s (Option.s, Value.s)
  	MediaInfoA_Inform.s (Reserved.i)
  	GetMediaInfoXML.i   ()
  EndInterface
  
  Declare.i New()
  Declare.i InitLibrary(alternatepath.s = "")
  Declare   FreeLibrary()
EndDeclareModule

Module AWMediaInfo
  ;{ Prototypes
  Prototype.i Proto_MediaInfo_New()
  Prototype   Proto_MediaInfo_Delete(*Handle)
  Prototype.i Proto_MediaInfo_Open(*Handle, File.p-unicode)
  Prototype.i Proto_MediaInfoA_Get(*Handle, StreamKind.i, StreamNumber.i, Parameter.p-utf8, InfoKind.i, SearchKind.i)
  Prototype.i Proto_MediaInfoA_GetI(*Handle, StreamKind.i, StreamNumber.i, Parameter.i, InfoKind.i)
  Prototype.i Proto_MediaInfoA_Inform(*Handle, Reserved.i)
  Prototype.i Proto_MediaInfoA_Option(*Handle, Option.p-utf8, Value.p-utf8)
  Prototype   Proto_MediaInfoA_Close(*Handle)
  Prototype.i Proto_MediaInfoA_Count_Get(*Handle, Streamkind.i, StreamNumber.i)
  ;}
  
  Define initialized.i
  
  Structure sMediaInfo
    *vt.VT
    
    *Handle
    IsOpen.i
  EndStructure

  Structure MILibrary
    LibHandle.i
    MediaInfo_New.Proto_MediaInfo_New
    MediaInfo_Delete.Proto_MediaInfo_Delete
    MediaInfo_Open.Proto_MediaInfo_Open
    MediaInfoA_Inform.Proto_MediaInfoA_Inform
    MediaInfoA_Option.Proto_MediaInfoA_Option
    MediaInfoA_Close.Proto_MediaInfoA_Close
    MediaInfoA_Get.Proto_MediaInfoA_Get
    MediaInfoA_GetI.Proto_MediaInfoA_GetI
    MediaInfoA_Count_Get.Proto_MediaInfoA_Count_Get
  EndStructure
  
  Global LibMediaInfo.MILibrary
    
  Procedure.i TestLibStructure(*ptr, entries.i)
  	Protected res.i, a.i, *p
  	
  	res = #True
  	*p  = *ptr
  	For a = 1 To entries
  		If PeekI(*p) = #Null
  			res = #False
  			Break
  		EndIf
  		*p + SizeOf(a)
  	Next
  	
  	ProcedureReturn res
  EndProcedure
  Procedure   FreeLibrary()
    If IsLibrary(LibMediaInfo\LibHandle) : CloseLibrary(LibMediaInfo\LibHandle) : EndIf
  
    ClearStructure(@LibMediaInfo, MILibrary)
  EndProcedure
  Procedure.i InitLibrary(alternatepath.s = "")
  	ClearStructure(@LibMediaInfo, MILibrary)
    
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
      	LibMediaInfo\LibHandle = AWInternal::OpenSharedLibrary("mediainfo.dll", alternatepath)
      CompilerCase #PB_OS_Linux
      	LibMediaInfo\LibHandle = AWInternal::OpenSharedLibrary("libmediainfo.so", alternatepath)
    CompilerEndSelect
    
    If IsLibrary(LibMediaInfo\LibHandle)
      With LibMediaInfo
        \MediaInfo_Delete     = GetFunction(\LibHandle, "MediaInfo_Delete")
        \MediaInfo_New        = GetFunction(\LibHandle, "MediaInfo_New")
        \MediaInfoA_Close     = GetFunction(\LibHandle, "MediaInfoA_Close")
        \MediaInfoA_Inform    = GetFunction(\LibHandle, "MediaInfoA_Inform")
        \MediaInfo_Open       = GetFunction(\LibHandle, "MediaInfo_Open")
        \MediaInfoA_Option    = GetFunction(\LibHandle, "MediaInfoA_Option")
        \MediaInfoA_Get		    = GetFunction(\LibHandle, "MediaInfoA_Get")
        \MediaInfoA_GetI	    = GetFunction(\LibHandle, "MediaInfoA_GetI")
        \MediaInfoA_Count_Get = GetFunction(\LibHandle, "MediaInfoA_Count_Get")
      EndWith 
      
      If TestLibStructure(@LibMediaInfo, SizeOf(MILibrary) / SizeOf(MILibrary\LibHandle))
  			ProcedureReturn #True
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  
  Procedure.i New()
    Protected *this.sMediaInfo
    
    *this = AllocateStructure(sMediaInfo)
    If *this
      *this\vt     = ?VT
      *this\Handle = LibMediaInfo\MediaInfo_New()
      *this\IsOpen = #False
      
      If *this\Handle
        LibMediaInfo\MediaInfoA_Option(*this\Handle, "CodePage", "UTF-8")
        
        ProcedureReturn *this
      EndIf
      
      FreeStructure(*this)
    EndIf
    
    ProcedureReturn #Null
  EndProcedure
  Procedure   Close(*this.sMediaInfo)
    If *this\IsOpen
      LibMediaInfo\MediaInfoA_Close(*this\Handle)
      *this\IsOpen = #False
    EndIf
  EndProcedure
  Procedure   Destroy(*this.sMediaInfo)
    Close(*this)
    LibMediaInfo\MediaInfo_Delete(*this\Handle)
    
    FreeStructure(*this)
  EndProcedure
  Procedure.i Open(*this.sMediaInfo, FileName.s)
    Close(*this)
    
    If FileName <> ""
      If LibMediaInfo\MediaInfo_Open(*this\Handle, FileName)
        *this\IsOpen = #True
        ProcedureReturn #True
      EndIf
    EndIf
    
    ProcedureReturn #False
  EndProcedure
  Procedure.s Version(*this.sMediaInfo)
    Protected res.s, *info
    
    res   = ""
    *info = LibMediaInfo\MediaInfoA_Option(*this\Handle, "Info_Version", "")
    If *info
      res = PeekS(*info, -1, #PB_UTF8)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoBasePlain(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
  	If *this\IsOpen
  						LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "")
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoFullPlain(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
   	If *this\IsOpen
      				LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "1")
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoBaseXML(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
  	If *this\IsOpen
  					  LibMediaInfo\MediaInfoA_Option(*this\Handle, "Inform", "XML")
  					  LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "")
   		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoFullXML(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
   	If *this\IsOpen
  					  LibMediaInfo\MediaInfoA_Option(*this\Handle, "Inform", "XML")
      				LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "1")
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoBaseHTML(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
   	If *this\IsOpen
  					  LibMediaInfo\MediaInfoA_Option(*this\Handle, "Inform", "HTML")
      				LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "")
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.s GetInfoFullHTML(*this.sMediaInfo)
  	Protected res.s, *info
  	
  	res = ""
   	If *this\IsOpen
  					  LibMediaInfo\MediaInfoA_Option(*this\Handle, "Inform", "HTML")
      				LibMediaInfo\MediaInfoA_Option(*this\Handle, "Complete", "1")
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, 0)
      If *info
      	res = PeekS(*info, -1, #PB_UTF8)
      EndIf
    EndIf
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.i GetFileSize(*this.sMediaInfo)
  	Protected *info, sz.i
  	
  	sz = -1
   	If *this\IsOpen
   		*info = LibMediaInfo\MediaInfoA_Get(*this\Handle, #Stream_General, 0, "FileSize", #Info_Text, #Info_Name)
      If *info
      	sz  = Val(PeekS(*info, -1, #PB_UTF8))
      EndIf
    EndIf
  	
  	ProcedureReturn sz
  EndProcedure
  Procedure.s MediaInfoA_Get(*this.sMediaInfo, StreamKind.i, StreamNumber.i, Parameter.s, InfoKind.i, SearchKind.i)
  	Protected *info, res.s
  	
  	res = ""
  	If *this\IsOpen
  		*info = LibMediaInfo\MediaInfoA_Get(*this\Handle, StreamKind, StreamNumber, Parameter, InfoKind, SearchKind)
  		If *info
  			res = PeekS(*info, -1, #PB_UTF8)
  		EndIf
  	EndIf
  	
  	ProcedureReturn res	
  EndProcedure
  Procedure.s MediaInfoA_GetI(*this.sMediaInfo, StreamKind.i, StreamNumber.i, Parameter.i, InfoKind.i)
  	Protected *info, res.s
  	
  	res = ""
  	If *this\IsOpen
  		*info = LibMediaInfo\MediaInfoA_GetI(*this\Handle, StreamKind, StreamNumber, Parameter, InfoKind)
  		If *info
  			res = PeekS(*info, -1, #PB_UTF8)
  		EndIf
  	EndIf
  	
  	ProcedureReturn res	
  EndProcedure
  Procedure.s MediaInfoA_Option(*this.sMediaInfo, Option.s, Value.s)
  	Protected *info, res.s
  	
  	res = ""
  	If *this\IsOpen
  		*info = LibMediaInfo\MediaInfoA_Option(*this\Handle, Option, Value)
  		If *info
  			res = PeekS(*info, -1, #PB_UTF8)
  		EndIf
  	EndIf
  	
  	ProcedureReturn res	
  EndProcedure
  Procedure.s MediaInfoA_Inform(*this.sMediaInfo, Reserved.i)
  	Protected *info, res.s
  	
  	res = ""
  	If *this\IsOpen
  		*info = LibMediaInfo\MediaInfoA_Inform(*this\Handle, Reserved)
  		If *info
  			res = PeekS(*info, -1, #PB_UTF8)
  		EndIf
  	EndIf
  	
  	ProcedureReturn res	
  EndProcedure
  Procedure.i GetMediaInfoXML(*this.sMediaInfo)
  	Protected xmlsrc.s, *xml.AWMediaInfoXML::MediaInfoXML
  	
  	xmlsrc = GetInfoFullXML(*this)
  	*xml = AWMediaInfoXML::New(xmlsrc)
  	
  	ProcedureReturn *xml
  EndProcedure
  
  DataSection ;{ Virtual Function Table
  VT:
    Data.i @Destroy()
    Data.i @Open()
    Data.i @Close()
    Data.i @Version()
    Data.i @GetInfoBasePlain()
    Data.i @GetInfoFullPlain()
    Data.i @GetInfoBaseXML()
    Data.i @GetInfoFullXML()
    Data.i @GetInfoBaseHTML()
    Data.i @GetInfoFullHTML()
    Data.i @GetFileSize()
    Data.i @MediaInfoA_Get()
    Data.i @MediaInfoA_GetI()
    Data.i @MediaInfoA_Inform()
    Data.i @MediaInfoA_Option()
    Data.i @GetMediaInfoXML()
  EndDataSection ;}
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 402
; Folding = CAAAA-
; EnableUnicode
; EnableXP