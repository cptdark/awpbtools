﻿;{   awmacros.pbi
;    Version 0.1 [2015/11/20]
;    Copyright (C) 2015 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWMacros
	
	;- debugging
	Macro AssertDoubleQuote
	    "
	EndMacro
	Macro AssertForce(X)
		If Not (X)
			If MessageRequester("Assertion failed", AssertDoubleQuote#X#AssertDoubleQuote + Chr(10) + Chr(10) +
								                    					"File:" + Chr(9) + #PB_Compiler_Filename + Chr(10) +
								                              "Path:" + Chr(9) + #PB_Compiler_FilePath + Chr(10) +
								                              "Line:" + Chr(9) + Str(#PB_Compiler_Line) + Chr(10) +
								                              "Module:" + Chr(9) + #PB_Compiler_Module + Chr(10) +
								                              "Proc:" + Chr(9) + #PB_Compiler_Procedure + Chr(10) +
								                              "Date:" + Chr(9) + FormatDate("%mm/%dd/%yyyy %hh:%ii:%ss", #PB_Compiler_Date) + Chr(10) + Chr(10) +
								                              "Abort program?", #PB_MessageRequester_YesNo) = #PB_MessageRequester_Yes
				End -1
			EndIf
		EndIf
	EndMacro	
	Macro Assert(X)
		CompilerIf #PB_Compiler_Debugger
			AssertForce(X)
		CompilerEndIf
	EndMacro
	Macro AssertCallForce(X)
		AssertForce(X)
	EndMacro
	Macro AssertCall(X)
		CompilerIf #PB_Compiler_Debugger
			AssertCallForce(X)
		CompilerElse
			X
		CompilerEndIf
	EndMacro
	
	;- generic
	Macro ReturnIfNot(X, Y)
		If Not (X)
			ProcedureReturn Y
		EndIf
	EndMacro
	Macro ReturnIf(X, Y)
		If X
			ProcedureReturn Y
		EndIf
	EndMacro
	
	;- class specific
	Macro DestroyAndNull(X)
		If X
			X\Destroy()
			X = #Null
		EndIf
	EndMacro
	
	;- generic free
	Macro FreeImageAndNull(X)
		If IsImage(X)
			FreeImage(X)
			X = #Null
		EndIf
	EndMacro
	Macro FreeFontAndNull(X)
		If IsFont(X)
			FreeFont(X)
			X = #Null
		EndIf
	EndMacro
	Macro FreeStructureAndNull(X)
		If X
			FreeStructure(X)
			X = #Null
		EndIf
	EndMacro
	Macro FreeMemoryAndNull(X)
		If X
			FreeStructure(X)
			X = #Null
		EndIf
	EndMacro
	Macro FreeXMLAndNull(X)
		If IsXML(X)
			FreeXML(X)
			X = #Null
		EndIf
	EndMacro
	Macro FreeDialogAndNull(X)
		If IsDialog(X)
			FreeDialog(X)
			X = #Null
		EndIf
	EndMacro
	
EndDeclareModule

Module AWMacros
	
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 115
; Folding = DAg
; EnableUnicode
; EnableXP