﻿;{   awrandom.pbi
;    Version 0.2 [2015/10/26]
;    Copyright (C) 2014-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

UseMD5Fingerprint()
UseCRC32Fingerprint()

DeclareModule AWRandom
  Declare.i SeedQ(value.q)          ; Quad-Value
  Declare.i SeedA(value.s)          ; ASCII-String
  Declare.i SeedW(value.s)          ; UNICODE-String
  Declare.i SeedB(*buf, bufsize)    ; Binary Buffer
  Declare.q Rnd(max.q = $7FFFFFFFFFFFFFFF, min.q = 0)   ; get next random number [63 Bit]
  Declare.s RandomStringA(length.i)
  Declare.s RandomStringW(length.i)
  Declare.s RandomStringAlpha(length.i)
  Declare.s RandomStringAlphaNum(length.i)
  Declare.s RandomStringNum(length.i)
  Declare   RandomBuffer(*buf, bufsize.i)

EndDeclareModule

Module AWRandom
  EnableExplicit
  
  Structure sRand
    a.q
    b.q
  EndStructure
  Structure sDoubleQuad
    a.sRand
    b.sRand
  EndStructure
  Structure sQuadLong
    a.l
    b.l
    c.l
    d.l
  EndStructure    
  Structure sKey
    StructureUnion 
      a.sDoubleQuad
      b.sQuadLong
    EndStructureUnion
    c.l
  EndStructure
  
  Global key.sKey, value.sRand, current.sRand
  Global *seed  = #Null, slen.i  = 0
  Global Dim table.q(1023)
  
  Procedure.q GenNextNum(init.i = #False)
    Static index.i = 0, lastnum.q = 0, cnt.i = 0
      
    If init
      index   = 0
      lastnum = 0
      cnt     = 0
    Else
      AESEncoder(@current, @value, SizeOf(sRand), @table(index), 128, #Null, #PB_Cipher_ECB)
      index = ((lastnum | key\c) & $3FF)
      If index > 1022 : index = 0 : EndIf
      CopyMemory(@value, @current, SizeOf(sRand))
      
      Select cnt % 3
        Case 0: lastnum = value\a
        Case 1: lastnum = value\b
        Case 2: lastnum = value\a ! value\b
      EndSelect
      cnt + 1
    EndIf

    ProcedureReturn lastnum
  EndProcedure  
  Procedure   FreeSeed()
    If *seed  <> #Null : FreeMemory(*seed)  : EndIf
    *seed  = #Null
    slen   = 0
  EndProcedure
  Procedure.i InitSeed()
    Protected md5.s, i.i, crc64.i
    
    md5     = LCase(Fingerprint(*seed, slen, #PB_Cipher_MD5))      ; 32 Bytes
    key\b\a = Val("$" + Left(md5, 8))
    key\b\b = Val("$" +  Mid(md5, 9, 8))
    key\b\c = Val("$" +  Mid(md5, 17, 8))
    key\b\d = Val("$" +  Mid(md5, 25, 8))
    key\c   = Val("$" + Fingerprint(*seed, slen, #PB_Cipher_CRC32))
    crc64   = (key\c << 32) | key\c
    
    current\a = (key\b\a << 32) | key\b\b
    current\b = (key\b\c << 32) | key\b\d
    
    For i = 0 To 511
      AESEncoder(@current, @value, SizeOf(sRand), @key, 128, #Null, #PB_Cipher_ECB)
      CopyMemory(@value, @current, SizeOf(sRand))
      table(i) = value\a ! crc64
      table(i + 512) = value\b ! crc64
    Next
    
    GenNextNum(#True)
    
    ProcedureReturn #True
  EndProcedure
  Procedure.i SeedB(*buf, bufsize)
    Protected md5.s

    FreeSeed()
    If (*buf <> #Null) And (bufsize > 0)
      *seed = AllocateMemory(bufsize + 32)
      If *seed <> #Null
        md5 = LCase(Fingerprint(*buf, bufsize, #PB_Cipher_MD5))
        PokeS(*buf, md5, -1, #PB_Ascii | #PB_String_NoZero) 
        CopyMemory(*buf, *seed + 32, bufsize)
        slen = bufsize + 32
        
        ProcedureReturn InitSeed()
      EndIf
    EndIf
    
    ProcedureReturn #False
  EndProcedure
  Procedure.i SeedQ(value.q)
    ProcedureReturn SeedB(@value, SizeOf(value))
  EndProcedure
  Procedure.i SeedA(value.s)
    Protected l.i, *b, res.i
    
    res = #False
    If value <> ""
      l = StringByteLength(value, #PB_Ascii)
      CompilerIf #PB_Compiler_Unicode
        *b = AllocateMemory(l)
        If *b <> #Null
          PokeS(*b, value, -1, #PB_Ascii | #PB_String_NoZero)
          res = SeedB(*b, l)
          FreeMemory(*b)
        EndIf
      CompilerElse
        res = SeedB(@value, l)
      CompilerEndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i SeedW(value.s)
    Protected l.i, *b, res.i
    
    res = #False
    If value <> ""
      l = StringByteLength(value, #PB_Unicode)
      CompilerIf #PB_Compiler_Unicode
        res = SeedB(@value, l)
      CompilerElse
        *b = AllocateMemory(l)
        If *b <> #Null
          PokeS(*b, value, -1, #PB_Unicode | #PB_String_NoZero)
          res = SeedB(*b, l)
          FreeMemory(*b)
        EndIf
      CompilerEndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.q Rnd(max.q = $7FFFFFFFFFFFFFFF, min.q = 0)
    ProcedureReturn ((GenNextNum() & $7FFFFFFFFFFFFFFF) % ( max - min + 1)) + min
  EndProcedure
  Procedure.s RandomStringA(length.i)
    Protected i.i, res.s, *buf, *p
    
    res  = ""
    *buf = AllocateMemory(length + 2)
    If *buf <> #Null
      *p = *buf
      For i = 1 To length
        PokeA(*p, Rnd($FF, 1))
        *p + 1
      Next
      PokeW(*p, 0)
      
      res = PeekS(*buf, -1, #PB_Ascii)
      FreeMemory(*buf)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s RandomStringW(length.i)
    Protected i.i, res.s, *buf, *p
    
    res  = ""
    *buf = AllocateMemory((length << 1) + 2)
    If *buf <> #Null
      *p = *buf
      For i = 1 To length
        PokeW(*p, Rnd($FFFF, 1))
        *p + 2
      Next
      PokeW(*p, 0)
      
      res = PeekS(*buf, -1, #PB_Unicode)
      FreeMemory(*buf)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s RandomStringAlpha(length.i)
    Protected i.i, res.s, *buf, *p, a.a
    ; 52 ... 1-26, 27-52
    
    res  = ""
    *buf = AllocateMemory(length + 2)
    If *buf <> #Null
      *p = *buf
      For i = 1 To length
        a = Rnd(51, 0)
        If a < 26
          a + 'A'
        Else
          a + 'a' - 26
        EndIf
        PokeA(*p, a)
        *p + 1
      Next
      PokeW(*p, 0)
      
      res = PeekS(*buf, -1, #PB_Ascii)
      FreeMemory(*buf)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s RandomStringAlphaNum(length.i)
    Protected i.i, res.s, *buf, *p, a.a
    ; 62 ... 1-26, 27-52, 53-62
    
    res  = ""
    *buf = AllocateMemory(length + 2)
    If *buf <> #Null
      *p = *buf
      For i = 1 To length
        a = Rnd(61, 0)
        If a < 26
          a + 'A'
        ElseIf a < 52
          a + 'a' - 26
        Else
          a + '0' - 52
        EndIf
        PokeA(*p, a)
        *p + 1
      Next
      PokeW(*p, 0)
      
      res = PeekS(*buf, -1, #PB_Ascii)
      FreeMemory(*buf)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.s RandomStringNum(length.i)
    Protected i.i, res.s, *buf, *p, a.a
    ; 10 ... 1-10
    
    res  = ""
    *buf = AllocateMemory(length + 2)
    If *buf <> #Null
      *p = *buf
      For i = 1 To length
        a = Rnd(9, 0) + '0'
        PokeA(*p, a)
        *p + 1
      Next
      PokeW(*p, 0)
      
      res = PeekS(*buf, -1, #PB_Ascii)
      FreeMemory(*buf)
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure   RandomBuffer(*buf, bufsize.i)
    Protected s.i, r.i, i.i, *p
    
    If (*buf <> #Null) And (bufsize > 0)
      *p = *buf
      s  = Int(bufsize / 2)
      r  = bufsize % 2
      For i = 1 To s
        PokeW(*p, Rnd($FFFF))
        *p + 2
      Next i
      For i = 1 To r
        PokeB(*p, Rnd($FF))
        *p + 1
      Next i
    EndIf
  EndProcedure
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 2
; Folding = ----
; EnableUnicode
; EnableXP