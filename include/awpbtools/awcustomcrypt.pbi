﻿;{   awcustomcrypt.pbi
;    Version 0.2 [2015/10/26]
;    Copyright (C) 2014-15 Ronny Krüger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

XIncludeFile "awsupport.pbi"
XIncludeFile "awinternal.pbi"

;- Library-Definition
; { ProcedureC.i InitLib(*userdata)
;  ProcedureC.i FreeLib(*userdata)
;  ProcedureC.i Encrypt(*from, fromsize.i, *encsize, *userdata)
;  ProcedureC.i Decrypt(*from, fromsize.i, *encsize, *userdata)
;  ProcedureC.i FreeBuffer(*buffer, *userdata)
;  ProcedureC.i LibVersion()
;  ProcedureC.i LibRevision()
;  ProcedureC.s LibName()
;  ProcedureC.s LibAuthor()
;  ProcedureC.s LibCopyright()
;  ProcedureC.s LibAbout()
;  ProcedureC.s LibLicence()
;}

DeclareModule AWCustomCrypt
  Interface CustomCrypt
    Destroy        ()
    InitLib.i      (*userdata = #Null)
    FreeLib.i      (*userdata = #Null)
    Encrypt.i      (*src, srcbytes.i, *encsize, *userdata = #Null)
    Decrypt.i      (*src, srcbytes.i, *decsize, *userdata = #Null)
    FreeBuffer.i   (*buffer, *userdata = #Null)
    LibVersion.i   ()
    LibRevision.i  ()
    LibName.s      ()
    LibAuthor.s    ()
    LibCopyright.s ()
    LibAbout.s     ()
    LibLicence.s   ()
    EncodeStr.i    (text.s, *encsize, format.b = #PB_Ascii, *userdata = #Null)
    DecodeStr.s    (*buf, buflen.i, *userdata = #Null)
  EndInterface
  
  Declare.i New(libname.s, altpath.s = "")
EndDeclareModule

Module AWCustomCrypt
  EnableExplicit
  
  ;{ Prototypes
  PrototypeC.i ProtoInitLib(*userdata)
  PrototypeC.i ProtoFreeLib(*userdata)
  PrototypeC.i ProtoEncrypt(*from, fromsize, *encsize, *userdata)
  PrototypeC.i ProtoDecrypt(*from, fromsize, *encsize, *userdata)
  PrototypeC.i ProtoFreeBuffer(*buffer, *userdata)
  PrototypeC.i ProtoLibVersion()
  PrototypeC.i ProtoLibRevision()
  PrototypeC.i ProtoLibName()
  PrototypeC.i ProtoLibAuthor()
  PrototypeC.i ProtoLibCopyright()
  PrototypeC.i ProtoLibAbout()
  PrototypeC.i ProtoLibLicence()
  ;}
  Structure sCryptLib
    Handle.i
    ; LibFunctions
    InitLib.ProtoInitLib
    FreeLib.ProtoFreeLib
    Encrypt.ProtoEncrypt
    Decrypt.ProtoDecrypt
    FreeBuffer.ProtoFreeBuffer
    LibVersion.ProtoLibVersion
    LibRevision.ProtoLibRevision
    LibName.ProtoLibName
    LibAuthor.ProtoLibAuthor
    LibCopyright.ProtoLibCopyright
    LibAbout.ProtoLibAbout
    LibLicence.ProtoLibLicence
  EndStructure 
  Structure sCustomCrypt
    *vt.VT_CustomCrypt
    lib.sCryptLib
    
  EndStructure
  
  Procedure.i TestLibStructure(*ptr, entries.i)
  	Protected res.i, a.i, *p
  	
  	res = #True
  	*p  = *ptr
  	For a = 1 To entries
  		If PeekI(*p) = #Null
  			res = #False
  			Break
  		EndIf
  		*p + SizeOf(a)
  	Next
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.i New(libname.s, altpath.s = "")
    Protected *this.sCustomCrypt
  
    *this = AllocateStructure(sCustomCrypt)
    If *this
      *this\vt                       = ?VT_CustomCrypt
      *this\lib\Handle               = #Null
      
	    CompilerSelect #PB_Compiler_OS
	      CompilerCase #PB_OS_Windows
	      	*this\lib\Handle = AWInternal::OpenSharedLibrary(libname, altpath)
	      CompilerCase #PB_OS_Linux
	      	*this\lib\Handle = AWInternal::OpenSharedLibrary(libname, altpath)
	    CompilerEndSelect

      If IsLibrary(*this\lib\Handle)
      	With *this\lib
      	  ; LIB Functions Init, Free, Encode, Decode, ...
      	  \InitLib       = GetFunction(\Handle, "InitLib")
          \FreeLib       = GetFunction(\Handle, "FreeLib")
          \Encrypt       = GetFunction(\Handle, "Encrypt")
          \Decrypt       = GetFunction(\Handle, "Decrypt")
          \FreeBuffer    = GetFunction(\Handle, "FreeBuffer")
          \LibVersion    = GetFunction(\Handle, "LibVersion")
          \LibRevision   = GetFunction(\Handle, "LibRevision")
          \LibName       = GetFunction(\Handle, "LibName")
          \LibAuthor     = GetFunction(\Handle, "LibAuthor")
          \LibCopyright  = GetFunction(\Handle, "LibCopyright")
          \LibAbout      = GetFunction(\Handle, "LibAbout")
          \LibLicence    = GetFunction(\Handle, "LibLicence")
      	EndWith
    		
        If TestLibStructure(@*this\lib, SizeOf(sCryptLib) / SizeOf(sCryptLib\Handle))
    			ProcedureReturn *this
    		EndIf
    	EndIf

      FreeStructure(*this)
    EndIf
  
    ProcedureReturn #Null
  EndProcedure  
  
  Procedure.i InitLib(*this.sCustomCrypt, *userdata = #Null)
    ProcedureReturn *this\lib\InitLib(*userdata)
  EndProcedure
  Procedure.i FreeLib(*this.sCustomCrypt, *userdata = #Null)
    ProcedureReturn *this\lib\FreeLib(*userdata)
  EndProcedure
  Procedure.i Encrypt(*this.sCustomCrypt, *src, srcbytes.i, *encsize, *userdata = #Null)
    ProcedureReturn *this\lib\Encrypt(*src, srcbytes, *encsize, *userdata)
  EndProcedure
  Procedure.i Decrypt(*this.sCustomCrypt, *src, srcbytes.i, *decsize, *userdata = #Null)
    ProcedureReturn *this\lib\Decrypt(*src, srcbytes, *decsize, *userdata)
  EndProcedure
  Procedure.i FreeBuffer(*this.sCustomCrypt, *buffer, *userdata = #Null)
    ProcedureReturn *this\lib\FreeBuffer(*buffer, *userdata)
  EndProcedure
  Procedure.i LibVersion(*this.sCustomCrypt)
    ProcedureReturn *this\lib\LibVersion()
  EndProcedure
  Procedure.i LibRevision(*this.sCustomCrypt)
    ProcedureReturn *this\lib\LibRevision()
  EndProcedure
  Procedure.s LibName(*this.sCustomCrypt)
    Protected *res
    
    *res = *this\lib\LibName()
    If *res <> #Null
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.s LibAuthor(*this.sCustomCrypt)
    Protected *res
    
    *res = *this\lib\LibAuthor()
    If *res <> #Null
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.s LibCopyright(*this.sCustomCrypt)
    Protected *res
    
    *res = *this\lib\LibCopyright()
    If *res <> #Null
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.s LibAbout(*this.sCustomCrypt)
    Protected *res
    
    *res = *this\lib\LibAbout()
    If *res <> #Null
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.s LibLicence(*this.sCustomCrypt)
    Protected *res
    
    *res = *this\lib\LibLicence()
    If *res <> #Null
      ProcedureReturn PeekS(*res, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure   Destroy(*this.sCustomCrypt)
    If IsLibrary(*this\lib\Handle) : CloseLibrary(*this\lib\Handle) : EndIf
    FreeStructure(*this)
  EndProcedure
  
  Procedure.i EncodeStr(*this.sCustomCrypt, text.s, *encsize, format.b = #PB_Ascii, *userdata = #Null)
    Protected *buf, *enc, buflen.i, slen.i
    
    *enc = #Null
    slen = Len(text)
    If slen > 0
      buflen = StringByteLength(text, format) + 7
      *buf   = AllocateMemory(buflen)
      If *buf <> #Null
        PokeL(*buf, slen)
        PokeA(*buf + 4, format)
        PokeS(*buf + 5, text, slen, format)
        PokeW(*buf + buflen - 2, 0)
        
        *enc = Encrypt(*this, *buf, buflen, *encsize, *userdata)
        
        FreeMemory(*buf)
      EndIf
    EndIf 
    
    ProcedureReturn *enc
  EndProcedure
  Procedure.s DecodeStr(*this.sCustomCrypt, *buf, buflen.i, *userdata = #Null)
    Protected *dec, res.s, declen.i, slen.i, format.b
    
    res = ""
    If (*buf <> #Null) And (buflen > 0)
      *dec = Decrypt(*this, *buf, buflen, @declen, *userdata)
      If *dec <> #Null
        slen   = PeekL(*dec)
        format = PeekA(*dec + 4)
        res = PeekS(*dec + 5, -1, format)
        
        FreeBuffer(*this, *dec, *userdata)
      EndIf
    EndIf
    
    ProcedureReturn res
  EndProcedure
  
  DataSection
    VT_CustomCrypt:
      Data.i @Destroy()
      Data.i @InitLib()
      Data.i @FreeLib()
      Data.i @Encrypt()
      Data.i @Decrypt()
      Data.i @FreeBuffer()
      Data.i @LibVersion()
      Data.i @LibRevision()
      Data.i @LibName()
      Data.i @LibAuthor()
      Data.i @LibCopyright()
      Data.i @LibAbout()
      Data.i @LibLicence()
      Data.i @EncodeStr()
      Data.i @DecodeStr()
  EndDataSection
EndModule



; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 22
; FirstLine = 1
; Folding = ----
; EnableUnicode
; EnableXP