;{ awsupport.pbi
;  Version 0.6 [2015/10/26]
;  Copyright (C) 2014-15 Ronny Krueger
;
;  This file is part of AWPB-Tools.
;
;  AWPB-Tools is free software: you can redistribute it and/or modify
;  it under the terms of the GNU Lesser General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  AWPB-Tools is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU Lesser General Public License for more details.
;
;  You should have received a copy of the GNU Lesser General Public License
;  along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;}

DeclareModule AWSupport
  #IP_NONE    = 0
  #IP_V4      = 1
  #IP_V6      = 2
  
  Declare.i TrimWSBin(*txt, len.i, *newlen)
  Declare.i TrimWSBinLeft(*txt, len.i, *newlen)
  Declare.i TrimWSBinRight(*txt, len.i, *newlen)
  Declare.s TrimWS(string.s)
  Declare.s TrimWSLeft(string.s)
  Declare.s TrimWSRight(string.s)
  Declare.s TrimWSQ(string.s)
  Declare.s TrimWSGL(string.s)
  Declare.i FindLastChar(text.s, char.c)
  Declare.s CombinePath(path1.s, path2.s, replace.b = #False)
  Declare.s GetLine(txt.s, start.i, *newstart)
  Declare.i IsIP(ip.s)
  Declare.i StartsWith(str1.s, str2.s, ignorecase.i = #False)
  Declare.i EndsWith(str1.s, str2.s, ignorecase.i = #False)
EndDeclareModule

Module AWSupport
  EnableExplicit
  
  #TRIM_LEFT  = 1
  #TRIM_RIGHT = 2
  #TRIM_BOTH  = #TRIM_LEFT | #TRIM_RIGHT
  
  Procedure.i TrimWSBin_(*txt, len.i, *newlen, trimmode.i)
    Protected *l, *r, *res
  
    *res = #Null
    If *txt And (len > 0) And *newlen
      *l = *txt                                                   ; erstes Zeichen
      *r = *l + len - 1                                           ; letztes Zeichen
      
      If (trimmode & #TRIM_LEFT) = #TRIM_LEFT
        While (PeekA(*l) < 33) And (*l < *r) : *l + 1 : Wend      ; erstes druckbares Zeichen von links suchen
      EndIf
      If (trimmode & #TRIM_RIGHT) = #TRIM_RIGHT
        While (PeekA(*r) < 33) And (*r > *l) : *r - 1 : Wend      ; erstes druckbares Zeichen von rechts suchen
      EndIf
      
      If *r = *l
        If PeekA(*l) >= 33
          *res = *l
          len  = 1
        EndIf
      Else
        *res = *l
        len  = *r - *l + 1
      EndIf
      
      PokeI(*newlen, len)
    EndIf
    
    ProcedureReturn *res
  EndProcedure
  Procedure.i TrimWSBin(*txt, len.i, *newlen)
    ProcedureReturn TrimWSBin_(*txt, len, *newlen, #TRIM_BOTH)
  EndProcedure
  Procedure.i TrimWSBinLeft(*txt, len.i, *newlen)
    ProcedureReturn TrimWSBin_(*txt, len, *newlen, #TRIM_LEFT)
  EndProcedure
  Procedure.i TrimWSBinRight(*txt, len.i, *newlen)
    ProcedureReturn TrimWSBin_(*txt, len, *newlen, #TRIM_RIGHT)
  EndProcedure
  Procedure.s TrimWS_(txt.s, trimmode.i)
    Protected *l, *r, res.s
    
    res = ""
    If Len(txt) > 0
      *l = @txt                                                 ; erstes Zeichen
      *r = *l + StringByteLength(txt) - SizeOf(Character)            ; letztes Zeichen
      
      If (trimmode & #TRIM_LEFT) = #TRIM_LEFT
        While (PeekC(*l) < 33) And (*l < *r) : *l + SizeOf(Character) : Wend 
      EndIf
      If (trimmode & #TRIM_RIGHT) = #TRIM_RIGHT
        While (PeekC(*r) < 33) And (*r > *l) : *r - SizeOf(Character) : Wend
      EndIf
      
      If *r = *l
        If PeekC(*l) < 33
          res = ""
        Else
          res = Chr(PeekC(*l))      
        EndIf
      Else
        res = PeekS(*l, ((*r - *l) / SizeOf(Character)) + 1)
      EndIf
    EndIf
  
  ProcedureReturn res   
EndProcedure
  Procedure.s TrimWS(txt.s)
    ProcedureReturn TrimWS_(txt, #TRIM_BOTH)
  EndProcedure
  Procedure.s TrimWSLeft(txt.s)
    ProcedureReturn TrimWS_(txt, #TRIM_LEFT)
  EndProcedure
  Procedure.s TrimWSRight(txt.s)
    ProcedureReturn TrimWS_(txt, #TRIM_RIGHT)
  EndProcedure
  Procedure.s TrimWSQ(txt.s)
    ; alle nicht druckbaren Zeichen einschliesslich "" entfernen (ASCII < 33)
    Protected *l, *r, res.s
  
    res = ""
    If Len(txt) > 0
      *l = @txt                                           ; erstes Zeichen
      *r = *l + StringByteLength(txt) - SizeOf(Character)      ; letztes Zeichen
      
      While ((PeekC(*l) < 33) Or (PeekC(*l) = 34)) And (*l < *r) : *l + SizeOf(Character) : Wend
      While ((PeekC(*r) < 33) Or (PeekC(*r) = 34)) And (*r > *l) : *r - SizeOf(Character) : Wend
      
      If *r = *l
        If ((PeekC(*l) < 33) Or (PeekC(*l) = 34))
          res = ""
        Else  
          res = Chr(PeekC(*l))
        EndIf
      Else
        res = PeekS(*l, ((*r - *l) / SizeOf(Character)) + 1)
      EndIf
    EndIf
    
    ProcedureReturn res   
EndProcedure
  Procedure.s TrimWSGL(txt.s)
    ; alle nicht druckbaren Zeichen einschliesslich <> entfernen (ASCII < 33)
    Protected *l, *r, res.s
    
    res = ""
    If Len(txt) > 0
      *l = @txt                                           ; erstes Zeichen
      *r = *l + StringByteLength(txt) - SizeOf(Character)      ; letztes Zeichen
      
      While ((PeekC(*l) < 33) Or (PeekC(*l) = '<')) And (*l < *r) : *l + SizeOf(Character) : Wend
      While ((PeekC(*r) < 33) Or (PeekC(*r) = '>')) And (*r > *l) : *r - SizeOf(Character) : Wend
      
      If *r = *l
        If ((PeekC(*l) < 33) Or (PeekC(*l) = 34))
          res = ""
        Else  
          res = Chr(PeekC(*l))
        EndIf
      Else
        res = PeekS(*l, ((*r - *l) / SizeOf(Character)) + 1)
      EndIf
    EndIf
    
    ProcedureReturn res   
  EndProcedure
  Procedure.i FindLastChar(txt.s, char.c)
    ; ermittelt das letzte Vorkommen eines Zeichens in einem String
    Protected *txt, len.i, pos.i
    
    len  = Len(txt)
    *txt = @txt + StringByteLength(txt) - SizeOf(Character)
    For pos = len To 1 Step -1
      If PeekC(*txt) = char : Break : EndIf
      *txt - SizeOf(Character)
    Next pos
    
    ProcedureReturn pos
  EndProcedure
  Procedure.s CombinePath(path.s, file.s, replace.b = #False)
    ; kombiniert einen Pfad mit einem Dateinamen/Ordnernamen wobei der Delimiter Betriebssystemabhängig korrekt gesetzt wird
    If Len(path) > 0
      If Len(file) > 0
        CompilerSelect #PB_Compiler_OS 
          CompilerCase #PB_OS_Windows
            If replace
              ReplaceString(path, "/", "\", #PB_String_InPlace)  
              ReplaceString(file, "/", "\", #PB_String_InPlace)  
            EndIf
            If Right(path, 1) <> "\"
              path + "\"
            EndIf  
            If Left(file, 1) = "\"
              file = Mid(file, 2)
            EndIf  
          CompilerCase #PB_OS_Linux
            If replace
              ReplaceString(path, "\", "/", #PB_String_InPlace)  
              ReplaceString(file, "\", "/", #PB_String_InPlace)  
            EndIf
            If Right(path, 1) <> "/"
              path + "/"
            EndIf  
            If Left(file, 1) = "/"
              file = Mid(file, 2)
            EndIf  
        CompilerEndSelect
        path + file
      EndIf  
      ProcedureReturn path
    Else
      ProcedureReturn file
    EndIf
  EndProcedure
  Procedure.s GetLine(txt.s, start.i, *newstart)
    Protected res.s, p.i, l.i
    
    res = ""
    l   = Len(txt)
    PokeI(*newstart, -1)
    If (l > 0) And (start <= l)
      p = FindString(txt, Chr(10), start)
      If p = 0
        p = FindString(txt, Chr(13), start)
      EndIf
      
      If p > start
        res = Mid(txt, start, p - start)
        PokeI(*newstart, p + 1)
      ElseIf p = start
        PokeI(*newstart, p + 1)
      Else
        res = Mid(txt, start)
      EndIf    
    EndIf
    
    ProcedureReturn res
  EndProcedure
  Procedure.i IsIP(ip.s)
    Protected p.i, res.i, i.i, b.s, v.i
    
    p = CountString(ip, ".")
    If p = 3
      res = #IP_V4
    ElseIf p = 5
      res = #IP_V6
    Else
      ProcedureReturn #IP_NONE
    EndIf
    For i = 0 To p
      b = StringField(ip, i + 1, ".")
      If b = "" : ProcedureReturn #IP_NONE : EndIf
      v = Val(b)
      If (Str(v) <> b) Or (v > 255) Or (v < 0)
        ProcedureReturn #IP_NONE
      EndIf
    Next i
    
    ProcedureReturn res
  EndProcedure
  Procedure.i StartsWith(str1.s, str2.s, ignorecase.i = #False)
    If ignorecase
      If LCase(Left(str1, Len(str2))) = LCase(str2)
        ProcedureReturn #True
      EndIf
    Else
      If Left(str1, Len(str2)) = str2
        ProcedureReturn #True
      EndIf
    EndIf
    
    ProcedureReturn #False
  EndProcedure
  Procedure.i EndsWith(str1.s, str2.s, ignorecase.i = #False)
    If ignorecase
      If LCase(Right(str1, Len(str2))) = LCase(str2)
        ProcedureReturn #True
      EndIf
    Else
      If Right(str1, Len(str2)) = str2
        ProcedureReturn #True
      EndIf
    EndIf
    
    ProcedureReturn #False
  EndProcedure
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 236
; FirstLine = 215
; Folding = ----
; EnableUnicode
; EnableXP