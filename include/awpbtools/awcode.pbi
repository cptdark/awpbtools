;    awcode.pbi
;    Version 0.4 [2015/11/11]
;    Copyright (C) 2010-15 Ronny Krueger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.

; s�mtliche Dekoder laufen auf ASCII-Basis, da eMails auf ASCII basieren; k�nnen aber Unicode-Strings handeln

DeclareModule AWCode
  Declare.s EncodeBase64StrToStr(text.s)
  Declare.s EncodeQuotedPrintableStrToStr(text.s)  
  Declare.s DecodeBase64StrToStr(text.s)
  Declare.s DecodeQuotedPrintableStrToStr(text.s)
  Declare.i EncodeBase64Str(text.s, len.i, *enclen, linelen.i = 76)
  Declare.i EncodeQuotedPrintableStr(text.s, len.i, *enclen, linelen.i = 75, encodews.i = #False)
  Declare.s DecodeBase64Str(*body, bodylen.i)
  Declare.i DecodeQuotedPrintable(*body, bodylen.i, *declen)
  Declare.s DecodeQuotedPrintableStr(*body, bodylen.i)
  Declare.i EncodeBase64(*bin, binlen.i, *enclen, linelen.i = 76)
  Declare.i DecodeBase64(*body, bodylen.i, *declen)
  Declare.s EncodeBase64BinToStr(*bin, binlen.i, linelen.i = 76)
  Declare.i DecodeBase64StrToBin(text.s, *binlen)
EndDeclareModule

Module AWCode
  EnableExplicit
  
  Procedure.i DecodeBase64(*body, bodylen.i, *declen)                
    ; angepasster Base64 Dekoder; CRLF werden �bersprungen; ASCII-Blockdekodierung
    Protected *unwrapped, unwrappedlen.i, linelen.i, a.i, *f, *t, *basedata, breaklen.i, linecount.i, lastlinelen.i, baselen.i
    Protected *bindata, binlen.i
  
    *bindata = #Null
    binlen   = 0
    *f       = *body
    *t       = *body + bodylen - 1
    For a = bodylen To 1 Step -1              ; Ende der Daten suchen; also alle WS am Ende r�ckw�rts �berspringen
      If PeekA(*t) >= 32 : Break : EndIf
      *t - 1
    Next a
    bodylen = a
    For a = 1 To bodylen                      ; Ende der ersten Zeile suchen
      If PeekA(*f) < 32 : Break : EndIf
      *f + 1
    Next a
    linelen = a - 1                            ; die L�nge einer Zeile ermitteln
  
    If bodylen > 0                ; wenn der Body daten enth�lt
      If linelen = bodylen        ; Body besteht aus nur einer Zeile, also nichts weiter tun
        *unwrapped   = #Null
        *basedata    = *body
        baselen      = bodylen
      Else                        ; Body hat mehr als eine Zeile
        *f = *body + linelen
        For a = linelen + 1 To bodylen            ; l�nge von zeilenumbruch und evtl einr�ckung ermitteln
          If PeekA(*f) >= 32 : Break : EndIf
          *f + 1
        Next a
        breaklen    = a - 1 - linelen                                 ; L�nge des Zeilenumbruches berechnen
        linecount   = Int(bodylen / (linelen + breaklen))             ; Zeilen anzahl berechnen
        lastlinelen = bodylen - ((linelen + breaklen) * linecount)    ; l�nge der letzten Zeile berechnen
        *unwrapped  = AllocateMemory(bodylen)                         ; Speicher anfordern f�r den Body
        If *unwrapped
          *basedata = *unwrapped
          *f        = *body
          *t        = *basedata
          For a = 1 To linecount
            CopyMemory(*f, *t, linelen)          ; Zeile des codierten Bodys nehmen und anf�gen
            *f + linelen + breaklen              ; Zeilenumbruch �berspringen
            *t + linelen                         ; n�chste Zeile
          Next a
          CopyMemory(*f, *t, lastlinelen)        ; letzte Zeile anh�ngen
          *t + lastlinelen
          baselen = *t - *basedata               ; neue L�nge ohne CRLF und Einr�ckungen
        EndIf
      EndIf
      If *basedata
        *bindata = AllocateMemory(baselen + 128) ; Speicher zum decodieren anfordern (inkl Sicherungspuffer)
        If *bindata
          binlen = Base64Decoder(*basedata, baselen, *bindata, baselen + 128)   ; Base64-Code dekodieren
        EndIf
      EndIf
      If *unwrapped
        FreeMemory(*unwrapped)
      EndIf
    EndIf
  
    PokeI(*declen, binlen)        ; dekodierte L�nge speichern und
    ProcedureReturn *bindata    ; adresse der dekodierten Daten zur�ckgeben
  EndProcedure
  Procedure.s DecodeBase64Str(*body, bodylen.i)
    ; angepasster Base64 Dekoder; CRLF werden �bersprungen; ASCII-Stringdekodierung
    Protected res.i, declen.i, text.s
  
    text = ""
    res  = DecodeBase64(*body, bodylen, @declen)
    If declen > 0
      text = PeekS(res, declen, #PB_Ascii)
      FreeMemory(res)
    EndIf
    ProcedureReturn text
  EndProcedure
  Procedure.i DecodeQuotedPrintable(*body, bodylen.i, *declen)
    ; angepasster QuotedPrintable Dekoder; CRLF werden �bersprungen; ASCII-Blockdekodierung
    Protected *bindata, binlen.i, *f, *t, c.a, *bende, h.w
    Protected value.s, *value
  
    value    = "$00"
    *value   = @value + SizeOf(Character)
    bodylen  + 128														; Sicherheitspuffer
    *bindata = AllocateMemory(bodylen)        ; Zieldatenpuffer
    binlen   = 0
    If *bindata
      *f     = *body
      *t     = *bindata
      *bende = *body + bodylen - 1
      binlen = 0
      While *f <= *bende                                    ; alle Zeichen durchgehen
        c  = PeekA(*f)
        *f + 1
        If c = 61 ; =                                       ; = gefunden -> ein Code
          h = PeekW(*f) : *f + 2                            ; gefolgt von zwei Bytes, die eine hexadezimale Zahl darstellen (z.b. =0A -> Chr(10) -> LF)
          If h <> $0a0d
            PokeC(*value,                      h & $00ff      )   ; value f�llen
            PokeC(*value + SizeOf(Character), (h & $ff00) >> 8)   ; -> value.s = "$0A" f�r obiges beispiel
            PokeA(*t, Val(value))                           ; Val macht aus derm Wert eine g�ltige Zahl zwischen $00 und $FF -> ASCII-Code, der wird angef�gt
            *t + 1
          EndIf
        Else
          PokeA(*t, c)                                      ; war kein code, also zeichen direkt wieder anh�ngen
          *t + 1
        EndIf
      Wend
      binlen = *t - *bindata
    EndIf
  
    PokeI(*declen, binlen)
    ProcedureReturn *bindata
  EndProcedure
  Procedure.s DecodeQuotedPrintableStr(*body, bodylen.i)             ; angepasster QuotedPrintable Dekoder; CRLF werden �bersprungen; ASCII-Stringdekodierung
    Protected res.i, declen.i, text.s
  
    text = ""
    res  = DecodeQuotedPrintable(*body, bodylen, @declen)
    If declen > 0
      text = PeekS(res, declen, #PB_Ascii)
      FreeMemory(res)
    EndIf
    ProcedureReturn text
  EndProcedure
  Procedure.i EncodeBase64(*bin, binlen.i, *enclen, linelen.i = 76)             ; Base 64 Encoder, der auch Zeilenumbr�che einf�gt
    Protected *encbin, *basebin, newlen.i, maxlen.i, lines.i, *a, *b, i.i, lll.i
  
    *encbin = #Null
    newlen  = 0
    If *bin And binlen
      newlen   = Int((binlen * 140) / 100) + 100 ; base64 wird ca 33% gr��er
      *basebin = AllocateMemory(newlen)
      If *basebin
        maxlen = Base64Encoder(*bin, binlen, *basebin, newlen)      ; Daten base64-kodieren
        If maxlen
          ; *basebin == codierter string
          ; maxlen   == codierte l�nge
          If linelen = #PB_Any    ; kein zeilenumbruch
            lines   = 1           ; also nur eine zeile
            lll     = 0           ; lll = LastLineLength, die L�nge der letzten Zeile, ist hier 0
            newlen  = maxlen + 2  ; l�nge inkl letztem CRLF
            linelen = maxlen
          Else  ; Standardl�nge ist max 80 zeichen inkl. CRLF; hier ist es als max. 76 Zeichen + CRLF definiert
            lines   = Int(maxlen / linelen)                            ; Zeilenanzahl berechnen
            lll     = maxlen % linelen : If lll : lines + 1 : EndIf   ; Anzahl an Zeilen (letzte u.U. nicht volle L�nge) -> lll = LastLineLength
            newlen  = maxlen + (lines * 2)                             ; l�nge mit zeilenumbr�chen
          EndIf
          *encbin = AllocateMemory(newlen)
          If *encbin
            *a = *encbin
            *b = *basebin
            For i = 2 To lines                                                      ; kodierten Block zerlegen
              CopyMemory(*b, *a, linelen)         : *a + linelen : *b + linelen     ; Zeile anh�ngen
              PokeW(*a, $0a0d)                    : *a + 2                          ; CRLF anh�ngen
            Next i
            If lll                                ; keine komplette Zeile           ; da schleife rechnerisch erst ab zeile 2 l�uft:
              CopyMemory(*b, *a, lll)             : *a + lll                        ; "echte" letzte zeile
            Else                                  ; eine komplette Zeile
              CopyMemory(*b, *a, linelen)         : *a + linelen                    ; da keine letzte (unvollst�ndige) zeile existiert ist noch eine komplette �brig
            EndIf
            PokeW(*a, $0a0d)                      : *a + 2                          ; letztes CRLF anh�ngen
          EndIf
        EndIf
        maxlen = *a - *encbin                 ; "echte" kodierte Gr��e berechnen
        FreeMemory(*basebin)
      EndIf
    EndIf
    If *encbin
      PokeI(*enclen, maxlen)
    Else
      PokeI(*enclen, 0)
    EndIf
    ProcedureReturn *encbin
  EndProcedure
  Procedure.s EncodeBase64BinToStr(*bin, binlen.i, linelen.i = 76)              ; Base 64 Encoder, der auch Zeilenumbr�che einf�gt (Stringr�ckgabe)
    Protected elen.i, res.s, *enc
  
    res  = ""
    *enc = EncodeBase64(*bin, binlen, @elen, linelen)
    If *enc
      res = PeekS(*enc, elen, #PB_Ascii)
      FreeMemory(*enc)
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i EncodeBase64Str(text.s, len.i, *enclen, linelen.i = 76)           ; Base 64 Encoder, der auch Zeilenumbr�che einf�gt
    Protected *res, tlen.i, *bin
  
    *res = #Null
    If (text <> "") And len
      *bin = AllocateMemory(len + 2)
      If *bin <> #Null
        PokeS(*bin, text, len, #PB_Ascii)
        *res = EncodeBase64(*bin, len, @tlen, linelen)
        FreeMemory(*bin)
      EndIf
    EndIf
    If *res
      PokeI(*enclen, tlen)
    Else
      PokeI(*enclen, 0)
    EndIf
  
    ProcedureReturn *res
  EndProcedure
  Procedure.s EncodeBase64StrToStr(text.s)                                      ; Base 64 String Encoder (Stringr�ckgabe)
    Protected *res, elen.i, res.s
  
    res = ""
    If text <> ""
      *res = EncodeBase64Str(text, Len(text), @elen, #PB_Any)
      If *res
        res = PeekS(*res, elen, #PB_Ascii)
        FreeMemory(*res)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i EncodeQuotedPrintable(*bin, binlen.i, *enclen, linelen.i = 75, encodews.i = #False)      ; QuotedPrintable Encoder, der auch Zeilenumbr�che einf�gt
    Protected *encbin, newlen, *a, *b, i.i, c.c, lines.i, lll.i, maxlen.i, llen.i
    Protected Dim hx.a(15)
  
    For i = 0 To 9 : hx(i)      = '0' + i : Next i
    For i = 0 To 5 : hx(i + 10) = 'A' + i : Next i
  
    *encbin = #Null
    maxlen  = 0
    If *bin And binlen
      maxlen = binlen * 3                   ; l�nge berechnen; ung�nstigster fall muss angenommen werden: JEDES zeichen muss durch =xx ersetzt werden
      If linelen = #PB_Any                  ; kein zeilenumbruch
        lines   = 1                         ; nur eine zeile
        newlen  = maxlen + 5                ; zeilenl�nge mit kodieren zeilenumbr�chen
        linelen = maxlen
      Else
        lines  = Int(maxlen / linelen) + 1  ; gesamtzahl der zeilen sch�tzen, exakt ist es nicht vorherzusagen
        newlen = maxlen + (lines * 3) + 2   ; gesamtl�nge sch�tzen (inkl. crlf)
      EndIf
      *encbin = AllocateMemory(newlen)
      If *encbin
        *a   = *encbin
        *b   = *bin
        llen = 0
        For i = 1 To binlen           ; alle Zeichen abklappern
          c = PeekA(*b) : *b + 1       ; Zeichen auslesen
          If (c < 33) Or (c > 126) Or (c = 61) Or (c = '!') Or (c = '"') Or (c = '#') Or (c = '$') Or (c = '@') Or (c = '[') Or (c = '\') Or (c = ']') Or (c = '^') Or (c = '`') Or (c = '{') Or (c = '|') Or (c = '}') Or (c = '~')
            ; ein kodierungspflichtiges zeichen gefunden -> kodieren
            If (c = 9) Or (c = 32)            ; Space/TAB muss nur direkt vor oder nach einem CRLF kodiert werden
              If (PeekA(*b) = $0d) Or (PeekA(*b - 2) = $0a) Or (llen = 0) Or encodews     ; CRLF war davor bzw ist danach ODER kodierung von WS ist erzwungen
                PokeA(*a, 61)                 : *a + 1                ; =
                PokeA(*a, hx((c & $F0) >> 4)) : *a + 1                ; erstes HEX zeichen
                PokeA(*a, hx( c & $0F))       : *a + 1 : llen + 3     ; zweites HEX zeichen
              Else
                PokeA(*a, c)                  : *a + 1 : llen + 1     ; nicht kodieren, direkt �bernehmen
              EndIf
            Else                              ; war kein WS
              If (c = 10) Or (c = 13)         ; CR oder LF -> direkt �bernehmen
                PokeA(*a, c)                  : *a + 1 : llen + 1
              Else                            ; Zeichen kodieren
                PokeA(*a, 61)                 : *a + 1                ; =
                PokeA(*a, hx((c & $F0) >> 4)) : *a + 1                ; erstes HEX zeichen
                PokeA(*a, hx( c & $0F))       : *a + 1 : llen + 3     ; zweites HEX zeichen
              EndIf
            EndIf
          Else      ; nicht kodierungspflichtig -> direkt �bernehmen
            PokeA(*a, c)                      : *a + 1 : llen + 1
          EndIf
          If llen > (linelen - 3)             ; zeilenumbruch, so dass max linelen-zeichen lange zeilen entstehen
            PokeA(*a, 61)                     : *a + 1      ; ein "soft" CRLF einf�gen -. diese werden beim dekodieren ignoriert
            PokeW(*a, $0a0d)                  : *a + 2
            llen = 0
          EndIf
        Next i
        If linelen    ; in der letzten zeile ist noch mindestens ein zeichen, also CRLF einf�gen; anderenfalls w�rden CRLFCRLF drin sein - und das geht nicht
          PokeW(*a, $0a0d)                    : *a + 2 : llen = 0
        EndIf
        maxlen = *a - *encbin       ; echte Zielgr��e berechnen
      EndIf
    EndIf
    If *encbin
      PokeI(*enclen, maxlen)
    Else
      PokeI(*enclen, 0)
    EndIf
    ProcedureReturn *encbin
  EndProcedure
  Procedure.i EncodeQuotedPrintableStr(text.s, len.i, *enclen, linelen.i = 75, encodews.i = #False)    ; QuotedPrintable Encoder, der auch Zeilenumbr�che einf�gt (Stringr�ckgabe)
    Protected *res, tlen.i, *bin
  
    *res = #Null
    If (text <> "") And len
      *bin = AllocateMemory(len + 2)
      If *bin
        PokeS(*bin, text, len, #PB_Ascii)
        *res = EncodeQuotedPrintable(*bin, len, @tlen, linelen, encodews)
        FreeMemory(*bin)
      EndIf
    EndIf
    If *res
      PokeI(*enclen, tlen)
    Else
      PokeI(*enclen, 0)
    EndIf
  
    ProcedureReturn *res
  EndProcedure
  Procedure.s EncodeQuotedPrintableStrToStr(text.s)                             ; Quoted Printable Encoder (Stringr�ckgabe)
    Protected *res, elen.i, res.s
  
    res = ""
    If text
      *res = EncodeQuotedPrintableStr(text, Len(text), @elen, #PB_Any, #True)
      If *res
        res = PeekS(*res, elen, #PB_Ascii)
        FreeMemory(*res)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure 
  Procedure.s DecodeBase64StrToStr(text.s)
    Protected len.i, res.s, *bin
  
    res = ""
    len = Len(text)
    If len
      *bin = AllocateMemory(len + 2)
      If *bin
        PokeS(*bin, text, len, #PB_Ascii)
        res = DecodeBase64Str(*bin, len)
        FreeMemory(*bin)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.s DecodeQuotedPrintableStrToStr(text.s)
    Protected len.i, res.s, *bin
  
    res = ""
    len = Len(text)
    If len
      *bin = AllocateMemory(len + 2)
      If *bin
        PokeS(*bin, text, len, #PB_Ascii)
        res = DecodeQuotedPrintableStr(*bin, len)
        FreeMemory(*bin)
      EndIf
    EndIf
  
    ProcedureReturn res
  EndProcedure
  Procedure.i DecodeBase64StrToBin(text.s, *binlen)
    Protected len.i, *bin, *res, declen.i
    
    *res = #Null
    len  = Len(text)
    If len
      *bin = AllocateMemory(len + 2)
      If *bin
      	PokeS(*bin, text, len, #PB_Ascii)
      	*res = DecodeBase64(*bin, len, *binlen)
      	FreeMemory(*bin)
      EndIf
    EndIf
  
    ProcedureReturn *res
  EndProcedure
EndModule


; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 1
; Folding = ---
; EnableUnicode
; EnableXP