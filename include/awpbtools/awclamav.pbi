﻿;{   awclamav.pbi
;    Version 0.2 [2015/10/27]
;    Copyright (C) 2014-15 Ronny Krüger
;
;    This file is part of AWPB-Tools.
;
;    AWPB-Tools is free software: you can redistribute it and/or modify
;    it under the terms of the GNU Lesser General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    AWPB-Tools is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public License
;    along with AWPB-Tools.  If not, see <http://www.gnu.org/licenses/>.
;} 

XIncludeFile "awinternal.pbi"

DeclareModule AWClamAV
  #CL_DB_PHISHING	         = $00000002
  #CL_DB_PHISHING_URLS     = $00000008
  #CL_DB_PUA	             = $00000010
  #CL_DB_PUA_MODE	         = $00000080
  #CL_DB_PUA_INCLUDE       = $00000100
  #CL_DB_PUA_EXCLUDE       = $00000200
  #CL_DB_OFFICIAL_ONLY     = $00001000
  #CL_DB_BYTECODE          = $00002000
  #CL_DB_BYTECODE_UNSIGNED = $00008000
  #CL_DB_BYTECODE_STATS    = $00020000
  #CL_DB_ENHANCED          = $00040000
  #CL_DB_STDOPT	           = (#CL_DB_PHISHING | #CL_DB_PHISHING_URLS | #CL_DB_BYTECODE)
  
  Enumeration
    #ERR_NONE
    #ERR_INITDB
    #ERR_LOADSIG
    #ERR_INITENGINE
    #ERR_INITCLAMAV
    #ERR_NOTINITIALIZED
    #ERR_NODATA
    #ERR_MEMORY
    #ERR_INFECTED
    #ERR_CLEAN
    #ERR_FAIL
    
  EndEnumeration
  
  Declare.i InitLibrary(altLibPath.s = "")
  Declare   FreeLibrary()
  Declare.i InitEngine(scanoptions.l = #CL_DB_STDOPT, altDbPath.s = "")
  Declare   FreeEngine()
  Declare.i ScanMemory(*buf, bufsize.i)
  Declare.s GetVirusName()
  Declare.i GetScannedSize()
  Declare.s GetLastError()
  Declare.i GetSignatureCount()

EndDeclareModule

Module AWClamAV
  EnableExplicit
  
  Enumeration
    #CL_CLEAN
    #CL_VIRUS
    #CL_ENULLARG
    #CL_EARG
    #CL_EMALFDB
    #CL_ECVD
    #CL_EVERIFY
    #CL_EUNPACK
    #CL_EOPEN
    #CL_ECREAT
    #CL_EUNLINK
    #CL_ESTAT
    #CL_EREAD
    #CL_ESEEK
    #CL_EWRITE
    #CL_EDUP
    #CL_EACCES
    #CL_ETMPFILE
    #CL_ETMPDIR
    #CL_EMAP
    #CL_EMEM
    #CL_ETIMEOUT
    #CL_BREAK
    #CL_EMAXREC
    #CL_EMAXSIZE
    #CL_EMAXFILES
    #CL_EFORMAT
    #CL_EPARSE
    #CL_EBYTECODE
    #CL_EBYTECODE_TESTFAIL
    #CL_ELOCK
    #CL_EBUSY
    #CL_ESTATE
    #CL_ELAST_ERROR
  EndEnumeration
  #CL_SUCCESS         = #CL_CLEAN
  #CL_INIT_DEFAULT	  = 0
  #CL_DB_CVDNOTMP	    = $00000020 
  #CL_DB_OFFICIAL	    = $00000040 
  #CL_DB_COMPILED	    = $00000400 
  #CL_DB_DIRECTORY	  = $00000800 
  #CL_DB_SIGNED	      = $00004000 
  #CL_DB_UNSIGNED	    = $00010000 
  #CL_COUNT_PRECISION = 4096
  
  PrototypeC.l Proto_cl_init(initoptions.l)
  PrototypeC.i Proto_cl_engine_new()
  PrototypeC.l Proto_cl_load_alt(path.p-ascii, *engine, *signo, dboptions.l); *signo: unsigned int *
  PrototypeC.l Proto_cl_load_def(*path, *engine, *signo, dboptions.l); *signo: unsigned int *
  PrototypeC.i Proto_cl_retdbdir()
  PrototypeC.i Proto_cl_strerror(clerror.l)
  PrototypeC.l Proto_cl_engine_free(*engine)
  PrototypeC.l Proto_cl_engine_compile(*engine)
  PrototypeC.i Proto_cl_fmap_open_memory(*start, len.i)
  PrototypeC   Proto_cl_fmap_close(*clfmap)
  PrototypeC.l Proto_cl_scanmap_callback(*clfmap, *virname, *scanned, *engine, scanoptions.l, *context); *virname: char **, *scanned: unsigned long int *
  
  Structure LibClamAV
    Handle.i
    cl_init.Proto_cl_init
    cl_engine_new.Proto_cl_engine_new
    cl_load_alt.Proto_cl_load_alt
    cl_load_def.Proto_cl_load_def
    cl_retdbdir.Proto_cl_retdbdir
    cl_strerror.Proto_cl_strerror
    cl_engine_free.Proto_cl_engine_free
    cl_engine_compile.Proto_cl_engine_compile
    cl_fmap_open_memory.Proto_cl_fmap_open_memory
    cl_fmap_close.Proto_cl_fmap_close
    cl_scanmap_callback.Proto_cl_scanmap_callback
  EndStructure
  
  Global Lib.LibClamAV, *virname, scanned.l, signo.l, *defDbDir, altDbDir.s, *engine, options.l, lasterr.s
  
  Procedure.i TestLibStructure(*ptr, entries.i)
  	Protected res.i, a.i, *p
  	
  	res = #True
  	*p  = *ptr
  	For a = 1 To entries
; Debug "Entry " + Str(a) + " = " + Str(PeekI(*p))
  		If PeekI(*p) = #Null
  			res = #False
;  			Break
  		EndIf
  		*p + SizeOf(a)
  	Next
  	
  	ProcedureReturn res
  EndProcedure
  Procedure.i InitLibrary(altLibPath.s = "")
    ClearStructure(@Lib, LibClamAV)
  
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
      	Lib\Handle = AWInternal::OpenSharedLibrary("libclamav.dll", altLibPath)
      CompilerCase #PB_OS_Linux
      	Lib\Handle = AWInternal::OpenSharedLibrary("libclamav.so", altLibPath)
    CompilerEndSelect

    If IsLibrary(Lib\Handle)
      With Lib
        \cl_init             = GetFunction(\Handle, "cl_init")
        \cl_engine_new       = GetFunction(\Handle, "cl_engine_new")
        \cl_load_alt         = GetFunction(\Handle, "cl_load")
        \cl_load_def         = \cl_load_alt
        \cl_retdbdir         = GetFunction(\Handle, "cl_retdbdir")
        \cl_strerror         = GetFunction(\Handle, "cl_strerror")
        \cl_engine_free      = GetFunction(\Handle, "cl_engine_free")
        \cl_engine_compile   = GetFunction(\Handle, "cl_engine_compile")
        \cl_fmap_open_memory = GetFunction(\Handle, "cl_fmap_open_memory")
        \cl_fmap_close       = GetFunction(\Handle, "cl_fmap_close")
        \cl_scanmap_callback = GetFunction(\Handle, "cl_scanmap_callback")
      EndWith 
      
      If TestLibStructure(@Lib, SizeOf(LibClamAV) / SizeOf(LibClamAV\Handle))
  			ProcedureReturn #True
  		EndIf
  	EndIf
  
  	ProcedureReturn #False
  EndProcedure
  Procedure.i InitEngine(scanoptions.l = #CL_DB_STDOPT, altDbPath.s = "")
    Protected res.i, rc.i
    
    signo     = 0
    altDbDir  = altDbPath
    *virname  = #Null
    scanned   = 0
    options   = scanoptions
    lasterr   = ""
    *engine   = #Null
    *defDbDir = #Null
    rc        = #ERR_NONE
    
    res = Lib\cl_init(#CL_INIT_DEFAULT) 
    If res = #CL_SUCCESS
      *engine = Lib\cl_engine_new()
      If *engine <> #Null
        *defDbDir = Lib\cl_retdbdir()
        If altDbDir <> ""
          res = Lib\cl_load_alt(altDbDir, *engine, @signo, options)
        Else
          res = Lib\cl_load_def(*defDbDir, *engine, @signo, options)
        EndIf
        If res = #CL_SUCCESS
          res = Lib\cl_engine_compile(*engine)
          If res = #CL_SUCCESS
            ProcedureReturn #ERR_NONE
          Else
            lasterr = "Database initialization error: " + PeekS(Lib\cl_strerror(res), -1, #PB_Ascii)
            rc      = #ERR_INITDB
          EndIf
        Else
          lasterr = "Signature loading error: " + PeekS(Lib\cl_strerror(res), -1, #PB_Ascii)
          rc      = #ERR_LOADSIG
        EndIf
        Lib\cl_engine_free(*engine)
        *engine = #Null
      Else
        lasterr = "Unable to initialize engine."
        rc      = #ERR_INITENGINE
      EndIf
    Else
      lasterr = "Unable to initialize libclamav."
      rc      = #ERR_INITCLAMAV
    EndIf
    
    ProcedureReturn rc
  EndProcedure
  Procedure   FreeEngine()
    If *engine <> #Null
      Lib\cl_engine_free(*engine)
      *engine = #Null
    EndIf
  EndProcedure
  Procedure   FreeLibrary()
    If IsLibrary(Lib\Handle)
      FreeEngine()
      CloseLibrary(Lib\Handle)
    EndIf
  
    ClearStructure(@Lib, LibClamAV)
  EndProcedure
  Procedure.i ScanMemory(*buf, bufsize.i)
    Protected *fmap, res.i, rt.i
    
    *virname = #Null
    lasterr  = ""
    scanned  = 0
    
    If *engine = #Null
      lasterr = "libclamav was not initialized"
      ProcedureReturn #ERR_NOTINITIALIZED
    EndIf
    If (*buf = #Null) Or (bufsize < 1)
      lasterr = "no Data to check"
      ProcedureReturn #ERR_NODATA
    EndIf
    
    *fmap = Lib\cl_fmap_open_memory(*buf, bufsize)
    If *fmap = #Null
      lasterr = "Could not access memory"
      ProcedureReturn #ERR_MEMORY
    EndIf
    
    res = Lib\cl_scanmap_callback(*fmap, @*virname, @scanned, *engine, options, #Null)
    Select res
      Case #CL_CLEAN
        rt = #ERR_CLEAN
      Case #CL_VIRUS
        rt = #ERR_INFECTED
      Default
        lasterr = "Scan error: " + PeekS(Lib\cl_strerror(res), -1, #PB_Ascii)
        rt      = #ERR_FAIL
    EndSelect
    
    Lib\cl_fmap_close(*fmap)
    
    ProcedureReturn rt
  EndProcedure
  Procedure.s GetVirusName()
    If *virname <> #Null
      ProcedureReturn PeekS(*virname, -1, #PB_Ascii)
    EndIf
    
    ProcedureReturn ""
  EndProcedure
  Procedure.i GetScannedSize()
    ProcedureReturn scanned * #CL_COUNT_PRECISION
  EndProcedure
  Procedure.s GetLastError()
    ProcedureReturn lasterr
  EndProcedure
  Procedure.i GetSignatureCount()
    ProcedureReturn signo
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.40 LTS (Windows - x64)
; CursorPosition = 2
; Folding = ---
; EnableUnicode
; EnableXP
