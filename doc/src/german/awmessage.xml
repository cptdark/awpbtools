<module name="AWMessage">
	<version>Version 0.12</version>
	<description>
		Das AWMessage-Modul enthält Funktionen, um eMails zu erstellen und zu dekodieren.
		Dazu muss ein Message-Objekt angelegt werden, über das die nötigen Funktionen aufgerufen
		werden können. @linebreak  
		@linebreak
		Um Die Möglichkeit der AES-Verschlüsselung zu nutzen, muss in der Deklaration des @refmodule(AWPBTools_Settings)-Moduls die Konstante
		@refconstant(Enable_EnableMessageAES, AWPBTools_Settings) definiert werden.
		@linebreak@linebreak
		Um dieses Modul zu benutzen, muss in der Deklaration des @refmodule(AWPBTools_Settings)-Moduls die Konstante
		@refconstant(Use_AWMessage, AWPBTools_Settings) definiert werden.
	</description>
	<command name="New">
		<description>
			Initialisiert ein Message-Objekt, das die eMail darstellt und die Funktionen zur Bearbeitung
			der Mail zur Verfügung stellt.
			Das Objekt muss nach der Benutzung mit der Member-Funktion @refmember(Destroy, Message) freigegeben werden.
		</description>
		<result type="AWMessage::Message" pointer="yes">Ein Zeiger auf das initialisierte Message-Objekt.</result>
	</command>
	<class name="Message">
		<command name="Destroy">
			<description>
				Gibt die von dem Messageobjekt belegten Ressourcen frei und zerstört das Objekt.
			</description>
		</command>
		<command name="Clear">
			<description>
				Gibt die von dem Messageobjekt belegten Ressourcen frei. Das Objekt kann danach weiterverwendet werden.
			</description>
		</command>
		<command name="Decode">
			<description>
				Dekodiert die eMail.
			</description>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="BuildAttachmentList">
			<description>
				Erstellt eine Liste aller Anhänge in der Mail. Wird ein AWMessage::MimePart angegeben, werden nur die Anhänge dieses Parts ermittelt.
				Die Rückgabe ist eine einfach verkettete Liste. 
				Die Liste muss mit @refmember(FreeAttachmentList) freigegeben werden.
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result pointer="yes" type="AttList">Zeiger auf eine AWMessage::AttList - Struktur.</result>
			<example name="Beispiel.pb">
*msgobj.AWMessage::Message = AWMessage::New()
*liste.AWMessage::AttList = *msgobj\BuildAttachmentList()
If *liste
	While *liste
		; mit *liste\Att arbeiten; Att ist ein Zeiger auf ein AWMessage::MimePart
		*liste = *liste\Next
	Wend
	*msgobj\FreeAttachmentList(*liste)
EndIf  
			</example>
		</command>
		<command name="FreeAttachmentList">
			<description>
				Gibt eine mit @refmember(BuildAttachmentList) erzeugte Liste wieder frei.
				Ein Beispiel ist unter @refmember(BuildAttachmentList) zu finden.
			</description>
			<arg index="1" name="attlist" pointer="yes" type="AttList" default="#Null">Zeiger auf eine AWMessage::AttList</arg>
		</command>
		<command name="Encode">
			<description>
				Erzeugt eine eMail aus den zur Verfügung gestellten Daten.
			</description>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SetBodyBinary">
			<description>
				Setzt den Body der Mail bzw des MailParts als die übergebenen Binärdaten.
				Die Binärdaten werden automatisch in Base64 codiert.
				Wird eine ID angegeben, wird ein Inline-Attachment erzeugt, ansonsten ein normales.
			</description>
			<arg index="1" name="src" pointer="yes">Zeiger auf Binärdaten</arg>
			<arg index="2" name="srclen" type="i">Länge der Binärdaten</arg>
			<arg index="3" name="name" type="s">Content-Name</arg>
			<arg index="4" name="filename" type="s">Content-Filename</arg>
			<arg index="5" name="id" type="s" default="">Content-ID</arg>
			<arg index="6" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SetBodyText">
			<description>
				Setzt den Body der Mail bzw des MailParts als die übergebenen Textdaten.
				Wenn kein Charset angegeben wird, wird das aktuelle Systemcharset genommen. @linebreak
			</description>
			<arg index="1" name="body" type="s">Text</arg>
			<arg index="2" name="type" type="i">
				Textart, einer folgender Werte:

				@indent
				@const(#TYPE_PLAIN) - Text/Plain @linebreak
				@const(#TYPE_HTML) - Text/Html
				@endindent
			</arg>
			<arg index="3" name="charset" type="s" default="">Zeichensatz der benutzt werden soll</arg>
			<arg index="4" name="encode" type="i" default="#ENCODE_AUTO">
				Encoding, mögliche Werte sind: @linebreak
				@indent
				@const(#ENCODE_AUTO) - die Codierung wird automatisch festgelegt @linebreak
				@const(#ENCODE_B64) - Base64-Codierung erzwingen @linebreak
				@const(#ENCODE_QP) - Quoted-Printable-Codierung erzwingen
				@endindent
			</arg>
			<arg index="5" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SetMultipart">
			<description>
				Die Mail bzw der MailPart wird als Multipart definiert.
			</description>
			<arg index="1" name="type" type="i">
				Multipart-Variante, einer folgender Werte:
				@indent
				@const(#TYPE_MP_RELATED) - multipart/related @linebreak
				@const(#TYPE_MP_MIXED) - multipart/mixed @linebreak
				@const(#TYPE_MP_ALTERNATIVE) - mulipart/alternative
				@endindent
			</arg>
			<arg index="2" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="AddMultipart">
			<description>
				Der Mail bzw dem MailPart wird ein Multipart hinzugefügt.
			</description>
			<arg index="1" name="type" type="i">
				Multipart-Variante, einer folgender Werte:
				@indent
				@const(#TYPE_MP_RELATED) - multipart/related @linebreak
				@const(#TYPE_MP_MIXED) - multipart/mixed @linebreak
				@const(#TYPE_MP_ALTERNATIVE) - mulipart/alternative
				@endindent
			</arg>
			<arg index="2" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result pointer="yes" type="MimePart">bei Erfolg wird der Zeiger auf den neu angelegten MailPart zurückgegeben. Im Fehlerfalle ist das Ergebnis @const(#Null)</result>
		</command>
		<command name="AddPartHTML">
			<description>
				Der Mail bzw dem MailPart wird ein Part vom Typ text/html hinzugefügt und dessen Body wird als der übergebene HTML-Code gesetzt.
				Die Mail bzw der MailPart muss als Multipart definiert sein.
				Wenn kein Charset angegeben wird, wird das aktuelle Systemcharset genommen.
			</description>
			<arg index="1" name="html" type="s">HTML-Code</arg>
			<arg index="2" name="charset" type="s" default="">Zeichensatz</arg>
			<arg index="3" name="encode" type="i" default="#ENCODE_AUTO">
				Encoding, mögliche Werte sind: @linebreak
				@indent
				@const(#ENCODE_AUTO) - die Codierung wird automatisch festgelegt @linebreak
				@const(#ENCODE_B64) - Base64-Codierung erzwingen @linebreak
				@const(#ENCODE_QP) - Quoted-Printable-Codierung erzwingen
				@endindent
			</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
			<arg index="4" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="AddPartText">
			<description>
				Der Mail bzw dem MailPart wird ein Part vom Typ text/plain hinzugefügt und dessen Body wird als der übergebene Text gesetzt.
				Die Mail bzw der MailPart muss als Multipart definiert sein.
				Wenn kein Charset angegeben wird, wird das aktuelle Systemcharset genommen.
			</description>
			<arg index="1" name="text" type="s">Plain-Text</arg>
			<arg index="2" name="charset" type="s" default="">Zeichensatz</arg>
			<arg index="3" name="encode" type="i" default="#ENCODE_AUTO">
				Encoding, mögliche Werte sind: @linebreak
				@indent
				@const(#ENCODE_AUTO) - die Codierung wird automatisch festgelegt @linebreak
				@const(#ENCODE_B64) - Base64-Codierung erzwingen @linebreak
				@const(#ENCODE_QP) - Quoted-Printable-Codierung erzwingen
				@endindent
			</arg>
			<arg index="4" name="part" pointer="yes" type="AWMessage::MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="AddPartBinary">
			<description>
				Der Mail bzw dem MailPart wird ein Attachment hinzugefügt.
				Die Mail bzw der MailPart muss als Multipart definiert sein.
				Die Daten werden automatisch Base64-codiert.
			</description>
			<arg index="1" name="src" pointer="yes">Zeiger auf Binärdaten, die angehangen werden</arg>
			<arg index="2" name="srclen" type="s">Länge des Binärblocks</arg>
			<arg index="3" name="name" type="s">Content-Name</arg>
			<arg index="4" name="filename" type="s">Content-Filename</arg>
			<arg index="5" name="id" type="s" default="">Content-ID</arg>
			<arg index="6" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="AddPartBinaryFile">
			<description>
				Der Mail bzw dem MailPart wird ein Attachment hinzugefügt. Die Daten dazu werden aus einer Datei geladen.
				Die Mail bzw der MailPart muss als Multipart definiert sein.
				Die Daten werden automatisch Base64-codiert. Der Content-Filename wird aus dem filenamen entnommen.
			</description>
			<arg index="1" name="name" type="s">Content-Name</arg>
			<arg index="2" name="filename" type="s">Datei, die angehangen wird</arg>
			<arg index="3" name="id" type="s" default="">Content-ID</arg>
			<arg index="4" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SetMailSource">
			<description>
				Der Quelltext der Mail wird gesetzt. Dieser kann anschließend mit @refmember(Decode) dekodiert werden.
			</description>
			<arg index="1" name="src" type="s">Quelltext der Mail</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SetMailSourceBin">
			<description>
				Der Quelltext der Mail wird gesetzt. Dieser kann anschließend mit @refmember(Decode) dekodiert werden.
			</description>
			<arg index="1" name="src" pointer="yes">Zeiger auf einen Binärblock mit dem Mailquelltext</arg>
			<arg index="2" name="srclen" type="i">Länge der Daten</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="GetMailSource">
			<description>
				Der Quelltext der Mail wird zurückgegeben. Falls die Mail selbst zusammengestellt wurde, muss vorher @refmember(Encode) aufgerufen werden.
			</description>
			<result type="s">der Quelltext der erzeugten Mail</result>
		</command>
		<command name="LoadEML">
			<description>
				Die Mail wird aus einer EML-Datei (einfache Mail, wie sie auch vom Server geliefert wird) geladen.
				Anschließend kann sie mit @refmember(Decode) dekodiert werden.
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SaveEML">
			<description>
				Die Mail wird in einer EML-Datei gespeichert. Falls die Mail selbst zusammengestellt wurde, muss vorher @refmember(Encode) aufgerufen werden.
				@bold "Achtung:" Die BCC-Empfänger werden NICHT gespeichert. Dafür muss die @refmember(Save)-Funktion benutzt werden.@endbold
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="Load">
			<description>
				Die Mail wird aus einer mit @refmember(Save) erzeugten Datei geladen.
				Anschließend kann sie mit @refmember(Decode) dekodiert werden.
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="Save">
			<description>
				Die Mail wird in einer @refmodule(AWChunkyFile)-Datei gespeichert. Falls die Mail selbst zusammengestellt wurde, muss vorher @refmember(Encode) aufgerufen werden.
				BCC-Empfänger werden gespeichert. Ausserdem kann die Komprimierungsstärke der Datei angegeben werden.
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<arg index="2" name="complevel" type="i" default="AWZip::#COMP_MAX">
				Kompressionslevel, einer folgender Werte:
				@indent
				@const(AWZip::#COMP_0) - keine Komprimierung @linebreak
				@const(AWZip::#COMP_1) - Kompressionsstufe 1 @linebreak
				@const(AWZip::#COMP_2) - Kompressionsstufe 2 @linebreak
				@const(AWZip::#COMP_3) - Kompressionsstufe 3 @linebreak
				@const(AWZip::#COMP_4) - Kompressionsstufe 4 @linebreak
				@const(AWZip::#COMP_5) - Kompressionsstufe 5 @linebreak
				@const(AWZip::#COMP_6) - Kompressionsstufe 6 @linebreak
				@const(AWZip::#COMP_7) - Kompressionsstufe 7 @linebreak
				@const(AWZip::#COMP_8) - Kompressionsstufe 8 @linebreak
				@const(AWZip::#COMP_9) - Kompressionsstufe 9 @linebreak
				@const(AWZip::#COMP_STORE) - keine Komprimierung @linebreak
				@const(AWZip::#COMP_NONE) - keine Komprimierung @linebreak
				@const(AWZip::#COMP_FAST) - schnelle Kompression @linebreak
				@const(AWZip::#COMP_NORMAL) - normale Kompression @linebreak
				@const(AWZip::#COMP_MAX) - Maximale Kompression 
				@endindent  				
			</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="GetHeaderField">
			<description>
				Das angeforderte Feld des Headers der Mail bzw des MailParts wird zurückgegeben.
			</description>
			<arg index="1" name="field" type="i">
				Name des Headerfelds, einer folgender Werte:
				@indent
				@const(#HDR_SUBJECT) @linebreak
				@const(#HDR_FROM) @linebreak
				@const(#HDR_CC) @linebreak
				@const(#HDR_CC_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_TO) @linebreak
				@const(#HDR_TO_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_BCC) @linebreak
				@const(#HDR_BCC_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_RETURNPATH) @linebreak
				@const(#HDR_MESSAGEID) @linebreak
				@const(#HDR_DATE) @linebreak
				@const(#HDR_DISPOSITIONNOTIFICATIONTO) @linebreak
				@const(#HDR_REPLYTO) @linebreak
				@const(#HDR_CONTENTTYPE) @linebreak
				@const(#HDR_CONTENTSUBTYPE) @linebreak
				@const(#HDR_CONTENTCHARSET) @linebreak
				@const(#HDR_CONTENTTRANSFERENCODING) @linebreak
				@const(#HDR_BOUNDARY) @linebreak
				@const(#HDR_CONTENTDISPOSITION) @linebreak
				@const(#HDR_CONTENTNAME) @linebreak
				@const(#HDR_CONTENTFILENAME) @linebreak
				@const(#HDR_CONTENTID) @linebreak
				@const(#HDR_XPRIORITY) @linebreak
				@endindent  
			</arg>
			<arg index="2" name="part" pointer="yes" type="AWMessage::MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="s">der Wert des Header-Feldes</result>
		</command>
		<command name="SetHeaderField">
			<description>
				Setzt ein Feld des Headers der Mail bzw des MailParts.
			</description>
			<arg index="1" name="field" type="i">
				Name des Headerfelds, einer folgender Werte:
				@indent
				@const(#HDR_SUBJECT) @linebreak
				@const(#HDR_FROM) @linebreak
				@const(#HDR_CC) @linebreak
				@const(#HDR_CC_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_TO) @linebreak
				@const(#HDR_TO_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_BCC) @linebreak
				@const(#HDR_BCC_MAILS) - nur die Mail-Adressen @linebreak
				@const(#HDR_RETURNPATH) @linebreak
				@const(#HDR_MESSAGEID) @linebreak
				@const(#HDR_DATE) @linebreak
				@const(#HDR_DISPOSITIONNOTIFICATIONTO) @linebreak
				@const(#HDR_REPLYTO) @linebreak
				@const(#HDR_CONTENTTYPE) @linebreak
				@const(#HDR_CONTENTSUBTYPE) @linebreak
				@const(#HDR_CONTENTCHARSET) @linebreak
				@const(#HDR_CONTENTTRANSFERENCODING) @linebreak
				@const(#HDR_BOUNDARY) @linebreak
				@const(#HDR_CONTENTDISPOSITION) @linebreak
				@const(#HDR_CONTENTNAME) @linebreak
				@const(#HDR_CONTENTFILENAME) @linebreak
				@const(#HDR_CONTENTID) @linebreak
				@const(#HDR_XPRIORITY) @linebreak
				@endindent  
			</arg>
			<arg index="2" name="value" type="s">Wert, der eingetragen wird</arg>
			<arg index="3" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="SetContentType">
			<description>
				Setzt den Content-Type der Mail bzw des MailParts.
			</description>
			<arg index="1" name="contenttype" type="s">Typ des Parts (z.B. text)</arg>
			<arg index="2" name="contentsubtype" type="s">Subtyp des Parts (z.B. html)</arg>
			<arg index="3" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="GetDate">
			<description>
				Das Feld Date des Headers der Mail bzw des MailParts wird zurückgegeben.
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">Datum, kompatibel zur PureBasic Date-Library</result>
		</command>
		<command name="SetDate">
			<description>
				Setzen des Date-Feldes des Headers. Datevalue ist kompatibel zur Date-Library von PureBasic.
			</description>
			<arg index="1" name="datevalue" type="i">Datum</arg>
			<arg index="2" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="SetDateNow">
			<description>
				Setzen des Date-Feldes des Headers. Die gesetzte Zeit/das gesetzte Datum ist @italic jetzt @enditalic.
				Entspricht dem Aufruf von @refmember(SetDate) mit @func(Date) als erstem Parameter.
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="GetXPriority">
			<description>
				Das Feld X-Priority des Headers der Mail bzw des MailParts wird zurückgegeben.
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="i">Priorität des Mailparts / der Mail; für mögliche Rückgabewerte siehe @refmember(SetXPriority).</result>
		</command>
		<command name="SetXPriority">
			<description>
				Setzen des X-Priority-Feldes des Headers.
			</description>
			<arg index="1" name="priority" type="i">
				Priorität, einer folgender Werte:
				@indent
				@const(#PRIO_NONE) @linebreak
				@const(#PRIO_HIGHEST) @linebreak
				@const(#PRIO_HIGH) @linebreak
				@const(#PRIO_NORMAL) @linebreak
				@const(#PRIO_LOW) @linebreak
				@const(#PRIO_LOWEST)
				@endindent
			</arg>
			<arg index="2" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
		</command>
		<command name="FindPartCID">
			<description>
				Es wird der Mail-Part (falls angegeben ausgehend vom Part @code*part@endcode) gesucht, der die angegebene Content-ID hat.
				Dabei kann angegeben werden, ob die Groß-/Kleinschreibung beachtet wird.
			</description>
			<arg index="1" name="cid" type="s">Content-ID</arg>
			<arg index="2" name="casesensitive" type="b" default="#False">Groß-/Kleinschreibung beachten</arg>
			<arg index="3" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result pointer="yes" type="MimePart">der gefundene MailPart oder @const(#Null)</result>
		</command>
		<command name="FindPartName">
			<description>
				Es wird der Mail-Part (falls angegeben ausgehend vom Part @code*part@endcode) gesucht, der den angegebenen Content-Namen hat.
				Dabei kann angegeben werden, ob die Groß-/Kleinschreibung beachtet wird.
			</description>
			<arg index="1" name="name" type="s">Content-Name</arg>
			<arg index="2" name="casesensitive" type="b" default="#False">Groß-/Kleinschreibung beachten</arg>
			<arg index="3" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result pointer="yes" type="MimePart">der gefundene MailPart oder @const(#Null)</result>
		</command>
		<command name="FindPartFileName">
			<description>
				Es wird der Mail-Part (falls angegeben ausgehend vom Part @code*part@endcode) gesucht, der den angegebenen Content-FileNamen hat.
				Dabei kann angegeben werden, ob die Groß-/Kleinschreibung beachtet wird.
			</description>
			<arg index="1" name="filename" type="s">Content-FileName</arg>
			<arg index="2" name="casesensitive" type="b" default="#False">Groß-/Kleinschreibung beachten</arg>
			<arg index="3" name="part" pointer="yes" type="MimePart" default="#Null">Zeiger auf einen AWMessage::MimePart</arg>
			<result pointer="yes" type="MimePart">der gefundene MailPart oder @const(#Null)</result>
		</command>
		<command name="GetHTML">
			<description>
				Es wird der HTML-Part der Mail gesucht und zurückgegeben.
			</description>
			<result type="s">der gefundene HTML-Code oder ein leerer String</result>
		</command>
		<command name="GetPlain">
			<description>
				Es wird der Plain-Text-Part der Mail gesucht und zurückgegeben.
			</description>
			<result type="s">der gefundene Plain-Text oder ein leerer String</result>
		</command>
		<command name="GetPartAsString">
			<description>
				Der übergebene Part wird als String zurückgegeben. 
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart">Zeiger auf einen AWMessage::MimePart</arg>
			<result type="s">der Part als String</result>
		</command>
		<command name="GetPartAsBin">
			<description>
				Der übergebene Part wird als Zeiger auf einen Binärblock zurückgegeben. Die Länge des Blocks wird in *partlen zurückgegeben. 
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart">Zeiger auf einen AWMessage::MimePart</arg>
			<arg index="2" name="partlen" pointer="yes">
				Zeiger auf einen Integer-Wert, der die Länge der Daten aufnimmt (Datentyp: @code.i@endcode @linebreak
				Wenn der Rückgabewert ungleich @const(#Null) ist, enthält die Variable, auf die *partlen zeigt die Länge der Daten in Bytes.
			</arg>
			<result type="i">Zeiger auf die Binärdaten des Parts oder @const(#Null)</result>
		</command>
		<command name="GetPlainAsHTML">
			<description>
				Der Plain-Text Part der Mail wird als HTML-Code zurückgegeben.
			</description>
			<result type="s">PlainText als HTML</result>
		</command>
		<command name="SavePartToFile">
			<description>
				Der übergebene Part wird in einer Datei gespeichert.
			</description>
			<arg index="1" name="part" pointer="yes" type="MimePart">Zeiger auf einen AWMessage::MimePart</arg>
			<arg index="2" name="filename" type="s">Dateiname</arg>
			<result type="i">@const(#True) bei Erfolg, sonst @const(#False)</result>
		</command>
		<command name="LoadAESCrypted">
			<description>
				Die Mail wird aus einer mit @refmember(SaveAESCrypted) erzeugten Datei geladen.
				Anschließend kann sie mit @refmember(Decode) dekodiert werden.@linebreak
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<arg index="2" name="key" pointer="yes">
				Ein Zeiger auf einen Speicherbereich oder String, der je nach Bitlänge entweder 16 (128-Bit), 24 (192-Bit) oder 32 (256-Bit) Bytes enthält.
			</arg>
			<arg index="3" name="initializationvector" pointer="yes">
				Ein Zeiger auf einen 16 Bytes großen Speicherbereich, der Zufallsdaten enthält.
			</arg>
			<arg index="4" name="bits" type="i" default="128">Schlüssellänge in Bits (128, 192 oder 256)</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="SaveAESCrypted">
			<description>
				Die Mail wird in einer verschlüsselten @refmodule(AWChunkyFile)-Datei gespeichert. Falls die Mail selbst zusammengestellt wurde, muss vorher @refmember(Encode) aufgerufen werden.
				BCC-Empfänger werden gespeichert. Ausserdem kann die Komprimierungsstärke der Datei angegeben werden.@linebreak
			</description>
			<arg index="1" name="filename" type="s">Dateiname der EML-Datei</arg>
			<arg index="2" name="key" pointer="yes">
				Ein Zeiger auf einen Speicherbereich oder String, der je nach Bitlänge entweder 16 (128-Bit), 24 (192-Bit) oder 32 (256-Bit) Bytes enthält.
			</arg>
			<arg index="3" name="initializationvector" pointer="yes">
				Ein Zeiger auf einen 16 Bytes großen Speicherbereich, der Zufallsdaten enthält.@linebreak
				Er muss die selben Daten enthalten, die beim Erzeugen der Datei angegeben wurden.
			</arg>
			<arg index="4" name="bits" type="i" default="128">Schlüssellänge in Bits (128, 192 oder 256)</arg>
			<arg index="5" name="complevel" type="i" default="AWZip::#COMP_MAX">
				Kompressionslevel, einer folgender Werte:
				@indent
				@const(AWZip::#COMP_0) - keine Komprimierung @linebreak
				@const(AWZip::#COMP_1) - Kompressionsstufe 1 @linebreak
				@const(AWZip::#COMP_2) - Kompressionsstufe 2 @linebreak
				@const(AWZip::#COMP_3) - Kompressionsstufe 3 @linebreak
				@const(AWZip::#COMP_4) - Kompressionsstufe 4 @linebreak
				@const(AWZip::#COMP_5) - Kompressionsstufe 5 @linebreak
				@const(AWZip::#COMP_6) - Kompressionsstufe 6 @linebreak
				@const(AWZip::#COMP_7) - Kompressionsstufe 7 @linebreak
				@const(AWZip::#COMP_8) - Kompressionsstufe 8 @linebreak
				@const(AWZip::#COMP_9) - Kompressionsstufe 9 @linebreak
				@const(AWZip::#COMP_STORE) - keine Komprimierung (entspricht @const(AWZip::#COMP_0)) @linebreak
				@const(AWZip::#COMP_NONE) - keine Komprimierung (entspricht @const(AWZip::#COMP_0)) @linebreak
				@const(AWZip::#COMP_FAST) - schnelle Kompression (entspricht @const(AWZip::#COMP_1)) @linebreak
				@const(AWZip::#COMP_NORMAL) - normale Kompression (entspricht @const(AWZip::#COMP_5)) @linebreak
				@const(AWZip::#COMP_MAX) - Maximale Kompression (entspricht @const(AWZip::#COMP_9))
				@endindent  				
			</arg>
			<result type="i">@const(#True) bei Erfolg, falls ein Fehler auftrat wird @const(#False) zurückgegeben</result>
		</command>
		<command name="GetSize">
			<description>
				Die Größe des Message-Quellcodes ermitteln.
			</description>
			<result type="i">Größe der Nachricht (Bytes)</result>
		</command>			
		<command name="IsDecoded">
			<description>
				Gibt @const(#True) zurück, wenn die mail dekodiert wurde, ansonsten @const(#False).
			</description>
			<result type="i">Status der Dekodierung</result>
		</command>
				
	</class>
	<constant name="PRIO_NONE" />
	<constant name="PRIO_HIGHEST" />
	<constant name="PRIO_HIGH" />
	<constant name="PRIO_NORMAL" />
	<constant name="PRIO_LOW" />
	<constant name="PRIO_LOWEST" />
	<constant name="TYPE_AUTO" />
	<constant name="TYPE_PLAIN" />
	<constant name="TYPE_MP_RELATED" />
	<constant name="TYPE_MP_MIXED" />
	<constant name="TYPE_MP_ALTERNATIVE" />
	<constant name="TYPE_BINARY" />
	<constant name="TYPE_ATTACHMENT" />
	<constant name="TYPE_INLINE" />
	<constant name="ENCODE_AUTO" />
	<constant name="ENCODE_QP" />
	<constant name="ENCODE_B64" />
	<constant name="HDR_SUBJECT" />
	<constant name="HDR_FROM" />
	<constant name="HDR_FROM_MAIL" />
	<constant name="HDR_FROM_MAIL_AB" />
	<constant name="HDR_CC" />
	<constant name="HDR_CC_MAILS" />
	<constant name="HDR_CC_MAILS_AB" />
	<constant name="HDR_TO" />
	<constant name="HDR_TO_MAILS" />
	<constant name="HDR_TO_MAILS_AB" />
	<constant name="HDR_BCC" />
	<constant name="HDR_BCC_MAILS" />
	<constant name="HDR_BCC_MAILS_AB" />
	<constant name="HDR_RETURNPATH" />
	<constant name="HDR_MESSAGEID" />
	<constant name="HDR_DATE" />
	<constant name="HDR_DISPOSITIONNOTIFICATIONTO" />
	<constant name="HDR_REPLYTO" />
	<constant name="HDR_CONTENTTYPE" />
	<constant name="HDR_CONTENTSUBTYPE" />
	<constant name="HDR_CONTENTCHARSET" />
	<constant name="HDR_CONTENTTRANSFERENCODING" />
	<constant name="HDR_BOUNDARY" />
	<constant name="HDR_CONTENTDISPOSITION" />
	<constant name="HDR_CONTENTNAME" />
	<constant name="HDR_CONTENTFILENAME" />
	<constant name="HDR_CONTENTID" />
	<constant name="HDR_XPRIORITY" />
</module>
