<module name="AWCustomCrypt">
	<version>Version 0.2</version>
	<description>
		Das AWCustomCrypt-Modul enthält Funktionen, um externe Bibliotheken zur Verschlüsselung oder anderweitige
		Umwandlung / Konvertierung, einfach einzubinden, und alle mit dem selben Interface gleichzeitig ansprechbar zu machen.
		@linebreak@linebreak
		Um dieses Modul zu benutzen, muss in der Deklaration des @refmodule(AWPBTools_Settings)-Moduls die Konstante
		@refconstant(Use_AWCustomCrypt, AWPBTools_Settings) definiert werden.
		@linebreak@linebreak
		Die externe Bibliothek muss folgende Funktionen exportieren:@linebreak
		PureBasic:@linebreak
		@indent @code
			ProcedureCDLL.i InitLib(*userdata)@linebreak
			ProcedureCDLL.i FreeLib(*userdata)@linebreak
			ProcedureCDLL.i Encrypt(*from, fromsize.i, *encsize, *userdata)@linebreak
			ProcedureCDLL.i Decrypt(*from, fromsize.i, *decsize, *userdata)@linebreak
			ProcedureCDLL.i FreeBuffer(*buffer, *userdata)@linebreak
			ProcedureCDLL.i LibVersion()@linebreak
			ProcedureCDLL.i LibRevision()@linebreak
			ProcedureCDLL.s LibName()@linebreak
			ProcedureCDLL.s LibAuthor()@linebreak
			ProcedureCDLL.s LibCopyright()@linebreak
			ProcedureCDLL.s LibAbout()@linebreak
			ProcedureCDLL.s LibLicence()@linebreak
		@endcode @endindent
		C:@linebreak
		@indent @code
			size_t InitLib(void *userdata)@linebreak
			size_t FreeLib(void *userdata)@linebreak
			size_t Encrypt(void *from, size_t fromsize, size_t *encsize, void *userdata)@linebreak
			size_t Decrypt(void *from, size_t fromsize, size_t *decsize, void *userdata)@linebreak
			size_t FreeBuffer(void *buffer, void *userdata)@linebreak
			size_t LibVersion()@linebreak
			size_t LibRevision()@linebreak
			char *LibName()@linebreak
			char *LibAuthor()@linebreak
			char *LibCopyright()@linebreak
			char *LibAbout()@linebreak
			char *LibLicence()@linebreak
		@endcode @endindent
	</description>
	<command name="New">
		<description>
			Initialisiert ein CustomCrypt-Objekt.
			Das Objekt muss nach der Benutzung mit der Member-Funktion @refmember(Destroy) freigegeben werden.
		</description>
		<arg index="1" name="libname" type="s">Name der DLL / des Shared Objects</arg>
		<arg index="2" name="altpath" type="s" default="">Pfad zur DLL / zum SO</arg>
		<result type="AWCustomCrypt::CustomCrypt" pointer="yes">Ein Zeiger auf das initialisierte CustomCrypt-Objekt.</result>
	</command>
	<class name="CustomCrypt">
		<command name="Destroy">
			<description>
				Gibt die von dem ChunkyFileobjekt belegten Ressourcen frei und zerstört das Objekt.@linebreak
				Eventuell noch offene Chunks und/oder Dateien werden vorher korrekt geschlossen.
			</description>
		</command>
		<command name="InitLib">
			<description>
				Öffnet und initialisiert die externe Bibliothek.
			</description>
			<arg index="1" name="userdata" default="#Null" pointer="yes">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result type="i">@const(#True) bei Erfolg, sonst @const(#False)</result>
		</command>
		<command name="FreeLib">
			<description>
				Gibt die von der Bibliothek belegten Ressourcen frei und schließt sie.
			</description>
			<arg index="1" name="userdata" default="#Null" pointer="yes">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result type="i">@const(#True) bei Erfolg, sonst @const(#False)</result>
		</command>
		<command name="Encrypt">
			<description>
				Ruft die Encrypt-Funktion der externen Bibliothek auf.
			</description>
			<arg index="1" name="src" pointer="yes">Zeiger auf einen Speicherbereich, der die zu kodierenden Daten enthält</arg>
			<arg index="2" name="srcbytes" type="i">Länge des zu kodierenden Speicherblocks</arg>
			<arg index="3" name="encsize" pointer="yes">Zeiger auf eine Variable vom Typ .i, der die Größe des kodierten Speicherblocks zugewiesen wird</arg>
			<arg index="4" name="userdata" pointer="yes" default="#Null">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result pointer="yes">Zeiger auf die kodierten Daten; muss mit @refcommand(FreeBuffer) freigegeben werden. Die Länge wird in *encsize zurückgegeben.@linebreak Im Fehlerfall wird @const(#Null) zurückgegeben.</result>
		</command>
		<command name="Decrypt">
			<description>
				Ruft die Decrypt-Funktion der externen Bibliothek auf.
			</description>
			<arg index="1" name="src" pointer="yes">Zeiger auf einen Speicherbereich, der die zu dekodierenden Daten enthält</arg>
			<arg index="2" name="srcbytes" type="i">Länge des zu dekodierenden Speicherblocks</arg>
			<arg index="3" name="decsize" pointer="yes">Zeiger auf eine Variable vom Typ .i, der die Größe des dekodierten Speicherblocks zugewiesen wird</arg>
			<arg index="4" name="userdata" pointer="yes" default="#Null">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result pointer="yes">Zeiger auf die dekodierten Daten; muss mit @refcommand(FreeBuffer) freigegeben werden. Die Länge wird in *decsize zurückgegeben.@linebreak Im Fehlerfall wird @const(#Null) zurückgegeben.</result>
		</command>
		<command name="EncodeStr">
			<description>
				Ruft die Encrypt-Funktion der externen Bibliothek auf, um einen übergebenen String zu kodieren.
			</description>
			<arg index="1" name="str" type="s">zu kodierender String</arg>
			<arg index="2" name="encsize" pointer="yes">Zeiger auf eine Variable vom Typ .i, der die Größe des kodierten Speicherblocks zugewiesen wird</arg>
			<arg index="3" name="format" type="i" default="#PB_Ascii">Format des String (@const(#PB_Ascii), @const(#PB_UTF8) oder @const(#PB_UniCode))</arg>
			<arg index="4" name="userdata" pointer="yes" default="#Null">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result pointer="yes">Zeiger auf die kodierten Daten; muss mit @refcommand(FreeBuffer) freigegeben werden. Die Länge wird in *encsize zurückgegeben.@linebreak Im Fehlerfall wird @const(#Null) zurückgegeben.</result>
		</command>
		<command name="DecodeStr">
			<description>
				Ruft die Decrypt-Funktion der externen Bibliothek auf, um die Daten in einen String zu dekodieren.
			</description>
			<arg index="1" name="buf" pointer="yes">Zeiger auf einen Speicherbereich, der die zu dekodierenden Daten enthält</arg>
			<arg index="3" name="buflen" type="i">Länge der kodierten Daten</arg>
			<arg index="3" name="userdata" pointer="yes" default="#Null">Zeiger auf eine benutzerdefinierte Datenstruktur, die der externen Bibliothek übergeben wird</arg>
			<result type="s">dekodierter String</result>
		</command>
		<command name="LibVersion">
			<description>
				Gibt die Version der externen Bibliothek zurück.
			</description>
			<result type="i">Versionsnummer</result>
		</command>
		<command name="LibRevision">
			<description>
				Gibt die Revision der externen Bibliothek zurück.
			</description>
			<result type="i">Revisionsnummer</result>
		</command>
		<command name="LibName">
			<description>
				Gibt den Namen der externen Bibliothek zurück.
			</description>
			<result type="s">Bibliotheksname</result>
		</command>
		<command name="LibAuthor">
			<description>
				Gibt den Autor der externen Bibliothek zurück.
			</description>
			<result type="s">Autorname</result>
		</command>
		<command name="LibCopyright">
			<description>
				Gibt das Copyright der externen Bibliothek zurück.
			</description>
			<result type="s">Copyright</result>
		</command>
		<command name="LibAbout">
			<description>
				Gibt Infos über die externe Bibliothek zurück.
			</description>
			<result type="s">Informationen</result>
		</command>
		<command name="LibLicence">
			<description>
				Gibt die Lizenz der externen Bibliothek zurück.
			</description>
			<result type="s">Lizenz</result>
		</command>
	</class>
</module>
