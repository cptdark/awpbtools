<module name="AWPop3">
	<globalos>Windows</globalos>
	<globalos>Linux</globalos>
	<version>Version 0.10</version>
	<description>
		Das AWPop3-Modul enthält Funktionen, um auf einen POP3-Server zuzugreifen, und
		eMails abzurufen und zu löschen. 
		Soll die OpenSSL-Unterstützung aktiviert werden, muss in der Deklaration des @refmodule(AWPBTools_Settings)-Moduls die Konstante
		@refconstant(Enable_AWOpenSSL, AWPBTools_Settings) definiert werden.
		@linebreak@linebreak
		Um dieses Modul zu benutzen, muss in der Deklaration des @refmodule(AWPBTools_Settings)-Moduls die Konstante
		@refconstant(Use_AWPop3, AWPBTools_Settings) definiert werden.
	</description>
	<command name="New">
		<description>
			Initialisiert ein POP3Client-Objekt, das den POP3 Clienten darstellt und die Funktionen zum Zugriff
			auf einen POP3-Server zur Verfügung stellt.
			Das Objekt muss nach der Benutzung mit der Member-Funktion @refmember(Destroy,POP3Client) freigegeben werden.
		</description>
		<result type="AWPop3::POP3Client" pointer="yes">Ein Zeiger auf das initialisierte POP3Client-Objekt.</result>
	</command>
	<command name="OpenSSLAvailable">
		<description>
			Gibt @const(#True) zurück, falls OpenSSL zur Verfügung steht, ansonsten @const(#False).
		</description>
		<result type="i">OpenSSL Verfügbarkeit</result>
	</command>
	<class name="POP3Client">
		<command name="Destroy">
			<description>
				Gibt die von dem POP3Clientobjekt belegten Ressourcen frei und zerstört das Objekt.
			</description>
		</command>
		<command name="NOOP">
			<description>
				Sendet den NOOP-Befehl an den POP3-Server.
			</description>
			<result type="i">@const(#ERR_FAIL) falls ein Fehler auftrat, sonst @const(#ERR_NONE)</result>			
		</command>
		<command name="RSET">
			<description>
				Sendet den RSET-Befehl an den POP3-Server.
			</description>
			<result type="i">@const(#ERR_FAIL) falls ein Fehler auftrat, sonst @const(#ERR_NONE)</result>			
		</command>
		<command name="Connect">
			<description>
				Stellt eine Verbindung mit dem POP3-Server her. Vorher müssen die Verbindungsdaten gesetzt werden. 
				Siehe hierzu @refmember(SetHost), @refmember(SetLogin) und @refmember(SetTimeOut).
			</description>
			<arg index="1" name="flags" type="i" default="AWPop3::#CONN_DEFAULT">
				Verbindungseinstellungen, folgende Werte sind mit @code|@endcode verknüpft nutzbar: @linebreak
				@indent
				@const(#CONN_USER) - Login mit AUTH PLAIN @linebreak
				@const(#CONN_APOP) - Login mit AUTH LOGIN @linebreak
				@const(#CONN_AUTO) - erst APOP und dann USER versuchen @linebreak
				@const(#SSL_NONE) - kein SSL nutzen @linebreak
				@const(#SSL_TRY) - SSL per STARTTLS versuchen @linebreak
				@const(#SSL_FORCE) - SSL per STARTTLS erzwingen @linebreak
				@const(#SSL_FULL) - komplette SSL-Verschlüsselung der Verbindung erzwingen @linebreak
				@const(#CONN_DEFAULT) - erst APOP und dann USER versuchen, SSL per STARTTLS wird versucht, aber nicht erzwungen (Standard)@linebreak
				@endindent
			</arg>
			<result type="i">
				@indent
				@const(#ERR_NONE) - kein Fehler @linebreak
				@const(#ERR_FAIL) - allgemeiner Fehler @linebreak
				@const(#ERR_LOGIN) - Fehler beim Login @linebreak
				@const(#ERR_PASS) - Passwort falsch; kann auch Benutzername falsch bedeuten @linebreak
				@const(#ERR_USER) - Benutzername falsch
				@endindent
			</result>
		</command>
		<command name="DisConnect">
			<description>
				Trennt die Verbindung vom POP3-Server.
			</description>
		</command>
		<command name="SetHost">
			<description>
				Setzt die Serveradresse und den Port.
			</description>
			<arg index="1" name="Host" type="s">Hostname der Servers</arg>
			<arg index="2" name="Port" type="i">Port des Servers</arg>
		</command>
		<command name="SetLogin">
			<description>
				Setzt die Logindaten.
			</description>
			<arg index="1" name="User" type="s">Benutzername</arg>
			<arg index="2" name="Pass" type="s">Passwort</arg>
		</command>
		<command name="SetProxyHost">
			<description>
				Setzt die Serveradresse, den Port und den Typ des Proxyservers.
			</description>
			<arg index="1" name="Type" type="i">Art des Proxys, entweder @refconstant(PROXY_SOCKS4, AWNetwork) oder @refconstant(PROXY_SOCKS5, AWNetwork)</arg>
			<arg index="2" name="Host" type="s">Hostname der Servers</arg>
			<arg index="3" name="Port" type="i">Port des Servers</arg>
		</command>
		<command name="SetProxyLogin">
			<description>
				Setzt die Logindaten des Proxyservers.
			</description>
			<arg index="1" name="User" type="s">Benutzername</arg>
			<arg index="2" name="Pass" type="s">Passwort</arg>
		</command>
		<command name="SetTimeOut">
			<description>
				Setzt die Timeout-Zeit in Millisekunden, die der Client auf Antworten wartet.
			</description>
			<arg index="1" name="Timeout" type="i" default="15000">Zeit in Millisekunden</arg>
		</command>
		<command name="SetSSLMethod">
			<description>
				Setzt die für die SSL-Verbindung zu nutzende Methode.@linebreak
				Diese Funktion steht nur zur Verfügung, wenn OpenSSL aktivert wurde.
			</description>
			<arg index="1" name="sslmethod" type="i">
				SSL-Methode, die genutzt werden soll. Mögliche Werte sind: @linebreak
				@indent
				@const(AWNetwork::#SSL_METHOD_NONE) - kein SSL (Standard) @linebreak
				@const(AWNetwork::#SSL_METHOD_V2) - SSL V2 @linebreak
				@const(AWNetwork::#SSL_METHOD_V3) - SSL V3 @linebreak
				@const(AWNetwork::#SSL_METHOD_V23) - SSL V2 / V3 und TLS V1.0 Unterstützung (Standard, wenn OpenSSL aktiviert wurde)@linebreak
				@const(AWNetwork::#SSL_METHOD_TLSV1) - TLS V1.0 @linebreak
				@const(AWNetwork::#SSL_METHOD_TLSV1_1) - TLS V1.1 @linebreak
				@const(AWNetwork::#SSL_METHOD_TLSV1_2) - TLS V1.2 @linebreak
				@endindent
				@linebreak
			</arg>
		</command>
		<command name="GetLastError">
			<description>
				Gibt den letzten Fehler als String zurück.
			</description>
			<result type="s">String, der die letzte Fehler enthält</result>
		</command>
		<command name="ClearLastError">
			<description>
				Löscht die letzten Fehlerinformationen.
			</description>
		</command>
		<command name="GetLog">
			<description>
				Gibt die mitgeloggten Daten als String zurück.
			</description>
			<result type="s">Log-Daten</result>
		</command>
		<command name="ClearLog">
			<description>
				Löscht die geloggten Daten.
			</description>
		</command>
		<command name="EnableLog">
			<description>
				Aktiviert/Deaktiviert das Logging.
			</description>
			<arg index="1" name="enabled" type="i">@const(#True) zum aktivieren, @const(#False) zum deaktivieren</arg>
		</command>
		<command name="GetMailCount">
			<description>
				Ermittelt die Anzahl der Mails auf dem Server.
			</description>
			<result type="i">Anzahl der Mails auf dem Server; im Fehlerfalle -1</result>
		</command>
		<command name="GetMailHeader">
			<description>
				Ruft den Header der angegebenen Mail ab. Zurückgegeben wird ein String, der den kompletten Header enthält.
			</description>
			<arg index="1" name="Index" type="i">Index der abzurufenden Mail, eine Zahl zwischen 1 und der Mailanzahl (@refmember(GetMailCount))</arg>
			<result type="s">String, der den Header enthält</result>
		</command>
		<command name="GetFullMail">
			<description>
				Ruft die vollständige angegebene Mail ab. Zurückgegeben wird ein String, der die komplette Mail enthält.
			</description>
			<arg index="1" name="Index" type="i">Index der abzurufenden Mail, eine Zahl zwischen 1 und der Mailanzahl (@refmember(GetMailCount))</arg>
			<result type="s">String, der den kompletten Mailsource enthält</result>
		</command>
		<command name="GetUID">
			<description>
				Ruft den Unique Identifier des Servers der Mail ab. Zurückgegeben wird ein String, der die UID enthält.
			</description>
			<arg index="1" name="Index" type="i">Index der abzurufenden Mail, eine Zahl zwischen 1 und der Mailanzahl (@refmember(GetMailCount))</arg>
			<result type="s">String, der den kompletten Mailsource enthält</result>
		</command>
		<command name="GetUIDList">
			<description>
				Ruft die Unique Identifier aller Mails ab. Diese werden in die übergebene @refstruct(sID)-Liste eingefügt.
			</description>
			<arg index="1" list="yes" name="ID" type="AWPop3::sID">Index der abzurufenden Mail, eine Zahl zwischen 1 und der Mailanzahl (@refmember(GetMailCount))</arg>
			<result type="s">String, der den kompletten Mailsource enthält</result>
		</command>
		<command name="RemoveMail">
			<description>
				Löscht die angegebene Mail vom Server. 
			</description>
			<arg index="1" name="Index" type="i">Index der zu löschenden Mail, eine Zahl zwischen 1 und der Mailanzahl (@refmember(GetMailCount))</arg>
			<result type="i">Fehlercode, @const(#ERR_NONE) wenn alles ok</result>
		</command>
		<command name="UserCommand">
			<description>
				Sendet einen benutzerdefinierten Befehl an den POP3-Server. Der String darf nur ASCII-Zeichen enthalten.
			</description>
			<arg index="1" name="cmd" type="s">Benutzerdefinierter Befehl OHNE CRLF am Ende</arg>
			<arg index="2" name="rc" pointer="yes">Zeiger auf eine Variable vom Typ .i die den Resultcode des Servers aufnimmt, das kann @refconstant(ERR_OK,AWPop3) oder @refconstant(ERR_ERR,AWPop3) sein</arg>
			<arg index="3" name="restype" type="i">@refconstant(#RES_MULTILINE,AWPop3) oder @refconstant(#RES_SINGLELINE,AWPop3)</arg>
			<result type="s">kompletter Result-String des Servers</result>
		</command>
	</class>
	<structure name="sID">
		<description>
			Enthält nach dem Aufruf von @refcommand(GetUIDList) eine Liste aller Mails des Servers mit Mail-Nummer und UID.
		</description>		
		<element offset="0" name="uid" type="s">Unique Identifier</element>
		<element offset="4" name="index" type="l">Index</element>
	</structure>
	<constant name="CONN_APOP" />
	<constant name="CONN_USER" />
	<constant name="CONN_AUTO" />
	<constant name="SSL_NONE" />
	<constant name="SSL_TRY" />
	<constant name="SSL_FORCE" />
	<constant name="SSL_FULL" />
	<constant name="CONN_DEFAULT" />
	<constant name="ERR_NONE" />
	<constant name="ERR_FAIL" />
	<constant name="ERR_LOGIN" />
	<constant name="ERR_USER" />
	<constant name="ERR_PASS" />
	<constant name="ERR_TIMEOUT" />
	<constant name="RES_SINGLELINE" />
	<constant name="RES_MULTILINE" />
	<constant name="ERR_ERR" />
	<constant name="ERR_OK" />
</module>